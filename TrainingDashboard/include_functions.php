<?php
//////////////////////////////////// ERROR REPORTING /////////////////////////////////////////////////
//error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR); // Shows ONLY fatal errors
error_reporting(E_ALL ^ E_NOTICE); // shows ALL notices, warnings, errors
ini_set('display_errors', true); // Uncomment while developing code
///////////////////////////////////////// ODBC Connection /////////////////////////////////////////
$server            = '10.86.0.33';
$user              = 'svc_moodle';
$pass              = 'Dino$aurr';
$port              = 'Port=1433';
$database          = 'moodle';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
// Test db conn - display status in footer
if ($conn) {
    $conn_status = "Successful";
    $status_color = '#98ff98';
} else {
    $conn_status = "Failed";
    $status_color = '#ff9898';
}
//////////////////////////////////////// ODBC Connection MCLEOD ///////////////////////////////////
$server_mcld     = '10.1.244.122';
$user_mcld       = 'svc_lorl';
$pass_mcld       = 'Rate4ookup';
$database_mcld   = 'McLeod_lme_1110_prod';
$connection_mcld = "DRIVER={SQL Server};SERVER=$server_mcld;$port;DATABASE=$database_mcld";
$conn_mcld       = odbc_connect($connection_mcld, $user_mcld, $pass_mcld);
////////////////////////////////////// USERNAME/ID ////////////////////////////////////////////////
if (isset($_SERVER['AUTH_USER'])) {
    // Gets userid with SWIFT\ at the beginning
    $username = $_SERVER['AUTH_USER'];
} else {
    $username = "admin";
}
// Remove "SWIFT\" from usernames
function stripUsername($username) {
    if ($username === "admin") {
        $user_id = "admin";
    } else {
        $user_id = substr(stripslashes($username), 5);
    }
    return $user_id;
}
//////////////////////////////////////// USER INFO ////////////////////////////////////////////////
$user_id          = stripUsername($username);
$user_info        = userInfo($user_id);
$user_full_name   = $user_info['full_name'];
$user_email       = $user_info['email'];
$user_office      = $user_info['office'];
$user_role        = $user_info['role'];
$user_user_icon   = $user_info['user_icon'];
$user_profile_pic = $user_info['profile_pic'];
//////////////////////////////////////////////// Date/Time Variables ////////////////////////////////////////////////
$today              = date('Y-m-d');
$today_datetime     = date('Y-m-d h:i:s');
function formatDate($datetimestr,$format) {
    // 'n/d/Y H:i'
    // 'Y-m-d'
    $formatted  = (new DateTime($datetimestr))->format($format);
    return $formatted;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// Redirect page URL
function redirect($url) {
    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
}
// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}
// Escape apostrophes in strings
function escapeApostrophe($string) {
    $string = str_replace("'", "''", $string);
    return $string;
}
// Retrieve user info
function userInfo($user_id) {
    global $conn;
    $str        = "SELECT * FROM td_users WHERE user_id = '$user_id'";
    $query      = odbc_prepare($conn, $str);
    odbc_execute($query);
    $user_arr[] = '';
    while ($row        = odbc_fetch_array($query)) {
        $user_arr['full_name']   = $row['full_name'];
        $user_arr['email']       = $row['email'];
        $user_arr['office']      = $row['office'];
        $user_arr['role']        = $row['role'];
        $user_arr['user_icon']   = $row['user_icon'];
        $user_arr['profile_pic'] = $row['profile_pic'];
    }
    return $user_arr;
}
// Check admin access
function getAdminAccess($user_id, $role_id) {
    if ($user_id === 'admin') {
        $access = true;
    } else {
        global $conn;
        $sql   = "SELECT role AS role FROM td_users WHERE user_id = '$user_id' AND is_active = 'Y'";
        $query = odbc_prepare($conn, $sql);
        odbc_execute($query);
        $row   = odbc_fetch_array($query);
        $role  = $row['role'];
        if ($role === $role_id) {
            $access = true;
        } else {
            $access = false;
        }
    }
    return $access;
}
// Create select box options
function createOptions($type) {
    global $conn;
    global $conn_mcld;
    if($type === 'active_users') {
        $dbc = $conn;
        $str = "SELECT * FROM td_users WHERE is_active = 'Y' ORDER BY full_name ASC";
        $value = 'user_id';
        $option = 'full_name';
    } elseif ($type === 'inactive_users') {
        $dbc = $conn;
        $str = "SELECT * FROM td_users WHERE is_active = 'N' ORDER BY full_name ASC";
        $value = 'user_id';
        $option = 'full_name';
    } elseif ($type === 'all_users') {
        $dbc = $conn;
        $str = "SELECT * FROM td_users ORDER BY full_name ASC";
        $value = 'user_id';
        $option = 'full_name';
    } elseif ($type === 'departments') {
        $dbc = $conn_mcld;
        $str = "SELECT id, descr FROM department ORDER BY descr ASC";
        $value = 'id';
        $option = 'descr';
    } elseif ($type === 'tasks') {
        $dbc = $conn;
        $str = "SELECT p_id, title FROM td_tasks ORDER BY title ASC";
        $value = 'p_id';
        $option = 'title';
    }
    $query = odbc_prepare($dbc, $str);
    odbc_execute($query);
    $options = '';
    while($row = odbc_fetch_array($query)) {
        $options .= "<option value='" . $row[$value] . "'>".$row[$option]."</option>";
    }
    echo $options;
}
// Create alert
function createAlert($title,$notes,$alert_user,$status) {
    global $conn;
    global $user_id;
    global $today_datetime;
    $str = "INSERT INTO td_alerts (
        alert_title,
        alert_notes,
        alert_user,
        created_by,
        created_on,
        status) VALUES (
        '$title',
        '$notes',
        '$alert_user',
        '$user_id',
        '$today_datetime',
        '$status')";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
}
// Retrieve user's full name
function getUserFullName($user_id) {
    global $conn;
    $str = "SELECT full_name FROM td_users WHERE user_id = '$user_id'";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    while($row = odbc_fetch_array($query)) {
        $full_name = $row['full_name'];
    }
    return $full_name;
}
// Retrieve file name
function getFileName($file_id) {
    global $conn;
    $str = "SELECT title FROM td_files WHERE p_id = '$file_id'";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    while($row = odbc_fetch_array($query)) {
        $file_name = $row['title'];
    }
    return $file_name;
}