<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'My Profile';

/* Profile Picture Upload */
$server_path = getcwd();
$target_dir = $server_path.'\images\\';
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is an actual image
if (isset($_POST["upload_pic"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        $message  .= "File is not an image. ";
        $uploadOk = 0;
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $message  .= "File already exists. ";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 2000000) {
        $message  .= "Your file is too large. ";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        $message  .= "Only JPG, JPEG, PNG & GIF files are allowed. ";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $message .= "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
    } else {
        // Rename file => Ex: profile-pic-admin.jpg
        $profile_pic_name = 'profile-pic-' . $user_id . '.' . $imageFileType;
        $newfilename = $target_dir . 'profile-pic-' . $user_id . '.' . $imageFileType;
        $move_file   = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $newfilename);
        if ($move_file) {
            $message .= "Your profile picture was successfully uploaded. ";
            $update_profile = "UPDATE td_users SET profile_pic = '$profile_pic_name' WHERE user_id = '$user_id'";
            $update_query = odbc_prepare($conn, $update_profile);
            $result = odbc_execute($update_query);
            if (!$result) {
                $message .= "Query Failed: " . odbc_errormsg($conn) . "<br/>";
            } else {
                $message .= "Updated your profile picture!";
            }
        } else {
            $message .= "Sorry, there was an error uploading your file.";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>
                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">
                                <div class="col-md-12 col-lg-8">
                                    <div class="jumbotron">
                                        <h1 class="mb-4">Hello, <?php echo $user_full_name;?>!</h1>

                                        <p class="lead">View all of your goals and activity here.</p>

                                    </div>
                                    
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Upload Profile Picture</h3>
                                            <h6>*It is recommended to use a square shaped picture for best results.</h6>
                                            <?php echo $message; ?>
                                            <form action="" method="post" enctype="multipart/form-data">
                                                Upload Profile Picture:
                                                <input type="file" name="fileToUpload" id="fileToUpload">
                                                <input type="submit" value="Upload Image" name="upload_pic">
                                            </form>
                                        </div>
                                    </div>
                                    
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Trophies</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle" type="button"><em class="fa fa-list-ul"></em> View All</button>

                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Latest Achievements</h6>

                                            <div class="divider" style="margin-top: 1rem;"></div>

                                            <div class="articles-container">
                                                <div class="article border-bottom">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-2 date">
                                                                <div class="large">30</div>

                                                                <div class="text-muted">Jun</div>
                                                            </div>

                                                            <div class="col-10">
                                                                <h4><a href="#">Lorem ipsum dolor sit amet</a></h4>

                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at sodales nisl. Donec malesuada orci ornare risus finibus feugiat.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div><!--End .article-->

                                                <div class="article">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-2 date">
                                                                <div class="large">30</div>

                                                                <div class="text-muted">Jun</div>
                                                            </div>

                                                            <div class="col-10">
                                                                <h4><a href="#">Lorem ipsum dolor sit amet</a></h4>

                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at sodales nisl. Donec malesuada orci ornare risus finibus feugiat.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div><!--End .article-->

                                                <div class="article">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-2 date">
                                                                <div class="large">30</div>

                                                                <div class="text-muted">Jun</div>
                                                            </div>

                                                            <div class="col-10">
                                                                <h4><a href="#">Lorem ipsum dolor sit amet</a></h4>

                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at sodales nisl. Donec malesuada orci ornare risus finibus feugiat.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div><!--End .article-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-4">
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Current Objectives</h3>

                                            <h6 class="card-subtitle mb-2 text-muted">Most active this week</h6>

                                            <div class="user-progress justify-center">
                                                <div class="col-sm-3 col-md-2" style="padding: 0;">
                                                    <!--<img src="images/crown.png" alt="profile photo" class="circle profile-photo" style="width: 100%; max-width: 100px;">-->
                                                    <i class="fa fa-3x fa-fw fa-laptop" style="width: 100%; max-width: 100px;"></i>
                                                </div>

                                                <div class="col-sm-6 col-md-8">
                                                    <h6 class="pt-1">Average Quiz Score</h6>

                                                    <div class="progress progress-custom">
                                                        <div class="progress-bar bg-primary" style="width: 85%" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 col-md-2">
                                                    <div class="progress-label">85%</div>
                                                </div>
                                            </div>

                                            <div class="user-progress justify-center">
                                                <div class="col-sm-3 col-md-2" style="padding: 0;">
                                                    <!--<img src="images/party.png" alt="profile photo" class="circle profile-photo" style="width: 100%; max-width: 100px;">-->
                                                    <i class="fa fa-3x fa-fw fa-pencil" style="width: 100%; max-width: 100px;"></i>
                                                </div>

                                                <div class="col-sm-6 col-md-8">
                                                    <h6 class="pt-1">Sign all SOP's</h6>

                                                    <div class="progress progress-custom">
                                                        <div class="progress-bar bg-primary" style="width: 50%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 col-md-2">
                                                    <div class="progress-label">50%</div>
                                                </div>
                                            </div>

                                            <div class="user-progress justify-center">
                                                <div class="col-sm-3 col-md-2" style="padding: 0;">
                                                    <!--<img src="images/wink.png" alt="profile photo" class="circle profile-photo" style="width: 100%; max-width: 100px;">-->
                                                    <i class="fa fa-3x fa-fw fa-book" style="width: 100%; max-width: 100px;"></i>
                                                </div>

                                                <div class="col-sm-6 col-md-8">
                                                    <h6 class="pt-1">Complete all Tutorials</h6>

                                                    <div class="progress progress-custom">
                                                        <div class="progress-bar bg-primary" style="width: 25%" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 col-md-2">
                                                    <div class="progress-label">25%</div>
                                                </div>
                                            </div>

                                            <div class="divider"></div>

                                            <div id="calendar"></div>
                                            
                                        </div>
                                    </div>

                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Todo List</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">A simple checklist</h6>

                                            <ul class="todo-list mt-2">
                                                <li class="todo-list-item">
                                                    <div class="form-check">
                                                        <input type="checkbox" id="checkbox-1" />

                                                        <label for="checkbox-1">Make coffee</label>

                                                        <div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>
                                                    </div>
                                                </li>
                                                <li class="todo-list-item">
                                                    <div class="form-check">
                                                        <input type="checkbox" id="checkbox-2" />

                                                        <label for="checkbox-2">Check emails</label>

                                                        <div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>
                                                    </div>
                                                </li>
                                                <li class="todo-list-item">
                                                    <div class="form-check">
                                                        <input type="checkbox" id="checkbox-3" />

                                                        <label for="checkbox-3">Reply to Jane</label>

                                                        <div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>
                                                    </div>
                                                </li>
                                                <li class="todo-list-item">
                                                    <div class="form-check">
                                                        <input type="checkbox" id="checkbox-4" />

                                                        <label for="checkbox-4">Work on the new design</label>

                                                        <div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>
                                                    </div>
                                                </li>
                                                <li class="todo-list-item">
                                                    <div class="form-check">
                                                        <input type="checkbox" id="checkbox-5" />

                                                        <label for="checkbox-5">Get feedback</label>

                                                        <div class="float-right action-buttons"><a href="#" class="trash"><em class="fa fa-trash"></em></a></div>
                                                    </div>
                                                </li>
                                            </ul>

                                            <div class="card-footer todo-list-footer">
                                                <div class="input-group">
                                                    <input id="btn-input" type="text" class="form-control input-md" placeholder="Add new task" /><span class="input-group-btn">
                                                        <button class="btn btn-primary btn-md" id="btn-todo">Add</button>
                                                    </span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
                            </section>
                        </div>
                    </section>
                </main>
            </div>
        </div>        

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>
        <script>
            window.onload = function () {
                var chart1 = document.getElementById("line-chart").getContext("2d");
                window.myLine = new Chart(chart1).Line(lineChartData, {
                    responsive: true,
                    scaleLineColor: "rgba(0,0,0,.2)",
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleFontColor: "#c5c7cc"
                });
            };
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>