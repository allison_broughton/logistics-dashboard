<?php
require 'include_functions.php';
$page_access = getAdminAccess($user_id, 'admin');
if($page_access) {
/*************************************** Create table ***************************************/
/*$string = "CREATE TABLE td_users (
            p_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
            user_id VARCHAR(10) NOT NULL,
            full_name VARCHAR(40) NOT NULL,
            email VARCHAR(50) NOT NULL,
            office VARCHAR(5) NOT NULL,
            role VARCHAR(15) NOT NULL,
            user_icon VARCHAR(20) NULL,
            profile_pic VARCHAR(30) NULL
            )";*/
/*$string = "CREATE TABLE td_tasks (
            p_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
            title VARCHAR(30) NOT NULL,
            notes VARCHAR(250) NOT NULL,
            priority VARCHAR(6) NOT NULL,
            start_date DATETIME NOT NULL,
            due_date DATETIME NOT NULL,
            assigned_to VARCHAR(10) NULL,
            created_by VARCHAR(10) NULL,
            created_on DATETIME NOT NULL,
            status VARCHAR(15) NOT NULL,
            modified_by VARCHAR(10) NULL,
            modified_on DATETIME NULL
            )";*/
/*$string = "CREATE TABLE td_alerts (
            p_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
            alert_title VARCHAR(30) NOT NULL,
            alert_notes VARCHAR(250) NULL,
            alert_user VARCHAR(10) NOT NULL,
            created_by VARCHAR(10) NOT NULL,
            created_on DATETIME NOT NULL,
            status VARCHAR(10) NOT NULL
            )";*/
/*************************************** Drop table ****************************************/
//$string = "DROP TABLE td_users";
/*************************************** Alter table ***************************************/
//$string = "ALTER TABLE td_files ADD deactivated_on DATETIME NULL";
//$string = "ALTER TABLE td_signoffs ALTER COLUMN sop_id INT NULL";
/*************************************** Insert rows ***************************************/
/*$string = "INSERT INTO td_users (
            user_id,
            full_name,
            email,
            office,
            role) 
        VALUES (
            'stral',
            'Allison Broughton',
            'allison_broughton@swifttrans.com',
            'PHX',
            'admin'
        )";*/
/************************************** Delete row(s) **************************************/
//$string = "DELETE FROM td_users WHERE p_id = '6'";
/************************************** Update row(s) **************************************/
//$string = "UPDATE td_users SET user_icon = 'fa-user-circle'";
/**************************************** Run query ****************************************/
$query = odbc_prepare($conn, $string);
$result = odbc_execute($query);
if (!$result) {
    echo "Query Failed: " . odbc_errormsg($conn) . "<br/>";
} else {
    echo "Success!";
}
echo "<pre>Query: " . $string . "</pre>";
/*************************************** McLeod Users ***************************************/
/*$select_mcld_users = "SELECT id, name, email_address, office_code, department_id
  FROM users
  WHERE web_user_type = 'U' 
  AND is_active = 'Y' 
  AND department_id NOT IN ('CR','APS','MCD','IT','TC','ACT','FIN','CM','OTHR','AUD','REV') 
  AND id NOT IN ('svc_mcldpd','stral','pitcn','labin')
  ORDER BY name ASC";
$get_mcld_users = odbc_prepare($conn_mcld, $select_mcld_users);
odbc_execute($get_mcld_users);
$insert_string = '';
while($row = odbc_fetch_array($get_mcld_users)) {
    
    $mcld_userid = $row['id'];
    $mcld_name   = $row['name'];
    $mcld_email  = $row['email_address'];
    $mcld_office = $row['office_code'];
    $mcld_dept   = $row['department_id'];
    
    $insert_string .= "INSERT INTO td_users (
            user_id,
            full_name,
            email,
            office,
            role,
            user_icon,
            profile_pic,
            is_active,
            department) 
        VALUES (
            '$mcld_userid',
            '$mcld_name',
            '$mcld_email',
            '$mcld_office',
            'student',
            'fa-user-circle',
            'default-pic.png',
            'Y',
            '$mcld_dept')<br/>";
}
echo $insert_string;*/
} else {
    echo "You do not have permission to view this page.";
}