<?php
/* Only admins have access to this page */
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title  = 'My Tasks';
/* Check page access */
$page_access = true;
/* Add New Task */
if (isset($_POST['add_task'])) {
    $title       = sanitize($_POST['title']);
    $notes       = escapeApostrophe(sanitize($_POST['notes']));
    $priority    = sanitize($_POST['priority']);
    $start_date  = sanitize($_POST['start_date']);
    $due_date    = sanitize($_POST['due_date']);
    $assigned_to = sanitize($_POST['assigned_to']);
    $created_by  = $user_id;
    $created_on  = $today_datetime;
    $status      = 'not_started';

    $string = "INSERT INTO td_tasks (
        title,
        notes,
        priority,
        start_date,
        due_date,
        assigned_to,
        created_by,
        created_on,
        status) 
    VALUES (
        '$title',
        '$notes',
        '$priority',
        '$start_date',
        '$due_date',
        '$assigned_to',
        '$created_by',
        '$created_on',
        '$status')";
    $query  = odbc_prepare($conn, $string);
    $result = odbc_execute($query);
    if (!$result) {
        $message = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<br/>". $string ."</h6>";
    } else {
        $message = "<h6 class='text-primary'>Success!</h6>";
    }
}
/* Choose Task to Edit */
if (isset($_POST['choose_task'])) {
    $task_id = sanitize($_POST['task_id']);
    $str = "SELECT * FROM td_tasks WHERE p_id = '$task_id'";
    $query = odbc_prepare($conn, $str);
    $result = odbc_execute($query);
    if (!$result) {
        $message_2 = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "</h6>";
    } else {
        while($row = odbc_fetch_array($query)) {
            $edit_title       = $row['title'];
            $edit_notes       = $row['notes'];
            $edit_priority    = $row['priority'];
            $edit_status      = $row['status'];
            $edit_start_date  = formatDate($row['start_date'], 'Y-m-d');
            $edit_due_date    = formatDate($row['due_date'], 'Y-m-d');
            $edit_assigned_to = $row['assigned_to'];
        }
        $message_2 = "<h6 class='text-primary'>Now editing Task $task_id: $edit_title</h6>";
    }
}
/* Edit Task */
if (isset($_POST['edit_task'])) {
    $task_id     = sanitize($_POST['task_id']);
    $title       = sanitize($_POST['title']);
    $notes       = escapeApostrophe(sanitize($_POST['notes']));
    $priority    = sanitize($_POST['priority']);
    $start_date  = sanitize($_POST['start_date']);
    $due_date    = sanitize($_POST['due_date']);
    $assigned_to = sanitize($_POST['assigned_to']);
    $status      = sanitize($_POST['status']);
    $modified_by = $user_id;
    $modified_on = $today_datetime;
    $string   = "UPDATE td_tasks SET 
        title = '$title',
        notes       = '$notes',
        priority    = '$priority',
        start_date  = '$start_date',
        due_date    = '$due_date',
        assigned_to = '$assigned_to',
        status      = '$status',
        modified_by = '$modified_by',
        modified_on = '$modified_on'
    WHERE p_id = '$task_id'";
    $query    = odbc_prepare($conn, $string);
    $result   = odbc_execute($query);
    if (!$result) {
        $message_3 = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<'h6>";
    } else {
        $message_3 = "<h6 class='text-primary'>Success!</h6>";
    }
}

// Retrieve all task records
function getRecords($status) {
    global $conn;
    global $user_id;
    if($user_id === 'admin') {
        $filter = '';
    } else {
        $filter = " AND assigned_to = '$user_id'";
    }
    if($status === 'active') {
        $str = "SELECT * FROM td_tasks WHERE status <> 'completed'$filter ORDER BY due_date ASC";
    } else {
        $str = "SELECT * FROM td_tasks WHERE status = 'completed'$filter ORDER BY due_date ASC";
    }
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    $records = '';
    while($row = odbc_fetch_array($query)) {
        $records .= "<tr>
            <td>".$row['title']."</td>
            <td>".$row['notes']."</td>
            <td>".$row['priority']."</td>
            <td>".$row['start_date']."</td>
            <td>".$row['due_date']."</td>
            <td>".$row['created_by']."</td>
            <td>".$row['created_on']."</td>
            <td>".$row['status']."</td>
            <td>".$row['modified_by']."</td>
            <td>".$row['modified_on']."</td>
            </tr>";
    }
    echo $records;
}
?>

<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>
                    <section class="row">
                        <?php if ($page_access) { ?>
                            <div class="col-sm-12">
                                <section class="row">
                                    <!-- Add New Task Form -->
                                    <div class="col-lg-6 md-4">
                                        <div class="card">
                                            <div class="card-block">
                                                <h3 class="card-title">Add New Task</h3>
                                                <?php
                                                if (isset($message)) {
                                                    echo $message;
                                                }
                                                ?>
                                                <form class="form-horizontal" action="" method="post">
                                                    <fieldset>
                                                        <!-- Title input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="title">Title</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="title" name="title" type="text" class="form-control" required autofocus>
                                                            </div>
                                                        </div>

                                                        <!-- Notes input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="notes">Notes</label>

                                                            <div class="col-12 no-padding">
                                                                <textarea id="notes" name="notes" class="form-control" maxlength="250"></textarea>
                                                            </div>
                                                        </div>

                                                        <!-- Priority selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="priority">Priority</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="priority" name="priority" class="form-control" required>
                                                                    <option value="Low">Low</option>
                                                                    <option value="Normal" selected>Normal</option>
                                                                    <option value="High">High</option>
                                                                </select>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <!-- Start Date selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="start_date">Start Date</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="start_date" name="start_date" type="date" class="form-control" required>
                                                                <!--<input id="start_date" name="start_time" type="time" class="form-control" required>-->
                                                            </div>
                                                        </div>

                                                        <!-- Due Date selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="due_date">Due Date</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="due_date" name="due_date" type="date" class="form-control" required>
                                                            </div>
                                                        </div>

                                                        <!-- Assigned To selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="assigned_to">Assigned To</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="assigned_to" name="assigned_to" class="form-control" required>
                                                                    <option value="">Choose user to assign to task</option>
                                                                    <?php createOptions('active_users'); ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Form actions -->
                                                        <div class="form-group">
                                                            <div class="col-12 widget-right no-padding">
                                                                <input type="submit" name="add_task" value="Submit" class="btn btn-secondary btn-md float-right">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Edit Task Form -->
                                    <div class="col-lg-6 md-4">
                                        <div class="card">
                                            <div class="card-block">
                                                <h3 class="card-title">Edit Task</h3>
                                                <?php
                                                if (isset($message_2)) {
                                                    echo $message_2;
                                                }
                                                if (isset($message_3)) {
                                                    echo $message_3;
                                                }
                                                ?>
                                                <form class="form-inline" action="" method="post">
                                                    <!-- Choose Task input-->
                                                    <label class="control-label" for="task_id">Choose Task </label>
                                                    <div class="ml-sm-2 mr-sm-2">
                                                        <select id="task_id" name="task_id" class="form-control no-padding" required>
                                                            <option value="">...</option>
                                                            <?php createOptions('tasks'); ?>
                                                        </select>
                                                    </div>
                                                    <input type="submit" name="choose_task" value="Edit" class="btn btn-secondary btn-md float-right">
                                                </form>
                                                
                                                <form class="form-horizontal" action="" method="post">
                                                    <fieldset>
                                                        <input type="hidden" name="task_id" value="<?php echo $task_id;?>">
                                                        <!-- Title input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="title">Title</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="title" name="title" type="text" class="form-control" required value="<?php if(isset($edit_title)) {echo $edit_title;}?>">
                                                            </div>
                                                        </div>

                                                        <!-- Notes input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="notes">Notes</label>

                                                            <div class="col-12 no-padding">
                                                                <textarea id="notes" name="notes" class="form-control"><?php if(isset($edit_notes)) {echo $edit_notes;}?></textarea>
                                                            </div>
                                                        </div>

                                                        <!-- Priority selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="priority">Priority</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="priority" name="priority" class="form-control" required>
                                                                    <?php if(isset($edit_priority)) {?>
                                                                    <option value="<?php echo $edit_priority;?>" selected>Currently: <?php echo $edit_priority;?></option>
                                                                    <?php }?>
                                                                    <option value="Low">Low</option>
                                                                    <option value="Normal">Normal</option>
                                                                    <option value="High">High</option>
                                                                </select>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <!-- Status selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="status">Status</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="status" name="status" class="form-control" required>
                                                                    <?php if(isset($edit_status)) {?>
                                                                    <option value="<?php echo $edit_status;?>" selected>Currently: <?php echo $edit_status;?></option>
                                                                    <?php }?>
                                                                    <option value="not_started">Not Started</option>
                                                                    <option value="in_progress">In Progress</option>
                                                                    <option value="completed">Completed</option>
                                                                </select>
                                                            </div>
                                                            
                                                        </div>

                                                        <!-- Start Date selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="start_date">Start Date</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="start_date" name="start_date" type="date" class="form-control" value="<?php echo $edit_start_date;?>" required>
                                                            </div>
                                                        </div>

                                                        <!-- Due Date selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="due_date">Due Date</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="due_date" name="due_date" type="date" class="form-control" value="<?php echo $edit_due_date;?>" required>
                                                            </div>
                                                        </div>

                                                        <!-- Assigned To selection -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="assigned_to">Assign To</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="assigned_to" name="assigned_to" class="form-control" required>
                                                                    <?php if(isset($edit_assigned_to)) {?>
                                                                    <option value="<?php echo $edit_assigned_to;?>" selected>Currently: <?php echo $edit_assigned_to;?></option>
                                                                    <?php }?>
                                                                    <?php createOptions('active_users'); ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Form actions -->
                                                        <div class="form-group">
                                                            <div class="col-12 widget-right no-padding">
                                                                <input type="submit" name="edit_task" value="Submit" class="btn btn-secondary btn-md float-right">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                                                      
                                </section>
                                <div class="card mb-4">
                                    <div class="card-block">
                                        <h3 class="card-title">Active Tasks</h3>
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Title</th>

                                                        <th>Notes</th>

                                                        <th>Priority</th>

                                                        <th>Start</th>
                                                        
                                                        <th>Due</th>
                                                        
                                                        <th>Created By</th>
                                                        
                                                        <th>Created On</th>
                                                        
                                                        <th>Status</th>
                                                        
                                                        <th>Modified By</th>
                                                        
                                                        <th>Modified On</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php getRecords('active');?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4">
                                    <div class="card-block">
                                        <h3 class="card-title text-muted">Completed Tasks</h3>
                                        <div class="table-responsive">
                                            <table class="table table-striped text-muted">
                                                <thead>
                                                    <tr>
                                                        <th>Title</th>

                                                        <th>Notes</th>

                                                        <th>Priority</th>

                                                        <th>Start</th>
                                                        
                                                        <th>Due</th>
                                                        
                                                        <th>Created By</th>
                                                        
                                                        <th>Created On</th>
                                                        
                                                        <th>Status</th>
                                                        
                                                        <th>Modified By</th>
                                                        
                                                        <th>Modified On</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php getRecords('completed');?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php } else { ?>
                            <div class="col-sm-12">
                                <div class="alert bg-danger" role="alert">
                                    <em class="fa fa-minus-circle mr-2"></em> You do not have access to this page. Please contact <a href="mailto:allison_broughton@swifttrans.com?Subject=Page%20Access%20for%20<?php echo $page_title; ?>" target="_top" class="alert-link">Support</a> if you feel that this is an error.</div>
                            </div>
<?php } ?>
                    </section>
                </main>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>
