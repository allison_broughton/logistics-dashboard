<footer id="footer">
    <div class="container-fluid">
        <p><i class="fa fa-copyright"></i> Swift Logistics, LLC &nbsp;
            <i class="fa fa-plug"></i>&nbsp;Connection status: <strong><span style="color:<?php echo $status_color; ?>;"><?php echo $conn_status; ?></span></strong>&nbsp;
            <i class="fa fa-user"></i>&nbsp;Logged in as: <?php echo $user_id; ?></p>
    </div>
</footer>