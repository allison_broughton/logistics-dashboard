<?php 
 require 'include_conn.php';
 //$user_profile_pic = 'profile-pic-admin.jpg';
 $str = "SELECT profile_pic FROM td_users WHERE user_id = '$user_id'";
 $query = odbc_prepare($conn, $str);
 odbc_execute($query);
 while($row = odbc_fetch_array($query)) {
     $user_profile_pic = $row['profile_pic'];
 }
?>
<header class="page-header row justify-center">
    <div class="col-md-6 col-lg-8" >
        <h1 class="float-left text-center text-md-left"><?php echo $page_title; ?></h1>
    </div>

    <div class="dropdown user-dropdown col-md-6 col-lg-4 text-center text-md-right"><a class="btn btn-stripped dropdown-toggle" href="https://example.com" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="images/<?php echo $user_profile_pic;?>" alt="profile photo" class="circle float-left profile-photo" width="50" height="auto">

            <div class="username mt-1">
                <h4 class="mb-1"><?php echo $user_id ?></h4>

                <h6 class="text-muted">Admin</h6>
            </div>
        </a>

        <div class="dropdown-menu dropdown-menu-right" style="margin-right: 1.5rem;" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="view_profile.php"><em class="fa fa-user-circle mr-1"></em> View Profile</a>
            <a class="dropdown-item" href="#"><em class="fa fa-sliders mr-1"></em> Preferences</a>
        </div>

        <div class="clear"></div>
</header>