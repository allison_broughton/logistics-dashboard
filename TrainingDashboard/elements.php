<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'UI Elements';
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>
                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">
                                <div class="col-12 mb-2">
                                    <h3 class="mb-4">Buttons</h3>

                                    <h5 class="mb-2 mt-4 pl-1 text-muted">Small</h5>

                                    <button type="button" class="btn btn-sm btn-primary">Primary</button>

                                    <button type="button" class="btn btn-sm btn-secondary">Secondary</button>

                                    <button type="button" class="btn btn-sm btn-success">Success</button>

                                    <button type="button" class="btn btn-sm btn-info">Info</button>

                                    <button type="button" class="btn btn-sm btn-warning">Warning</button>

                                    <button type="button" class="btn btn-sm btn-danger">Danger</button>

                                    <button type="button" class="btn btn-sm btn-link">Link</button>

                                    <h5 class="mb-2 mt-4 pl-1 text-muted">Medium</h5>

                                    <button type="button" class="btn btn-md btn-primary">Primary</button>

                                    <button type="button" class="btn btn-md btn-secondary">Secondary</button>

                                    <button type="button" class="btn btn-md btn-success">Success</button>

                                    <button type="button" class="btn btn-md btn-info">Info</button>

                                    <button type="button" class="btn btn-md btn-warning">Warning</button>

                                    <button type="button" class="btn btn-md btn-danger">Danger</button>

                                    <button type="button" class="btn btn-md btn-link">Link</button>

                                    <h5 class="mb-2 mt-4 pl-1 text-muted">Large</h5>

                                    <button type="button" class="btn btn-lg btn-primary">Primary</button>

                                    <button type="button" class="btn btn-lg btn-secondary">Secondary</button>

                                    <button type="button" class="btn btn-lg btn-success">Success</button>

                                    <button type="button" class="btn btn-lg btn-info">Info</button>

                                    <button type="button" class="btn btn-lg btn-warning">Warning</button>

                                    <button type="button" class="btn btn-lg btn-danger">Danger</button>

                                    <button type="button" class="btn btn-lg btn-link">Link</button>
                                </div>
                            </section>

                            <section class="row">
                                <div class="col-12 mb-2">
                                    <h3 class="mt-4 mb-4">Alerts</h3>

                                    <div class="alert bg-primary" role="alert"><em class="fa fa-comment mr-2"></em> Welcome to the admin dashboard panel bootstrap template <a href="#" class="float-right"><em class="fa fa-remove"></em></a></div>

                                    <div class="alert bg-info" role="alert"><em class="fa fa-tag mr-2"></em> Welcome to the admin dashboard panel bootstrap template <a href="#" class="float-right"><em class="fa fa-remove"></em></a></div>

                                    <div class="alert bg-success" role="alert"><em class="fa fa-check-circle mr-2"></em> Welcome to the admin dashboard panel bootstrap template <a href="#" class="float-right"><em class="fa fa-remove"></em></a></div>

                                    <div class="alert bg-warning" role="alert"><em class="fa fa-exclamation-triangle mr-2"></em> Welcome to the admin dashboard panel bootstrap template <a href="#" class="float-right"><em class="fa fa-remove"></em></a></div>

                                    <div class="alert bg-danger" role="alert"><em class="fa fa-minus-circle mr-2"></em> Welcome to the admin dashboard panel bootstrap template <a href="#" class="float-right"><em class="fa fa-remove"></em></a></div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
                            </section>
                        </div>
                    </section>
                </main>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>