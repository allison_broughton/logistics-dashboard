<?php
// Include this file in each page to display the sidebar menu
?>
<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2 bg-faded sidebar-style-1">
    <h1 class="site-title"><a href="index.php"><em class="fa fa-bolt"></em> Swift.Logistics</a></h1>

    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>

    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link" href="index.php"><em class="fa fa-fw fa-dashboard"></em> Dashboard</a></li>
        <li class="nav-item"><a class="nav-link" href="tasks.php"><em class="fa fa-fw fa-tasks"></em> My Tasks</a></li>
        <li class="nav-item"><a class="nav-link" href="trainer_mgmt.php"><em class="fa fa-fw fa-diamond"></em> Trainer Management</a></li>
        <li class="nav-item"><a class="nav-link" href="help.php"><em class="fa fa-fw fa-question-circle-o"></em> Help Section</a></li>
        <!--<li class="nav-item"><a class="nav-link" href="charts.php"><em class="fa fa-fw fa-bar-chart"></em> Charts</a></li>
        <li class="nav-item"><a class="nav-link" href="elements.php"><em class="fa fa-fw fa-hand-o-up"></em> UI Elements</a></li>
        <li class="nav-item"><a class="nav-link" href="cards.php"><em class="fa fa-fw fa-clone"></em> Cards</a></li>-->
        <!--<li class="nav-item"><a class="nav-link" href="quiz.php"><em class="fa fa-fw fa-pencil"></em> Quizzes</a></li>
        <li class="nav-item"><a class="nav-link" href="#"><em class="fa fa-fw fa-info"></em> SOP's</a></li>-->
        <li class="nav-item"><a class="nav-link" href="user_management.php"><em class="fa fa-fw fa-user-circle"></em> User Management</a></li>
    </ul>
</nav>