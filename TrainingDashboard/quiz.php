<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'Quizzes';
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php';?>
                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">
                                <div class="col-12">
                                    <h3 class="mb-4"><i class='fa fa-lightbulb-o'></i> Test Your Knowledge</h3>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card card-inverse card-primary">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card card-inverse card-success">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card card-inverse card-info">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card card-inverse card-warning">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 mb-4">
                                    <div class="card card-inverse card-danger">
                                        <div class="card-header">Sample Quiz</div>

                                        <div class="card-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
                                                ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus
                                                nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui
                                                pharetra molestie. Fusce et dui urna.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="row">
                                <div class="col-12">
                                    <h3 class="mt-4 mb-4">Cards with Navigation</h3>
                                </div>

                                <div class="col-lg-6 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header">
                                            <ul class="nav nav-tabs card-header-tabs">
                                                <li class="nav-item"><a class="nav-link active" href="#">Active</a></li>
                                                <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
                                                <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
                                            </ul>
                                        </div>

                                        <div class="card-block">
                                            <h4 class="card-title">Special title treatment</h4>

                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-md btn-primary">Go somewhere</a></div>
                                    </div>
                                </div>

                                <div class="col-lg-6 mb-4">
                                    <div class="card text-center">
                                        <div class="card-header">
                                            <ul class="nav nav-pills card-header-pills">
                                                <li class="nav-item"><a class="nav-link active" href="#">Active</a></li>
                                                <li class="nav-item"><a class="nav-link" href="#">Link</a></li>
                                                <li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a></li>
                                            </ul>
                                        </div>

                                        <div class="card-block">
                                            <h4 class="card-title">Special title treatment</h4>

                                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                            <a href="#" class="btn btn-primary">Go somewhere</a></div>
                                    </div>
                                </div>
                        </div>
                    </section>
                    <section class="row">
                        <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
                    </section>
            </div>
        </section>
    </main>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/custom.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

</body>
</html>
