<?php
/* Only admins have access to this page */
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title  = 'User Management';
/* Check page access */
if(getAdminAccess($user_id, 'admin') || getAdminAccess($user_id, 'trainer')) {
    $page_access = true;
} else {
    $page_access = false;
}
/* Add New User */
if (isset($_POST['add_user'])) {
    $userid   = sanitize($_POST['userid']);
    $fullname = sanitize($_POST['fullname']);
    $email    = sanitize($_POST['email']);
    $office   = sanitize($_POST['office']);
    $role     = sanitize($_POST['role']);
    $dept     = sanitize($_POST['dept']);
    // Check if user exists first
    $select   = "SELECT COUNT(user_id) AS num_rows FROM td_users WHERE user_id = '$userid'";
    $get      = odbc_prepare($conn, $select);
    odbc_execute($get);
    $row      = odbc_fetch_array($get);
    $num_rows = $row['num_rows'];
    if ($num_rows > 0) {
        $message = "<h6 class='text-danger'>User already exists!</h6>";
    } else {
        $string = "INSERT INTO td_users (
            user_id,
            full_name,
            email,
            office,
            role,
            user_icon,
            profile_pic,
            is_active,
            department) 
        VALUES (
            '$userid',
            '$fullname',
            '$email',
            '$office',
            '$role',
            'fa-user-circle',
            'default-pic.png',
            'Y',
            '$dept')";
        $query  = odbc_prepare($conn, $string);
        $result = odbc_execute($query);
        if (!$result) {
            $message = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "</h6>";
        } else {
            $message = "<h6 class='text-white'>Success!</h6>";
        }
    }
}
/* Choose User to Edit */
if (isset($_POST['choose_user'])) {
    $edit_userid = sanitize($_POST['edit_userid']);
    $str = "SELECT * FROM td_users WHERE user_id = '$edit_userid'";
    $query = odbc_prepare($conn, $str);
    $result = odbc_execute($query);
    if (!$result) {
        $message_2 = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "</h6>";
    } else {
        while($row = odbc_fetch_array($query)) {
            $edit_full_name   = $row['full_name'];
            $edit_email       = $row['email'];
            $edit_office      = $row['office'];
            $edit_role        = $row['role'];
            $edit_department  = $row['department'];
        }
        $message_2 = "<h6 class='text-white'>Now editing User $edit_userid: $edit_full_name</h6>";
    }
}
/* Edit User */
if (isset($_POST['edit_user'])) {
    $userid   = sanitize($_POST['userid']);
    $fullname = sanitize($_POST['fullname']);
    $email    = sanitize($_POST['email']);
    $office   = sanitize($_POST['office']);
    $role     = sanitize($_POST['role']);
    $dept     = sanitize($_POST['dept']);
    $string   = "UPDATE td_users SET 
        full_name = '$fullname',
        email     = '$email',
        office    = '$office',
        role      = '$role',
        department = '$dept'
    WHERE user_id = '$userid'";
    $query    = odbc_prepare($conn, $string);
    $result   = odbc_execute($query);
    if (!$result) {
        $message_2 = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<'h6>";
    } else {
        $message_2 = "<h6 class='text-white'>Success!</h6>";
    }
}
/* Deactivate/Reactivate User*/
if(isset($_POST['change_user_status'])) {
    $userid = sanitize($_POST['userid']);
    $status = sanitize($_POST['status']);
    $string = "UPDATE td_users SET is_active = '$status'
            WHERE user_id = '$userid'";
    $query = odbc_prepare($conn, $string);
    $result   = odbc_execute($query);
    if (!$result) {
        $message_3 = "<h6 class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<'h6>";
    } else {
        $message_3 = "<h6 class='text-white'>Success!</h6>";
    }
}
// Retrieve all user records
function getRecords() {
    global $conn;
    $str = "SELECT * FROM td_users ORDER BY full_name ASC";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    $records = '';
    while($row = odbc_fetch_array($query)) {
        $records .= "<tr>
            <td>".$row['user_id']."</td>
            <td>".$row['full_name']."</td>
            <td>".$row['email']."</td>
            <td>".$row['office']."</td>
            <td>".$row['role']."</td>
            <td>".$row['department']."</td>
            <td>".$row['is_active']."</td>
            </tr>";
    }
    echo $records;
}
?>

<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>
                    <section class="row">
                        <?php if ($page_access) { ?>
                            <div class="col-sm-12">
                                <section class="row">
                                    <!-- Add New User Form -->
                                    <div class="col-lg-4 mb-4">
                                        <div class="card card-inverse card-success">
                                            <div class="card-block">
                                                <h3 class="card-title">Add New User</h3>
                                                <?php
                                                if (isset($message)) {
                                                    echo $message;
                                                }
                                                ?>
                                                <form class="form-horizontal" action="" method="post">
                                                    <fieldset>
                                                        <!-- User ID input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="name">User ID</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="name" name="userid" type="text" class="form-control"  required autofocus>
                                                            </div>
                                                        </div>

                                                        <!-- Name input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="name">Name</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="name" name="fullname" type="text" class="form-control"  required>
                                                            </div>
                                                        </div>

                                                        <!-- Email input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="email">E-mail</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="email" name="email" type="text" class="form-control"  required>
                                                            </div>
                                                        </div>

                                                        <!-- Office select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="office">Office</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="office" name='office' class="form-control" required>
                                                                    <option value='' selected>Choose office</option>
                                                                    <option value='LRDO'>Laredo</option>
                                                                    <option value='NWA'>Nothwest Arkansas</option>
                                                                    <option value='PHX'>Phoenix</option>
                                                                    <option value='SLC'>Salt Lake City</option>
                                                                    <option value='WMA'>Western Massachusetts</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Role select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="role">Role</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="role" name='role' class="form-control" required>
                                                                    <option value='' selected>Choose role</option>
                                                                    <option value='admin'>Admin</option>
                                                                    <option value='trainer'>Trainer</option>
                                                                    <option value='student'>Student</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Department select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="dept">Department</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="dept" name='dept' class="form-control" required>
                                                                    <option value='' selected>Choose department</option>
                                                                    <?php createOptions('departments'); ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Form actions -->
                                                        <div class="form-group">
                                                            <div class="col-12 widget-right no-padding">
                                                                <input type="submit" name="add_user" value="Submit" class="btn btn-secondary btn-md float-right">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Edit User Form -->
                                    <div class="col-lg-4 mb-4">
                                        <div class="card card-inverse card-primary">
                                            <div class="card-block">
                                                <h3 class="card-title">Edit User</h3>
                                                <?php
                                                if (isset($message_2)) {
                                                    echo $message_2;
                                                }
                                                ?>
                                                 <form class="form-inline" action="" method="post">
                                                    <!-- Choose Task input-->
                                                    <label class="control-label" for="edit_userid">Choose User </label>
                                                    <div class="ml-sm-2 mr-sm-2">
                                                        <select id="edit_userid" name="edit_userid" class="form-control no-padding" required>
                                                            <option value="">...</option>
                                                            <?php createOptions('active_users'); ?>
                                                        </select>
                                                    </div>
                                                    <input type="submit" name="choose_user" value="Edit" class="btn btn-secondary btn-md float-right">
                                                </form>

                                                <form class="form-horizontal" action="" method="post">
                                                    <fieldset>

                                                        <!-- Name input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="name">Name</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="name" name="fullname" type="text" class="form-control" required value="<?php if(isset($edit_full_name)) {echo $edit_full_name;}?>">
                                                            </div>
                                                        </div>

                                                        <!-- Email input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="email">E-mail</label>

                                                            <div class="col-12 no-padding">
                                                                <input id="email" name="email" type="text" class="form-control" required value="<?php if(isset($edit_email)) {echo $edit_email;}?>">
                                                            </div>
                                                        </div>

                                                        <!-- Office select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="office">Office</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="office" name='office' class="form-control" required>
                                                                    <?php if(isset($edit_office)) {?>
                                                                    <option value="<?php echo $edit_office;?>" selected>Currently: <?php echo $edit_office;?></option>
                                                                    <?php }?>
                                                                    <option value=''>Choose office</option>
                                                                    <option value='LRDO'>Laredo</option>
                                                                    <option value='NWA'>Nothwest Arkansas</option>
                                                                    <option value='PHX'>Phoenix</option>
                                                                    <option value='SLC'>Salt Lake City</option>
                                                                    <option value='WMA'>Western Massachusetts</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Role select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="role">Role</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="role" name='role' class="form-control" required>
                                                                    <?php if(isset($edit_role)) {?>
                                                                    <option value="<?php echo $edit_role;?>" selected>Currently: <?php echo $edit_role;?></option>
                                                                    <?php }?>
                                                                    <option value=''>Choose role</option>
                                                                    <option value='admin'>Admin</option>
                                                                    <option value='trainer'>Trainer</option>
                                                                    <option value='student'>Student</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Department select -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="dept">Department</label>

                                                            <div class="col-12 no-padding">
                                                                <select id="dept" name='dept' class="form-control" required>
                                                                    <?php if(isset($edit_department)) {?>
                                                                    <option value="<?php echo $edit_department;?>" selected>Currently: <?php echo $edit_department;?></option>
                                                                    <?php }?>
                                                                    <option value=''>Choose department</option>
                                                                    <?php createOptions('departments'); ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Form actions -->
                                                        <div class="form-group">
                                                            <div class="col-12 widget-right no-padding">
                                                                <input type="submit" name="edit_user" value="Submit" class="btn btn-secondary btn-md float-right">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Change User Status -->
                                    <div class="col-lg-4 mb-4">
                                        <div class="card card-inverse card-danger">
                                            <div class="card-block">
                                                <h3 class="card-title">Deactivate/Reactivate User</h3>
                                                <?php
                                                if (isset($message_3)) {
                                                    echo $message_3;
                                                }
                                                ?>
                                                <form class="form-horizontal" action="" method="post">
                                                    <fieldset>
                                                        <!-- User ID input-->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="name">Choose User</label>

                                                            <div class="col-12 no-padding">
                                                                <select name="userid" class="form-control" required>
                                                                    <option value=""></option>
                                                                    <?php createOptions('all_users'); ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Choose Status -->
                                                        <div class="form-group">
                                                            <label class="col-12 control-label no-padding" for="status">Status</label>

                                                            <div class="col-12 no-padding">
                                                                <select name="status" class="form-control" required>
                                                                    <option value=""></option>
                                                                    <option value="Y">Reactivate</option>
                                                                    <option value="N">Deactivate</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <!-- Form actions -->
                                                        <div class="form-group">
                                                            <div class="col-12 widget-right no-padding">
                                                                <input type="submit" name="change_user_status" value="Submit" class="btn btn-secondary btn-md float-right">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <div class="card mb-4">
                                    <div class="card-block">
                                        <h3 class="card-title">All Users</h3>


                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>User ID</th>

                                                        <th>Name</th>

                                                        <th>E-mail</th>

                                                        <th>Office</th>
                                                        
                                                        <th>Role</th>
                                                        
                                                        <th>Dept</th>
                                                        
                                                        <th>Active</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php getRecords();?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php } else { ?>
                            <div class="col-sm-12">
                                <div class="alert bg-danger" role="alert">
                                    <em class="fa fa-minus-circle mr-2"></em> You do not have access to this page. Please contact <a href="mailto:allison_broughton@swifttrans.com?Subject=Page%20Access%20for%20<?php echo $page_title; ?>" target="_top" class="alert-link">Support</a> if you feel that this is an error.</div>
                            </div>
<?php } ?>
                    </section>
                </main>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>
