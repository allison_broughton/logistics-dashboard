<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'Charts';
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">

                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>

                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">
                                <div class="col-12 mb-2">
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Line Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Bar Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="bar-chart" height="200" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-6 ">
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Donut Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="doughnut-chart" height="300" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Pie Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="pie-chart" height="300" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-6">
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Radar Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="radar-chart" height="300" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Polar Area Chart</h3>

                                            <div class="dropdown card-title-btn-container">
                                                <button class="btn btn-sm btn-subtle dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><em class="fa fa-cog"></em></button>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="#"><em class="fa fa-search mr-1"></em> More info</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-thumb-tack mr-1"></em> Pin Window</a>
                                                    <a class="dropdown-item" href="#"><em class="fa fa-remove mr-1"></em> Close Window</a></div>
                                            </div>

                                            <h6 class="card-subtitle mb-2 text-muted">Sample Data</h6>

                                            <div class="canvas-wrapper">
                                                <canvas class="main-chart" id="polar-area-chart" height="300" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-2">
                                    <div class="card mb-3 text-center ">
                                        <div class="card-block">
                                            <div class="easypiechart" id="easypiechart-1" data-percent="25" ></div>

                                            <h5 class="mt-2 mb-1">Label 1</h5>

                                            <p class="mb-1">25%</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-2">
                                    <div class="card mb-3 text-center ">
                                        <div class="card-block">
                                            <div class="easypiechart" id="easypiechart-2" data-percent="50" ></div>

                                            <h5 class="mt-2 mb-1">Label 2</h5>

                                            <p class="mb-1">50%</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 mb-2">
                                    <div class="card mb-3 text-center ">
                                        <div class="card-block">
                                            <div class="easypiechart" id="easypiechart-3" data-percent="75" ></div>

                                            <h5 class="mt-2 mb-1">Label 3</h5>

                                            <p class="mb-1">75%</p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </section>
                    <section class="row">
                        <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
                    </section>
            </div>
        </section>
    </main>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

<script src="js/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/custom.js"></script>
<script>
    window.onload = function () {
        var chart1 = document.getElementById("line-chart").getContext("2d");
        window.myLine = new Chart(chart1).Line(lineChartData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleFontColor: "#c5c7cc"
        });
        var chart2 = document.getElementById("bar-chart").getContext("2d");
        window.myBar = new Chart(chart2).Bar(barChartData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleFontColor: "#c5c7cc"
        });
        var chart3 = document.getElementById("doughnut-chart").getContext("2d");
        window.myDoughnut = new Chart(chart3).Doughnut(doughnutData, {
            responsive: true,
            segmentShowStroke: false
        });
        var chart4 = document.getElementById("pie-chart").getContext("2d");
        window.myPie = new Chart(chart4).Pie(pieData, {
            responsive: true,
            segmentShowStroke: false
        });
        var chart5 = document.getElementById("radar-chart").getContext("2d");
        window.myRadarChart = new Chart(chart5).Radar(radarData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.05)",
            angleLineColor: "rgba(0,0,0,.2)"
        });
        var chart6 = document.getElementById("polar-area-chart").getContext("2d");
        window.myPolarAreaChart = new Chart(chart6).PolarArea(polarData, {
            responsive: true,
            scaleLineColor: "rgba(0,0,0,.2)",
            segmentShowStroke: false
        });
    };</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

</body>
</html>