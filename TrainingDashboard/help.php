<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'Help Section';

/* Sign Off on SOP */
if(isset($_GET['sign'])) {
    $file_id = $_GET['sign'];
    // Check if row exists first
    $select   = "SELECT COUNT(signed_by) AS num_rows FROM td_signoffs 
        WHERE signed_by = '$user_id' AND file_id = '$file_id'";
    $get      = odbc_prepare($conn, $select);
    odbc_execute($get);
    $row      = odbc_fetch_array($get);
    $num_rows = $row['num_rows'];
    if ($num_rows > 0) {
        $debug = "<h6 class='text-danger'>You have already signed off on this file!</h6>";
    } else {
        $str = "INSERT INTO td_signoffs (
        file_id,
        signed_by,
        signed_on) VALUES (
        '$file_id',
        '$user_id',
        '$today_datetime')";
        $query = odbc_prepare($conn, $str);
        $result = odbc_execute($query);
        if (!$result) {
            $debug .= "<span class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<br/></span>";
        } else {
            $debug .= "<span class='text-success'>You have signed off on this document.</span>";
        }
        // Trigger alert
        $title = "User Signed Off";
        $notes = getUserFullName($user_id) . " signed off on file " . getFileName($file_id);
        $debug .= createAlert($title, $notes, 'pitcn', 'unread');
    }
}

function getFiles() {
    global $conn;
    $str = "SELECT * FROM td_files WHERE is_active = 'Y' ORDER BY subject,title ASC";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    $files = '';
    while($row = odbc_fetch_array($query)) {
        $files .= "<tr>
            <td><a href='files/".$row['file_name']."' title='Download' download><i class='fa fa-download'></i></a><span class='sr-only'>Download</span>&nbsp;
                <a href='help.php?sign=".$row['p_id']."' title='Sign'><i class='fa fa-pencil'></i></a><span class='sr-only'>Sign</span>
            </td>

            <td>".$row['title']."</td>

            <td>".$row['type']."</td>

            <td>".$row['subject']."</td>

            <td>".formatDate($row['uploaded_on'], 'n/d/Y H:i')."</td>
        </tr>";
    }
    echo $files;
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>
                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">
                                <div class="col-md-12 col-lg-8">
                                    <!--<div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Choose Topic</h3>
                                            <h6><a href='help.php?T=Commissions'>Commissions</a></h6>
                                            <h6><a href='help.php?T=McLeod'>McLeod</a></h6>                                            
                                        </div>
                                    </div>-->
                                    <?php echo $debug;?>
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">SOP's, FAQ's, and Tips & Tricks</h3>
                                       
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Actions</th>
                                                            
                                                            <th>Title</th>

                                                            <th>Type</th>
                                                            
                                                            <th>Subject</th>

                                                            <th>Uploaded On</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php getFiles();?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <span class="sr-only">Template by <a href="https://www.medialoot.com">Medialoot</a></span>
                            </section>
                        </div>
                    </section>
                </main>
            </div>
        </div>        

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>
        <script>
            window.onload = function () {
                var chart1 = document.getElementById("line-chart").getContext("2d");
                window.myLine = new Chart(chart1).Line(lineChartData, {
                    responsive: true,
                    scaleLineColor: "rgba(0,0,0,.2)",
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleFontColor: "#c5c7cc"
                });
            };
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>