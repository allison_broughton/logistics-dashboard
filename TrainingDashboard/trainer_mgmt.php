<?php
require 'include_functions.php';
require 'include_menu.php';
require 'footer.php';

$page_title = 'Trainer Management';
/* Check page access */
if(getAdminAccess($user_id, 'admin') || getAdminAccess($user_id, 'trainer')) {
    $page_access = true;
} else {
    $page_access = false;
}
/* Dismiss Alert */
if(isset($_GET['dismiss'])) {
    $alert_id = $_GET['dismiss'];
    $str = "UPDATE td_alerts SET status = 'dismissed' WHERE p_id = '$alert_id'";
    $query = odbc_prepare($conn, $str);
    $result = odbc_execute($query);
    if(!$result) {
        $message_2 = "<span class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<br/></span>";
    } else {
        $message_2 = "<span class='text-success'>Dismissed alert!</span>";
    }
}
/* Deactivate File */
if(isset($_GET['deactivate'])) {
    $file_id = $_GET['deactivate'];
    $str = "UPDATE td_files SET is_active = 'N',
        deactivated_by = '$user_id',
        deactivated_on = '$today_datetime'
        WHERE p_id = '$file_id'";
    $query = odbc_prepare($conn, $str);
    $result = odbc_execute($query);
    if(!$result) {
        $message_3 = "<span class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<br/></span>";
    } else {
        $message_3 = "<span class='text-success'>File Removed!</span>";
    }
}
/*********************************************** File Upload *************************************************/
if (isset($_POST["upload_file"])) {
    $server_path = getcwd();
    $uploaddir   = $server_path . '/files/';
    // Rename file with randomly generated number
    $temp        = explode(".", $_FILES["fileToUpload"]["name"]);
    $newfilename = round(microtime(true)) . '.' . end($temp);
    // Move file to the /files directory of the root folder for the Training Dashboard
    $move_file   = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $uploaddir . $newfilename);
    if ($move_file) {
        $message      .= "<span class='text-success'>File is valid, and was successfully uploaded.</span>";
        // Insert file info into moodle.td_files
        $title        = sanitize($_POST['title']);
        $type         = sanitize($_POST['type']);
        $subject      = sanitize($_POST['subject']);
        $role         = sanitize($_POST['role']);
        $insert_file  = "INSERT INTO td_files (
                title,
                file_name,
                type,
                subject,
                role,
                uploaded_by,
                uploaded_on,
                is_active) VALUES (
                '$title',
                '$newfilename',
                '$type',
                '$subject',
                '$role',
                '$user_id',
                '$today_datetime',
                'Y')";
        //$message .= "<br/><span class='text-primary'>" . $insert_file . "</span><br/>";
        $insert_query = odbc_prepare($conn, $insert_file);
        $result       = odbc_execute($insert_query);
        if (!$result) {
            $message .= "<span class='text-danger'>Query Failed: " . odbc_errormsg($conn) . "<br/></span>";
        } else {
            $message .= "<span class='text-success'>Done!</span>";
        }
    } else {
        switch ($_FILES['fileToUpload'] ['error']) {
                case 1:
                    $error_msg = 'The file is bigger than this PHP installation allows.';
                    break;
                case 2:
                    $error_msg = 'The file is bigger than this form allows.';
                    break;
                case 3:
                    $error_msg = 'Only part of the file was uploaded.';
                    break;
                case 4:
                    $error_msg = 'No file was uploaded.';
                    break;
                case 6:
                    $error_msg = 'Missing a temporary folder.';
                    break;
                case 7:
                    $error_msg = 'Failed to write file to disk.';
                    break;
                case 8:
                    $error_msg = 'A PHP extension stopped the file upload.';
                    break;
            }
        $message .= "<span class='text-danger'>Sorry, your file was not uploaded. 
            <i class='fa fa-frown-o'></i><br/>ERROR CODE: " . $error_msg . "</span>";
    }
}
function getAlerts() {
    global $conn;
    global $user_id;
    //$user_id = 'pitcn';
    $str = "SELECT * FROM td_alerts WHERE alert_user = '$user_id' AND status <> 'dismissed' 
        ORDER BY created_on DESC";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    $alerts = '';
    while($row = odbc_fetch_array($query)) {
        $alerts .= "<tr>
            <td>
                <a href='trainer_mgmt.php?dismiss=".$row['p_id']."' title='Dismiss'><i class='fa fa-close'></i></a><span class='sr-only'>Dismiss Alert</span>
            </td>

            <td>".$row['alert_title']."</td>

            <td>".$row['alert_notes']."</td>

            <td>".formatDate($row['created_on'], 'n/d/Y H:i')."</td>
        </tr>";
    }
    echo $alerts;
}
function getFiles() {
    global $conn;
    $str = "SELECT * FROM td_files WHERE is_active = 'Y' 
        ORDER BY title,subject ASC";
    $query = odbc_prepare($conn, $str);
    odbc_execute($query);
    $files = '';
    while($row = odbc_fetch_array($query)) {
        $files .= "<tr>
            <td>
                <a href='trainer_mgmt.php?deactivate=".$row['p_id']."' title='Deactivate'><i class='fa fa-close'></i></a><span class='sr-only'>Deactivate File</span>
            </td>

            <td>".$row['title']."</td>

            <td>".$row['type']."</td>
                
            <td>".$row['subject']."</td>
                
            <td>".$row['uploaded_by']."</td>

            <td>".formatDate($row['uploaded_on'], 'n/d/Y H:i')."</td>
        </tr>";
    }
    echo $files;
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php require 'include_html.html'; ?>
    <body>
        <div class="container-fluid" id="wrapper">
            <div class="row">
                <main class="col-xs-12 col-sm-8 offset-sm-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2 pt-3 pl-4">
                    <?php require 'include_top.php'; ?>

                    <section class="row">
                        <div class="col-sm-12">
                            <section class="row">

                                <div class="col-sm-12 col-md-6">
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Upload Files</h3>

                                            <h6 class="card-subtitle mb-2 text-muted">Upload SOP/FAQ/Tips to the dashboard</h6>
                                            <?php echo $message; ?>
                                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                                                <fieldset>
                                                    <!-- File selection -->
                                                    <div class="form-group">
                                                        <label class="col-12 control-label no-padding" for="file"><span class="sr-only">Choose File</span></label>

                                                        <div class="col-12 no-padding">
                                                            <input id="name" name="fileToUpload" type="file" placeholder="Your name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <!-- Title input -->
                                                    <div class="form-group">
                                                        <label class="col-12 control-label no-padding" for="title">Title</label>

                                                        <div class="col-12 no-padding">
                                                            <input id="title" name="title" type="text" placeholder="Document Title" class="form-control">
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Type selection -->
                                                    <div class="form-group">
                                                        <label class="col-12 control-label no-padding" for="type">Type</label>

                                                        <div class="col-12 no-padding">
                                                            <select id="type" name='type' class="form-control" required>
                                                                <option value='' selected>Choose type</option>
                                                                <option value='SOP'>SOP</option>
                                                                <option value='FAQ'>FAQ</option>
                                                                <option value='TIPS'>TIPS</option>
                                                                <option value='OTHER'>OTHER</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Subject selection -->
                                                    <div class="form-group">
                                                        <label class="col-12 control-label no-padding" for="subject">Subject</label>

                                                        <div class="col-12 no-padding">
                                                            <select id="subject" name='subject' class="form-control" required>
                                                                <option value='' selected>Choose subject</option>
                                                                <option value='Commissions'>Commissions</option>
                                                                <option value='McLeod'>McLeod</option>
                                                                <option value='Other'>Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Role selection -->
                                                    <div class="form-group">
                                                        <label class="col-12 control-label no-padding" for="role">Role</label>

                                                        <div class="col-12 no-padding">
                                                            <select id="role" name='role' class="form-control" required>
                                                                <option value='' selected>Choose role that file is intended for</option>
                                                                <option value="ALL">*****All Roles*****</option>
                                                                <?php createOptions('departments');?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Form actions -->
                                                    <div class="form-group">
                                                        <div class="col-12 widget-right no-padding">
                                                            <input type="submit" name="upload_file" value="Upload" class="btn btn-primary btn-md float-right">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Alerts</h3>

                                            <h6 class="card-subtitle mb-2 text-muted">Check here for alerts</h6>
                                            <?php echo $message_2; ?>
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            
                                                            <th>Alert</th>
                                                            
                                                            <th>Message</th>
                                                            
                                                            <th>Created On</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php getAlerts();?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mb-4">
                                        <div class="card-block">
                                            <h3 class="card-title">Manage Files</h3>

                                            <h6 class="card-subtitle mb-2 text-muted">Deactivate files that are no longer needed</h6>
                                            <?php echo $message_3; ?>
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            
                                                            <th>Title</th>
                                                            
                                                            <th>Type</th>
                                                            
                                                            <th>Subject</th>
                                                            
                                                            <th>Uploaded By</th>
                                                            
                                                            <th>Uploaded On</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php getFiles();?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="col-12 mt-1 mb-4">Template by <a href="https://www.medialoot.com">Medialoot</a></div>
                            </section>
                        </div>
                    </section>
                </main>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="dist/js/bootstrap.min.js"></script>

        <script src="js/chart.min.js"></script>
        <script src="js/chart-data.js"></script>
        <script src="js/easypiechart.js"></script>
        <script src="js/easypiechart-data.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/custom.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

    </body>
</html>
