<?php
// Set up and test odbc connection
$server            = '10.1.244.122';
$user              = 'svc_lorl';
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'McLeod_lme_1110_prod';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);

// Get available orders and store in array
function getOrders() {
    global $conn;
    $string = "SELECT o.id            AS Order_ID,
                o.equipment_type_id   AS Trailer_Type,
                o.bill_distance       AS Miles,
                os.city_name          AS Origin_city,
                os.state              AS Origin_State,
                os.sched_arrive_early AS PickupTime,
                u.NAME                AS Broker_Name,
                u.office_phone        AS Broker_Phone,
                u.email_address       AS Broker_Email,
                ds.city_name          AS Dest_City,
                ds.state              AS Dest_State,
                ds.sched_arrive_early AS DeliveryTime,
                CASE
                  WHEN stopcount <= 2 THEN 0
                  ELSE stopcount - 2
                END                   AS Additional_Stops
         FROM   McLeod_lme_1110_prod.dbo.orders o (nolock)
                JOIN McLeod_lme_1110_prod.dbo.movement_order mo (nolock)
                  ON o.id = mo.order_id
                JOIN McLeod_lme_1110_prod.dbo.stop ds (nolock)
                  ON o.consignee_stop_id = ds.id
                JOIN McLeod_lme_1110_prod.dbo.stop os (nolock)
                  ON o.shipper_stop_id = os.id
                JOIN McLeod_lme_1110_prod.dbo.movement m (nolock)
                  ON mo.movement_id = m.id
                JOIN Logistics_Data_Warehouse.dbo.lb_broker_area ba (nolock)
                  ON os.state = ba.state
                JOIN McLeod_lme_1110_prod.dbo.users u (nolock)
                  ON Lower( ba.broker ) = u.id
                JOIN ( SELECT order_id,
                              Max( order_sequence ) AS stopcount
                       FROM   McLeod_lme_1110_prod.dbo.stop (nolock)
                       GROUP  BY order_id ) AS ms
                  ON o.id = ms.order_id
         WHERE  m.brokerage_status = 'READY'
                AND o.status = 'A' AND o.on_hold <> 'Y'
                AND o.loadboard = 'Y'
         GROUP  BY o.id,o.equipment_type_id,o.bill_distance,os.city_name,os.state,os.sched_arrive_early,u.NAME,u.email_address,u.office_phone,ds.city_name,ds.state,ds.sched_arrive_early,ms.stopcount
         ORDER  BY o.id";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row    = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}
// Store json data array in variable
$data = getOrders();

header('Content-Type: application/json');
echo json_encode($data);