<?php
/*
 * Can you roll a script to do the following:
 * I want to post a variable $req
 * When it = orders
 * Connect to sql 1802
 * Select order Id from prod_orders where loadboard= Y and status = A
 * Jsonencode the response array and echo it

 */

// Set up and test odbc connection
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = '';
$pass              = '';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_Dev';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
if ($conn) {
    $conn_status = "Connected";
} else {
    die;
    $conn_status = "Could not connect";
}

// Get available orders and store in table rows
  function getOrders() {
  global $conn;
  $string = "
       SELECT o.id           AS Order_ID,
       o.equipment_type_id   AS Trailer_Type,
       o.bill_distance       AS Miles,
       os.city_name          AS Origin_City,
       os.state              AS Origin_State,
       os.sched_arrive_early AS PickupTime,
       u.NAME                AS Broker_Name,
       u.office_phone        AS Broker_Phone,
       u.email_address       AS Broker_Email,
       ds.city_name          AS Dest_City,
       ds.state              AS Dest_State,
       ds.sched_arrive_early AS DeliveryTime,
       CASE
         WHEN stopcount <= 2 THEN 0
         ELSE stopcount - 2
       END                   AS Additional_Stops
FROM   Logistics_Data_Warehouse_Dev.dbo.prod_orders o (nolock)
       JOIN Logistics_Data_Warehouse_Dev.dbo.vw_mcld_prod_movement_order mo (nolock)
         ON o.id = mo.order_id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_stop ds (nolock)
         ON o.consignee_stop_id = ds.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_stop os (nolock)
         ON o.shipper_stop_id = os.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_movement m (nolock)
         ON mo.movement_id = m.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.lb_broker_area ba (nolock)
         ON os.state = ba.state
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_users u (nolock)
         ON Lower( ba.broker ) = u.id
       JOIN ( SELECT order_id,
                     Max( order_sequence ) AS stopcount
              FROM   Logistics_Data_Warehouse_Dev.dbo.prod_stop (nolock)
              GROUP  BY order_id ) AS ms
         ON o.id = ms.order_id
WHERE  m.brokerage_status = 'AVAILABL'
       AND o.status = 'A'
       AND o.loadboard = 'Y'
GROUP  BY o.id,o.equipment_type_id,o.bill_distance,os.city_name,os.state,os.sched_arrive_early,u.NAME,u.email_address,u.office_phone,ds.city_name,ds.state,ds.sched_arrive_early,ms.stopcount
ORDER  BY o.id ";
  $stmt = odbc_prepare($conn, $string);
  odbc_execute($stmt);
  $results = "";
  while ($row     = odbc_fetch_array($stmt)) {
      $t = strtotime($row['PickupTime']);
      $pickup_time = date('m/d/Y Hi',$t);
      $results .= "<tr><td>" . $row['Order_ID'] . "</td><td>" . $pickup_time . "</td><td>" . $row['Origin_City'] . "</td><td>" . $row['Origin_State'] . "</td><td>" . $row['Dest_City'] . "</td><td>" . $row['Dest_State'] . "</td><td>" . $row['Miles'] . "</td><td>V</td><td>" . $row['Broker_Name'] . "</td><td>" . $row['Broker_Phone'] . "</td><td>" . $row['Broker_Email'] . "</td></tr>";
  }
  if (empty($results)) {
  $results = "No available orders found.";
  }
  return $results;
  }

  $orders = getOrders();
 

// Get available orders and store in json array
/*function getOrders() {
    global $conn;
    //$string = "SELECT id FROM prod_orders WHERE loadboard = 'Y' and status = 'A'";
    $string = "
       SELECT o.id           AS Order_ID,
       o.equipment_type_id   AS Trailer_Type,
       o.bill_distance       AS Miles,
       os.city_name          AS Origin_city,
       os.state              AS Origin_State,
       os.sched_arrive_early AS PickupTime,
       u.NAME                AS Broker_Name,
       u.office_phone        AS Broker_Phone,
       u.email_address       AS Broker_Email,
       ds.city_name          AS Dest_City,
       ds.state              AS Dest_State,
       ds.sched_arrive_early AS DeliveryTime,
       CASE
         WHEN stopcount <= 2 THEN 0
         ELSE stopcount - 2
       END                   AS Additional_Stops
FROM   Logistics_Data_Warehouse_Dev.dbo.prod_orders o (nolock)
       JOIN Logistics_Data_Warehouse_Dev.dbo.vw_mcld_prod_movement_order mo (nolock)
         ON o.id = mo.order_id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_stop ds (nolock)
         ON o.consignee_stop_id = ds.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_stop os (nolock)
         ON o.shipper_stop_id = os.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_movement m (nolock)
         ON mo.movement_id = m.id
       JOIN Logistics_Data_Warehouse_Dev.dbo.lb_broker_area ba (nolock)
         ON os.state = ba.state
       JOIN Logistics_Data_Warehouse_Dev.dbo.prod_users u (nolock)
         ON Lower( ba.broker ) = u.id
       JOIN ( SELECT order_id,
                     Max( order_sequence ) AS stopcount
              FROM   Logistics_Data_Warehouse_Dev.dbo.prod_stop (nolock)
              GROUP  BY order_id ) AS ms
         ON o.id = ms.order_id
WHERE  m.brokerage_status = 'AVAILABL'
       AND o.status = 'A'
       AND o.loadboard = 'Y'
GROUP  BY o.id,o.equipment_type_id,o.bill_distance,os.city_name,os.state,os.sched_arrive_early,u.NAME,u.email_address,u.office_phone,ds.city_name,ds.state,ds.sched_arrive_early,ms.stopcount
ORDER  BY o.id ";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row    = odbc_fetch_array($stmt)) {
        $json[][] = $row;
    }
    return $json;
}*/

// Set var $json equal to the array returned by the getOrders() function
//$json = getOrders();

/* Here is an example of the data returned by the above query
  "Order_ID": "0332609",
  "Trailer_Type": "V",
  "Miles": "661.0",
  "Origin_city": "MEMPHIS",
  "Origin_State": "TX",
  "PickupTime": "2017-04-18 07:00:00.000",
  "Broker_Name": "Dylan Sprague",
  "Broker_Phone": "479-254-1077",
  "Broker_Email": "dylan_sprague@swifttrans.com",
  "Dest_City": "MEMPHIS",
  "Dest_State": "TN",
  "DeliveryTime": "2017-04-19 08:00:00.000",
  "Additional_Stops": "0"
 */
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Swift Logistics Available Loads</title>

        <!-- Bootstrap -->
        <link href="Css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- Data Tables -->


    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Swift Logistics</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="http://swiftlogistics.com/contact-us/payment-and-document-submission">Quick Pay/ACH</a></li>

                        <li>
                            <p class="navbar-btn">
                                <a href="http://swiftcarriers.com/" class="btn btn-success">Join our Carrier Network</a>
                            </p>
                        </li>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>


        <div class="jumbotron">
            <div class="container">
                <h2>Available Loads</h2>
                <form class="form-inline">
                    <div class="input-group date" id="datetimepicker1">
                        <input type="text" class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <label class="sr-only" for="inlineFormInput">Origin</label>
                    <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Origin">
                    <label class="sr-only" for="inlineFormInput">Destination</label>
                    <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Destination">
                    <label class="sr-only" for="inlineFormInput">Equipment</label>
                    <select class="form-control">
                        <option>Van</option>
                        <option>Flat</option>
                        <option>Reefer</option>
                    </select>
                    <button type="submit" class="btn btn-primary">Find Loads</button>
                </form>

            </div>
        </div>

        <div class="container">

            <table id="example" class ="table table-striped table-bordered">
                <thead>
                    <tr><th>Load ID</th><th>Ship Date</th><th>Ship City</th><th>Ship State</th><th>Destination City</th><th>Destination State</th><th>Miles</th><th>Trailer Type</th><th>Contact</th><th>Phone</th><th>Email</th></tr>
                </thead>
                <tbody>
                    <?php
                    echo $orders;
                    ?>
                    

                </tbody>
            </table>
        </div>


        <footer class="footer">
            <div class="container">
                <p class="text-muted">Swift Logistics</p>
            </div>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true,
                    responsive: true
                });
            })
        </script>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
    </body>
</html>