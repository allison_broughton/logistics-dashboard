<?php
// Uncomment to show ALL notices, warnings, errors
/* error_reporting(E_ALL ^ E_NOTICE); 
  ini_set('display_errors', 1); */

// Set up and test odbc connection
$server            = '10.1.244.122'; //'10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; // Does not have access to this server
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'McLeod_lme_1110_prod'; //'Logistics_Data_Warehouse_Dev';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
// Test db conn - display status in footer
if ($conn) {
    $connection_status = "Successful";
    $status_color = '#00903b';
} else {
    $connection_status = "Failed";
    $status_color = '#dd0014';
}
// Retrieve user id
if(isset($_SERVER['AUTH_USER'])) {
    $username = $_SERVER['AUTH_USER'];
} else {
    $username = "localhost\DEV";
}
// Remove "SWIFT\" from usernames
function stripUsername($username) {
    if ($username === "localhost\DEV") {
        $user_id = "DEV";
    } else {
        $user_id = substr(stripslashes($username), 5);
    }
    return $user_id;
}
$user_id = stripUsername($username);
/* - Username: <?php echo $username;?> | User ID: <?php echo $user_id;?>*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Swift Logistics Available Loads</title>
        <!-- Favicon -->
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- Font Awesome CDN -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/my.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- Data Tables -->
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#">Swift Logistics</a>-->
                    <a id="logo" class='navbar-brand' href='#'><img src="img/Swift_Logistics.png" alt="Swift Logistics logo"/></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="http://swiftlogistics.com/contact-us/payment-and-document-submission">Quick Pay/ACH</a></li>

                        <li>
                            <p class="navbar-btn">
                                <a href="http://swiftcarriers.com/" class="btn btn-primary">Join our Carrier Network</a>
                            </p>
                        </li>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">
                <h2>Available Loads</h2>
            </div>
        </div>

        <div class="container-fluid">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <table id="example" class ="table table-striped table-bordered">
                    <thead style = "color:white;">
                        <tr><th data-priority="1">Load ID</th>
                            <th data-priority="2">Ship Date</th>
                            <th data-priority="3">Ship City</th>
                            <th data-priority="4">Ship State</th>
                            <th data-priority="5">Destination City</th>
                            <th data-priority="6">Destination State</th>
                            <th data-priority="7">Miles</th>
                            <th data-priority="8">Trailer Type</th>
                            <th data-priority="9">Contact</th>
                            <th data-priority="11">Phone</th>
                            <th data-priority="12">Email</th>
                            <th data-priority="10">Delivery</th>
                            <th data-priority="13">Additional Stops</th></tr>
                    </thead>
                    <tfoot>
                        <tr><th>Load ID</th><th>Ship Date</th><th>Ship City</th><th>Ship State</th><th>Destination City</th><th>Destination State</th><th>Miles</th><th>Trailer Type</th><th>Contact</th><th>Phone</th><th>Email</th><th>Delivery</th><th>Additional Stops</th></tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-1"></div>

        </div>


        <footer class="footer">
            <div class="container-fluid">
                <p class="text-muted"><i class="fa fa-copyright"></i> Swift Logistics, LLC &nbsp;
                    <i class="fa fa-plug"></i>&nbsp;Connection status: <strong><span style="color:<?php echo $status_color;?>;"><?php echo $connection_status;?></span></strong>&nbsp;
                    <i class="fa fa-user"></i>&nbsp;Logged in as: <?php echo $user_id;?></p>
            </div>
        </footer>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script>

            $(document).ready(function () {
                //Add text input
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });

                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    "autoWidth": false,
                    "searching": true,
                    "dom": '<l<t>ip>',
                    "ajax": "/get_orders.php",
                    "columns": [
                        {"data": "Order_ID"},
                        {"data": "PickupTime"},
                        {"data": "Origin_city"},
                        {"data": "Origin_State"},
                        {"data": "Dest_City"},
                        {"data": "Dest_State"},
                        {"data": "Miles"},
                        {"data": "Trailer_Type"},
                        {"data": "Broker_Name"},
                        {"data": "Broker_Phone"},
                        {"data": "Broker_Email"},
                        {"data": "DeliveryTime"},
                        {"data": "Additional_Stops"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0}
                    ],
                    "order": [[1, "asc"]]
                });

                setInterval(function () {
                    table.ajax.reload(null, false); // user paging is not reset on reload
                }, 30000);
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            });//end tag
        </script>
    </body>
</html>