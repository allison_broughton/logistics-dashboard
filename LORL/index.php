<?php
/*
 * LORL Project ~ Logistics Optimized Rate Lookup
 * Rate lookup tool to be used by Logistics employees
 */
////////////////////////////////////////////////// REQUIRED FILE(S) //////////////////////////////////////////////////
require 'include_functions.php'; 
/////////////////////////////////////////////////// ERROR REPORTING //////////////////////////////////////////////////
// Shows ONLY errors - use this setting when page is live
/*error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR);*/
error_reporting(E_ALL ^ E_NOTICE); // shows ALL notices, warnings, errors
ini_set('display_errors', 1);
// Check if user is using any version of IE
if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) {
    //display a javascript alert
    echo "<script>";
    echo "alert('Would you kindly view this page in Chrome or Edge?')";
    echo "</script>";
}
//////////////////////////////////////////////// INITIALIZE VARIABLES ///////////////////////////////////////////////
$jedi         = false; //When set to true, allows user to see error details/debug/query info
$debug        = "";
$pricing_team = false;
$support_body = "";
$today    = date('Y-m-d');
$tomorrow = (new DateTime('tomorrow'))->format('Y-m-d');
// Variables from Rate Lookup Form (lookup_rate)
$origin       = "";
$destination  = "";
$pickup_date  = "";
$weight       = "";
$type         = "";
//////////////////////////////////////////////// ODBC Connection ///////////////////////////////////////////////////
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_Dev';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
if ($conn) {
    $conn_status = "Connected";
} else {
    die;
    $conn_status = "Could not connect";
}

//////////////////////////////////////////////// IP/SERVER/USER INFO ///////////////////////////////////////////////
// THIS ONLY WORKS IF IMPERSONATION IS ENABLED IN PHP.INI (fastcgi.impersonate = 1)
// WEB CONFIG FILE MUST BE CONFIGURED (<identity impersonate="true" />)
// IIS WINDOWS AUTHENTICATION IS ENABLED AND ANONYMOUS AUTHENTICATION IS DISABLED
$ip          = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$server_name = $_SERVER['SERVER_NAME'];
$username    = $_SERVER['AUTH_USER'];
$user_id     = stripUsername($username);
//////////////////////////////////////////////// USER PERMISSIONS //////////////////////////////////////////////////
// Set jedi status and page style based on username
if ($username === "SWIFT\stral" || $username === "SWIFT\labin") {
    $jedi       = true;
    $stylesheet = "css/violet-theme.css";
} else if ($username === NULL && $server_name === "localhost") {
    // When the page is accessed on the server via localhost:8000
    $username   = "localhost\DEV";
    $jedi       = true;
    $stylesheet = "css/spot-rate-style.css";
} else {
    $stylesheet = "css/spot-rate-style.css";
}
// Set form results permission based on whether user is part of pricing team
if ($username === "SWIFT\stral" || // Allison Broughton
        $username === "localhost\DEV" || // Netbeans (Testing)
        $username === "SWIFT\everd" || // Don Everhart
        $username === "SWIFT\marro" || // Marcus Roberts
        $username === "SWIFT\casia" || // Angel Casillas
        $username === "SWIFT\skinb" || // Barack Skinner
        $username === "SWIFT\chamr" // 'Fina' Chamberlain
) {
    $pricing_team = true;
}
// Hidden stats for debugging IP/Server/Username
$stats    = "IP/Host: " . $ip . "</br>Server: " . $server_name . "</br>User ID: " . $user_id;



// When user clicks Get Quote, do the following
if (isset($_POST["lookup_rate"])) {
    // Start CPU time
    $rustart      = getrusage();
    // Start Real time
    $time_start   = date('Y-m-d H:i:s');
    global $conn;
    // Sanitize user input and store in variables
    $origin       = sanitize($_POST["origin"]);
    $destination  = sanitize($_POST["destin"]);
    $pickup_date  = sanitize($_POST["pickup_date"]);
    $weight       = sanitize($_POST["weight"]);
    $type         = sanitize($_POST["type"]);
    // Make trailer type user friendly when displaying results
    $trailer_type = "";
    if ($type == "V") {
        $trailer_type = "DRY VAN";
    } else if ($type == "R") {
        $trailer_type = "REEFER";
    } else {
        $trailer_type = "FLATBED";
    }
    // Set body contents for support e-mail when user clicks E-Mail Support AFTER submitting the spot rate form
    $support_body = "%0A%0A%0AOrigin:%20$origin%0ADestination:%20$destination%0APickup%20Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";

    // Make sure no fields were left empty
    if (!empty($origin && $destination && $pickup_date && $weight && $type)) {
        
        // Initialize error count and record count variables
        $has_errors = 0;
        $record_count = 1;
        
        // Validate cities first
        $origin_info = fuzzySearch($origin);
        $destin_info = fuzzySearch($destination);
        
        // If one or both cities were not found, kill the script and alert user
        if ((strpos($origin_info, 'No cities') !== false) || (strpos($destin_info, 'No cities') !== false)) {
            $message = "No cities found. Please check the spelling and state abbreviation(s).";
        } else {
            // Calculate miles
            $miles     = getMileage($origin, $destination);
            // Look up market lane id for cities
            $oMarket = getMarket($origin_info);
            $dMarket = getMarket($destin_info);
            // Next check to see if any markets exist for the chosen orign-destination pair
            if ((strpos($oMarket, 'No market') !== false) || (strpos($dMarket, 'No market') !== false)) {
                // Set error count and record count to be inserted into Job Log
                $has_errors = 1;
                $record_count = 0;
                $time_end = $time_start;
                $total_time = "00:00 (mm:ss)";
                $market_lane = "NO LANE FOUND";
                
                // Fill out e-mail to be sent to Pricing team if no markets are found
                $to      = "logisticspricing@swifttrans.com";
                $subject = "Spot%20Rate%20Tool-No%20Market%20Found%20For%20Lane";
                $body    = "Requesting%20spot%20rate%20for:%0ALane:%20$origin%20to%20$destination%0APickup Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";
                $message = "No markets found. Please contact <a href='mailto:$to?subject=$subject&body=$body' class='btn btn-info' target='_top'>Logistics Pricing.</a>";
            } else {
                // Concatenate origin and destination to create the market lane
                $market_lane   = $oMarket . "-" . $dMarket;
                // Look up van rate for the market
                $linehaul_rate = getLineHaul($oMarket, $dMarket, $type);
                // Make it pretty for page output
                $linehaul      = number_format($linehaul_rate, 2);
                // If linehaul rate returns NULL, do not continue. Alert user to contact Pricing team
                if ($linehaul_rate == NULL) {
                    // Set error count and record count to be inserted into Job Log
                    $has_errors = 1;
                    $record_count = 0;
                    $time_end = $time_start;
                    $total_time = "00:00 (mm:ss)";
                    // Fill out e-mail to be sent to Pricing team if no rates are found
                    $to      = "logisticspricing@swifttrans.com";
                    $subject = "Spot%20Rate%20Tool-No%20Linehaul%20Rate%20Found";
                    $body    = "Requesting%20spot%20rate%20for:%0ALane:%20$origin%20to%20$destination%0APickup Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";
                    $message = "No linehaul rate found for chosen market lane. Please contact <a href='mailto:$to?subject=$subject&body=$body' class='btn btn-info' target='_top'>Logistics Pricing.</a>";
                } else {
                    $fsc    = getFuel();
                    // Calculate spot rate for a range of margin values and display each one
                    $margin_range = ['Low' => 1.11, 'Recommended' => 1.15, 'High' => 1.20];
                    $margin    = [];
                    $spot_rate = [];
                    $results   = "";
                    $margin_array = [];
                    foreach ($margin_range as $key => $percentage) {
                        $margin[]    .= $key;
                        
                        // Check if there is an active Spot Modifier for the lane
                        $modifier_array = getModifier($market_lane, $type, $pickup_date);
                        // If modifier is P - add percent to linehaul rate
                        if($modifier_array['uom'] === 'P') {
                            $mod = $modifier_array['value'];
                            $percent  = $mod / 100;
                            $test = "<strong>Modifier Found</strong><br/>The percent modifier is " . $mod . "% and the original linehaul rate was " . $linehaul . ".<br/>";
                            $linehaul_mod = round(($linehaul + ($linehaul * $percent)),2);
                            $spot_rate[] .= round(((($linehaul_mod + $fsc) * $miles) * $percentage), 0, PHP_ROUND_HALF_UP);
                            $show_formula = "((<strong>$linehaul_mod</strong> Linehaul + <strong>$fsc</strong> Fuel Surcharge) * <strong>$miles</strong> Miles) * <strong>Margin</strong>";
                        } elseif($modifier_array['uom'] === 'F') { // If modifier is F - add flat amount to spot rate
                            $mod = $modifier_array['value'];
                            $test = "<strong>Modifier Found</strong><br/>The flat modifier is: " . $mod . ".<br/>";
                            $spot_rate[] .= round((((($linehaul + $fsc) * $miles) * $percentage) + $mod), 0, PHP_ROUND_HALF_UP);
                            $show_formula = "(((<strong>$linehaul</strong> Linehaul + <strong>$fsc</strong> Fuel Surcharge) * <strong>$miles</strong> Miles) * <strong>Margin</strong>) + <strong>$mod</strong> Flat Rate";
                        } elseif($modifier_array['uom'] === 'R') { // If modifier is R - add rate based on miles to linehaul
                            $rpm = $modifier_array['value'];
                            $test = "<strong>Modifier Found</strong><br/>The rate modifier is " . $rpm . ".<br/>";
                            $spot_rate[] .= round((((($linehaul + $rpm) + $fsc) * $miles) * $percentage), 0, PHP_ROUND_HALF_UP);
                            $show_formula = "(((<strong>$linehaul</strong> Linehaul + <strong>$rpm</strong> RPM) + <strong>$fsc</strong> Fuel Surcharge) * <strong>$miles</strong> Miles) * <strong>Margin</strong>";
                        } else { // If no modifier exists for the given lane and pickup date, calculate without it
                            $test = "No Modifier<br/>";
                            $spot_rate[] .= round(((($linehaul + $fsc) * $miles) * $percentage), 0, PHP_ROUND_HALF_UP);
                            $show_formula = "((<strong>$linehaul</strong> Linehaul + <strong>$fsc</strong> Fuel Surcharge) * <strong>$miles</strong> Miles) * <strong>Margin</strong>";
                        }
                        // Display spot rate for low, recommended, and high margin rates
                        $rate        = "";
                        for ($i = 0; $i < count($spot_rate); $i++) {
                            $rate = $spot_rate[$i];
                        }
                        $results .= $key . " Margin: $" . $rate . "<br/>";
                        $margin_array[] .= $percentage;
                    }
                    // Insert Recommended spot rate and margin rate for now until we add more columns to audit logs
                    $insert_spot_rate = $spot_rate[1];//implode(",", $spot_rate); //Uncomment to insert all 3 values
                    $insert_margin    = $margin_array[1];//implode(",", $margin_array); //Uncomment to insert all 3 values
                    // If user is on Pricing Team, display calculation details
                    if ($pricing_team) {
                        $form_results = "<div id='spot_rate' class='alert alert-success'>
                            " . $test . "
                            <strong>Recommended Rates: </strong><br/>" . $results . "<br/>
                            <strong>How we calculated this rate:</strong><br/>
                            " . $show_formula . "<br/>
                         </div>
                         <!--<div id='spot_rate' class='alert alert-warning'>
                            <strong>Can you find a carrier for this rate?</strong>
                            <form name='valid_rate' method='post' action=''>
                                <select name='broker_valid'>
                                    <option value='Y'>Yes</option>
                                    <option value='N'>No</option>
                                </select>
                                <input type='submit' name='validate' value='Submit'/>
                            </form>
                         </div>-->
                         <div id='spot_rate' class='alert alert-info'>
                            For the lane <strong>$origin_info</strong> to <strong>$destin_info</strong><br/>
                            <strong>Approximated Miles: </strong>" . $miles . "<br/>
                            <strong>Pickup Date: </strong>" . $pickup_date . "<br/>
                            <strong>Weight: </strong>" . number_format($weight) . " lbs<br/>
                            <strong>Trailer Type: </strong>" . $trailer_type . "<br/>
                            <strong>Market Lane: </strong>" . $market_lane . "
                         </div>";
                    } else {
                        // For all other users, simply display the Spot Rate Quote
                        $form_results = "<div id='spot_rate' class='alert alert-success'>
                            <strong>Recommended Rate: </strong><br/>" . $results . "<br/>
                            </div>
                            <!--<div id='spot_rate' class='alert alert-warning'>
                                <strong>Can you find a carrier for this rate?</strong>
                                <form name='valid_rate' method='post' action=''>
                                    <select name='broker_valid'>
                                        <option value='Y'>Yes</option>
                                        <option value='N'>No</option>
                                    </select>
                                    <input type='submit' name='validate' value='Submit'/>
                                </form>
                            </div>-->
                            <div id='spot_rate' class='alert alert-info'>
                            For the lane <strong>$origin_info</strong> to <strong>$destin_info</strong><br/>
                            <strong>Approximated Miles: </strong>" . $miles . "<br/>
                            <strong>Pickup Date: </strong>" . $pickup_date . "<br/>
                            <strong>Weight: </strong>" . number_format($weight) . " lbs<br/>
                            <strong>Trailer Type: </strong>" . $trailer_type . "<br/>
                            <strong>Market Lane: </strong>" . $market_lane . "
                         </div>";
                    }

                    // Calculate CPU usage and output runtime
                    function rutime($ru, $rus, $index) {
                        return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000)) - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
                    }
                    // End CPU time and calculate total CPU processing time
                    $ru              = getrusage();
                    $processing_time = rutime($ru, $rustart, "utime");
                    // End Real time
                    $time_end        = date('Y-m-d H:i:s');
                    // Calculate total seconds the query took in REAL TIME
                    $total_time      = date_create($time_end)->diff(date_create($time_start))->format('%I:%S (mm:ss)');
                    ;
                    // Details on cpu usage will print in footer on form submit
                    $cpu             = "&nbsp;&nbsp;<span class='glyphicon glyphicon-dashboard'></span>&nbsp;";
                    $cpu             .= "This process used " . rutime($ru, $rustart, "utime") .
                            " ms for its computations.\n It spent " . rutime($ru, $rustart, "stime") .
                            " ms in system calls.\n";
                }
            }
            // Insert Job details to Job_Log
            $job_log_insert = "INSERT INTO Job_Log (
                        Job_Name,
                        Has_Errors,
                        User_Name,
                        Record_Count,
                        Start_Date_Time,
                        End_Date_Time,
                        Processing_Time
                    ) 
                    VALUES (
                        'Spot_Rate_Tool',
                        '$has_errors',
                        '$user_id',
                        '$record_count',
                        '$time_start',
                        '$time_end',
                        '$total_time'
                        )";
            $job_log_stmt   = odbc_prepare($conn, $job_log_insert);
            odbc_execute($job_log_stmt);
            // Select the JobID generated in the db to use for the Spot_Rate_Log
            $job_id = getJobID();
            // Insert input and output data to Spot_Rate_Log for auditing purposes
            $insert = "INSERT INTO Spot_Rate_Log (
                        JobID,
                        User_Name,
                        IP,
                        User_Computer_Name,
                        Input_Trailer_Type,
                        Input_Origin,
                        Input_Destin,
                        Input_Weight,
                        Input_Pickup_Date,
                        Kma_Kma_Lane,
                        Output_Miles,
                        Output_Spot_Rate,
                        Output_Margin_Percent,
                        Output_Linehaul,
                        Output_FSC,
                        Query_Processing_Time            
                    )
                    VALUES (
                        '$job_id',
                        '$user_id',
                        '$ip',
                        '$server_name',
                        '$type',
                        '$origin',
                        '$destination',
                        '$weight',
                        '$pickup_date',
                        '$market_lane',
                        '$miles',
                        '$insert_spot_rate',
                        '$insert_margin',
                        '$linehaul',
                        '$fsc',
                        '00:$processing_time (ss:ms)'
                    )";
            $stmt   = odbc_prepare($conn, $insert);
            odbc_execute($stmt);
            
            // Display insert queries if user is a jedi
            if ($jedi) {
                $debug = "<br/>" . $job_log_insert;
                $debug .= "<br/>" . $insert;
                $debug .= "<br/>JobID = " . $job_id;
            }
        }
    } else {
        $trailer_type = "";
        $form_results = "<div id='spot_rate' class='alert alert-danger'>
                        <span class='glyphicon glyphicon-warning-sign'></span>
                        <strong> Please ensure you have filled out every field. </strong>
                        </div>";
    }
}

/*if (isset($_POST['validate'])) {
    $job_id        = getJobID();
    $valid         = $_POST['broker_valid'];
    $sql           = "UPDATE Spot_Rate_Log SET Broker_Valid = '$valid' WHERE JobID = '$job_id'";
    $query         = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $debug         .= "<pre>UPDATE STATEMENT: $sql</pre>";
    $form2_results = "<div id='spot_rate' class='alert alert-success'>
                        <strong>Thank you for your feedback.</strong>
                        </div>";
}*/
// If user is a jedi, allow them to see previous 30 lookups from everyone
// For everyone else, allow them to see their own lookups - also limited by 30
if($jedi) {
    $limit = "";
} else {
    $limit = "AND JobLog.User_Name = '$user_id'";
}
$lookups_today = getTodaysLookups($limit);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LORL</title>
        <link rel="icon" type="image/ico" href="images/swift_logistics_logo_circle.ico"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $stylesheet; ?>"><!--Personalized stylesheet-->
        <script type="text/javascript" src="js/jquery-3.1.1.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="js/egg.js"></script>
        <script>
            var egg = new Egg("up,up,down,down,left,right,left,right,b,a", function () {
                alert("Achievement Unlocked! Unlimited Spot Rate Lookups!");
                $('#egg').css('display', 'block');
                $('#caption').css('display', 'block');
                $('#health').show();
            }).listen();
            // Triggers the IP/Username stats to appear
            var egg = new Egg("alt,s,t", function () {
                //alert("Stats Enabled");
                $('#stats').css('display', 'block');
            }).listen();
        </script>
        <script type="text/javascript">
            /*Resets the variables and form when user clicks Reset button*/
            $(document).ready(function () {
                $('#reset').click(function () {
                    $form.find('input:text, input:password, input:file, select, textarea').val('');
                    $form.find('input:radio, input:checkbox')
                            .removeAttr('checked').removeAttr('selected');
                });
            });
        </script>
        <script>
            /* JQuery for swapping the origin and destination values on button click */
            function swapValues() {
                var tmp = document.getElementById("o").value;
                document.getElementById("o").value = document.getElementById("d").value;
                document.getElementById("d").value = tmp;
            }
        </script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
    </head>

    <body>
        <div id="message">
            <?php
            echo $message;
            if ($jedi) {
                echo $debug;
            };
            ?>
            <span id="stats"><?php echo $stats; ?></span>
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <p id="logo" class="navbar-brand"><img src="images/swift_logistics_logo_circle.png" alt="Swift Logistics logo"/>&nbsp;Logistics Optimized Rate Lookup | Spot Rate Tool</p>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="#n">Problems or Suggestions?</a></li>

                        <li>
                            <p class="navbar-btn">
                                <a href="mailto:allison_broughton@swifttrans.com?subject=Spot%20Rate%20Tool%20Issue&body=Please enter your comments here:<?php echo $support_body; ?>" target="_top" class="btn btn-info">E-Mail Support</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--[if gt IE 10]><!-->
        <div id="egg"><span id="caption">Unlimited Lookups!</span><img id="health" src="images/health_meter.png"/></div>
        <!--<![endif]-->
        <div class="container">
            <div class="row clearfix center">
                <div class="span6 offset3">
                    <form class="form form-horizontal" name="spot_rate" action="" method="POST" autocomplete="off">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="button" id="swap" onclick="swapValues()" class="btn btn-primary" value="Swap Cities">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Origin</label>
                            <div class="col-xs-4">
                                <input type="text" class="form-control form-control-inline" name="origin" id="o" placeholder="City, ST" value="<?php echo $origin; ?>" onkeyup="showResult(this.value)"/>
                                <div id="livesearch"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Destination</label>
                            <div class="col-xs-4">
                                <input type="text" class="form-control form-control-inline" name="destin" id="d" placeholder="City, ST" value="<?php echo $destination; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Pickup Date</label>
                            <div class="col-xs-4">
                                <input type="date" class="form-control form-control-inline" name="pickup_date" min="<?php echo $today; ?>" value="<?php if ($pickup_date === '') {echo $tomorrow;} else {echo date($pickup_date);}?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Weight (lbs)</label>
                            <div class="col-xs-4">
                                <input type="number" class="form-control form-control-inline" name="weight"  min="1000" max="45000" step="500" value="<?php echo $weight; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Trailer Type</label>
                            <div class="col-xs-4">
                                <select class="form-control form-control-inline" name="type">
                                    <option value="" disabled selected><?php echo $trailer_type; ?></option>
                                    <option value="V">DRY VAN</option>
                                    <option value="R">REEFER</option>
                                    <option value="F">FLATBED</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-success" name="lookup_rate" id="button"><span class="glyphicon glyphicon-usd"></span> Get Quote</button>
                            <button type="submit" class="btn btn-warning" name="reset" id="reset"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="spacer"></div>

            <?php
            if (isset($_POST['lookup_rate'])) {
                echo $form_results;
            }
            /*if (isset($_POST['validate'])) {
                echo $form2_results;
            }*/
            ?>

        </div>
        <div class="spacer"></div>
        <div class="container">
            <div class="row clearfix center">
                <h2>Previous Lookups</h2>           
                <table class ="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Date/Time</th>
                            <th>Trailer</th>
                            <th>Origin</th>
                            <th>Destination</th>
                            <th>Market Lane</th>
                            <th>Miles</th>
                            <th>Spot Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php echo $lookups_today; ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <footer class="footer container-fluid text-left">
            <p>&copy; Swift Logistics, LLC&nbsp;&nbsp;<span class="glyphicon glyphicon-signal"></span>&nbsp;Connection Status: <?php echo $conn_status;
            echo $cpu;
            ?></p>
            <!--Icons courtesy of http://glyphicons.com-->
        </footer>
    </body>
</html>