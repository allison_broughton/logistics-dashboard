<?php
/* Testing ground for login script and zip code mile lookup
 * Seems to work; need to make sure zip codes are included in e-mail templates
 * If we push this to the live page, will need Nikki to create new columns in the Spot_Rate_Log and Job_Log tables
 * and update insert queries to include the zip codes, if entered
 */
// Initialize session and login variable
session_start();
$_SESSION['login'] = "";
$login_error = "";
// Initialize error reporting
error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR); // Shows ONLY errors
//error_reporting(E_ALL ^ E_NOTICE); // shows ALL notices, warnings, errors
ini_set('display_errors', 1);

// Set up and test ldap connection
$link = ldap_connect('swift.com');
if (!$link) {
    $ldap_status = "Could not connect to server";
} else {
    $ldap_status = "LDAP Status: Connected to swift.com server";
}

if (isset($_POST['login_submit'])) {
    //Append supplied username to "@swift.com"
    $ad_user = (sanitize($_POST['login_username'])) . "@swift.com";
    $ad_pass = sanitize($_POST['login_password']);

    // Check for empty strings
    if (empty($ad_user) or empty($ad_pass)) {
        exit();
    }
    if ($link) {
        ldap_set_option($link, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap_bind = ldap_bind($link, $ad_user, $ad_pass);
        if ($ldap_bind) {
            $_SESSION['login'] = "true";
            //$login_status .= "SESSION['login'] = " . $_SESSION['login'];
        } else {
            //$login_status = "FAIL";
            $login_error = "<h3 class='alert alert-danger'><strong>Oh snap!</strong> Those are invalid credentials.</h3>";
        }
    }
}

// Write logout function and unset the SESSION login variable
if(isset($_POST['logout'])) {
    unset($_SESSION['login']);
}

// Set up and test db connection
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_Dev';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
if ($conn) {
    $conn_status = "Connected";
} else {
    die;
    $conn_status = "Could not connect";
}

// Capture IP, Server, and User info
$ip          = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$server_name = $_SERVER['SERVER_NAME'];
$username    = $_SERVER['AUTH_USER']; // get_current_user() returns the owner of current script, not the user php is currently running

/* If impersonation is turned on in php.ini (fastcgi.impersonate = 1) then IUSR (default IIS User) will be shown by default,
 * otherwise IIS AppPool\[your website app pool] -> Which in this case is currently moscc
 * I enabled the fastcgi.impersonate feature in php.ini file and edited the web config file so that <identity impersonate="true" /> 
 */

// Jedi status allows user to see error details/debug/query info
$jedi         = false;
$debug        = "";
$pricing_team = false;
// Set stylesheet and jedi status based on username
if ($username === "SWIFT\stral" || $username === "SWIFT\labin") {
    $jedi       = true;
    $stylesheet = "css/violet-theme.css";
} else if ($username === NULL && $server_name === "localhost") {
    // When the page is accessed on the server via localhost:8000
    $username   = "localhost\DEV";
    $jedi       = true;
    $stylesheet = "css/spot-rate-style.css";
} else {
    $stylesheet = "css/spot-rate-style.css";
}
// Set form results permission based on whether user is part of pricing team
if ($username === "SWIFT\stral" ||
        $username === "localhost\DEV" ||
        $username === "SWIFT\everd" ||
        $username === "SWIFT\marro" ||
        $username === "SWIFT\casia" ||
        $username === "SWIFT\skinb" ||
        $username === "SWIFT\chamr") {
    $pricing_team = true;
}
$stats    = "IP/Host: " . $ip . "</br>Server: " . $server_name . "</br>Username: " . $username;
// Set tomorrow's date to be used as default value for the Pickup Date
$today    = date('Y-m-d');
$tomorrow = (new DateTime('tomorrow'))->format('Y-m-d');

// Initialize variable to be used in support e-mail body
// Blank unless the user has submitted a spot rate request,
// in which case, the e-mail will display user input values.
$support_body = "";

// Self explanatory - cleans spaces from string
function cleanSpaces($string) {
    $no_spaces   = preg_replace('/\s*,\s*/', ',', $string);
    $clean_space = str_replace(',', ', ', $no_spaces);
    return $clean_space;
}

// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}

// Declare variables in form input fields
$origin      = $o_zip = $destination = $d_zip = $pickup_date = $weight      = $type        = "";

// Validate user entered cities
function fuzzySearch($cityState) {
    global $conn;
    $string  = "SELECT TOP 1 City, State FROM UsMarkets WHERE Cityname LIKE '$cityState%' AND City IS NOT NULL";
    $stmt    = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $results = "";
    while ($row     = odbc_fetch_array($stmt)) {
        $results .= $row['City'] . ", " . $row['State'];
    }
    if (empty($results)) {
        $results = "No cities found. Please check your spelling.";
    }
    return $results;
}

// Look up Origin and Destination Market ID
function getMarket($cityState) {
    global $conn;
    $string = "SELECT TOP 1 DATMarketAreaID FROM USMarkets WHERE Cityname LIKE '%$cityState%' AND City IS NOT NULL";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $market = "";
    while ($row    = odbc_fetch_array($stmt)) {
        $market = $row['DATMarketAreaID'];
    }
    if (empty($market)) {
        $market = "No market";
    }
    return $market;
}

// Create COM object to connect to PC*Miler API functions and calculate distance between origin and destination
function getMileage($origin, $destin) {
    try {
        $pcms = new COM("PCMServer.PCMServer");
        $dist = $pcms->CalcDistance("$origin", "$destin");
    } catch (com_exception $e) {
        /* Intentionally leaving empty catch block
         * Already validate city names when selecting from Logistics Dev Warehouse
         * Unnecessary to display this same error twice
         * Can uncomment code below if you really need more details on the error
         * Commented out as users do not need to see the full error details
         */

        /* echo $e . "\n";
          if (strpos($e, 'Invalid place name') !== false) {
          echo "Invalid place name. Please check your spelling.";
          } */
    }
    /* Returned distance is a INT that needs to be divided by 10
     * (or the number of decimal places specified in the 
     * PCMSServe.ini file located in C:\Windows
     */
    $properDist = ($dist / 10);
    return $properDist;
}

// Create same distance lookup with Pc*Miler API except search by zip code
function getMileageByZip($o_zip,$d_zip) {
    try {
        $pcms = new COM("PCMServer.PCMServer");
        $dist = $pcms->CalcDistance("$o_zip", "$d_zip");
    } catch (com_exception $e) {
    }
    $properDist = ($dist / 10);
    return $properDist;
}

// Query Spot_DM table to retrieve market rate for linehaul
function getLinehaul($oMarket, $dMarket, $type) {
    global $conn;
    $market       = $oMarket . "-" . $dMarket;
    $trailer_type = "";
    if ($type == "V") {
        $trailer_type = "Van_Rpm";
    } else if ($type == "R") {
        $trailer_type = "Ref_Rpm";
    } else {
        $trailer_type = "Flat_Rpm";
    }
    //echo "The trailer type is: " . $trailer_type . "<br/>";
    $query_dm = "SELECT $trailer_type FROM Spot_Gl_Model_Output WHERE Kma_Kma_Lane = '$market'";
    //echo $query_dm . "<br/>";
    $get_dm   = odbc_prepare($conn, $query_dm);
    odbc_execute($get_dm);
    while ($row      = odbc_fetch_array($get_dm)) {
        $linehaul = $row["$trailer_type"];
        //echo "The returned linehaul rate is: " . $linehaul;
        return $linehaul;
    }
}

// Query MCLD_Prod_Fuel_Price to get the most current fsc price
function getFuel() {
    global $conn;
    $query = "SELECT TOP 1 average_price FROM MCLD_Prod_Fuel_Price ORDER BY price_date DESC";
    $get   = odbc_prepare($conn, $query);
    odbc_execute($get);
    while ($row   = odbc_fetch_array($get)) {
        $avg = $row['average_price'];
        $fsc = number_format((($avg - 1.46) / 5), 2);
        return $fsc;
    }
}

// Must assign session variable or the reset button will clear it and log the user out
if(isset($_POST['reset'])) {
    session_start();
    $_SESSION['login'] = true;
}

// When user clicks Get Quote, do the following
if (isset($_POST["lookup_rate"])) {
    session_start();
    $_SESSION['login'] = true;
    // Track CPU used during script execution
    $rustart      = getrusage();
    // Get time when script starts
    $time_start   = date('Y-m-d H:i:s');
    global $conn;
    // Sanitize user input and store in variables
    $origin       = sanitize($_POST["origin"]);
    $o_zip        = sanitize($_POST["o_zip"]);
    $destination  = sanitize($_POST["destin"]);
    $d_zip        = sanitize($_POST["d_zip"]);
    $pickup_date  = sanitize($_POST["pickup_date"]);
    $weight       = sanitize($_POST["weight"]);
    $type         = sanitize($_POST["type"]);
    $zip_search   = sanitize($_POST["zip_search"]);
    // Make trailer type user friendly when displaying results
    $trailer_type = "";
    if ($type == "V") {
        $trailer_type = "DRY VAN";
    } else if ($type == "R") {
        $trailer_type = "REEFER";
    } else {
        $trailer_type = "FLATBED";
    }
    // Set body contents for support e-mail when user clicks E-Mail Support AFTER submitting the spot rate form
    $support_body = "%0A%0A%0AOrigin:%20$origin%0ADestination:%20$destination%0APickup%20Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";

    // Make sure no fields were left empty
    if (!empty($origin && $destination && $pickup_date && $weight && $type)) {
        
        // Initialize error count and record count variables
        $has_errors = 0;
        $record_count = 1;
        
        // Validate cities first
        $origin_info = fuzzySearch($origin);
        $destin_info = fuzzySearch($destination);
        
        // If one or both cities were not found, kill the script and alert user
        if ((strpos($origin_info, 'No cities') !== false) || (strpos($destin_info, 'No cities') !== false)) {
            $message = "No cities found. Please check the spelling and state abbreviation(s).";
        } else {
            // Calculate miles
            // If zip-search was clicked, use getMileageByZip, else do a regular getMileage lookup
            if($zip_search) {
                $miles     = getMileageByZip($o_zip, $d_zip);
            } else {
                $miles     = getMileage($origin, $destination);
            }
            // Look up market lane id for cities
            $oMarket = getMarket($origin_info);
            $dMarket = getMarket($destin_info);
            // Next check to see if any markets exist for the chosen orign-destination pair
            if ((strpos($oMarket, 'No market') !== false) || (strpos($dMarket, 'No market') !== false)) {
                // Set error count and record count to be inserted into Job Log
                $has_errors = 1;
                $record_count = 0;
                $time_end = $time_start;
                $total_time = "00:00 (mm:ss)";
                $market_lane = "NO LANE FOUND";
                
                // Fill out e-mail to be sent to Pricing team if no markets are found
                $to      = "logisticspricing@swifttrans.com";
                $subject = "Spot%20Rate%20Tool-No%20Market%20Found%20For%20Lane";
                $body    = "Requesting%20spot%20rate%20for:%0ALane:%20$origin%20to%20$destination%0APickup Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";
                $message = "No markets found. Please contact <a href='mailto:$to?subject=$subject&body=$body' class='btn btn-info' target='_top'>Logistics Pricing.</a>";
            } else {
                // Concatenate origin and destination to create the market lane
                $market_lane   = $oMarket . "-" . $dMarket;
                // Look up van rate for the market
                $linehaul_rate = getLineHaul($oMarket, $dMarket, $type);
                // Make it pretty for page output
                $linehaul      = number_format($linehaul_rate, 2);
                // If linehaul rate returns NULL, do not continue. Alert user to contact Pricing team
                if ($linehaul_rate == NULL) {
                    // Set error count and record count to be inserted into Job Log
                    $has_errors = 1;
                    $record_count = 0;
                    $time_end = $time_start;
                    $total_time = "00:00 (mm:ss)";
                    // Fill out e-mail to be sent to Pricing team if no rates are found
                    $to      = "logisticspricing@swifttrans.com";
                    $subject = "Spot%20Rate%20Tool-No%20Linehaul%20Rate%20Found";
                    $body    = "Requesting%20spot%20rate%20for:%0ALane:%20$origin%20to%20$destination%0APickup Date:%20$pickup_date%0AWeight:%20$weight%0ATrailer%20Type:%20$trailer_type";
                    $message = "No linehaul rate found for chosen market lane. Please contact <a href='mailto:$to?subject=$subject&body=$body' class='btn btn-info' target='_top'>Logistics Pricing.</a>";
                } else {
                    $fsc       = getFuel();
                    //$miles     = getMileage($origin, $destination);
                    $margin    = 15;
                    $spot_rate = round(((($linehaul + $fsc) * $miles) * 1.15), 0, PHP_ROUND_HALF_UP);
                    // If user is on Pricing Team, display calculation details
                    if ($pricing_team) {
                        $form_results = "<div id='spot_rate' class='alert alert-success'>
                            <strong>Recommended Rate: </strong>$" . number_format($spot_rate, 2) . "<br/>
                            <strong>How we calculated this rate:</strong><br/>
                            ((<strong>$linehaul</strong> (Linehaul) + <strong>$fsc</strong> (Fuel Surcharge)) * <strong>$miles</strong> (Miles)) * <strong>1.15</strong><br/>
                         </div>
                         <div id='spot_rate' class='alert alert-info'>
                            <strong>Your results:</strong><br/>
                            <strong>Origin: </strong>" . $origin_info . " " . $o_zip . "<br/>
                            <strong>Destination: </strong>" . $destin_info . " " . $d_zip . "<br/>
                            <strong>Pickup Date: </strong>" . $pickup_date . "<br/>
                            <strong>Weight: </strong>" . number_format($weight) . " lbs<br/>
                            <strong>Trailer Type: </strong>" . $trailer_type . "<br/>
                            <strong>Market Lane: </strong>" . $market_lane . "
                         </div>";
                    } else {
                        // For all other users, simply display the Spot Rate Quote
                        $form_results = "<div id='spot_rate' class='alert alert-success'>
                            <strong>Recommended Rate: </strong>$" . number_format($spot_rate, 2) . "<br/>
                            </div>
                            <div id='spot_rate' class='alert alert-info'>
                            <strong>Your results:</strong><br/>
                            <strong>Origin: </strong>" . $origin_info . " " . $o_zip . "<br/>
                            <strong>Destination: </strong>" . $destin_info . " " . $d_zip . "<br/>
                            <strong>Pickup Date: </strong>" . $pickup_date . "<br/>
                            <strong>Weight: </strong>" . number_format($weight) . " lbs<br/>
                            <strong>Trailer Type: </strong>" . $trailer_type . "<br/>
                            <strong>Market Lane: </strong>" . $market_lane . "
                         </div>";
                    }

                    // Calculate CPU usage and output runtime
                    function rutime($ru, $rus, $index) {
                        return ($ru["ru_$index.tv_sec"] * 1000 + intval($ru["ru_$index.tv_usec"] / 1000)) - ($rus["ru_$index.tv_sec"] * 1000 + intval($rus["ru_$index.tv_usec"] / 1000));
                    }

                    $ru              = getrusage();
                    $processing_time = rutime($ru, $rustart, "utime");
                    // Get time when script ends
                    $time_end        = date('Y-m-d H:i:s');
                    // Calculate total seconds the query took - this is NOT cpu time - this is real time
                    $total_time      = date_create($time_end)->diff(date_create($time_start))->format('%I:%S (mm:ss)');
                    ;
                    // Details on cpu usage will print in footer on form submit
                    $cpu             = "&nbsp;&nbsp;<span class='glyphicon glyphicon-dashboard'></span>&nbsp;";
                    $cpu             .= "This process used " . rutime($ru, $rustart, "utime") .
                            " ms for its computations.\n It spent " . rutime($ru, $rustart, "stime") .
                            " ms in system calls.\n";
                }
            }
            // Insert Job details to Job_Log
            $job_log_insert = "INSERT INTO Job_Log (
                        Job_Name,
                        Has_Errors,
                        User_Name,
                        Record_Count,
                        Start_Date_Time,
                        End_Date_Time,
                        Processing_Time
                    ) 
                    VALUES (
                        'Spot_Rate_Tool',
                        '$has_errors',
                        '$username',
                        '$record_count',
                        '$time_start',
                        '$time_end',
                        '$total_time'
                        )";
            $job_log_stmt   = odbc_prepare($conn, $job_log_insert);
            odbc_execute($job_log_stmt);
            // Select the JobID generated in the db to use for the Spot_Rate_Log
            function getJobID() {
                global $conn;
                global $username;
                $query_job_id = "SELECT TOP 1 JobID FROM Job_Log WHERE Job_Name = 'Spot_Rate_Tool' AND User_Name = '$username' ORDER BY JobID DESC";
                $get_job_id = odbc_prepare($conn, $query_job_id);
                odbc_execute($get_job_id);
                while ($row = odbc_fetch_array($get_job_id)) {
                    $job_id = $row['JobID'];
                    return $job_id;
                }
            }
            $job_id = getJobID();
            // Insert input and output data to Spot_Rate_Log for auditing purposes
            $insert = "INSERT INTO Spot_Rate_Log (
                        JobID,
                        User_Name,
                        IP,
                        User_Computer_Name,
                        Input_Trailer_Type,
                        Input_Origin,
                        Input_Destin,
                        Input_Weight,
                        Input_Pickup_Date,
                        Kma_Kma_Lane,
                        Output_Miles,
                        Output_Spot_Rate,
                        Output_Margin_Percent,
                        Output_Linehaul,
                        Output_FSC,
                        Query_Processing_Time            
                    )
                    VALUES (
                        '$job_id',
                        '$username',
                        '$ip',
                        '$server_name',
                        '$type',
                        '$origin',
                        '$destination',
                        '$weight',
                        '$pickup_date',
                        '$market_lane',
                        '$miles',
                        '$spot_rate',
                        '0.$margin',
                        '$linehaul',
                        '$fsc',
                        '00:$processing_time (ss:ms)'
                    )";
            $stmt   = odbc_prepare($conn, $insert);
            odbc_execute($stmt);
            
            // Display insert queries if user is a jedi
            if ($jedi) {
                $debug = $job_log_insert;
                $debug .= "<br/>" . $insert;
            }
        }
    } else {
        $form_results = "<div id='spot_rate' class='alert alert-danger'>
                        <span class='glyphicon glyphicon-warning-sign'></span>
                        <strong> Please ensure you have filled out every field. </strong>
                        </div>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LORL</title>
        <link rel="icon" type="image/ico" href="images/swift_logistics_logo_circle.ico"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $stylesheet; ?>"><!--Personalized stylesheet-->
        <script type="text/javascript" src="js/jquery-3.1.1.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="js/egg.js"></script>
        <script>
            var egg = new Egg("up,up,down,down,left,right,left,right,b,a", function () {
                alert("Achievement Unlocked! Unlimited Spot Rate Lookups!");
                $('#egg').css('display', 'block');
                $('#caption').css('display', 'block');
                $('#health').show();
            }).listen();
            // Triggers the IP/Username stats to appear
            var egg = new Egg("alt,s,t", function () {
                //alert("Stats Enabled");
                $('#stats').css('display', 'block');
            }).listen();
        </script>
        <script>
            $(document).ready(function () {
                // Hide zip code inputs initially
                $('#o-zip').hide();
                $('#d-zip').hide();
                // If "Search by ZIP" button is clicked:
                $('#zip-search-btn').click(function () {
                    // Set hidden input to "Yes" to trigger getMileageByZip function
                    $('input[id="zip-search"]').val(true);
                    // Display zip code inputs
                    $('#o-zip').show();
                    $('#d-zip').show();
                });
            });
        </script>
        <script type="text/javascript">
            /*Resets the variables and form when user clicks Reset button*/
            $(document).ready(function () {
                $('#reset').click(function () {
                    $form.find('input:text, input:password, input:file, select, textarea').val('');
                    $form.find('input:radio, input:checkbox')
                            .removeAttr('checked').removeAttr('selected');
                });
            });
        </script>
        <script>
            /* JQuery for swapping the origin and destination values on button click */
            function swapValues() {
                var tmp = document.getElementById("o").value;
                document.getElementById("o").value = document.getElementById("d").value;
                document.getElementById("d").value = tmp;
            }
        </script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
    </head>
    <body>
        <div id="message">
            <?php
            echo $message;
            if ($jedi) {
                echo $debug;
            };
            ?>
            <span id="stats"><?php echo $stats; ?></span>
        </div>
       <?php 
       // If login session variable is empty, show the login form
       if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {?>
            <div class="container">

                <form class="form-signin" action="" method="POST" autocomplete="off">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <?php echo $login_error;?>
                    <input type="text" name="login_username" class="form-control" placeholder="Username" required autofocus>
                    <br/>
                    <input type="password" name="login_password" class="form-control" placeholder="Password" required>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me"> Remember me
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit" name="login_submit">Sign in</button>
                </form>

            </div>

       <?php } else {?>
        
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <p id="logo" class="navbar-brand"><img src="images/swift_logistics_logo_circle.png" alt="Swift Logistics logo"/>&nbsp;Logistics Optimized Rate Lookup</p>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a href="#n">Problems or Suggestions?</a></li>
                        <li>
                            <p class="navbar-btn">
                                <a href="mailto:allison_broughton@swifttrans.com?subject=Spot%20Rate%20Tool%20Issue&body=Please enter your comments here:<?php echo $support_body; ?>" target="_top" class="btn btn-info">E-Mail Support</a>
                            </p>
                        </li>
                        <li> &nbsp; </li>
                        <li>
                            <p class="navbar-btn">
                                <a href="" class="btn btn-warning" name="logout">Logout</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--[if gt IE 10]><!-->
        <div id="egg"><span id="caption">Unlimited Lookups!</span><img id="health" src="images/health_meter.png"/></div>
        <!--<![endif]-->
        <div class="container">
            <div class="row clearfix center">
                <div class="span6 offset3">
                    <form class="form form-horizontal" name="spot_rate" action="" method="POST" autocomplete="off">
                        
                        <div class="form-group">
                            <div class="col-xs-4">
                                <input type="hidden" class="form-control form-control-inline" name="zip_search" id="zip-search" value="" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input type="button" id="swap" onclick="swapValues()" class="btn btn-primary" value="Swap Cities">
                                <input type="button" id="zip-search-btn" class="btn btn-info" value="Search by ZIP">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Origin</label>
                            <div class="col-xs-4">
                                <input type="text" class="form-control form-control-inline" name="origin" id="o" placeholder="City, ST" value="<?php echo $origin; ?>"/>
                                <input type="text" class="form-control form-control-inline" name="o_zip" id="o-zip" placeholder="ZIP Code" value="<?php echo $o_zip; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Destination</label>
                            <div class="col-xs-4">
                                <input type="text" class="form-control form-control-inline" name="destin" id="d" placeholder="City, ST" value="<?php echo $destination; ?>"/>
                                <input type="text" class="form-control form-control-inline" name="d_zip" id="d-zip" placeholder="ZIP Code" value="<?php echo $d_zip; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Pickup Date</label>
                            <div class="col-xs-4">
                                <input type="date" class="form-control form-control-inline" name="pickup_date" min="<?php echo $today; ?>" value="<?php if ($pickup_date === '') {echo $tomorrow;} else {echo date($pickup_date);}?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Weight (lbs)</label>
                            <div class="col-xs-4">
                                <input type="number" class="form-control form-control-inline" name="weight"  min="1000" max="45000" step="500" value="<?php echo $weight; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label lb-size col-xs-2 col-xs-offset-2">Trailer Type</label>
                            <div class="col-xs-4">
                                <select class="form-control form-control-inline" name="type">
                                    <option value="" disabled selected><?php echo $trailer_type; ?></option>
                                    <option value="V">DRY VAN</option>
                                    <option value="R">REEFER</option>
                                    <option value="F">FLATBED</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-success" name="lookup_rate" id="button"><span class="glyphicon glyphicon-usd"></span> Get Quote</button>
                            <button type="submit" class="btn btn-warning" name="reset" id="reset"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
                        </div>

                    </form>
                </div>
            </div>
            <div class="spacer"></div>

            <?php
            if (isset($_POST['lookup_rate'])) {
                echo $form_results;
            }
            ?>

        </div>
        <footer class="footer container-fluid text-left">
            <p>&copy; Swift Logistics, LLC&nbsp;&nbsp;<span class="glyphicon glyphicon-signal"></span>&nbsp;Connection Status: <?php echo $conn_status;
            echo $cpu;
            ?></p>
            <!--Icons courtesy of http://glyphicons.com-->
        </footer>
        <?php } ?>
    </body>
</html>