<?php

/*
 * PHP functions that can be called from index.php
 */

// Remove "SWIFT\" from usernames
function stripUsername($username) {
    if ($username === "localhost\DEV") {
        $user_id = "localhost\DEV";
    } else {
        $user_id = substr(stripslashes($username), 5);
    }
    return $user_id;
}

// Self explanatory - cleans spaces from string
function cleanSpaces($string) {
    $no_spaces   = preg_replace('/\s*,\s*/', ',', $string);
    $clean_space = str_replace(',', ', ', $no_spaces);
    return $clean_space;
}

// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}

// Validate user entered cities
function fuzzySearch($cityState) {
    global $conn;
    $string  = "SELECT TOP 1 City, State FROM UsMarkets WHERE Cityname LIKE '$cityState%' AND City IS NOT NULL";
    $stmt    = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $results = "";
    while ($row     = odbc_fetch_array($stmt)) {
        $results .= $row['City'] . ", " . $row['State'];
    }
    if (empty($results)) {
        $results = "No cities found. Please check your spelling.";
    }
    return $results;
}

// Look up Origin and Destination Market ID
function getMarket($cityState) {
    global $conn;
    $string = "SELECT TOP 1 DATMarketAreaID FROM USMarkets WHERE Cityname LIKE '%$cityState%' AND City IS NOT NULL";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $market = "";
    while ($row    = odbc_fetch_array($stmt)) {
        $market = $row['DATMarketAreaID'];
    }
    if (empty($market)) {
        $market = "No market";
    }
    return $market;
}

// Create COM object to connect to PC*Miler API functions and calculate distance between origin and destination
function getMileage($origin, $destin) {
    try {
        $pcms = new COM("PCMServer.PCMServer");
        $dist = $pcms->CalcDistance("$origin", "$destin");
    } catch (com_exception $e) {
        /* Intentionally leaving empty catch block
         * Already validate city names when selecting from Logistics Dev Warehouse
         * Unnecessary to display this same error twice
         * Can uncomment code below if you really need more details on the error
         * Commented out as users do not need to see the full error details
         */

        /* echo $e . "\n";
          if (strpos($e, 'Invalid place name') !== false) {
          echo "Invalid place name. Please check your spelling.";
          } */
    }
    /* Returned distance is a INT that needs to be divided by 10
     * (or the number of decimal places specified in the 
     * PCMSServe.ini file located in C:\Windows
     */
    $properDist = ($dist / 10);
    return $properDist;
}

// See if there is an active modifier for the lane and pickup date
function getModifier($market, $type, $pickup_date) {
    global $conn;
    $select_mod = "SELECT Value, UOM
            FROM Spot_Modifier WHERE KMA_KMA_Lane = '$market' AND Equipment_Type = '$type' AND '$pickup_date' BETWEEN Effective_Date AND Expiration_Date";
    $query_mod  = odbc_prepare($conn, $select_mod);
    odbc_execute($query_mod);
    $row_mod    = odbc_fetch_array($query_mod);
    $mod        = $row_mod['Value'];
    $uom        = $row_mod['UOM'];
    $mod_array  = ['value'=> $mod,
                   'uom'=> $uom];
    return $mod_array;
}

// Query Spot_DM table to retrieve market rate for linehaul
function getLinehaul($oMarket, $dMarket, $type) {
    global $conn;
    $market       = $oMarket . "-" . $dMarket;
    $trailer_type = "";
    if ($type == "V") {
        $trailer_type = "Van_Rpm";
    } else if ($type == "R") {
        $trailer_type = "Ref_Rpm";
    } else {
        $trailer_type = "Flat_Rpm";
    }
    $query_dm = "SELECT $trailer_type FROM Spot_Gl_Model_Output WHERE Kma_Kma_Lane = '$market'";
    $get_dm   = odbc_prepare($conn, $query_dm);
    odbc_execute($get_dm);
    while ($row      = odbc_fetch_array($get_dm)) {
        $linehaul = $row["$trailer_type"];
        return $linehaul;
    }
}

// Query MCLD_Prod_Fuel_Price to get the most current fsc price
function getFuel() {
    global $conn;
    $query = "SELECT TOP 1 average_price FROM MCLD_Prod_Fuel_Price ORDER BY price_date DESC";
    $get   = odbc_prepare($conn, $query);
    odbc_execute($get);
    while ($row   = odbc_fetch_array($get)) {
        $avg = $row['average_price'];
        $fsc = number_format((($avg - 1.46) / 5), 2);
        return $fsc;
    }
}

// Select the JobID generated in the db to use for the Spot_Rate_Log
function getJobID() {
    global $conn;
    global $user_id;
    $query_job_id = "SELECT TOP 1 JobID FROM Job_Log WHERE Job_Name = 'Spot_Rate_Tool' AND User_Name = '$user_id' ORDER BY JobID DESC";
    $get_job_id   = odbc_prepare($conn, $query_job_id);
    odbc_execute($get_job_id);
    while ($row          = odbc_fetch_array($get_job_id)) {
        $job_id = $row['JobID'];
        return $job_id;
    }
}

function getTodaysLookups($limit) {
    global $conn;
    $today_datetime = date('Y-m-d') . " 00:00:00";
    $string = "SELECT TOP 30
       JobLog.Has_Errors
      ,JobLog.User_Name
      ,JobLog.Start_Date_Time
      ,JobLog.End_Date_Time
      ,JobLog.Processing_Time
      ,Spot.IP
      ,Spot.Input_Trailer_Type
      ,Spot.Input_Origin
      ,Spot.Input_Destin
      ,Spot.Kma_Kma_Lane
      ,Spot.Output_Miles
      ,Spot.Output_Spot_Rate
      ,Spot.Output_Linehaul
      ,Spot.Output_FSC
  FROM Job_Log JobLog
  LEFT JOIN Spot_Rate_Log Spot ON JobLog.JobID = Spot.JobID
  WHERE Job_Name = 'Spot_Rate_Tool' AND Start_Date_Time >= '$today_datetime' $limit
  ORDER BY JobLog.JobID DESC";
    $get_lookups = odbc_prepare($conn, $string);
    odbc_execute($get_lookups);
    while ($row = odbc_fetch_array($get_lookups)) {
        if($row['Has_Errors'] === '1') {
            $row_color = "style='color: #ff2247'";
        } else {
            $row_color = "style='color: #00cc66'";
        }
        $lookups_today .= "<tr $row_color>
                <td>" . $row['User_Name'] . "</td>
                <td>" . $row['Start_Date_Time'] . "</td>
                <td>" . $row['Input_Trailer_Type'] . "</td>
                <td>" . $row['Input_Origin'] . "</td>
                <td>" . $row['Input_Destin'] . "</td>
                <td>" . $row['Kma_Kma_Lane'] . "</td>
                <td>" . number_format($row['Output_Miles'],2) . "</td>
                <td>$" . number_format($row['Output_Spot_Rate'],2) . "</td>
            </tr>";
    } return $lookups_today;
}