<?php
session_start();
require 'include_functions.php';

// Select hotloads that have been adjusted
function getHotloads() {
    global $conn;
    $string = "SELECT Order_ID,
        Movement_ID,
        Dispatcher_User_ID,
        Hotload_Valid,
        Hotload_Added_By,
        Outside_Broker_Area,
        Origin_State,
        Revenue_Code_ID,
        Equipment_Type_ID,
        Movement_Margin,
        reassign_user_id,
        reassign_user_id_amount,
        reassign_user2_id,
        reassign_user2_id_amount,
        reassign_user3_id,
        reassign_user3_id_amount,
        reassign_user4_id,
        reassign_user4_id_amount
        FROM BC_Hotload_Preview WHERE Moved_to_Approved = 'N'";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getHotloads();
header('Content-Type: application/json');
echo json_encode($data);