<?php
session_start();
require 'include_functions.php';
require 'include_navbar.php';
$page_id = 'Negative_Margin';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
///////////////////// Initialize Variables ////////////////////////////////////////////
$title       = "Negative Margin Override";
$page_access = getPageAccess($user_id, $page_id);
//////////////////// Get User Options /////////////////////////////////////////////////
$select_users = "SELECT id, name FROM prod_users WHERE is_active = 'Y' AND id IN ('vaugd','everd','maeski') ORDER BY name ASC";
$get_users    = odbc_prepare($conn, $select_users);
odbc_execute($get_users);
$options = "";
while($row = odbc_fetch_array($get_users)) {
    $options .= "<option value='".$row['id']."'> ".$row['name']." </option>";
}
//////////////////// Form Submission //////////////////////////////////////////////////
if (isset($_POST['submit'])) {
    $order_id = sanitize($_POST['order_id']);
    $reason   = sanitize($_POST['reason']);
    $approved_by = sanitize($_POST['approval']);
    // Verify order id exists in BC_PayAll_temp and margin is negative
    $select   = "SELECT order_id,movement_margin,dispatcher_user_id,Processing_Status FROM BC_PayAll_temp WHERE order_id = '$order_id'";
    $query    = odbc_prepare($conn, $select);
    odbc_execute($query);
    $row      = odbc_fetch_array($query);
    $margin   = number_format($row['movement_margin'],2);
    $dispatcher = $row['dispatcher_user_id'];
    // Verify order id does NOT exist in BC_NegMargin_Overrides before inserting
    $verify_select = "SELECT Order_ID FROM BC_NegMargin_Overrides WHERE Order_ID = '$order_id'";
    $verify_query = odbc_prepare($conn, $verify_select);
    odbc_execute($verify_query);
    $row_2 = odbc_fetch_array($verify_query);
    $order_exists = $row_2['Order_ID'];
    if(!empty($order_exists)) {
        $order_exists = true;
    } else {
        $order_exists = false;
    }
    if($order_exists) { 
        $result = "Order already exists in Negative Margin table!";
    } else {
        if($margin < 0 && $row['Processing_Status'] != 'Paid') {
            $insert = "INSERT INTO BC_NegMargin_Overrides
                (Order_ID,
                Dispatcher_User,
                Entered_By_User,
                Entered_Date,
                Approved_By_User,
                Override_Reason,
                Is_Active) VALUES (
                '$order_id',
                '$dispatcher',
                '$current_user',
                '$today_datetime',
                '$approved_by',
                '$reason',
                'Y'
                )";
            $prepared_insert = odbc_prepare($conn, $insert);
            set_error_handler(function($errno, $errstr, $errfile, $errline ) {
                throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
            });
            try {
                odbc_execute($prepared_insert);
            } catch (Exception $exc) {
                $insert_result = odbc_errormsg($conn);//$exc->getTraceAsString();
            }
        } else {
            $error_reason = "";
            if($margin >= 0) {
                $error_reason .= "Order does not have negative margin.<br/>";
            }
            if($row['Processing_Status'] === 'Paid') {
                $error_reason .= "Order has already been paid.";
            }
            $error = "<div class='alert alert-danger'><strong>Override denied for the following reason(s):</strong><br/>$error_reason  You cannot override.</div>";
        }
        $result = "Successfully inserted order.";
    }
}

?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Data Tables -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if($page_access) {?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Negative Margin Override<br/>
                                <small>Enter order numbers with negative margin to be excluded from margin.</small>
                            </h1><?php echo $dev_server;?>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4">
                            <form method="POST" action="" class="form-horizontal">
                                <div class="form-group">
                                    <label for="order" class="control-label col-xs-3">Order ID: </label>
                                    <div class="col-xs-9">
                                        <input type="number" name="order_id" id="order" required="true" autofocus/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="reason" class="control-label col-xs-3">Reason: </label>
                                    <div class="col-xs-9">
                                        <textarea name="reason" id="reason" rows="2" cols="25" maxlength="255" required="true"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="approval" class="control-label col-xs-3">Approved By: </label>
                                    <div class="col-xs-9">
                                        <select name="approval" id="approval" required="true">
                                            <option value="">Please Choose a User</option>
                                            <?php echo $options;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-3 col-xs-9">
                                        <button type="submit" name="submit" class="btn btn-primary col-lg-offset-2">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <?php if(isset($_POST['submit'])) {
                                echo "<div class='alert alert-info'>
                                        <strong>Order ID: </strong>$order_id <br/>
                                        <strong>Reason: </strong>$reason <br/>
                                        <strong>Approved By: </strong>$approved_by <br/>
                                        <strong>Margin: </strong>$margin <br/>
                                        <strong>Result: </strong>$result
                                      </div>";
                                if(isset($error)) {
                                    echo $error;
                                }
                                if(isset($insert_result)) {
                                    echo "<div class='alert alert-warning'>
                                            $insert_result
                                          </div>";
                                }
                            } ?>
                        </div>
                    </div><!-- /.row -->
                    <?php } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please contact a manager to override an order.</div>";
                    }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    </body>
</html>
