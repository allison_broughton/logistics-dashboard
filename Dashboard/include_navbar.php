<?php
session_start();
//////////////////////////////////////////////// IP/SERVER/USER INFO ///////////////////////////////////////////////
// THIS ONLY WORKS IF IMPERSONATION IS ENABLED IN PHP.INI (fastcgi.impersonate = 1)
// WEB CONFIG FILE MUST BE CONFIGURED (<identity impersonate="true" />)
// IIS WINDOWS AUTHENTICATION IS ENABLED AND ANONYMOUS AUTHENTICATION IS DISABLED
$ip          = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$server_name = $_SERVER['SERVER_NAME'];
if (isset($_SERVER['AUTH_USER'])) {
    $username = $_SERVER['AUTH_USER'];
} else {
    $username = "localhost\DEV";
}
if ($_SERVER['HTTP_HOST'] === 'devinside.swiftlogistics.com' || $_SERVER['HTTP_HOST'] === 'localhost:82') {
    $dev_indicator = '| DEV';
} else {
    $dev_indicator = '';
}

// Strip SWIFT\ from usernames
function stripSWIFT($username) {
    if ($username === "localhost\DEV") {
        $user_id = "localhost\DEV";
    } else {
        $user_id = substr(stripslashes($username), 5);
    }
    return $user_id;
}

//////////////////////////////////////////////// ODBC Connection /////////////////////////////////////////////////
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
//////////////////////////////////////////////// USER ICON //////////////////////////////////////////////////////
// Get user icon from Web_Page_Roles to display next to their username in the menu
$user_id_nav       = stripSWIFT($username);
$sql               = "SELECT User_Icon AS icon FROM Web_Page_Roles WHERE User_ID = '$user_id_nav'";
$query             = odbc_prepare($conn, $sql);
odbc_execute($query);
$row               = odbc_fetch_array($query);
$icon              = $row['icon'];
// Check if icon is set
if (!empty($row['icon'])) {
    $icon = $row['icon'];
} elseif ($user_id_nav === 'localhost\DEV') {
    $icon = 'fa-key';
} else {
    $icon = 'fa-user-circle';
}
//////////////////////////////////////////////// CHECK USER ACCESS FOR MENU //////////////////////////////////////
// Get user role to establish which menu links to display
function getRole($user_id_nav, $page_id) {
    if ($user_id_nav === 'localhost\DEV') {
        $access = true;
    } else {
        global $conn;
        $sql   = "SELECT User_Role_Name FROM Web_Page_Roles WHERE User_ID = '$user_id_nav' AND Page_ID = '$page_id'";
        $query = odbc_prepare($conn, $sql);
        odbc_execute($query);
        while ($row   = odbc_fetch_array($query)) {
            $role = $row['User_Role_Name'];
        }
        if ($role === NULL) {
            $access = false;
        } else {
            $access = true;
        }
    }
    return $access;
}
//////////////////////////////////////////////// SET MENU LINK PERMISSIONS //////////////////////////////////////
$brokers_access  = getRole($user_id_nav, 'BC_Broker');
$managers_access = getRole($user_id_nav, 'Broker_Activity_Tracker');
$leaders_access  = getRole($user_id_nav, 'Negative_Margin');
$pricing_access  = getRole($user_id_nav, 'Spot_Rate_Modifier');
$tools_access    = getRole($user_id_nav, 'Transit_Calculator');
$warren_access   = getRole($user_id_nav, 'Routing_Guide');
$jedi_access     = getRole($user_id_nav, 'Activity_Tracker');
?>
<!-- //////////////////////////////////////////////// NAV BAR + SIDE BAR /////////////////////////////////////////////// -->
<nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class='navbar-header'>
        <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-ex1-collapse'>
            <span class='sr-only'>Toggle navigation</span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
        </button>
        <a id="logo" class='navbar-brand' href='index.php'><img src="images/sl_nav_logo.png" alt="Swift Logistics logo"/>&nbsp;LOOP <?php echo $dev_indicator; ?> <i class="fa fa-home"></i></a>
    </div>
    <!-- Top Menu Items -->
    <ul class='nav navbar-right top-nav'>
        <!--<li>
            <a href="faq.php" target="_blank" class="dropdown-toggle"><i class="fa fa-question-circle"></i> FAQ </a>
        </li>-->
        <li class='dropdown'>
            <a href="mailto:allison_broughton@swifttrans.com?subject=LOOP%20Feedback&body=Please describe the issue and include a screenshot as well as the page URL if necessary:" target="_top" class="dropdown-toggle"><i class="fa fa-envelope"></i> E-Mail Support </a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa <?php echo $icon; ?>"></i> <?php echo $user_id_nav; ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="user_settings.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class='collapse navbar-collapse navbar-ex1-collapse'>
        <ul class='nav navbar-nav side-nav'>
            <?php if ($brokers_access) { ?>
                <li>
                    <a href='javascript:;' data-toggle='collapse' data-target='#brokers'><i class='fa fa-fw fa-truck'></i> Brokers <i class='fa fa-fw fa-caret-down'></i></a>
                    <ul id='brokers' class='collapse'>
                        <li>
                            <a href='commissions.php'><i class='fa fa-fw fa-dashboard'></i> Commissions</a>
                        </li>
                        <li>
                            <a href='broker_reports.php'><i class='fa fa-fw fa-table'></i> Reports</a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($managers_access) { ?>
                <li>
                    <a href='javascript:;' data-toggle='collapse' data-target='#demo'><i class='fa fa-fw fa-black-tie'></i> Managers <i class='fa fa-fw fa-caret-down'></i></a>
                    <ul id='demo' class='collapse'>
                        <?php if (getRole($user_id_nav, 'Broker_Activity_Tracker')) {?>
                        <li>
                            <a href='broker_activity.php'><i class='fa fa-fw fa-bar-chart-o'></i> Broker Activity</a>
                        </li>
                        <?php }?>
                        <li>
                            <a href='office_leaderboard.php'><i class='fa fa-fw fa-dashboard'></i> Leaderboard</a>
                        </li>
                        <?php if ($leaders_access) { ?>
                        <li>
                            <a href='hotload_adjustments.php'><i class='fa fa-fw fa-fire'></i> Hotload Adjustments</a>
                        </li>
                        <li>
                            <a href='negative_margin.php'><i class='fa fa-fw fa-scissors'></i> Negative Margin</a>
                        </li>
                        <?php } // Remove this when page goes live
                        if (getRole($user_id_nav, 'New_Hire_Form')) { ?>
                        <li>
                            <a href='hire_or_fire.php'><i class='fa fa-fw fa-exclamation-triangle'></i> Hire or Fire</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php if ($pricing_access) { ?>
                <li>
                    <a href='javascript:;' data-toggle='collapse' data-target='#pricing'><i class='fa fa-fw fa-dollar'></i> Pricing <i class='fa fa-fw fa-caret-down'></i></a>
                    <ul id='pricing' class='collapse'>
                        <li>
                            <a href='spot_rate_modifier.php'><i class='fa fa-fw fa-edit'></i> Spot Rate Modifier</a>
                        </li>
                    </ul>
                </li>
                <?php } if ($warren_access) { ?>
                <li>
                    <a href='javascript:;' data-toggle='collapse' data-target='#warren'><i class='fa fa-fw fa-wikipedia-w'></i> Warren <i class='fa fa-fw fa-caret-down'></i></a>
                    <ul id='warren' class='collapse'>
                        <li>
                            <a href='warren_routing.php'><i class='fa fa-fw fa-map'></i> Routing Guide</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            <li>
                <a href='javascript:;' data-toggle='collapse' data-target='#tools'><i class='fa fa-fw fa-wrench'></i> Tools <i class='fa fa-fw fa-caret-down'></i></a>
                <ul id='tools' class='collapse'>
                    <li>
                        <a href='mcloop.php'><i class='fa fa-fw fa-database'></i> McLoop</a>
                    </li>
                    <li>
                        <a href='carrier_lookup.php'><i class='fa fa-fw fa-truck'></i> Carrier Lookup</a>
                    </li>
                    <?php if ($tools_access) { ?>
                        <li>
                            <a href='transit_calculator.php'><i class='fa fa-fw fa-clock-o'></i> Transit Calculator</a>
                        </li>
                    <?php } ?>
                    <li>
                        <a target="_blank" href='http://inside.swiftlogistics.com:8080/'><i class='fa fa-fw fa-dollar'></i> Spot Rate Tool</a>
                    </li>
                    <li>
                        <a target="_blank" href='http://inside.swiftlogistics.com:8070/'><i class='fa fa-fw fa-dollar'></i> Buy Side Tool</a>
                    </li>
                </ul>
            </li>
<?php if ($jedi_access) { ?>
                <li>
                    <a href='javascript:;' data-toggle='collapse' data-target='#jedi'><i class='fa fa-fw fa-rebel'></i> Jedi <i class='fa fa-fw fa-caret-down'></i></a>
                    <ul id='jedi' class='collapse'>
                        <li>
                            <a href='activity_tracker.php'><i class='fa fa-fw fa-bar-chart-o'></i> Activity Tracker</a>
                        </li>
                        <li>
                            <a href='user_management.php'><i class='fa fa-fw fa-user'></i> User Management</a>
                        </li>
                        <li>
                            <a target="_blank" href='loadboard.php'><i class='fa fa-fw fa-wrench'></i> Loadboard</a>
                        </li>
                        <li>
                            <a target="_blank" href='carrier_onboarding.html'><i class='fa fa-fw fa-wrench'></i> Carrier Onboarding</a>
                        </li>
                    </ul>
                </li>
<?php } ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
