<?php
require 'include_functions.php';
require 'include_navbar.php';
$page_id = 'BC_Broker';
$title   = 'Commissions';
$last_updated = getLastUpdate();
$current_user = stripUsername($username);
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Set user emulation form
$emulate_user_form = emulateUser('CS');
// Set current $user_id to chosen user
if(isset($_POST['emulate'])) {
    $user_id = $_POST['emulate_user'];
    $update_emulated_user = "UPDATE Web_Page_Roles SET Emulate_User_ID = '$user_id' 
        WHERE Page_ID = '$page_id' AND User_ID = '$current_user'";
    $prepare_update = odbc_prepare($conn, $update_emulated_user);
    try {
        odbc_execute($prepare_update);
    } catch (Exception $ex) {
        echo "<pre>" . odbc_errormsg($conn) . "</pre>";
    }
}
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'campaa';
}
$page_access = getPageAccess($user_id, $page_id);
$office  = getOffice($user_id);
// Set limiter for manager or broker
$limiter = "dispatcher_user_id = '$user_id'";
//$limiter = "office_code = '$office'";
$cycle   = getCycle($user_id);
if($cycle === 'Q') {
    $cycle_full = 'Quarterly';
} elseif($cycle === 'A') {
    $cycle_full = 'Annual';
}
//////////////////////////////////////////////// CALCULATE BROKER AGGREGATE DETAILS ///////////////////////////////////////////
// Let user choose quarter/year to filter by
if (isset($_POST['filter_page_date'])) {
    $quarter = sanitize($_POST['quarter_filter']);
    $year    = sanitize($_POST['year_filter']);
} else {
    $quarter = $current_quarter;
    $year    = $current_year;
}
// Get broker's current margin level (0%, 3%, 6%, 7%, 10%)
$margin_level    = getCommissionLevel($limiter,$quarter,$year,$cycle);
// Bar Chart Values for "My Current Margin Level" chartist graph
$bar_potential   = getMargin('potential',$limiter,$quarter,$year,$cycle);
$bar_pending_pay = getMargin('pending',$limiter,$quarter,$year,$cycle);  
$bar_paid        = getMargin('paid',$limiter,$quarter,$year,$cycle);
$total_bars      = $bar_potential + $bar_pending_pay + $bar_paid;
$bar_goal_line   = getGoalLine($limiter);
// Set max y axis to ensure chart doesn't become skewed
if ($bar_goal_line == 3750000 && $cycle === 'Q') {
    $bar_goal_line = $total_bars;
    $set_max_y_axis = $total_bars + 1000;
} elseif ($margin_level >= 10 && $cycle === 'A') {
    $set_max_y_axis = 12000000;
} else {
    //$set_max_y_axis  = $total_bars + 5000;
    $set_max_y_axis = $bar_goal_line + 1000;
}
// Blue Panel for Gross Margin
$min_date             = getMinDate('BC_Pending_Pay_Details',$cycle);
$max_date             = getMaxDate('BC_Pending_Pay_Details');
$eom_last_date        = getEOMDate($conn,'last_commission_end_date');
$eom_current_date     = getEOMDate($conn,'current_commission_end_date');
$gross_margin         = getGrossMargin($limiter);
// Green Panel for Paid Commission
$comm_pay_current     = getCommPay($limiter);
// Donut Chart for Load Count by Status
$paid_load_count      = getDonuts('BC_Paid_Details',$limiter);
$pending_load_count   = getDonuts('BC_Pending_Pay_Details',$limiter);
$potential_load_count = getDonuts('BC_Potential_Details',$limiter);
$total_load_count     = $paid_load_count + $pending_load_count + $potential_load_count;
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <style>
             .ct-target-line {
                stroke: red;
                stroke-width: 2px;
                stroke-dasharray: 4px;
                shape-rendering: crispEdges;
            }   
            .panel-table table {
                margin-left: auto;
                margin-right: auto;
            }
            .panel-table table, .panel-table th, .panel-table td {
                color: #fff;
                font-size: 22px;
                border: 1px solid #fff;
                padding: 3px;   
                text-align: center;
            }
            #donut-chart-legend table, #donut-chart-legend th, #donut-chart-legend td {
                color: #36454F;
                font-size: 16px;
                border: 1px solid #36454F;
                font-weight: bold;
                padding: 3px;
                text-align: left;
            }
        </style>
        <!--Display spinning loader div-->
        <script>
            $(window).load(function () {
                $("#loader").fadeOut("slow");
                $(".overlay-loader").fadeOut("slow");
            });
        </script>
    </head>
    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    
                    <?php 
                    if($jedi_access) {
                        //echo "<pre><strong>Debug for Admin Use:<br/></strong>Margin Level: " . $margin_level . "% <br/>Goal Line: " . $bar_goal_line . "<br/>Y Axis Limit: " . $set_max_y_axis . "<br/>Total SUM of Bars: " . $total_bars . "</pre>";
                    }
                    if($admin) {
                        echo "<div class='col-lg-12'>Emulating: <strong>" . $user_id . "</strong>$emulate_user_form</div>";
                    }
                    if ($page_access) {?>
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    My Dashboard<br/>
                                    <small><strong>Commission Details for <?php echo $cycle_full; ?> Cycle</strong></small><small class='blink_me' style='color: #9400D3;'> Page last updated: <strong><?php echo $last_updated;?></strong></small><br/>
                                </h1><?php echo $dev_server;?>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-lg-4">
                                    <!--Chartist Stacked Chart-->
                                    <div class="col-lg-12">
                                        <div class="panel panel-charcoal">
                                            <div class="panel-heading">
                                                <h3 class="panel-title hint"><i class="fa fa-bar-chart "></i> My Current Margin Level</h3>
                                                <?php if($cycle === 'Q') {?>
                                                <br/>Potential & Pending Pay are running totals NOT filtered by any date.<br/>
                                                Paid Margin is the sum of loads paid out in current quarter (Q<?php echo $quarter;?>).
                                                <!--<form method="post" action="">
                                                    <select name="quarter_filter">
                                                        <option value="" selected disabled>Select Quarter</option>
                                                        <option value="1">Quarter 1</option>
                                                        <option value="2">Quarter 2</option>
                                                        <option value="3">Quarter 3</option>
                                                        <option value="4">Quarter 4</option>
                                                    </select>
                                                    <select name="year_filter">
                                                        <option value="2017" selected>2017</option>
                                                    </select>
                                                    <input type="submit" name="filter_page_date" value="Filter">
                                                </form>-->
                                                <?php } elseif ($cycle === 'A') {?>
                                                    <br/>Showing margin for <?php echo $current_year;?>
                                                <?php }?>
                                            </div>
                                            <div class="panel-body">
                                                <div class="flot-chart">
                                                    <div class="ct-chart ct-perfect-fifth"></div>
                                                </div>
                                                <div class="text-center"><?php //echo $margin_message; ?></div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="panel panel-charcoal">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <span class="col-xs-5"><strong>Quarterly Margin</strong></span>
                                                            <span class="col-xs-2 text-center"><strong>Level</strong></span>
                                                            <span class="col-xs-5 text-right"><strong>Annual Margin</strong></span>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-xs-5">
                                                                0 - 62,500<br/>
                                                                62,501 - 175,000<br/>
                                                                175,001 - 250,000<br/>
                                                                250,001 - 3,750,000<br/>
                                                            </div>
                                                            <div class="col-xs-2 text-center">
                                                                3%<br/>
                                                                6%<br/>
                                                                7%<br/>
                                                                10%<br/>
                                                            </div>
                                                            <div class="col-xs-5 text-right">
                                                                0 - 187,500<br/>
                                                                187,501 - 525,000<br/>
                                                                525,001 - 750,000<br/>
                                                                750,001 - 12,000,000<br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- panel-footer div -->
                                        </div>
                                    </div>
                                </div>
                            <div class="col-lg-4">
                                <!--GROSS MARGIN - PRIMARY BLUE PANEL-->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-1">
                                                <i class="fa fa-dollar fa-5x"></i>
                                                <div></div>
                                            </div>
                                            <div class="col-xs-11 text-center">
                                                <div class="panel-title">MY GROSS MARGIN</div>
                                                <div class="panel-table">
                                                    <table>
                                                        <tr><th>Total Pending<br/><?php echo $eom_last_date;?></th>
                                                            <th>Total Pending<br/><?php echo $eom_current_date;?></th>
                                                            <th>Total Potential</th></tr>
                                                            <?php echo $gross_margin; ?></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-left"><!--<strong>Current Period: <?php //echo $min_date;?> - <?php //echo $max_date;?></strong><br/>-->
                                            Margin is calculated from a DAILY running total. For details, please visit the 
                                            <a href="broker_reports.php">Reports</a> page.</span>
                                                    <span class="pull-right"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                </div>
                                <div class="row">
                                        <!--COMMISSION PAY - GREEN PANEL-->
                                        <div class="col-lg-12">
                                            <div class="panel panel-green">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-1">
                                                            <i class="fa fa-money fa-5x"></i>
                                                            <div></div>
                                                        </div>
                                                        <div class="col-xs-11 text-center">
                                                            <div class='panel-title'>MY PAID COMMISSIONS<br/></div>
                                                            <div class="panel-table">
                                                                <table>
                                                                    <tr><th>Pay Date</th><th>Pay Amount</th><th>Gross Margin</th></tr>
                                                                    <?php echo $comm_pay_current; ?>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <span class="pull-left">Commissions from new program starting 4/1/2017. 
                                                        For details, please visit the <a href="broker_reports.php">Reports</a> page.</span>
                                                    <span class="pull-right"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!--DONUT CHART PANEL-->
                            <div class="col-lg-4">
                                <div class="panel panel-charcoal">
                                    <div class="panel-heading">
                                        <h3 class="panel-title hint"><i class="fa fa-pie-chart"></i> Load Count by Status<span class="hinttext">
                                                For load details, go to the <a href="broker_reports.php" style="color: #fff;">Reports</a> page.</span></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="pie-chart"></div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                <div style="margin-left: 20px;">
                                                    <table id="donut-chart-legend">
                                                        <tr><td><div class="legend-key green-key"></div></td><td>PAID</td><td>Loads scheduled to be paid</td></tr>
                                                        <tr><td><div class="legend-key orange-key"></div></td><td>PENDING PAY</td><td>Loads to be paid next cycle</td></tr>
                                                        <tr><td><div class="legend-key yellow-key"></div></td><td>POTENTIAL</td><td>Loads in transit and/or missing POD/Delivery Date</td></tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                </div>
                            </div>
                        </div><!-- ./row-->
                        
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <!--jQuery Temp Gauge Plugin-->
        <!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>-->
        <script type="text/javascript" src="js/jquery.tempgauge.js"></script>
        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>
        <script src="js/plugins/chartist-goal-line-gh-pages/scripts/chartist-goal-line.js"></script>
        <script>
            var chart = new Chartist.Bar('.ct-chart', { 
                /* Edit colors here, starting on line 191: 
                 * C:\Users\stral\Documents\NetBeansProjects\Dashboard\css\plugins\chartist.min.css*/
                labels: ['Margin Amount'],
                series: [
                    [
                        {meta: 'Paid Margin', value: <?php echo $bar_paid; ?>}
                    ],
                    [
                        {meta: 'Pending Margin', value: <?php echo $bar_pending_pay; ?>}
                    ],
                    [
                        {meta: 'Potential Margin', value: <?php echo $bar_potential; ?>}
                    ]
                ]
            },
            {
                fullWidth: true,
                plugins: [
                /* Change tooltip colors: C:\Users\stral\Documents\NetBeansProjects\Dashboard\
                 * js\plugins\chartist-plugin-tooltip-master\src\css\chartist-plugin-tooltip.css*/
                    Chartist.plugins.tooltip({
                    currency: '$',
                    anchorToPoint: false
                  }),
                  Chartist.plugins.ctPointLabels({
                    textAnchor: 'middle'
                  })
                ],
                targetLine: {
                    value: <?php echo $bar_goal_line;?>,
                    class: 'ct-target-line'
                },
                chartPadding: 30,
                stackBars: true,
                axisY: {
                    labelInterpolationFnc: function (value) {
                         return '$' + (value / 1000) + 'k';
                    },
                    high: <?php echo $set_max_y_axis;?>/*,
                    scaleMinSpace: 100*/
                }
            });
            // draw the bar chart
            chart.on('draw', function (data) {
                if (data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 60px'
                    });
                }
            });
            
            function projectY(chartRect, bounds, value) {
                return chartRect.y1 - (chartRect.height() / bounds.max * value);
              }

              chart.on('created', function(context) {
                var targetLineY = projectY(context.chartRect, context.bounds, context.options.targetLine.value);

                context.svg.elem('line', {
                  x1: context.chartRect.x1,
                  x2: context.chartRect.x2,
                  y1: targetLineY,
                  y2: targetLineY
                }, context.options.targetLine.class);

                context.svg.elem('line', {
                  x1: context.chartRect.x1,
                  x2: context.chartRect.x2,
                  y1: 175000,
                  y2: 175000
                }, context.options.targetLine.class);

              });
        </script>
        <script>
            // Pie Chart for Load Count :  Pending v. Paid
            new Morris.Donut({
                // ID of the element in which to draw the chart.
                element: 'pie-chart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    {label: 'Paid', value: '<?php echo $paid_load_count ?>'},
                    {label: 'Pending Pay', value: '<?php echo $pending_load_count ?>'},
                    {label: 'Potential', value: '<?php echo $potential_load_count ?>'}
                ],
                colors: [
                    '#5CB85C', //green
                    '#ec971f', //orange
                    '#f3bf25' //yellow
                ],
                formatter: function(y, data) { return y + ' loads' + '\n' + Math.round(y/<?php echo $total_load_count;?> *100) + '%';},
                resize: true
            });
        </script>
    </body>
</html>