<?php

/* 
 * Embed Tableau View in Webpage - TEST
 */

?>

<html>
    <head>
        <title>Tableau Viz Test</title>
        <script type="text/javascript" src="https://public.tableau.com/javascripts/api/tableau-2.js"></script>
        <script>
            var viz;
            window.onload = function() {
                var vizDiv = document.getElementById('myViz');
                var vizURL = "https://public.tableau.com/views/SharknadoRecreationIronVIz/Sharknado?:embed=y&:loadOrderID=0&:display_count=yes";
                var options = {
                    width: '1200px',
                    height: '900px'
                };
                viz = new tableau.Viz(vizDiv, vizURL, options);
            };
            
            // Content below is to resolve warning for missing 'tableau' object
            var tableau;
        </script>
    </head>
    <body>
        <img src="images/swift_logistics_logo.png"/>
        <br/><br/>
        <div id="myViz"></div>
    </body>
</html>