<?php
/////////////////////////////////////////////////// ERROR REPORTING //////////////////////////////////////////////////
/* Shows ONLY fatal errors - use this setting when page is live */
error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR);
//error_reporting(E_ALL ^ E_NOTICE); // shows ALL notices, warnings, errors
//ini_set('display_errors', true); // Uncomment while developing code
/////////////////////////////////////////////////// INITIALIZE VARIABLES //////////////////////////////////////////////////
//$ip                  = gethostbyaddr($_SERVER['REMOTE_ADDR']);
//$server_name         = $_SERVER['SERVER_NAME'];
//$username            = $_SERVER['AUTH_USER']; // "SWIFT\user_id"
if (isset($_SERVER['AUTH_USER'])) {
    $username = $_SERVER['AUTH_USER'];
} else {
    $username = "localhost\DEV";
}
$user_id = stripUsername($username); // "user_id"
if ($_SERVER['HTTP_HOST'] === 'devinside.swiftlogistics.com' || $_SERVER['HTTP_HOST'] === 'localhost:82' || $_SERVER['HTTP_HOST'] === 'phxlogwebd01:8000') {
    $dev_server = "<h2 class='blink_me' style='color: #ff3333;'>This is the DEV environment. 
        <a href='http://inside.swiftlogistics.com/'>Click here</a> to go to Production.</h2>";
}
///////////////////////////////////////// ODBC Connection /////////////////////////////////////////
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
//////////////////////////////////////// ODBC Connection DEV //////////////////////////////////////
$database_dev      = 'Logistics_Data_Warehouse_Dev';
$connection_dev    = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database_dev";
$conn_dev          = odbc_connect($connection_dev, $user, $pass);
//////////////////////////////////////// ODBC Connection MCLEOD ///////////////////////////////////
$server_mcld       = '10.1.244.125';//'10.1.244.122';
$database_mcld     = 'McLeod_lme_1110_prod';
$connection_mcld   = "DRIVER={SQL Server};SERVER=$server_mcld;$port;DATABASE=$database_mcld";
$conn_mcld         = odbc_connect($connection_mcld, $user, $pass);
///////////////////////// USER PERMISSIONS ///////////////////////////////////////////////////////
if($user_id === 'localhost\DEV'||$user_id === 'stral'||$user_id === 'labin') {
    //$violet_css = "<link href='css/hot_pink.css' rel='stylesheet'>";
    //$violet_css = "<link href='css/violet.css' rel='stylesheet'>";
    $violet_css = '';
} elseif ($user_id === 'amezs') {
    $violet_css = "<link href='css/hot_pink.css' rel='stylesheet'>";
} else {
    $violet_css = '';
}
//////////////////////////////////////////////// REDIRECT FUNCTION //////////////////////////////////////////////
function redirect($url) {
    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
}
//////////////////////////////////////////////// CHECK PAGE PERMISSIONS ///////////////////////////////////////////
function getPageAccess($user_id, $page_id) {
    if ($user_id === 'localhost\DEV') {
        $access = true;
    } else {
        global $conn;
        $sql   = "SELECT RoleID AS role FROM Web_Page_Roles WHERE User_ID = '$user_id' AND Page_ID = '$page_id'";
        $query = odbc_prepare($conn, $sql);
        odbc_execute($query);
        $row   = odbc_fetch_array($query);
        $role  = $row['role'];
        if ($role === NULL) {
            $access = false;
        } else {
            $access = true;
        }
    }
    return $access;
}

function getLeaderStatus($user_id,$page_id) {
    global $conn;
    $sql = "SELECT User_Role_Name AS role FROM Web_Page_Roles WHERE User_ID = '$user_id' AND Page_ID = '$page_id'";
    $query = odbc_prepare($conn,$sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $role = $row['role'];
    if($role == 'Office Director'||$role == 'Team Lead') {
        $access = true;
    } else {
        $access = false;
    }
    return $access;
}
function getAdminStatus($username,$page_id) {
    global $conn;
    $username = stripUsername($username);
    $sql = "SELECT User_Role_Name AS role FROM Web_Page_Roles WHERE User_ID = '$username' AND Page_ID = '$page_id'";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $role = $row['role'];
    if($role == 'Admin') {
        $admin = true;
    } else {
        $admin = false;
    }
    return $admin;
}
function getBrokerStatus($user_id) {
    if(!($user_id === 'localhost\DEV')) {
        $user_id = strtolower($user_id);
    }
    if ($user_id === 'localhost\DEV' || $user_id === 'stral' || $user_id === 'bendc' || $user_id == 'campaa' || $user_id === 'cartja' ||
            $user_id === 'durhco' || $user_id === 'filad' || $user_id === 'guidj' || $user_id === 'pitcn' ||
            $user_id === 'hudys' || $user_id === 'johshi' || $user_id === 'nguyr' || $user_id === 'ortet' ||
            $user_id === 'sienj' || $user_id === 'sprad' || $user_id === 'steije' || $user_id === 'torris' || $user_id === 'labin') {
        $broker = true;
    } else {
        $broker = false;
    } return $broker;
}
//////////////////////////////////////////////// Date/Time Variables ////////////////////////////////////////////////
$today              = date('Y-m-d');
$today_datetime     = date('Y-m-d h:i:s');
$tomorrow           = (new DateTime('tomorrow'))->format('Y-m-d');
$tomorrow_datetime  = (new DateTime('tomorrow'))->format('Y-m-d h:i:s');
$yesterday          = (new DateTime('yesterday'))->format('Y-m-d');
$yesterday_datetime = (new DateTime('yesterday'))->format('Y-m-d h:i:s');
$this_week_start    = (new DateTime('this week'))->format('Y-m-d');
$this_week_end      = (new DateTime('this week +6 days'))->format('Y-m-d');
$this_month_start   = (new DateTime('first day of this month'))->format('Y-m-d');
$this_month_end     = (new DateTime('last day of this month'))->format('Y-m-d');
$today_numerical    = date(j); // day of the month without leading zeros
$current_month      = date(n); // displays current month in number format w/o leading zeros
$current_month_name = date(F); // displays current month in number format w/o leading zeros
$last_month         = Date('F', strtotime($current_month_name . " last month"));
$current_year       = date(Y); // four digit representation of the current year
// lz = leading zero
function lz($num) {
    return (strlen($num) < 2) ? "0{$num}" : $num;
}
// Convert hours to minutes
function convertTime($decimal) {
    $hour = floor($decimal);
    $min = round(60*($decimal - $hour));
    $total = $hour*60 + $min;
    return $total;
    // Converts decimal to HH:MM
    /*$hour = floor($decimal);
    $min = round(60*($decimal - $hour));
    // return the time formatted HH:MM
    return lz($hour)."H".lz($min)."M";*/
}
function userFriendlyDate($datetimestr) {
    $formatted  = (new DateTime($datetimestr))->format('n/d/Y H:i');
    return $formatted;
}
//////////////////////////////////////////////// FIRST DAY OF NEW MONTH ///////////////////////////////////////////////////////
// Check if it is the first day of the month, if so, then use last month to pull data
// Otherwise some values will be NULL since the nightly process doesn't run until midnight to get new month's data
function setMonth($today_numerical,$current_month) {
    if ($today_numerical == '1') {
        if($current_month == '1') {
            $current_month = 12; // If it is January, set month to December
        } else {
            $current_month = $current_month - 1; // subtract 1 from current month
        }
    } else {
        $current_month = date(n); // displays current month in number format w/o leading zeros
    }
    return $current_month;
}
$current_month = setMonth($today_numerical,$current_month);
///////////////////////////////////////////////// GET CURRENT QUARTER //////////////////////////////////////
function getQuarter($current_month) {
    if($current_month >= 1 && $current_month <= 3) {
        $current_quarter = 1;
    } elseif($current_month >= 4 && $current_month <= 6) {
        $current_quarter = 2;
    } elseif($current_month >= 7 && $current_month <= 9) {
        $current_quarter = 3;
    } else {
        $current_quarter = 4;
    }
    return $current_quarter;
}
$current_quarter = getQuarter($current_month);
// Get the month values that exist in current quarter
function getQMonths($current_quarter) {
    if($current_quarter === 1) {
        $q_months = '1,2,3';
    } elseif($current_quarter === 2) {
        $q_months = '4,5,6';
    } elseif($current_quarter === 3) {
        $q_months = '7,8,9';
    } elseif($current_quarter === 4) {
        $q_months = '10,11,12';
    }
    return $q_months;
}
$q_months = getQMonths($current_quarter);
//////////////////////////////////////////////////// GLOBAL FUNCTIONS ///////////////////////////////////////////////////////
function trimSpaces($string) {
    $trimmed = str_replace(' ', '', ($string));
    return $trimmed;
}
// Remove "SWIFT\" from usernames
function stripUsername($username) {
    if ($username === "localhost\DEV") {
        $user_id = "localhost\DEV";
    } else {
        $user_id = substr(stripslashes($username), 5);
    }
    return $user_id;
}
// Strip SWIFT\ from usernames
$current_user = stripUsername($username);
// Assign certain users to manager permissions
if ($current_user === 'stral' || $current_user === 'localhost\DEV' || $current_user === 'labin' || $current_user === 'everd' || $current_user === 'vaughn' || $current_user === 'maeski' || $current_user === 'tund') {
    $manager = true;
} else {
    $manager = false;
}
// GET FULL NAME FROM USER ID
function getFullName($user_id) {
    global $conn;
    if($user_id === 'localhost\DEV') {
        $full_name = 'Allison Broughton (Testing in DEV)';
    } else {
        $sql = "SELECT name FROM prod_users WHERE id = '$user_id'";
        $query = odbc_prepare($conn, $sql);
        odbc_execute($query);
        $row = odbc_fetch_array($query);
        $full_name = $row['name'];
    }
    return $full_name;
}
// GET EMAIL FOR USER
function getEmail($user_id) {
    global $conn;
    if($user_id === 'localhost\DEV') {
        $email = 'allison_broughton@swifttrans.com';
    } else {
        $sql = "SELECT email_address FROM prod_users WHERE id = '$user_id'";
        $query = odbc_prepare($conn, $sql);
        odbc_execute($query);
        $row = odbc_fetch_array($query);
        $email = $row['email_address'];
    }
    return $email;
}
// EMULATE USER
function emulateUser($user_role) {
    global $conn;
    $select_users = "SELECT id,name FROM prod_users WHERE is_active = 'Y' 
        AND department_id IN ('$user_role') ORDER BY name ASC";
    /*$select_users = "SELECT DISTINCT User_ID FROM Web_Page_Roles 
        WHERE User_Role_Name = '$user_role' AND User_ID NOT IN ('crawda','bendc') ORDER BY User_ID ASC";*/
    $get_users    = odbc_prepare($conn, $select_users);
    odbc_execute($get_users);
    $options      = "";
    while ($row = odbc_fetch_array($get_users)) {
        //$name    = getFullName($row['User_ID']);
        $options .= "<option value='" . $row['id'] . "'>".$row['name']."</option>";
    }
    $emulate_user_form = "<form method='post' action=''>
                            <select name='emulate_user' required>
                                <option selected disabled>Choose User...</option>
                                $options
                            </select>
                            <button type='submit' name='emulate' id='button'>Emulate</button>
                          </form>";
    return $emulate_user_form;
}
function checkEmulation($page_id,$user_id) {
    global $conn;
    $sql = "SELECT Emulate_User_ID AS Emulate FROM Web_Page_Roles 
        WHERE Page_ID = '$page_id' AND User_ID = '$user_id'";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $emulate = $row['Emulate'];
    if($emulate === NULL) {
        $emulate = $user_id;
    }
    return $emulate;
}
// Get list of brokers for option tags
function createOptions($user_role) {
    global $conn;
    $select_users = "SELECT DISTINCT User_ID FROM Web_Page_Roles WHERE User_Role_Name = '$user_role' 
        AND User_ID NOT IN ('crawda','bendc') ORDER BY User_ID ASC";
    $get_users    = odbc_prepare($conn, $select_users);
    odbc_execute($get_users);
    $options      = "";
    while ($row          = odbc_fetch_array($get_users)) {
        $name    = getFullName($row['User_ID']);
        $options .= "<option value='" . $row['User_ID'] . "'>$name</option>";
    }
    return $options;
}
// Track page visits/events
function trackVisit($page_id,$user_id,$event) {
    global $conn;
    global $today_datetime;
    $sql = "INSERT INTO Web_Page_Visits (Page_ID, User_ID, Visit_Datetime, Event) VALUES ('$page_id','$user_id','$today_datetime','$event')";
    $query = odbc_prepare($conn, $sql);
    try {
        odbc_execute($query);
    } catch (Exception $exc) {
        $exc->getTraceAsString();
    }
}
// Self explanatory - cleans spaces from string
function cleanSpaces($string) {
    $no_spaces   = preg_replace('/\s*,\s*/', ',', $string);
    $clean_space = str_replace(',', ', ', $no_spaces);
    return $clean_space;
}
// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}
////////////////////////////////////////////////// COMMISSIONS PAGE //////////////////////////////////////////////////
// Get datetime of last data refresh
function getLastUpdate() {
    global $conn;
    $sql = "SELECT DISTINCT(job_run_date) AS Last_Updated FROM BC_Pending_Pay_Details";
    $query = odbc_prepare($conn,$sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $datetime = strtotime($row['Last_Updated']);
    $last_updated = date('m/d/y H:i A',$datetime);
    if($last_updated === '12/31/69 17:00 PM') {
        $last_updated = 'No data available.';
    }
    return $last_updated;
}
// Get office code assigned to user
function getOffice($user_id) {
    global $conn;
    $sql = "SELECT Office_Code FROM Web_Page_Roles WHERE User_ID = '$user_id'";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $office = $row['Office_Code'];
    return $office;
}
// Get commission cycle chosen by user
function getCycle($user_id) {
    global $conn;
    $sql = "SELECT commission_cycle AS cycle FROM prod_users WHERE id = '$user_id'";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $cycle = $row['cycle'];
    return $cycle;
}
// Get Broker's current margin level
function getCommissionLevel($limiter,$quarter,$year,$cycle) {
    global $conn;
    global $current_month;
    global $current_year;
    if($cycle === 'Q'||$cycle === '') {
        $sql = "SELECT MAX(commission_percent) AS Commission_Level 
          FROM VW_AGG_BC_PayALL_temp WHERE $limiter 
          AND commission_quarter = '$quarter' AND commission_year = '$year'";
    } elseif($cycle === 'A') {
        $sql = "SELECT MAX(commission_percent) AS Commission_Level 
          FROM VW_AGG_BC_PayALL_temp WHERE $limiter 
          AND commission_month = '$current_month' AND commission_year = '$current_year'";
    }
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $percent = round($row['Commission_Level']*100);
    return $percent;
}
// Calculate margin for potential, pending, and paid
/*function getMargin($status,$limiter,$quarter,$year,$cycle) {
    global $conn;
    global $current_year;
    if ($status === 'potential') {
        $column = 'potential_margin';
        $table  = 'BC_Potential_Details';
    } elseif ($status === 'pending') {
        $column = 'pending_pay';
        $table  = 'BC_Pending_Pay_Details';
    } elseif ($status === 'paid') {
        $column = 'paid';
        $table  = 'BC_Paid_Details';
        $filter = " AND Pay_New = 'Y'";
    }
    if($cycle === 'Q'||$cycle === '') {
        $sql = "SELECT SUM(movement_margin) AS $column FROM $table
                WHERE $limiter AND commission_quarter = '$quarter' AND commission_year = '$year'$filter";
    } elseif($cycle === 'A') {
        $sql = "SELECT SUM(movement_margin) AS $column FROM $table 
            WHERE $limiter AND commission_year = '$current_year'$filter";
    }
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $margin = round($row[$column]);
    return $margin;
}*/
/*New getMargin function*/
function getMargin($status,$limiter,$quarter,$year,$cycle) {
    global $conn;
    global $current_year;
    global $q_months;
    if ($status === 'potential') {
        $column = 'potential_margin';
        $table  = 'BC_Potential_Details';
    } elseif ($status === 'pending') {
        $column = 'pending_pay';
        $table  = 'BC_Pending_Pay_Details';
    } elseif ($status === 'paid') {
        $column = 'paid';
        $table  = 'BC_Paid_Details';
    }
    if (($cycle === 'Q' || $cycle === '') & ($status === 'paid')) {
        $sql = "SELECT SUM(movement_margin) AS $column FROM $table
                WHERE $limiter AND MONTH(Commission_End_Date) IN ($q_months) AND YEAR(Commission_End_Date) = '$current_year'";
    } elseif (($cycle === 'A') & ($status === 'paid')) {
        $sql = "SELECT SUM(movement_margin) AS $column FROM $table
                WHERE $limiter AND YEAR(Commission_End_Date) = '$current_year'";
    } else {
        $sql = "SELECT SUM(movement_margin) AS $column FROM $table WHERE $limiter";
    }    
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $margin = round($row[$column]);
    return $margin;
}
// Calculate margin amount to level up and display as dotted line
function getGoalLine($limiter) {
    global $conn;
    $sql = "SELECT MAX(All_MaxMargin) AS goal FROM BC_Agg_ALL_Accrual_Final
        WHERE $limiter";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $goal .= $row['goal'];
    return $goal;
}
// Calculate load counts for donut chart
function getDonuts($table, $limiter) {
    global $conn;
    $str        = "SELECT COUNT(*) AS Load_Count FROM $table WHERE $limiter";
    $query      = odbc_prepare($conn, $str);
    odbc_execute($query);
    $row        = odbc_fetch_array($query);
    $load_count = $row['Load_Count'];
    if (empty($row['Load_Count'])) {
        $load_count = 0;
    }
    return $load_count;
}
// Get Pay Date, Pay Amount, and Paid Gross Margin to display in Green Panel on Commissions page
function getCommPay($limiter) {
    global $conn;
    //global $current_year;
    /*$sql        = "SELECT SUM(payroll_amount) AS total_paid, CONVERT(DATE, pay_date) AS pay_date, 
        SUM(movement_margin) AS paid_margin
        FROM BC_Paid_Details WHERE $limiter AND dispatcher_user_id <> 'crawda' GROUP BY pay_date 
        ORDER BY pay_date ASC";*/
    /*This is NOT currently limited by year. Need to select TOP 12 or something?*/
    $sql = "SELECT total_paid AS total_paid, CONVERT(DATE, Pay_Date) AS pay_date, 
        paid_margin AS paid_margin
        FROM BC_Web_Paid_Commissions WHERE $limiter ORDER BY pay_date ASC";
    $stmt       = odbc_prepare($conn, $sql);
    odbc_execute($stmt);
    $table_rows = '';
    while ($row     = odbc_fetch_array($stmt)) {
        $datestamp  = strtotime($row['pay_date']);
        $pay_date   = date("m/d/Y", $datestamp);
        $sum        = number_format($row['total_paid']);
        $paid_margin = number_format($row['paid_margin']);
        $table_rows .= "<tr><td>$pay_date</td><td>$$sum</td><td>$$paid_margin</td></tr>";
    }
    return $table_rows;
}
// Get oldest date from any given table
function getMinDate($table,$cycle) {
    global $conn;
    global $current_quarter;
    global $current_year;
    if($cycle === 'Q') {
        $sql = "SELECT CONVERT(DATE, MIN(dispatch_date)) AS Min_Date FROM $table WHERE dispatch_date IS NOT NULL
                AND Commission_Quarter = '$current_quarter'";
    } elseif($cycle === 'A') {
        $sql = "SELECT CONVERT(DATE, MIN(dispatch_date)) AS Min_Date FROM $table WHERE dispatch_date IS NOT NULL
                AND Commission_Year = '$current_year'";
    }
    $get = odbc_prepare($conn, $sql);
    try {
        odbc_execute($get);
    } catch (Exception $exc) {
        $exc->getTraceAsString();
    }
    $row = odbc_fetch_array($get);
    $datestamp = strtotime($row['Min_Date']);
    $min_date = date("m/d/Y", $datestamp);
    return $min_date;
}
// Get newest date from any given table
function getMaxDate($table) {
    global $conn;
    $sql = "SELECT CONVERT(DATE, MAX(dispatch_date)) AS Max_Date FROM $table WHERE dispatch_date IS NOT NULL";
    $get = odbc_prepare($conn, $sql);
    try {
        odbc_execute($get);
    } catch (Exception $exc) {
        $exc->getTraceAsString();
    }
    $row = odbc_fetch_array($get);
    $datestamp = strtotime($row['Max_Date']);
    $max_date = date("m/d/Y", $datestamp);
    return $max_date;
}
// Calculate EOM Date for Blue Panel
function getEOMDate($conn,$col) {
    $sql = "SELECT CAST($col AS DATE) AS eom_date FROM bc_commission_end_date";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $datestamp = strtotime($row['eom_date']);
    $eom = date("m/d/Y", $datestamp);
    if($eom === '12/31/1969') {
        $eom = 'No Data';
    }
    return $eom;
}
// Calculate potential & pending margin for Blue Panel
function getGrossMargin($limiter) {
    global $conn;
    //global $current_year;
    //global $current_quarter;
    // Calculate EOM pending margin
    $select_eom_pending = "SELECT SUM(current_pending_margin) AS pending_eom_pay 
        FROM BC_Agg_ALL WHERE $limiter AND current_pending_margin IS NOT NULL";
    $get_eom_pending    = odbc_prepare($conn, $select_eom_pending);
    odbc_execute($get_eom_pending);
    $row_eom_pending    = odbc_fetch_array($get_eom_pending);
    $pending_eom_margin = $row_eom_pending['pending_eom_pay'];
    if ($pending_eom_margin === NULL) {
        $pending_eom_margin = 0;
    }
    $pending_eom_margin   = number_format($pending_eom_margin);
    // Calculate pending margin
    /*$select_pending = "SELECT SUM(movement_margin) AS pending_pay FROM BC_Pending_Pay_Details
                WHERE commission_year = '$current_year' AND $limiter";*/
    $select_pending = "SELECT SUM(future_pending_margin) AS pending_pay FROM BC_Agg_ALL
                WHERE $limiter AND future_pending_margin IS NOT NULL";
    $get_pending    = odbc_prepare($conn, $select_pending);
    odbc_execute($get_pending);
    $row_pending    = odbc_fetch_array($get_pending);
    $pending_margin = $row_pending['pending_pay'];
    if ($pending_margin === NULL) {
        $pending_margin = 0;
    }
    $pending_margin   = number_format($pending_margin);
    // Calculate potential margin
    /*$select_potential = "SELECT SUM(movement_margin) AS potential_margin FROM BC_Potential_Details
                WHERE commission_year = '$current_year' AND $limiter";*/
    $select_potential = "SELECT (ISNULL(SUM(future_potential_margin),0) + ISNULL(SUM(Current_Potential_Margin),0)) AS potential_margin 
        FROM BC_Agg_ALL WHERE $limiter";
    $get_potential    = odbc_prepare($conn, $select_potential);
    odbc_execute($get_potential);
    $row_potential    = odbc_fetch_array($get_potential);
    $potential_margin = $row_potential['potential_margin'];
    if ($potential_margin === NULL) {
        $potential_margin = 0;
    }
    $potential_margin = number_format($potential_margin);
    $table_rows       = "<tr><td>$$pending_eom_margin</td><td>$$pending_margin</td><td>$$potential_margin</td></tr>";
    return $table_rows;
}
/////////////////////////////////////////// OFFICE LEADERBOARD COMMISSIONS VIEW ///////////////////////////////////////////////////
// Get Broker's current gross margin for green bar
function getCurrentGrossMargin($limiter,$quarter,$year,$cycle) {
    global $conn;
    global $current_year;
    global $current_quarter;
    if($cycle === 'Q'||$cycle === '') {
        $sql = "SELECT MAX(Max_Running_Total) AS Max_Running_Total FROM VW_AGG_BC_PayALL_temp 
            WHERE $limiter AND Commission_Quarter = '$quarter' AND Commission_Year = '$year'";
    } elseif($cycle === 'A') {
        $sql = "SELECT MAX(Max_Running_Total) AS Max_Running_Total FROM VW_AGG_BC_PayALL_temp 
            WHERE $limiter AND Commission_Quarter = '$current_quarter' AND Commission_Year = '$current_year'";
    }
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $gross = round($row['Max_Running_Total']);
    return $gross;
}
// Get brokers to use for function parameters on office leaderboard
function getBrokers() {
    global $conn;
    $select_brokers = "SELECT id,name,office_code FROM prod_users 
        WHERE is_active = 'Y' AND department_id = 'CS' ORDER BY office_code,name ASC";
    $get_brokers    = odbc_prepare($conn, $select_brokers);
    odbc_execute($get_brokers);
    $array_brokers[]  = "";
    while($row = odbc_fetch_array($get_brokers)) {
        $array_brokers[] .= $row['id'];
    }
    // Remove first element from array as it is empty
    array_shift($array_brokers);
    return $array_brokers;
}
// Get array of brokers full names to use for labels
function getBrokerFullName($broker_id) {
    global $conn;
    $select_brokers = "SELECT Dispatcher_User AS Dispatcher_User
        FROM VW_AGG_BC_PayALL_temp WHERE dispatcher_user_id = '$broker_id'";
    $get_brokers    = odbc_prepare($conn, $select_brokers);
    odbc_execute($get_brokers);
    $row = odbc_fetch_array($get_brokers);
    $full_name = $row['Dispatcher_User'];
    return $full_name;
}
// Get Broker's current gross margin for data set
function getMarginforDataSet($broker) {
    global $conn;
    global $current_year;
    /*$sql = "SELECT MAX(Max_Running_Total) AS Max_Running_Total FROM VW_AGG_BC_PayALL_temp 
        WHERE dispatcher_user_id = '$broker' AND Commission_Year = '$current_year'";*/
    $sql = "SELECT SUM(movement_margin) AS paid FROM BC_Paid_Details
        WHERE dispatcher_user_id = '$broker' AND YEAR(Commission_End_Date) = '$current_year'";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $gross = round($row['paid']);
    if($gross === NULL) {
        $gross = 0;
    }
    return $gross;
}
// Get bar colors based on office
function getBackgroundColor($broker_id) {
    // Look up office code for broker
    $office = getOffice($broker_id);
    // Assign bar color dependant on office
    if($office === 'NWA') { // Blue 
        $background_color = '#253a5d';
    } elseif ($office === 'PHX') { // Green
        $background_color = '#008000';
    } elseif ($office === 'SLC') { // Red
        $background_color = '#c21807';
    } elseif ($office === 'WMA') { // Orange 
        $background_color = '#cc5500';
    } elseif ($office === 'LRDO') { // Yellow
        $background_color = '#ffd700';
    } else { // Purple
        $background_color = '#800080'; 
    }
    return $background_color;
}
function getBorderColor($broker_id) {
    // Look up office code for broker
    $office = getOffice($broker_id);
    // Assign bar color dependant on office
    if($office === 'NWA') { // Blue
        $border_color = '#091935';
    } elseif ($office === 'PHX') { // Green
        $border_color = '#005900';
    } elseif ($office === 'SLC') { // Red
        $border_color = '#871004';
    } elseif ($office === 'WMA') { // Orange 
        $border_color = '#8E3B00';
    } elseif ($office === 'LRDO') { // Yellow
        $border_color = '#e5c100';
    }
    return $border_color;
}
function getHoverColor($broker_id) {
    // Look up office code for broker
    $office = getOffice($broker_id);
    // Assign bar color dependant on office
    if($office === 'NWA') { // Blue
        $hover_color = '#3a465d';
    } elseif ($office === 'PHX') { // Green
        $hover_color = '#327a32';
    } elseif ($office === 'SLC') { // Red
        $hover_color = '#9f3f36';
    } elseif ($office === 'WMA') { // Orange 
        $hover_color = '#a46232';
    } elseif ($office === 'LRDO') { // Yellow
        $hover_color = '#b29600';
    }
    return $hover_color;
}
// Create the dataset for each broker
function createDataSet($broker_id) {
    global $current_quarter;
    global $current_year;
    // Look up office code for broker
    $office = getOffice($broker_id);
    // Calculate current gross margin
    $gross_margin = getCurrentGrossMargin("dispatcher_user_id = '$broker_id'",$current_quarter,$current_year,'');
    // Assign bar color dependant on office
    if($office === 'NWA') { // Blue
        $background_color = '#0d254c';
        $border_color = '#091935';
    } elseif ($office === 'PHX') { // Green
        $background_color = '#008000';
        $border_color = '#005900';
    } elseif ($office === 'SLC') { // Red
        $background_color = '#c21807';
        $border_color = '#871004';
    } elseif ($office === 'WMA') { // Orange
        $background_color = '#cc5500';
        $border_color = '#8E3B00';
    }
    // Create data set from above info
    $data_set = "label: '$broker_id',
                data: [$gross_margin],
                backgroundColor: ['$background_color'],
                borderColor: ['$border_color'],
                borderWidth: 2";
    return $data_set;
}
/////////////////////////////////////////// SPOT RATE MODIFIER FUNCTIONS ///////////////////////////////////////
// Validate user-entered cities
function validateCity($cityState) {
    global $conn;
    $string  = "SELECT TOP 1 City, State FROM UsMarkets WHERE Cityname LIKE '%$cityState%' AND City IS NOT NULL";
    $stmt    = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $results = "";
    while ($row     = odbc_fetch_array($stmt)) {
        $results .= $row['City'] . ", " . $row['State'];
    }
    if (empty($results)) {
        $valid = false;
    } else {
        $valid = true;
    }
    //return $valid;
    return $results;
}
// Return user-entered cities
function fuzzySearch($cityState) {
    global $conn;
    $string  = "SELECT TOP 1 City, State FROM UsMarkets WHERE Cityname LIKE '$cityState%' AND City IS NOT NULL";
    $stmt    = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $results = "";
    while ($row     = odbc_fetch_array($stmt)) {
        $results .= $row['City'] . ", " . $row['State'];
    }
    if (empty($results)) {
        $results = "No cities found. Please check your spelling.";
    }
    return $results;
}
// Look up Origin and Destination Market ID
function getMarket($cityState) {
    global $conn;
    $string = "SELECT TOP 1 DATMarketAreaID FROM USMarkets WHERE Cityname LIKE '%$cityState%' AND City IS NOT NULL";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $market = "";
    while ($row    = odbc_fetch_array($stmt)) {
        $market = $row['DATMarketAreaID'];
    }
    if (empty($market)) {
        $market = "No market";
    }
    return $market;
}
//////////////////////////////////////////// ACTIVITY TRACKER PAGE //////////////////////////////////////////////
// Get usernames to display in bar chart
// Consider adding radio box for user to check whether or not to exclude admins
function getUsers($type) {
    global $conn;
    global $today;
    if($type === 'All') {
        $select_users = "SELECT DISTINCT(User_ID) AS User_ID FROM Web_Page_Roles 
            WHERE User_ID NOT IN ('stral','labin','everd')
            ORDER BY User_ID ASC";
    } elseif($type === 'Broker') {
        $select_users = "SELECT id AS User_ID,name FROM prod_users 
            WHERE is_active = 'Y' AND department_id IN ('CS') ORDER BY name ASC";
    } elseif($type === 'Admin') {
        $select_users = "SELECT DISTINCT(User_ID) AS User_ID FROM Web_Page_Roles 
            WHERE User_ID IN ('stral','labin','everd','vaugd','pitcn')
            ORDER BY User_ID ASC";
    } elseif($type === 'Active' || $type === '') {
        $select_users = "SELECT DISTINCT(User_ID) AS User_ID FROM Web_Page_Visits 
            WHERE CAST(Visit_Datetime AS DATE) = '$today' AND User_ID <> 'stral'
            ORDER BY User_ID ASC";
    }
    $get_users    = odbc_prepare($conn, $select_users);
    odbc_execute($get_users);
    $array_users[]  = "";
    while($row = odbc_fetch_array($get_users)) {
        $array_users[] .= $row['User_ID'];
    }
    // Remove 1st array element
    array_shift($array_users);
    return $array_users;
}
// Get # of page visits for each user
function getPageVisits($user,$page_id,$date,$event) {
    global $conn;
    $sql = "SELECT COUNT(*) AS Visits FROM Web_Page_Visits 
        WHERE CAST(Visit_Datetime AS DATE) = '$date' AND 
        User_ID = '$user' AND Page_ID = '$page_id' AND $event";
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $data = round($row['Visits']);
    return $data;
}
////////////////////////////////////////// BROKER KPI PAGE /////////////////////////////////////////////////////
// Convert Date
function convertDate($format,$date) {
    $time = strtotime($date);
    $converted_date = date($format,$time);
    return $converted_date;
}
// Calculate aggregate totals for each chart
function getAggChartTotals($chart,$time_period,$user_id) {
    global $conn;
    global $yesterday;
    global $this_week_start;
    global $this_week_end;
    global $this_month_start;
    global $this_month_end;
    if($chart === 'loadsPerBroker') {
        $sql_start = "SELECT COUNT(DISTINCT(Movement_ID)) AS Total
            FROM Broker_KPI ";
    } elseif($chart === 'loadsPerCarrier') {
        $sql_start = "SELECT COUNT(DISTINCT(Movement_ID))/COUNT(DISTINCT(Payee_ID)) AS Total
            FROM Broker_KPI ";
    } elseif($chart === 'marginPercent') {
        $sql_start = "SELECT CAST((SUM(Margin)/SUM(Revenue))*100 AS DECIMAL(18,1)) AS Total
            FROM Broker_KPI ";
    } elseif($chart === 'marginDollar') {
        $sql_start = "SELECT SUM(Margin) AS Total
            FROM Broker_KPI ";
    } elseif($chart === 'buyMarketRate') {
        $sql_start = "SELECT CAST(((SUM(Carrier_Pay) - SUM(Market))/SUM(Market))*100 AS DECIMAL(18,1)) AS Total
            FROM Broker_KPI ";
    }
    if($time_period === 'Yesterday') {
        $sql = "WHERE BrokerID = '$user_id' AND CAST(Dispatched_Date AS DATE) = '$yesterday' AND PT > '0'";
    } elseif($time_period === 'Week') {
        $sql = "WHERE BrokerID = '$user_id' AND CAST(Dispatched_Date AS DATE) BETWEEN '$this_week_start' AND '$this_week_end' AND PT > '0'";
    } elseif($time_period === 'Month') {
        $sql = "WHERE BrokerID = '$user_id' AND CAST(Dispatched_Date AS DATE) BETWEEN '$this_month_start' AND '$this_month_end' AND PT > '0'";
    }
    $full_sql = $sql_start . $sql;
    $query = odbc_prepare($conn,$full_sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $total = $row['Total'];
    if($total === NULL) {
        $total = 'N/A';
    }
    return $total;
}
// Get values for each chart point
function createChartData($kpi, $user, $date) {
    global $conn;
    if ($kpi === 'loadsPerBroker') { // Loads per Broker (Red Chart)
        $sql    = "SELECT COUNT(DISTINCT(Movement_ID)) AS Loads
            FROM Broker_KPI 
            WHERE BrokerID = '$user' AND CAST(Dispatched_Date AS DATE) = '$date' AND PT > '0'";
        $column = 'Loads';
    } elseif ($kpi === 'loadsPerCarrier') { // Loads per Carrier (Yellow Chart)
        $sql    = "SELECT COUNT(DISTINCT(Movement_ID))/COUNT(DISTINCT(Payee_ID)) AS Loads
            FROM Broker_KPI 
            WHERE BrokerID = '$user' AND CAST(Dispatched_Date AS DATE) = '$date' AND PT > '0'";
        $column = 'Loads';
    } elseif ($kpi === 'marginPercent') { // Margin % (Green Chart)
        $sql    = "SELECT CAST((SUM(Margin)/SUM(Revenue))*100 AS DECIMAL(18,1)) AS Percentage
            FROM Broker_KPI 
            WHERE BrokerID = '$user' AND CAST(Dispatched_Date AS DATE) = '$date' AND PT > '0'";
        $column = 'Percentage';
    } elseif ($kpi === 'marginDollar') { // Margin $ (Blue Chart)
        $sql = "SELECT SUM(Margin) AS Margin
            FROM Broker_KPI 
            WHERE BrokerID = '$user' AND CAST(Dispatched_Date AS DATE) = '$date' AND PT > '0'";
        $column = 'Margin';
    } elseif ($kpi === 'buyMarketRate') { // Buy v Market Rate (Purple Chart)
        $sql    = "SELECT CAST(((SUM(Carrier_Pay) - SUM(Market))/SUM(Market))*100 AS DECIMAL(18,1)) AS Market_Rate
            FROM Broker_KPI 
            WHERE BrokerID = '$user' AND CAST(Dispatched_Date AS DATE) = '$date' AND PT > '0'";
        $column = 'Market_Rate';
    }
    $query = odbc_prepare($conn, $sql);
    odbc_execute($query);
    $row   = odbc_fetch_array($query);
    $data  = $row[$column];
    if($data === NULL) {
        $data = 0;
    }
    return $data;
}
////////////////////////////////////// MCLEOD BACKUP TOOL ////////////////////////////////////////////////////
// Check if user has loadboard profile assigned to their McLeod account
function getProfile($user_id) {
    global $conn_mcld;
    $sql = "SELECT brokerage_planning FROM users WHERE id = '$user_id'";
    $query = odbc_prepare($conn_mcld, $sql);
    odbc_execute($query);
    $row = odbc_fetch_array($query);
    $profile = $row['brokerage_planning'];
    if($profile === NULL) {
        $profile = 'DEFAULT';
    }
    return $profile;
}
// Get user loadboard settings for brokerage planning profile
function getSettings($profile) {
    global $conn_mcld;
    $array[]          = '';
    $sql              = "SELECT order_ob_zones,revenue_code,brokerage_status FROM brokerage_planning WHERE id = '$profile'";
    $query            = odbc_prepare($conn_mcld, $sql);
    odbc_execute($query);
    $row              = odbc_fetch_array($query);
    /* Save each piece into an array to be called later as filters when pulling loads */
    $array['zones']   = $row['order_ob_zones'];
    $array['revenue'] = $row['revenue_code'];
    $array['status']  = $row['brokerage_status'];
    return $array;
}
// Select order details from mcleod database
function getOrderDetails($order_id) {
    global $conn_mcld;
    $sql = "SELECT O.id AS Order_ID,
                O.freight_charge AS Freight_Charge,
                M.brokerage_status AS Brokerage_Status,
                M.status AS Move_Status,
                P.name AS Carrier,
                O.order_type_id AS Order_Type,
                OS.city_name AS Origin_City,
                OS.state AS Origin_State,
                DS.city_name AS Destin_City,
                DS.state AS Destin_State,
                O.bill_distance AS Distance,
                O.customer_id,
                O.operations_user AS Ops_User,
                OS.location_id AS Ship_ID,
                OS.location_name AS Shipper_Name,
                DS.location_id AS Cons_ID,
                DS.location_name AS Consignee_Name,
                O.commodity AS Commodity,
                O.hazmat AS Hazmat,
                O.revenue_code_id AS Revenue,
                O.weight AS Weight,
                O.pieces AS Pieces,
                OS.zone_id AS Origin_Zone,
                OS.zip_code AS Origin_Zip,
                DS.zone_id AS Dest_Zone,
                DS.zip_code AS Dest_Zip,
                O.curr_movement_id AS Movement,
                O.otherchargetotal AS Other_Charges,
                O.total_charge AS Total_Charge,
                O.pay_gross AS Total_Pay,
                O.blnum AS Blnum,
                O.consignee_refno AS Consignee_refno,
                O.equipment_type_id AS Equipment,
                OS.sched_arrive_early AS Early_Pickup,
                OS.sched_arrive_late AS Late_Pickup,
                DS.sched_arrive_early AS Early_Delivery,
                DS.sched_arrive_late AS Late_Delivery,
                O.ordered_date AS Ordered_Date,
                O.planning_comment AS Planning_Comment,
                M.carrier_contact AS Carrier_Contact,
                M.carrier_phone AS Carrier_Phone,
                M.carrier_email AS Carrier_Email,
                O.carrier_refno AS Carrier_Refno,
                OS.location_id AS Origin_Location_ID,
                RTRIM(OS.address) + ', ' + RTRIM(OS.city_name) + ', ' + RTRIM(OS.state) + ', ' + OS.zip_code AS Origin_Address,
                RTRIM(DS.address) + ', ' + RTRIM(DS.city_name) + ', ' + RTRIM(DS.state) + ', ' + DS.zip_code AS Destin_Address,
                DS.location_id AS Destin_Location_ID,
                OC.name AS Pickup_Contact,
                OC.phone AS Pickup_Phone,
                OC.mobile_phone AS Pickup_Mobile,
                OC.email AS Pickup_Email,
                DC.name AS Destin_Contact,
                DC.phone AS Destin_Phone,
                DC.mobile_phone AS Destin_Mobile,
                DC.email AS Destin_Email,
                Origin_PO_Ref.reference_number AS Origin_PO_Ref,
                Origin_PU_Ref.reference_number AS Origin_PU_Ref,
                M.operations_user AS Ops_User,
                M.dispatcher_user_id AS Dispatcher
        FROM orders O
        LEFT JOIN movement M
                ON M.id = O.curr_movement_id
        LEFT JOIN stop OS
                ON OS.id = M.origin_stop_id AND OS.movement_id = M.id
        LEFT JOIN stop DS
                ON DS.id = M.dest_stop_id AND DS.movement_id = M.id
        LEFT JOIN contact OC
                ON OC.parent_row_id = OS.location_id
        LEFT JOIN contact DC
                ON DC.parent_row_id = DS.location_id
        LEFT JOIN reference_number Origin_PO_Ref
                ON Origin_PO_Ref.stop_id = OS.id AND Origin_PO_Ref.reference_qual = 'PO'
        LEFT JOIN reference_number Origin_PU_Ref
                ON Origin_PU_Ref.stop_id = OS.id AND Origin_PU_Ref.reference_qual = 'PU'
        LEFT JOIN payee P
                ON P.id = M.override_payee_id
        WHERE O.id = '$order_id'";
    $query = odbc_prepare($conn_mcld, $sql);
    odbc_execute($query);
    $details[]                   = '';
    $details['o_contact_info'][] = '';
    $details['d_contact_info'][] = '';
    $details['po_ref'][]         = '';
    $details['pu_ref'][]         = '';
    while($row = odbc_fetch_array($query)) {
        $details['movement_id']    = $row['Movement'];
        $details['freight_charge'] = $row['Freight_Charge'];
        $details['bol']            = $row['Blnum'];
        $details['b_status']       = $row['Brokerage_Status'];
        $details['equipment']      = $row['Equipment'];
        $details['m_status']       = $row['Move_Status'];

        $details['dispatcher'] = $row['Dispatcher'];
        $details['ops']        = $row['Ops_User'];
        $details['miles']      = $row['Distance'];
        $details['order_type'] = $row['Order_Type'];
        $details['revenue']    = $row['Revenue'];

        $details['carrier']         = $row['Carrier'];
        $details['carrier_contact'] = $row['Carrier_Contact'];
        $details['carrier_phone']   = $row['Carrier_Phone'];
        $details['carrier_email']   = $row['Carrier_Email'];
        $details['carrier_refno']   = $row['Carrier_Refno'];

        $details['shipper']      = $row['Shipper_Name'];
        $details['o_address']    = $row['Origin_Address'];
        $details['o_contact']    = $row['Pickup_Contact'];
        $details['o_phone']      = $row['Pickup_Phone'];
        $details['o_mobile']     = $row['Pickup_Mobile'];
        $details['comment']      = $row['Planning_Comment'];
        $details['early_pickup'] = $row['Early_Pickup'];
        $details['late_pickup']  = $row['Late_Pickup'];

        $details['consignee']      = $row['Consignee_Name'];
        $details['d_address']      = $row['Destin_Address'];
        $details['d_contact']      = $row['Destin_Contact'];
        $details['d_phone']        = $row['Destin_Phone'];
        $details['d_mobile']       = $row['Destin_Mobile'];
        $details['d_mobile']       = $row['Destin_Mobile'];
        $details['d_refno']        = $row['Consignee_refno'];
        $details['early_delivery'] = $row['Early_Delivery'];
        $details['late_delivery']  = $row['Late_Delivery'];

        $details['commodity']     = $row['Commodity'];
        $details['hazmat']        = $row['Hazmat'];
        $details['weight']        = $row['Weight'];
        $details['pieces']        = $row['Pieces'];
        $details['other_charges'] = $row['Other_Charges'];
        $details['total_charge']  = $row['Total_Charge'];
        $details['total_pay']     = $row['Total_Pay'];

        $details['o_contact_info'][] .= $row['Pickup_Contact'] . ' ' . $row['Pickup_Phone'] . ' ' . $row['Pickup_Mobile'] . ' ' . $row['Pickup_Email'];
        $details['d_contact_info'][] .= $row['Destin_Contact'] . ' ' . $row['Destin_Phone'] . ' ' . $row['Destin_Mobile'] . ' ' . $row['Destin_Email'];
        $details['po_ref'][]         .= $row['Origin_PO_Ref'];
        $details['pu_ref'][]         .= $row['Origin_PU_Ref'];
    }
    $details['o_contact_string'] = ltrim((implode("<br/>", $details['o_contact_info'])), ", ");
    $details['d_contact_string'] = ltrim((implode("<br/>", $details['d_contact_info'])), ", ");
    $details['po_ref_string']    = ltrim((implode(", ", $details['po_ref'])), ", ");
    $details['pu_ref_string']    = ltrim((implode(", ", $details['pu_ref'])), ", ");
    return $details;
}

function getContactInfo($order_id,$type) {
    global $conn_mcld;
    $select_location = "SELECT location_id FROM stop WHERE order_id = '$order_id' AND stop_type = '$type'";
    $get_location = odbc_prepare($conn_mcld, $select_location);
    odbc_execute($get_location);
    while($row = odbc_fetch_array($get_location)) {
        $location_id = $row['location_id'];
    }
    $sql = "SELECT * FROM contact 
        WHERE parent_row_type ='L' AND parent_row_id ='$location_id' AND is_active = 'Y'
        ORDER BY sequence";
    $get = odbc_prepare($conn_mcld, $sql);
    odbc_execute($get);
    $contact_info[] = '';
    while($row2 = odbc_fetch_array($get)) {
        $contact_info['details'][] .= $row2['name'] . ' ' . $row2['phone'] . ' ' . $row2['mobile_phone'] . ' ' . $row2['email'];
    }
    $contact_info['contact_string'] = ltrim((implode("<br/>", $contact_info['details'])),", ");
    return $contact_info;
}
///////////////////////////////////// WELCOME PAGE NEWS UPDATES ////////////////////////////////////////////////////////////////////
function createNewsPanel($admin,$id,$title,$author,$posted,$newsstring,$color) {
    if($admin) {
        $delete = "<a class='btn btn-danger' href='index.php?id=$id' title='Delete'><i class='fa fa-trash-o'></i></a>";
    } else {
        $delete = '';
    }
    $author = getFullName($author);
    echo "<div class='col-lg-offset-2 col-lg-8'>
            <div class='panel panel-$color'>
                <div class='panel-heading'>
                    <div class='panel-title'><i class='fa fa-newspaper-o'></i> $title 
                        <span class='pull-right'>Posted by: $author on $posted $delete
                        </span></div>
                </div>
                <div class='panel-footer'>
                    <span class='pull-left' style='color:$color;'><h4>".$newsstring."</h4></span>
                    <span class='pull-right'></span>
                    <div class='clearfix'></div>
                </div>
            </div>
          </div>";
}
///////////////////////////////////// TRANSIT CALCULATOR ///////////////////////////////////////////////////////
// Create COM object to connect to PC*Miler API functions and calculate distance between origin and destination
function getMileage($origin, $destin) {
    try {
        $pcms = new COM("PCMServer.PCMServer");
        $dist = $pcms->CalcDistance("$origin", "$destin");
    } catch (com_exception $e) {}
    $properDist = ($dist / 10);
    return $properDist;
}