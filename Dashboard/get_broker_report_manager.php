<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Manager';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'tund';
}
$office = getOffice($user_id);
// Get active commission paystructure records and store in array
function getRecords($office) {
    global $conn;
    $string = "SELECT order_id, 
                movement_id, 
                movement_margin, 
                processing_status, 
                bol_received,
                CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                CONVERT(DATE, Delivery_Date) AS Delivery_Date,
                override_payee_id,
                Dispatcher_User,
                CONVERT(DATE, Pay_Date) AS Pay_Date
            FROM BC_Paid_Details
            WHERE office_code = '$office' AND is_active = 'Y'
                UNION 
                select order_id, 
                movement_id, 
                movement_margin, 
                processing_status, 
                bol_received ,
                CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                CONVERT(DATE, Delivery_Date) AS Delivery_Date,
                override_payee_id,
                Dispatcher_User,
                null AS Pay_Date
            FROM BC_Potential_Details
            WHERE office_code = '$office' AND is_active = 'Y'
                UNION
                select order_id, 
                movement_id, 
                movement_margin, 
                processing_status, 
                bol_received ,
                CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                CONVERT(DATE, Delivery_Date) AS Delivery_Date,
                override_payee_id,
                Dispatcher_User,
                null AS Pay_Date
            FROM BC_Pending_Pay_Details
            WHERE office_code = '$office' AND is_active = 'Y'";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getRecords($user_id);
header('Content-Type: application/json');
echo json_encode($data);