<?php
require 'include_functions.php';

function getData() {
    global $conn;
    $string = "SELECT TOP 1000 
        Category,
        Kma,
        Legal_Name,
        Carrier_Mc,
        Broker_Mc,
        Total_Cnt,
        Hq_State,
        Main_Phone,
        Power_Units,
        Dest_Kma,
        Dest_Perc_Lane
  FROM css_vans_test";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getData();
header('Content-Type: application/json');
echo json_encode($data);