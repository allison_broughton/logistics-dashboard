<?php
require 'include_functions.php';
// Get Order ID from URL or form input
if (isset($_GET['OID'])) {
    $order_id = $_GET['OID'];
} else {
    $order_id = NULL;
}
// Get order info to use in getRecords function
function getOrderInfo($order_id) {
    global $conn_mcld;
    $sql = "SELECT 
                o.id AS id, 
                Concat(RTRIM(s1.city_name), ',', s1.state) AS origin, 
                LEFT(s1.zip_code, 3) AS ozip, 
                Concat(RTRIM(s2.city_name), ',', s2.state) AS destin, 
                LEFT(s2.zip_code, 3) AS dzip, 
                RTRIM(o.customer_id) AS customerid, 
                RTRIM(o.equipment_type_id) AS equip
         FROM   orders AS o 
                LEFT JOIN stop AS s1 
                       ON o.shipper_stop_id = s1.id 
                LEFT JOIN stop AS s2 
                       ON o.consignee_stop_id = s2.id 
         WHERE  o.id = '$order_id'";
    $query        = odbc_prepare($conn_mcld, $sql);
    odbc_execute($query);
    $order_info[] = '';
    while ($row          = odbc_fetch_array($query)) {
        $order_info['id']          = $row['id'];
        $order_info['origin']      = $row['origin'];
        $order_info['ozip']        = $row['ozip'];
        $order_info['destination'] = $row['destin'];
        $order_info['dzip']        = $row['dzip'];
        $order_info['customerid']  = $row['customerid'];
        $order_info['equip']       = $row['equip'];
    }
    return $order_info;
    //return $sql;
}
$order_info = getOrderInfo($order_id);
// Get routing guide records using order info
function getRecords($order_info) {
    global $conn_dev;
    if ($order_info['equip'] === 'V') {
        $trailer_type = "(trailer_type = 'V' OR trailer_type = 'R')";
    } else {
        $trailer_type = "trailer_type = '" . $order_info['equip'] . "'";
    }
    $string = "SELECT * 
        FROM   dm_warrenrouting 
        WHERE  $trailer_type
               AND customer_id = '".$order_info['customerid']."' 
               AND ((origin LIKE '".$order_info['origin']."' 
               AND destination LIKE '".$order_info['destination']."') 
                OR (origin LIKE '%".$order_info['ozip']."%' 
               AND destination LIKE '%".$order_info['dzip']."%')) 
        ORDER BY trailer_type DESC, 
                lane_type ASC, 
                carrier_commit_id ASC, 
                rank ASC";
    $stmt   = odbc_prepare($conn_dev, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row    = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
    //return $string;
}

$data = getRecords($order_info);
header('Content-Type: application/json');
echo json_encode($data);
//echo $data;
//echo $order_info;