<?php
require 'include_functions.php';
require 'include_navbar.php';
///////////////////// Initialize Variables ////////////////////////////////////////////
$title             = 'Modifier';
$page_id           = 'Spot_Rate_Modifier';
$page_access = getPageAccess($user_id, $page_id);
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
//////////////////// Form Submission //////////////////////////////////////////////////
if (isset($_POST['submit'])) {
    $message = "This page has been frozen due to inactivity. Please use the E-Mail Support
        button above to request that it be unlocked.";
    /*$origin     = sanitize($_POST['origin']);
    $destin     = sanitize($_POST['destination']);
    $equipment  = sanitize($_POST['equipment']);
    $value      = sanitize($_POST['value']);
    $uom        = sanitize($_POST['uom']);
    $effective  = sanitize($_POST['effective']);
    $expiration = sanitize($_POST['expiration']);
    $comment    = sanitize($_POST['comment']);

    // Validate cities first
    $valid_origin = fuzzySearch($origin);
    $valid_destin = fuzzySearch($destin);

    // If one or both cities were not found, kill the script and alert user
    if ((strpos($valid_origin, 'No cities') !== false) || (strpos($valid_destin, 'No cities') !== false)) {
        $alert_type = 'alert-warning';
        $message    = "No cities found. Please check the spelling and state abbreviation(s).";
    } else {
        // continue
        $alert_type = 'alert-success';
        // Look up KMA Lane
        $kma_origin = getMarket($valid_origin);
        $kma_destin = getMarket($valid_destin);
        // Check to see if any markets exist for the chosen origin-destination pair
        if ((strpos($oMarket, 'No market') !== false) || (strpos($dMarket, 'No market') !== false)) {
            $alert_type = 'alert-warning';
            $message    = "NO LANE FOUND";
        } else {
            $kma_lane = $kma_origin . "-" . $kma_destin;
            $insert   = "INSERT INTO Spot_Modifier (
                KMA_KMA_Lane,Equipment_Type,Value,UOM,Effective_Date,Expiration_Date,Comment,Entered_By) 
                VALUES ('$kma_lane','$equipment','$value','$uom','$effective','$expiration','$comment','$user_id')";
            $query    = odbc_prepare($conn, $insert);
            $result   = odbc_execute($query);
            if (!$result) {
                $message_header = "Insert query failed: " . odbc_errormsg($conn);
            } else {
                $message_header = "Inserted modifier successfully!";
            }
        }
    }*/
}
//////////////////////////////////// Get data rows for table //////////////////////////////////
function getSpotMods() {
    global $conn;
    $select_mods = "SELECT * FROM Spot_Modifier";
    $get_mods = odbc_prepare($conn, $select_mods);
    odbc_execute($get_mods);
    $spot_mods = "";
    while($row = odbc_fetch_array($get_mods)) {
        $spot_mods .= "<form method='POST' action=''><tr>
            <td>" . $row['KMA_KMA_Lane'] . "</td>
            <td><select name='equipment'>
                    <option selected value='".$row['Equipment_Type']."'>".$row['Equipment_Type']." (Current)</option>
                    <option value='D'>Dry Van</option>
                    <option value='R'>Reefer</option>
                    <option value='F'>Flat Bed</option>
                </select></td>
            <td><input type='number' name='value' value='".$row['Value']."' /></td>
            <td><select name='UOM'>
                    <option selected value='".$row['UOM']."'>".$row['UOM']." (Current)</option>
                    <option value='P'>Percent</option>
                    <option value='F'>Flat</option>
                    <option value='R'>Rate</option>
                </select></td>
            <td><input type='date' name='effective' value='".$row['Effective_Date']."' /></td>
            <td><input type='date' name='expiration' value='".$row['Expiration_Date']."' /></td>
            <td><input type='text' name='comment' value='".$row['Comment']."' /></td>
            <td>".$row['Entered_By']."</td>
            <input type='hidden' name='Mod_ID' value='".$row['Mod_ID']."' />
            <td><input type='submit' value='Edit' name='edit_mod' class='btn btn-success'/>
                <input type='submit' value='Delete' name='delete_mod' class='btn btn-danger' DISABLED/></td>
            </tr></form>";
    } return $spot_mods;
}
$spot_mods = getSpotMods();
//////////////////////////////////// Edit row form submission /////////////////////////////////////
if (isset($_POST['edit_mod'])) {
    $row_id = sanitize($_POST['Mod_ID']);
    $equipment = sanitize($_POST['equipment']);
    $value = sanitize($_POST['value']);
    $uom = sanitize($_POST['UOM']);
    $effective = sanitize($_POST['effective']);
    $expiration = sanitize($_POST['expiration']);
    $comment = sanitize($_POST['comment']);
    
    $edit_string = "UPDATE Spot_Modifier SET
        Equipment_Type = '$equipment',
        Value = '$value',
        UOM = '$uom',
        Effective_Date = '$effective',
        Expiration_Date = '$expiration',
        Comment = '$comment' WHERE Mod_ID = '$row_id'";
    $edit_query = odbc_prepare($conn, $edit_string);
    $edit_result = odbc_execute($edit_query);
    if(!$edit_result) {
        $message = "Query Failed: " . odbc_errormsg($conn) . "<br/>";
    } else {
        $message = "Query was successful.<br/>";
    }
    $message .= $edit_string;
}
//////////////////////////////////// Delete row form submission /////////////////////////////////////
/*Delete buttons are disabled until we add column 'active' to Spot_Modifier table
* Then we can deactivate the record rather than actually delete it */
if (isset($_POST['delete_mod'])) {
    $row_id = sanitize($_POST['Mod_ID']);
    $delete_string = "DELETE FROM Spot_Modifier WHERE Mod_ID = '$row_id'";
    $message = $delete_string;
}
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <style>
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #add_mod {
                color: #fff;
                border-color: #005590;/*blue*/
                background-color: #005590
            }
            #view_mods {
                color: #fff;
                border-color: #5CB85C;/*green*/
                background-color: #449D44
            }
            #adj_status {
                color: #fff;
                border-color: #F0AD4E;/*yellow*/
                background-color: #F0AD4E
            }
            table {
                /*display: block;*/
                table-layout: fixed;
                max-height: 500px;
                /*overflow-x: auto;
                overflow-y: auto;*/
            }
            th,td {
                column-width: 100px;
                /*min-width: 100px;
                max-width: 175px;*/
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!--Check user permissions-->
                    <?php if ($page_access) { 
                        if(isset($message)) {
                            echo "<pre>" . $message . "</pre>";
                        }?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Spot Rate Modifiers<br/>
                                    <small></small>
                                </h1><?php echo $dev_server;?>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#add_mod">Add New Modifier</button>
                                <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#view_mods">View All Modifiers</button>
                                <!--Blue Panel-->
                                <div id="add_mod" class="div-content collapse in">
                                    <h1 class="panel-heading">Add New Modifier
                                        <small>Enter modifier to modify spot rate for chosen date range.</small>
                                    </h1>
                                    <div class="panel-footer">
                                            <?php if (isset($_POST['submit'])) { ?>
                                            <div class="alert <?php echo $alert_type; ?> alert-dismissable">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <strong><?php echo $message_header; ?></strong><br/>
                                            <?php echo $message; ?>
                                            </div>
                                            <?php } ?>
                                        <form method="POST" action="" class="form-horizontal">

                                            <div class="form-group">
                                                <label for="origin" class="control-label col-xs-3">Lane Origin: </label>
                                                <div class="col-xs-9">
                                                    <input type="text" name="origin" id="origin" required="true" placeholder="City, ST" autofocus/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="destination" class="control-label col-xs-3">Lane Destination: </label>
                                                <div class="col-xs-9">
                                                    <input type="text" name="destination" id="destination" required="true" placeholder="City, ST" autofocus/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="equipment" class="control-label col-xs-3">Equipment Type: </label>
                                                <div class="col-xs-9">
                                                    <select name="equipment" id="equipment" required="true">
                                                        <option value="" selected disabled>Please Choose...</option>
                                                        <option value="V">Dry Van</option>
                                                        <option value="R">Reefer</option>
                                                        <option value="F">Flat Bed</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="value" class="control-label col-xs-3">Value: </label>
                                                <div class="col-xs-9">
                                                    <input type="number" name="value" id="value" required="true"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="uom" class="control-label col-xs-3">Unit of Measure: </label>
                                                <div class="col-xs-9">
                                                    <select name="uom" id="uom" required="true">
                                                        <option value="" selected disabled>Please Choose...</option>
                                                        <option value="P">Percent</option>
                                                        <option value="F">Flat</option>
                                                        <option value="R">Rate</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="effective" class="control-label col-xs-3">Effective Date: </label>
                                                <div class="col-xs-9">
                                                    <input type="date" name="effective" id="effective" required="true"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="expiration" class="control-label col-xs-3">Expiration Date: </label>
                                                <div class="col-xs-9">
                                                    <input type="date" name="expiration" id="expiration" required="true"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="comment" class="control-label col-xs-3">Comment: </label>
                                                <div class="col-xs-9">
                                                    <textarea name="comment" id="comment" rows="2" cols="25" maxlength="255"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-offset-1 col-xs-11">
                                                    <button type="submit" name="submit" class="btn btn-primary col-lg-offset-2">Submit</button>
                                                </div>
                                            </div>

                                        </form>
                                    <?php
                                    if (isset($_POST['submit'])) {
                                        echo "<div class='alert alert-info'>
                                        <strong>You Entered:</strong> <br/><br/>
                                            <strong>KMA Lane: </strong>$kma_lane <br/>
                                            <strong>Equipment: </strong>$equipment <br/>
                                            <strong>Value: </strong>$value <br/>
                                            <strong>UOM: </strong>$uom <br/>
                                            <strong>Effective Date: </strong>$effective <br/>
                                            <strong>Expiration Date: </strong>$expiration <br/>
                                            <strong>Comment: </strong>$comment <br/>
                                          </div>";}?>
                                    </div>
                                </div>
                                <!--Green Panel-->
                                <div id="view_mods" class="div-content collapse">
                                    <h1 class="panel-heading">View All Modifiers
                                        <small>Review and edit modifiers</small>
                                    </h1>
                                    <div class="panel-footer">
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>KMA_KMA_Lane</th>
                                            <th>Equipment_Type</th>
                                            <th>Value</th>
                                            <th>UOM</th>
                                            <th>Effective_Date</th>
                                            <th>Expiration_Date</th>
                                            <th>Comment</th>
                                            <th>Entered_By</th>
                                            <th><i class="fa fa-floppy-o fa-lg" aria-hidden="true"></i></th>
                                            </thead>
                                            <?php echo $spot_mods;?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <?php } else {
                            echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                        }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

    </body>
</html>
