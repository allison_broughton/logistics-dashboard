<?php
require 'include_functions.php';
// Insert user info to database to track page visits
if (!($user_id === 'localhost\DEV')) {
    trackVisit('BC_Leaderboard', $user_id, 'page_visit');
}
$title  = 'Office Leaderboard';
$office = getOffice($user_id);
// Emulate user
if ($user_id === 'stral' || $user_id === 'localhost\DEV' || $user_id === 'labin' || $user_id === 'pitcn' || $user_id === 'everd') {
    $user_id = 'tund';
    $office  = getOffice($user_id);
}
//////////////////////////////////////////////// CALCULATE OFFICE AGGREGATE DETAILS ///////////////////////////////////////////
/*$my_current_margin    = getCommissionLevel($limiter);
$current_gross_margin = getCurrentGrossMargin($limiter);
$goal_to_level_up     = getLevelUp($limiter);
// Define max bar height once level 10 is reached
if ($my_current_margin >= 10) {
    $set_max_y_axis = 'high: 375000';
} else {
    $set_max_y_axis = '';
}*/
// Stacked Bar Chart Labels/Values
$office_array         = getOfficeLocations();
$bar_labels           = implode("','", $office_array);
$broker_margin_array  = array_map("getBrokerMargin", $office_array);
$broker_level_array   = array_map("getBrokerLevel", $office_array);
//$bar_series           = createChartValues($broker_margin_array, 'Current Margin', $broker_level_array);
$bar_series_1         = "{meta: 'Aaron Campbell', value: '125000' },
    {meta: 'Chris Bender', value: '296000' },
    {meta: 'Shiloh Johnson', value: '98000' },
    {meta: 'Steven Hudyka', value: '109000' }";
$broker_levelup_array = array_map("getBrokerLevelUp", $office_array);
//$stacked_bar_series   = createChartValues2($broker_levelup_array, 'Margin to Next Level');
$bar_series_2 = "{meta: 'Jason Carter', value: '56000' },
    {meta: 'Dominic Filardi', value: '42000' },
    {meta: '', value: '0' },
    {meta: 'Travis Ortensi', value: '94000' }";
$bar_series_3 = "{meta: 'Cory Durham', value: '35000' },
    {meta: 'Richard Nguyen', value: '52000' },
    {meta: '', value: '0' },
    {meta: 'Jeff Sienkiewicz', value: '104000' }";
$bar_series_4 = "{meta: 'Josh Guidry', value: '161000' },
    {meta: 'Jeffrey Stein', value: '106000' },
    {meta: '', value: '0' },
    {meta: '', value: '0' }";
$bar_series_5 = "{meta: 'Dylan Sprague', value: '85000' },
    {meta: 'Israel Torres', value: '118000' },
    {meta: '', value: '0' },
    {meta: '', value: '0' }";
///////////////////////////////// SWITCHED TO CHART.JS LIBRARY TO HAVE MORE CONTROL OVER BAR COLORS //////////////////////////////
$background_colors = "'#0d254c',
                      '#008000',
                      '#c21807',
                      '#cc5500'";
$border_colors = "'#091935',
                  '#005900',
                  '#871004',
                  '#8E3B00'";
$bar_dataset_1 = "125000, 296000, 98000, 109000";
$bar_dataset_2 = "56000, 42000, 0, 94000";
$bar_dataset_3 = "35000, 52000, 0, 104000";
$bar_dataset_4 = "161000, 106000, 0, 0";
$bar_dataset_5 = "85000, 118000, 0, 0";
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Swift Logistics | <?php echo $title; ?></title>
        <link rel="icon" type="image/ico" href="images/swift_logistics_logo_circle.ico"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($username === 'localhost\DEV') {
                        echo "<pre>Current day: <strong>" . $today_numerical . "</strong> Current month: <strong>" . $current_month . "</strong></pre>";
                    }
                    if ($user_id === 'tund' || $user_id === 'stobm' || $user_id === 'lowst' ||
                            $user_id === 'ginem' || $user_id === 'scottri' || $user_id === 'uriav' || $user_id === 'bustro') {
                        ?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Office Leaderboard<br/>
                                </h1>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <!--Chartist Stacked Chart-->
                                <div id="leaderboard">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-bar-chart "></i> Gross Margin by Office</h3>
                                    </div>
                                    <div class="panel-body">
                                        <canvas id="officeLeaderboard" width="200" height="75"></canvas>
                                        <!--<div class="ct-chart morris-default-style"></div>-->
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-left">*Estimated based on orders dispatched period to date</span>
                                        <span class="pull-right"></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <?php
                        } else {
                            echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                        }
                        ?>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <!--jQuery Temp Gauge Plugin-->
        <!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>-->
        <script type="text/javascript" src="js/jquery.tempgauge.js"></script>
        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>
        
        <script>
            var ctx = document.getElementById("officeLeaderboard");
            var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                labels: [["NWA","broker1","broker2","broker3","broker4","broker5"], ["PHX"], ["SLC"], ["WMA"]],
                datasets: [{
                    label: "",
                    data: [<?php echo $bar_dataset_1;?>],
                    backgroundColor: [
                      <?php echo $background_colors;?>
                    ],
                    borderColor: [
                      <?php echo $border_colors;?>
                    ],
                    borderWidth: 2
                  },
                  {
                    label: "",
                    data: [<?php echo $bar_dataset_2;?>],
                    backgroundColor: [
                      <?php echo $background_colors;?>
                    ],
                    borderColor: [
                      <?php echo $border_colors;?>
                    ],
                    borderWidth: 2
                  },
                  {
                    label: "",
                    data: [<?php echo $bar_dataset_3;?>],
                    backgroundColor: [
                      <?php echo $background_colors;?>
                    ],
                    borderColor: [
                      <?php echo $border_colors;?>
                    ],
                    borderWidth: 2
                  },
                  {
                    label: "",
                    data: [<?php echo $bar_dataset_4;?>],
                    backgroundColor: [
                      <?php echo $background_colors;?>
                    ],
                    borderColor: [
                      <?php echo $border_colors;?>
                    ],
                    borderWidth: 2
                  },
                  {
                    label: "",
                    data: [<?php echo $bar_dataset_5;?>],
                    backgroundColor: [
                      <?php echo $background_colors;?>
                    ],
                    borderColor: [
                      <?php echo $border_colors;?>
                    ],
                    borderWidth: 2
                  }
                ]
              },
              options: {
                legend: {
                  display: false
                },
                scales: {
                  yAxes: [{
                    stacked: true,
                    ticks: {
                      beginAtZero: true,
                      callback: function(value) {
                          return '$' + (value / 1000) + 'k';
                      }
                    }
                  }],
                  xAxes: [{
                    stacked: true,
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                }/*,
                tooltips: {
                  custom: function(tooltipModel) {
                    
                  }
                }*/
              }
            });
        </script>
        
        <script>
            new Chartist.Bar('.ct-chart', {
                labels: ['<?php echo $bar_labels; ?>'],
                series: [
                    [
                        <?php echo $bar_series_1;?>
                    ],
                    [
                        <?php echo $bar_series_2;?>
                    ],
                    [
                        <?php echo $bar_series_3;?>
                    ],
                    [
                        <?php echo $bar_series_4;?>
                    ],
                    [
                        <?php echo $bar_series_5;?>
                    ]
                ]
            },
                {
                    fullWidth: true,
                    plugins: [
                        Chartist.plugins.tooltip({
                            currency: '$',
                            anchorToPoint: false,
                        }),
                        Chartist.plugins.ctPointLabels({
                            textAnchor: 'middle'
                        })
                    ],
                    stackBars: true,
                    /*distributeSeries: true,*/
                    axisY: {
                        labelInterpolationFnc: function (value) {
                            return '$' + (value / 1000) + 'k';
                        },
                    <?php echo $set_max_y_axis; ?> /*Set this value to define a max value for the bar(s) */
                    }
                }).on('draw', function (data) {
                    if (data.type === 'bar') {
                        data.element.attr({
                            style: 'stroke-width: 100px'
                        });
                    }
                });
        </script>
    </body>
</html>