<?php
if(!isset($_SESSION)) {
    session_start();
}
/*if (!isset($_SESSION['username'])) {
    header("location:login/main_login.php");
}*/
require 'include_navbar.php';
require 'include_functions.php';

$title = "Tableau";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>LOOP | <?php echo $title;?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Data Tables -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!--Tableau-->
        <script type="text/javascript" src="https://public.tableau.com/javascripts/api/tableau-2.js"></script>
        <script>
            var viz;
            window.onload = function() {
                var vizDiv = document.getElementById('myViz');
                var vizURL = "https://public.tableau.com/views/SharknadoRecreationIronVIz/Sharknado?:embed=y&:loadOrderID=0&:display_count=yes";
                var options = {
                    width: '1200px',
                    height: '900px'
                };
                viz = new tableau.Viz(vizDiv, vizURL, options);
            };
            
            // Content below is to resolve warning for missing 'tableau' object
            var tableau;
        </script>
    </head>

    <body>

        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Tableau <small>Embedded Viz</small>
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->

                    <!-- DataTable -->
                    <div class="col-xs-12">
                        
                        <div id="myViz"></div>
                        
                    </div><!-- /.DataTable -->
                </div><!-- /.container-fluid-->
            </div> <!-- /.page-wrapper-->
            <div id="footer"></div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script>

            $(document).ready(function () {
                //Add text input
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });

                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    "autoWidth": false,
                    "searching": true,
                    "dom": '<l<t>ip>',
                    "ajax": "/get_orders.php",
                    "columns": [
                        {"data": "Order_ID"},
                        {"data": "PickupTime"},
                        {"data": "Origin_city"},
                        {"data": "Origin_State"},
                        {"data": "Dest_City"},
                        {"data": "Dest_State"},
                        {"data": "Miles"},
                        {"data": "Trailer_Type"},
                        {"data": "Broker_Name"},
                        {"data": "Broker_Phone"},
                        {"data": "Broker_Email"},
                        {"data": "DeliveryTime"},
                        {"data": "Additional_Stops"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0}
                    ]
                });
                setInterval( function () {							
			table.ajax.reload( null, false ); // user paging is not reset on reload
		}, 30000 );
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });
            });//end tag
        </script>
    </body>
</html>