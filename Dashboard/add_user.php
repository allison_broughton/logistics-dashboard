<?php
session_start();
require 'include_functions.php';
require 'include_navbar.php';
/////////////////////////////////////////// Initialize Variables ////////////////////////////////////////////
$title   = "User Management";
$page_id = "User_Management";
///// Clean up records /////
/*$sql = "UPDATE Web_Page_Visits SET User_ID = 'stral' WHERE VisitID = '1378'";
$query = odbc_prepare($conn, $sql);
odbc_execute($query);*/
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
$page_access = getPageAccess($user_id, $page_id);
// Get data for table rows
function getUserRoles() {
    global $conn;
    $select = "SELECT * FROM Web_Page_Roles ORDER BY User_ID,Page_ID ASC";
    $get    = odbc_prepare($conn, $select);
    odbc_execute($get);
    $user_roles = "";
    while ($row = odbc_fetch_array($get)) {
        $user_roles .= "<form name='edit_user' method='POST' action=''><tr>
                <td><input type='text' name='page' value='" . $row['Page_ID'] . "'/></td>
                <td>" . $row['User_ID'] . "</td>
                <td><input type='text' name='office' value='" . $row['Office_Code'] . "'/></td>
                <td><input type='text' name='role' value='" . $row['User_Role_Name'] . "'/></td>
                <td>" . $row['Emulate_User_ID'] . "</td>
                <input type='hidden' name='role_id' value='" . $row['RoleID'] . "' />
                <td><input type='submit' value='Update' name='edit_user' class='btn btn-success' /></td>
                <td><input type='submit' value='Delete' name='delete_user' class='btn btn-danger' disabled/></td>
            </tr></form>";        
    } return $user_roles;
}
$user_roles = getUserRoles();
/////////////////////////////////////////// Form Submission //////////////////////////////////////////////////
// Add New User
if (isset($_POST['add_user'])) {
    $userid = sanitize($_POST['userid']);
    $office = sanitize($_POST['office']);
    $role   = sanitize($_POST['role']);
    $page   = sanitize($_POST['page']);
    $sql    = "INSERT INTO Web_Page_Roles (Page_ID,User_ID,Office_Code,User_Role_Name) VALUES ('$page','$userid','$office','$role')";
    $query  = odbc_prepare($conn, $sql);
    $result = odbc_execute($query);
    if(!$result) {
        $message = 'Failed to add user: ' . odbc_errormsg($conn);
    } else {
        $message = 'Successfully added user ' . $userid;
    }
}
// Edit record
if (isset($_POST['edit_user'])) {
    $page = sanitize($_POST['page']);
    $office = sanitize($_POST['office']);
    $role = sanitize($_POST['role']);
    $row_id = sanitize($_POST['role_id']);
    $edit_string = "UPDATE Web_Page_Roles SET
        Page_ID = '$page',
        Office_Code = '$office',
        User_Role_Name = '$role' WHERE RoleID = '$row_id'";
    $message = $edit_string;
    $edit_query = odbc_prepare($conn, $edit_string);
    $edit_result = odbc_execute($edit_query);
    if(!$edit_result) {
        $message = 'Failed to update record: ' . odbc_errormsg($conn) . "<br/>";
    } else {
        $message = 'Successfully updated record number: ' . $row_id . "<br/>";
    }
    $message .= $edit_string;
}
// Delete record
if(isset($_POST['delete_user'])) {
    $roleID = sanitize($_POST['role_id']);
    $delete_string = "DELETE FROM Web_Page_Roles WHERE RoleID = '$roleID'";
    $delete_query = odbc_prepare($conn, $delete_string);
    $deleted_result = odbc_execute($delete_query);
    if(!$deleted_result) {
        $message = 'Failed to delete record: ' . odbc_errormsg($conn) . '<br/>';
    } else {
        $message = 'Successfully deleted record number: ' . $roleID . '<br/>';
    }
    $message .= $delete_string;
}
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <!-- Data Tables -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <style>
            .btn-purple,.btn-purple:visited,.btn-purple:hover {
                color: #fff;
                border-color: #8500bd;/*purple*/
                background-color: #9400d3
            }
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #add_user {
                color: #fff;
                border-color: #8500bd;/*purple*/
                background-color: #9400d3
            }
            #edit_user {
                color: #fff;
                border-color: #5CB85C;/*green*/
                background-color: #449D44
            }
            table {
                table-layout: fixed;
                max-height: 500px;
            }
            th,td {
                column-width: 100px;
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if($page_access) {?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">User Management</h1>
                            <?php echo $dev_server;?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-purple" data-toggle="collapse" data-target="#add_user">Add User</button>
                            <button type="button" class="btn btn-success" data-toggle="collapse" data-target="#edit_user">Edit User(s)</button>

                            <div id="add_user" class="div-content collapse in">
                                <h1 class="panel-heading">Add New User</h1>
                                <div class="panel-footer">
                                    <?php if(isset($_POST['add_user'])) {echo $message;}?>
                                <form name="add_user" method="POST" action="" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="userid" class="control-label col-xs-4" style='font-size:16px;'>User ID</label>
                                        <div class="col-xs-8">
                                            <input type='text' name='userid' autofocus required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="office" class="control-label col-xs-4" style='font-size:16px;'>Office</label>
                                        <div class="col-xs-8">
                                            <select name='office'>
                                                <option value='' selected>Choose office</option>
                                                <option value='NWA'>NWA</option>
                                                <option value='PHX'>PHX</option>
                                                <option value='SLC'>SLC</option>
                                                <option value='WMA'>WMA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="control-label col-xs-4" style='font-size:16px;'>Role</label>
                                        <div class="col-xs-8">
                                            <select name='role'>
                                                <option value='' selected>Choose role</option>
                                                <option value='Admin'>Admin</option>
                                                <option value='Broker'>Broker</option>
                                                <option value='LASM'>LASM</option>
                                                <option value='Pricing'>Pricing</option>
                                                <option value='Team Lead'>Team Lead</option>
                                                <option value='Office Director'>Office Director</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="page" class="control-label col-xs-4" style='font-size:16px;'>Page</label>
                                        <div class="col-xs-8">
                                            <select name='page'>
                                                <option value='' selected>Choose page</option>
                                                <option value='Activity_Tracker'>Activity Tracker</option>
                                                <option value='BC_Broker'>Broker Commissions</option>
                                                <option value='Broker_KPI'>Broker KPI</option>
                                                <option value='BC_Broker_Report'>Broker Reports</option>
                                                <option value='Carrier_Lookup'>Carrier Lookup</option>
                                                <option value='Home_Page'>Home Page</option>
                                                <option value='Hotload_Override'>Hot Load Override</option>
                                                <option value='Hotload_Valid'>Hot Load Valid</option>
                                                <option value='BC_Manager'>Manager Dashboard</option>
                                                <option value='New_Hire_Form'>New Hire/Termination Form</option>
                                                <option value='BC_Leaderboard'>Office Leaderboard</option>                                                
                                                <option value='Spot_Rate_Modifier'>Spot Rate Modifier</option>
                                                <option value='User_Management'>User Management</option>
                                            </select>
                                        </div>
                                        * Add new entry per user per page
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-offset-3 col-xs-9">
                                            <input type="submit" name="add_user" class="btn btn-primary col-lg-offset-2" value="Submit"/>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                            
                            <div id="edit_user" class="div-content collapse">
                                <h1 class="panel-heading">Edit User(s)</h1>
                                <div class="panel-footer">
                                    <?php if (isset($_POST['delete_user'])||isset($_POST['edit_user'])) { echo $message;}?>
                                    <table id='hotloadtable' class="table table-bordered">
                                            <thead>
                                            <th>Page ID</th>
                                            <th>User ID</th>
                                            <th>Office</th>
                                            <th>Role</th>
                                            <th>Emulating User</th>
                                            <th>Click to Save</th>
                                            <th>Click to Delete</th>
                                            </thead>
                                            <?php echo $user_roles; ?>
                                        </table>
                                </div>
                            </div>

                        </div>

                    </div><!-- /.row -->
                        <?php if (isset($_POST['submit'])) {
                            echo "<pre>" . $sql . "</pre>";
                        } 
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                    }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
    </body>
</html>
