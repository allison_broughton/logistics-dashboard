<?php
/////////////////////////////////////////////////// ERROR REPORTING /////////////////////////////////////////
/* Uncomment when page is live */
//error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR); //shows ONLY fatal errors
/* Uncomment while developing code */
error_reporting(E_ALL ^ E_NOTICE); //shows ALL notices, warnings, errors
ini_set('display_errors', true);
/////////////////////////////////////////////// ODBC Connection MCLEOD ////...///////////////////////////////
$user              = 'svc_lorl';
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$server_mcld       = '10.1.244.122';
$database_mcld     = 'McLeod_lme_1110_prod';
$connection_mcld   = "DRIVER={SQL Server};SERVER=$server_mcld;$port;DATABASE=$database_mcld";
$conn_mcld         = odbc_connect($connection_mcld, $user, $pass);
/////////////////////////////////////////////////// ODBC Connection /////////////////////////////////////////
$server            = '10.86.0.33';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$dbc               = odbc_connect($connection_string, $user, $pass);
/////////////////////////////////////////////////// Functions //////////////////////////////////////////////
function formatDate($datetimestr, $format) {
    $formatted = (new DateTime($datetimestr))->format($format);
    return $formatted;
}
// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}
// Look up latitude and longitude for city, state, zip
function getCoordinates($city, $zip) {
    global $dbc;
    $str    = "SELECT X AS longitude,
		      Y AS latitude
	FROM zipcode
	WHERE CITY = '$city' AND ZIPCHAR = '$zip'";
    $query  = odbc_prepare($dbc, $str);
    $result = odbc_execute($query);
    if (!$result) {
        $debug = 'Error: ' . odbc_errormsg($dbc);
    } else {
        $row                 = odbc_fetch_array($query);
        $coords['longitude'] = abs($row['longitude']);
        $coords['latitude']  = $row['latitude'];
        $debug               = "The coordinates for $city $zip are: " . $coords['longitude'] . ", " . $coords['latitude'];
    }
    return $coords;
}
/////////////////////////////////////////////////// Select loads /////////////////////////////////////////////
if (isset($_POST['search_loads_submit'])) {
    $origin             = sanitize($_POST['search_origin']);
    $o_citystate        = preg_replace('/\d+/u', '', $origin);
    $o_city             = strtok($origin, ',');
    $o_zip              = intval(preg_replace('/[^0-9]+/', '', $origin), 10);
    $origin_radius      = sanitize($_POST['search_origin_radius']);
    $destination        = sanitize($_POST['search_destination']);
    $d_citystate        = preg_replace('/\d+/u', '', $destination);
    $d_city             = strtok($destination, ',');
    $d_zip              = intval(preg_replace('/[^0-9]+/', '', $destination), 10);
    $destination_radius = sanitize($_POST['search_destination_radius']);
    $pickup             = sanitize($_POST['search_pickup_date']);
    $pickup_date        = formatDate($pickup, 'Y-m-d H:i:s');
    $deliver            = sanitize($_POST['search_deliver_date']);
    $deliver_date       = formatDate($deliver, 'Y-m-d H:i:s');
    $equipment          = sanitize($_POST['search_equipment']);
    // Look up coordinates for origin and destination
    $o_coords           = getCoordinates($o_city, $o_zip);
    $d_coords           = getCoordinates($d_city, $d_zip);
    if (!empty($origin) && empty($origin_radius)) {
        $where_clause = "AND os.zip_code = '$o_zip'";
    }
    if (!empty($origin_radius)) {
        $where_clause .= " AND os.latitude BETWEEN " . $o_coords['latitude'] . " - ($origin_radius / 69) AND os.latitude + ($origin_radius / 69)
	AND os.longitude BETWEEN " . $o_coords['longitude'] . " - ($origin_radius / (69 * COS(RADIANS(os.latitude)))) AND " . $o_coords['longitude'] . " + ($origin_radius / (69* COS(RADIANS(os.latitude))))";
    }
    if (!empty($destination) && empty($destination_radius)) {
        $where_clause .= " AND ds.zip_code = '$d_zip'";
    }
    if (!empty($destination_radius)) {
        $where_clause .= " AND ds.latitude BETWEEN " . $d_coords['latitude'] . " - ($destination_radius / 69) AND ds.latitude + ($destination_radius / 69)
	AND ds.longitude BETWEEN " . $d_coords['longitude'] . " - ($destination_radius / (69 * COS(RADIANS(ds.latitude)))) AND " . $d_coords['longitude'] . " + ($destination_radius / (69* COS(RADIANS(ds.latitude))))";
    }
    if (!($equipment === 'A' || $equipment === '')) {
        $where_clause .= " AND o.equipment_type_id = '$equipment'";
    }
    if (!empty($pickup)) {
        $where_clause .= " AND CAST(os.sched_arrive_early AS DATE) = '$pickup_date'";
    }
    if (!empty($deliver)) {
        $where_clause .= " AND CAST(ds.sched_arrive_early AS DATE) = '$deliver_date'";
    }
} else {
    $where_clause = '';
}

// Retrieve available loads for datatable
function getAvailLoads($where_clause) {
    global $conn_mcld;
    $select      = "SELECT o.id                              AS Order_ID,
                o.equipment_type_id                          AS Equipment,
                o.bill_distance                              AS Miles,
		O.weight                                     AS Weight,
		RTRIM(os.city_name) + ', ' + RTRIM(OS.state) AS Pickup_Location,
                os.zip_code                                  AS Pickup_Zip,
                os.latitude                                  AS Pickup_Lat,
                os.longitude                                 AS Pickup_Long,
                ds.latitude                                  AS Delivery_Lat,
                ds.longitude                                 AS Delivery_Long,
                os.sched_arrive_early                        AS Pickup_Time,
                u.NAME                                       AS Broker_Name,
                RTRIM(u.office_phone)                        AS Broker_Phone,
                u.email_address                              AS Broker_Email,
		RTRIM(DS.city_name) + ', ' + RTRIM(DS.state) AS Delivery_Location,
                ds.zip_code                                  AS Delivery_Zip,
                ds.sched_arrive_early                        AS Delivery_Time
         FROM   McLeod_lme_1110_prod.dbo.orders o (nolock)
                JOIN McLeod_lme_1110_prod.dbo.movement_order mo (nolock)
                  ON o.id = mo.order_id
                JOIN McLeod_lme_1110_prod.dbo.stop ds (nolock)
                  ON o.consignee_stop_id = ds.id
                JOIN McLeod_lme_1110_prod.dbo.stop os (nolock)
                  ON o.shipper_stop_id = os.id
                JOIN McLeod_lme_1110_prod.dbo.movement m (nolock)
                  ON mo.movement_id = m.id
                JOIN Logistics_Data_Warehouse.dbo.lb_broker_area ba (nolock)
                  ON os.state = ba.state
                JOIN McLeod_lme_1110_prod.dbo.users u (nolock)
                  ON Lower( ba.broker ) = u.id
                JOIN ( SELECT order_id,
                              Max( order_sequence ) AS stopcount
                       FROM   McLeod_lme_1110_prod.dbo.stop (nolock)
                       GROUP  BY order_id ) AS ms
                  ON o.id = ms.order_id
         WHERE  m.brokerage_status = 'READY'
                AND o.status = 'A' AND o.on_hold <> 'Y'
                AND o.loadboard = 'Y'
                $where_clause
         GROUP  BY o.id,o.equipment_type_id,o.bill_distance,o.weight,os.city_name,os.state,
         os.latitude,os.longitude,ds.latitude,ds.longitude,os.sched_arrive_early,
         u.NAME,u.email_address,u.office_phone,ds.city_name,ds.state,ds.sched_arrive_early,ms.stopcount,
         os.zip_code,ds.zip_code
         ORDER  BY o.id";
    $get         = odbc_prepare($conn_mcld, $select);
    odbc_execute($get);
    $avail_loads = '';
    while ($row         = odbc_fetch_array($get)) {
        $avail_loads .= "<tr>
                        <td>" . formatDate($row['Pickup_Time'], 'n/d H:i') . "<br>
                            " . $row['Pickup_Location'] . "<br>
                                " . $row['Pickup_Zip'] . "
                        </td>
                        <td>N/A</td>
                        <td>" . formatDate($row['Delivery_Time'], 'n/d H:i') . "<br>
                            " . $row['Delivery_Location'] . "<br>
                                " . $row['Delivery_Zip'] . "
                        </td>
                        <td>N/A</td>
                        <td>" . number_format($row['Miles']) . "</td>
                        <td>" . number_format($row['Weight']) . "</td>
                        <td>" . $row['Equipment'] . "</td>
                        <td>
                            <div class='popup'>
                                <i class='fa fa-fw fa-2x fa-envelope fa-blue'></i>
                                <span class='popuptext email-badge'><a href='mailTo:" . $row['Broker_Email'] . "?subject=Interested in Order # " . $row['Order_ID'] . "'> 
                                    " . $row['Broker_Email'] . "
                                </a></span>
                            </div>
                            <div class='popup'>
                                <i class='fa fa-fw fa-2x fa-phone fa-blue'></i>
                                <span class='popup popuptext phone-badge'><a href='tel:" . $row['Broker_Phone'] . "'>
                                " . $row['Broker_Phone'] . "
                                </a></span>
                            </div>
                        </td>
                    </tr>";
    } return $avail_loads;
}
$table_rows = getAvailLoads($where_clause);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Search Loads | Swift Logistics, LLC</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

        <link rel="shortcut icon" href="images/swift_logistics_icon.ico" />

        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!--This styles the equipment dropdown select-->
        <link href="css/plugins/knight_loadboard/bootstrap-select.min.css" rel="stylesheet"/>
        <!--This moves the labels inside the search form input fields-->
        <link href="css/plugins/knight_loadboard/materialize.min.css" rel="stylesheet"/>
        <!--General CSS styling for the page-->
        <link href="css/plugins/knight_loadboard/knight_site.css" rel="stylesheet"/>
        <link href="css/plugins/knight_loadboard/daterangepicker.css" rel="stylesheet"/>

        <script src="js/jquery-3.1.1.js"></script>
        <!--<script src="js/modernizr-2.6.2.js"></script>-->
        <script src='js/bootstrap.js' defer></script>
        <script src='js/bootstrap-select.min.js' defer></script>
        <!--<script src='js/respond.js' defer></script>-->
        <script src='js/materialize.min.js' defer></script>
        <script src="js/common.js"></script>
        <script src="js/knight_site.js"></script>
        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
        <script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <!--[if IE 10]>
        <sctipt>
        $('.form-control').on('focus blur', function (e) {
            $(this).siblings('label').toggleClass('active', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
            <sctipt>
        <![endif]-->
        <!-- Data Tables -->
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css">
        <link href='css/plugins/knight_loadboard/dataTables.fontAwesome.css'>
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
        <!--Display spinning loader div-->
        <script>
            $(window).load(function () {
                $("#loader").fadeOut("slow");
                $(".overlay-loader").fadeOut("slow");
            });
        </script>
        <!--Loads auto suggestions for origin & destination input-->
        <script type="text/javascript">
            /* Origin */
            function lookup(inputString) {
                if (inputString.length === 0) {
                    // Hide the suggestion box
                    $('#suggestions').hide();
                } else {
                    $.post("ajax_suggest.php", {
                        queryString: "" + inputString + "",
                        stopType: "pickup"
                    }, function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').html(data);
                        }
                    });
                }
            }
            /* Fill input field with location onclick */
            function fill(thisValue) {
                $('#inputString').val(thisValue);
                setTimeout("$('#suggestions').hide();", 200);
            }
            /************************************************************************************************/
            /* Destination */
            function lookupD(inputStringD) {
                if (inputString.length === 0) {
                    // Hide the suggestion box
                    $('#d-suggestions').hide();
                } else {
                    $.post("ajax_suggest.php", {
                        queryString: "" + inputStringD + "",
                        stopType: "destination"
                    }, function (data) {
                        if (data.length > 0) {
                            $('#d-suggestions').show();
                            $('#autoSuggestionsListD').html(data);
                        }
                    });
                }
            }
            /* Fill input field with location onclick */
            function fillD(thisValue) {
                $('#inputStringD').val(thisValue);
                setTimeout("$('#d-suggestions').hide();", 200);
            }
        </script>
        <script>
            /* Resets AND clears form input whether it has been submitted or not! :D */
            function resetForm($form) {
                $form.find('input:text, input:password, select, textarea').val('');
                $form.find(':input[type=number]').val('');
                $form.find('input:radio, input:checkbox')
                     .removeAttr('checked').removeAttr('selected');
            }
        </script>
        <!--Date picker options for Pick Up and Deliver inputs-->
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('input[name="search_pickup_date"],input[name="search_deliver_date"]').daterangepicker({
                    autoUpdateInput: false,
                    singleDatePicker: true,
                    showDropdowns: true
                });
                $('input[name="search_pickup_date"],input[name="search_deliver_date"]').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY'));
                });

                $('input[name="search_pickup_date"],input[name="search_deliver_date"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });
            });
        </script>
        <script>
            // When user clicks on icon, show contact info
            $(document).ready(function() {
                $(".popup").click(function (e) {
                    e.stopPropagation();
                    //Toggles class to set visibility: hidden/visible
                    $(this).children('.popuptext').toggleClass('popup show');
                });
            });
        </script>
        <!--Include axios js script-->
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script>
            /*GET load info from Don's API*/
            axios.post('http://devinside.swiftlogistics.com:3000/api/loadboard', {
                Token: '781353E5B9B311AF22B7D382CA9BAC9725E890720E5D8EE54A75C204C5D4725A'})
                .then(function (response) {
                  console.log(response);
                  res = response.data;
                })
                .catch(function (error) {
                  console.log(error);
                });
        </script>
        <style>
            table.dataTable thead th {
                    position: relative;
                    /* Remove DataTables bootstrap integration styling */
                    background-image: none !important; 
            }

            table.dataTable thead th.sorting:after,
            table.dataTable thead th.sorting_asc:after,
            table.dataTable thead th.sorting_desc:after {
                    position: absolute;
                    top: 12px;
                    right: 8px;
                    display: block;
                    font-family: 'FontAwesome';
                    color: #DAAA00;
                    font-size: 1.6em;
            }

            table.dataTable thead th.sorting:after {
                    content: "\f0dc";
                    font-size: 1.0em;
                    padding-top: 0.12em;
            }
            table.dataTable thead th.sorting_asc:after {
                    content: "\f0de";
            }
            table.dataTable thead th.sorting_desc:after {
                    content: "\f0dd";
            }

            div.dataTables_scrollBody table.dataTable thead th.sorting:after,
            div.dataTables_scrollBody table.dataTable thead th.sorting_asc:after,
            div.dataTables_scrollBody table.dataTable thead th.sorting_desc:after {
                    content: "";
            }

            /* In Bootstrap and Foundation the padding top is a little different from the DataTables stylesheet */
            table.table thead th.sorting:after,
            table.table thead th.sorting_asc:after,
            table.table thead th.sorting_desc:after {
                    top: 8px;
            }

            /* DataTables style pagination controls */
            div.dataTables_paginate a.paginate_button.first,
            div.dataTables_paginate a.paginate_button.previous {
                    position: relative;
                    padding-left: 24px;
            }

            div.dataTables_paginate a.paginate_button.next,
            div.dataTables_paginate a.paginate_button.last {
                    position: relative;
                    padding-right: 24px;
            }

            div.dataTables_paginate a.first:before, 
            div.dataTables_paginate a.previous:before {
                    position: absolute;
                    top: 8px;
                    left: 10px;
                    display: block;
                    font-family: 'FontAwesome';
            }

            div.dataTables_paginate a.next:after,
            div.dataTables_paginate a.last:after {
                    position: absolute;
                    top: 8px;
                    right: 10px;
                    display: block;
                    font-family: 'FontAwesome';
            }

            div.dataTables_paginate a.first:before {
                    content: "\f100";
            }

            div.dataTables_paginate a.previous:before {
                    content: "\f104";
            }

            div.dataTables_paginate a.next:after {
                    content: "\f105";
            }

            div.dataTables_paginate a.last:after {
                    content: "\f101";
            }
            /* Bootstrap and foundation style pagination controls*/
            div.dataTables_paginate li.first > a,
            div.dataTables_paginate li.previous > a {
                    position: relative;
                    padding-left: 24px;
            }

            div.dataTables_paginate li.next > a,
            div.dataTables_paginate li.last > a {
                    position: relative;
                    padding-right: 24px;
            }

            div.dataTables_paginate li.first a:before, 
            div.dataTables_paginate li.previous a:before {
                    position: absolute;
                    top: 6px;
                    left: 10px;
                    display: block;
                    font-family: 'FontAwesome';
            }

            div.dataTables_paginate li.next a:after,
            div.dataTables_paginate li.last a:after {
                    position: absolute;
                    top: 6px;
                    right: 10px;
                    display: block;
                    font-family: 'FontAwesome';
            }

            div.dataTables_paginate li.first a:before {
                    content: "\f100";
            }

            div.dataTables_paginate li.previous a:before {
                    content: "\f104";
            }

            div.dataTables_paginate li.next a:after {
                    content: "\f105";
            }

            div.dataTables_paginate li.last a:after {
                    content: "\f101";
            }

            /* In Foundation we don't want the padding like in bootstrap */
            div.columns div.dataTables_paginate li.first a:before, 
            div.columns div.dataTables_paginate li.previous a:before,
            div.columns div.dataTables_paginate li.next a:after,
            div.columns div.dataTables_paginate li.last a:after {
                    top: 0;
            }
        </style>
    </head>
    <body>
        <div id="loader"></div>
        <div class="overlay-loader"></div>
        <div class="navbar menu-header navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-sm-3">
                        <div class="navbar-header">
                            <!--Button for mobile menu - doesn't currently do anything as we only have one page-->
                            <button title='TESTING' data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-right"><i class="fa fa-2x fa-blue fa-bars"></i></button>
                            <!--Swift Brand logo + Home Page link-->
                            <a class="navbar-brand" href="loadboard.php"><img src="images/sl_nav_logo.png" alt="logo" ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-wrapper" id="page">
            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
            <div class="container-fluid">
                <div id="grdAvailableLoad">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 desktop-p-r30 available-loads">
                            <div id="sticky-anchor" style="height: 0px;"></div>
                            <div class="fixed-filter sticky" style="width: 1088.33px;">
                                <section class="search-load">
                                    <div class="well bg-blue no-margin">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <span class="control-label text-bold text-white h-24x" id="lblAvailableLoads">
                                                        <span id="total_loads"></span>
                                                    </span>
                                                </div>
                                                <div class="col-xs-4 text-right">
                                                    <div>
                                                        <a href="#" id="Clearall" onclick="resetForm($('#searchLoadsForm'))">Reset Fields</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-t10">
                                                <div class="col-xs-12">
                                                    <div class="search-form-wrap label-within-box">
                                                        <div class="modal-box-ipad-mob" id="divSearchPopup">
                                                            <form id="searchLoadsForm" class="searchForLoads" method="post" action="">
                                                                <ul class="none ">
                                                                    <li class="origin">
                                                                        <div class="input-field"><input type="text" name="search_origin" class="form-control" id="inputString" onkeyup="lookup(this.value);" onblur="fill();" <?php if (isset($origin)) {
                                                                            echo "value='$origin'";
                                                                        } ?> autocomplete="off"><label>Origin</label></div>
                                                                        <!--Suggestion box for origin-->
                                                                        <div class="suggestionsBox" id="suggestions" style="display: none;">
                                                                            <div class="suggestionList" id="autoSuggestionsList">
                                                                                &nbsp;
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-field radius"><input type="number" name="search_origin_radius" min="0" max="1000" class="form-control" maxlength="4" <?php if (isset($origin_radius)) {
                                                                            echo "value='$origin_radius'";
                                                                        }?> autocomplete="off"><label>Radius</label></div>
                                                                    </li>
                                                                    <li class="destination">
                                                                        <div class="input-field"><input type="text" name="search_destination" class="form-control" id="inputStringD" onkeyup="lookupD(this.value);" onblur="fillD();" <?php if (isset($destination)) {
                                                                            echo "value='$destination'";
                                                                            } ?> autocomplete="off"><label>Destination</label></div>
                                                                        <!--Suggestion box for destination-->
                                                                        <div class="suggestionsBoxD" id="d-suggestions" style="display: none;">
                                                                            <div class="suggestionList" id="autoSuggestionsListD">
                                                                                &nbsp;
                                                                            </div>
                                                                        </div>
                                                                        <div class="input-field radius"><input type="number" name="search_destination_radius" class="form-control" maxlength="4" <?php if (isset($destination_radius)) {
                                                                            echo "value='$destination_radius'";
                                                                        }?>><label>Radius</label></div>
                                                                    </li>
                                                                    <li class="date pick-up">
                                                                        <div class="input-field date pick-up"><input type="text" name="search_pickup_date" class="form-control" <?php if (isset($pickup)) {
                                                                            echo "value='$pickup'";
                                                                        } ?>><span class="fa fa-1x fa-calendar fa-span"></span><label>Pick Up</label></div>
                                                                    </li>
                                                                    <li class="date deliver">
                                                                        <div class="input-field date pick-up"><input type="text" name="search_deliver_date" class="form-control" <?php if (isset($deliver)) {
                                                                                echo "value='$deliver'";
                                                                            } ?>><span class="fa fa-1x fa-calendar fa-span"></span><label>Deliver</label></div>
                                                                    </li>
                                                                    <li class="equipment">
                                                                        <select name="search_equipment" id="equipment-select" class="form-control">
                                                                            <option value="A" 
                                                                            <?php if (isset($equipment) && $equipment == 'A') {
                                                                                echo ' selected="selected"';
                                                                            }?>>All Equipment</option>
                                                                            <option value="V"
                                                                            <?php if (isset($equipment) && $equipment == 'V') {
                                                                                echo ' selected="selected"';
                                                                            }?>>Dry Van</option>
                                                                            <option value="R"
                                                                            <?php if (isset($equipment) && $equipment == 'R') {
                                                                                echo ' selected="selected"';
                                                                            }?>>Refrigerated</option>
                                                                            <option value="F"
                                                                            <?php if (isset($equipment) && $equipment == 'F') {
                                                                                echo ' selected="selected"';
                                                                            }?>>Flatbed</option>
                                                                            <option value="O"
                                                                            <?php if (isset($equipment) && $equipment == 'O') {
                                                                                echo ' selected="selected"';
                                                                            }?>>Other</option>
                                                                        </select><i class="fa fa-caret-down fa-span" aria-hidden="true"></i>
                                                                    </li>
                                                                    <li class="action-search">
                                                                        <button type="submit" name="search_loads_submit" id="btnSearch" class="btn btn-lg btn-yellow m-r10" value="">GO</button>
                                                                    </li>
                                                                </ul>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </section>
                                <div class="clearfix"></div>
                            </div>
                            <div id="grdAvailableLoad">
                                    <table id="example" class="row-border dataTable loads-board no-footer" style="padding: 0px;" cellspacing="0" width="100%">

                                        <thead>
                                            <tr>
                                                <th data-priority="1">Pick Up</th>
                                                <th data-priority="2" title="Origin">
                                                    <small>Origin</small><br>
                                                    Deadhead</th>
                                                <th data-priority="3">Deliver</th>
                                                <th data-priority="4">
                                                    <small>Destination</small><br>
                                                    Deadhead</th>
                                                <th data-priority="5">Miles</th>
                                                <th data-priority="6">Weight</th>
                                                <th data-priority="7">Equipment</th>
                                                <th data-priority="9">Contact</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php echo $table_rows; ?>
                                        </tbody>
                                    </table>
                            </div>
                            <div class="mb30-per"></div>
                        </div>
                    </div>
                </div>
                <footer id="footer">
                    <p><i class="fa fa-copyright"></i> Swift Logistics, LLC &nbsp;<i class="fa fa-phone"></i> 800-477-8025</p>
                </footer>
            </div>
        </div>
        <script>
            $(document).ready(function () {
         
                $('input[type="text"]').blur(function () {
                    var $this = $(this);
                    console.log($this.val());
                    if ($this.val()) {
                        $this.addClass('active');
                    } else {
                        $this.removeClass('active');
                    }
                });
                               
                /// Hides navbar so it doesn't cover table on scroll down ///
                function sticky_relocate() {
                    var window_top = $(window).scrollTop();
                    var div_top = $('#sticky-anchor').offset().top;
                    if (window_top > div_top) {
                        $('.sticky').addClass('stick');
                        $('#sticky-anchor').height($('.sticky').outerHeight());
                        $('.navbar').addClass('navbar-hide');
                        $('html').removeClass('menu-open');
                        $('#example tbody').offset({top: 500});
                    } else {
                        $('.sticky').removeClass('stick');
                        $('#sticky-anchor').height(0);
                        $('.navbar').removeClass('navbar-hide');
                        $('html').addClass('menu-open');
                    }
                }
                
                $(function () {
                    $(window).scroll(sticky_relocate);
                    sticky_relocate();
                });
            });
        </script>
    <script src="js/moment.min.js"></script>
    <script src="js/daterangepicker.js"></script>
    <script src="js/jquery/jquery.mmenu.all.min.js"></script><!--Mobile Menu plugin-->
    <script>
        $(document).ready(function () {
            // Suppress errors/warnings/alerts
            //$.fn.dataTable.ext.errMode = 'none';
            var table = $('#example').DataTable({
                colReorder: true,
                "fixedHeader": {
                    header: true,
                    headerOffset: $('.search-load').height() - 10
                },
                /*"data": res['loads'],
                "columns": [
                    {"data": "Pickup_Location",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html(oData.Pickup_Time + "<br>" + oData.Pickup_Location + "<br>" + oData.Pickup_Zip);
                        }},
                    { "data" : "Order_ID",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("0");
                        }},
                    { "data" : "Delivery_Location",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html(oData.Delivery_Time + "<br>" + oData.Delivery_Location + "<br>" + oData.Delivery_Zip);
                        }},
                    { "data" : "Order_ID",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("0");
                        }},
                    { "data" : "Miles",
                        render: $.fn.dataTable.render.number(',')},
                    { "data" : "Weight",
                        render: $.fn.dataTable.render.number(',')},
                    { "data" : "Equipment" },
                    { "data" : "Broker_Name",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<div class='popup'><i class='fa fa-fw fa-2x fa-envelope fa-blue'></i><span class='popuptext email-badge'><a href='mailTo:" + oData.Broker_Email +"'?subject=Interested in Order #" + oData.Order_ID + "'>" + oData.Broker_Email + "</a></span></div><div class='popup'><i class='fa fa-fw fa-2x fa-phone fa-blue'></i><span class='popup popuptext phone-badge'><a href='tel:" + oData.Broker_Phone + "'>" + oData.Broker_Phone + "</a></span></div>");
                        }}
                ],*/
                "columnDefs": [
                    { "width" : "10px" }
                ],
                responsive: true,
                stateSave: false,
                "autoWidth": false,
                "searching": false,
                dom: '<"top">tri<"bottom"lp><"clear">',
                "initComplete": function (settings, json) {
                    /* Writes total # of records in search div header */
                    var data = $('#example').dataTable().fnGetData();
                    var total_records = data.length;
                    if (total_records === 1) {
                        $("#total_loads").html(total_records + " Load Available");
                    } else {
                        $("#total_loads").html(total_records + " Loads Available");
                    }
                }
            });
            //Toggle class to set visibility: hidden/visible on contact icon click
            $('#example tbody').on( 'click', '.popup', function (e) {
                e.stopPropagation();
                $(this).children('.popuptext').toggleClass('popup show');
            } );
        });
    </script>
    <script>
    $(document).ready(function () {
        /* Adds sub-menu below navbar */
        $('.navbar').append('<span class="nav-bg"></span>');
        if ($('#hdnUrlName').val() === "availableloads") {
        } else if ($('#hdnUrlName').val() === "myloads") {
        } else if ($('#hdnUrlName').val() === "pastloads") {
        } else {
            $('.dropdown-toggle').click(function () {
                if (!$(this).parent().hasClass('open')) {
                    $('html').addClass('menu-open');
                } else {
                    $('html').removeClass('menu-open');
                }
            });
        }
        /* Adds padding to body content and toggles sub-menu display */
        if ('false' === 'false') {
            $('html').removeClass('menu-open');
            $('html').addClass('close-menu');
        }
    });
    </script>
    </body>
</html>

