<?php
session_start();
require 'include_functions.php';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit('FAQ',$user_id,'page_visit');}
// Set page title
$title   = 'FAQ';
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>

    <body>

        <div id="wrapper">

            <!--Nav bar loaded here via include_navbar.php-->

            <div id="page-wrapper">

                <div class="container-fluid">
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    FAQ's and SOP's<br/>
                                    <small></small>
                                </h1><?php echo $dev_server;?>
                                <img class="center-block" src='images/can_i_help_you.png' style='height: 300px; width: auto;'/>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="spacer"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-purple">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <div class="panel-title">Choose a topic below</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-left"></span>
                                        <h3><a href='under_construction.php'>Broker Commissions</a></h3>
                                        <!--<h3><a href='images/faq.pdf' download>Manager Dashboard</a></h3>-->
                                        <!--<h3><a href='images/faq.pdf' download>Office Leaderboard</a></h3>-->
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- ./row-->
                        
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
    </body>
</html>