<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Manager';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'tund';
}
$office = getOffice($user_id);

// Get active commission paystructure records and store in array
function getRecords($office) {
    global $conn;
    global $user_id;
    if($user_id === 'stobm') {
        $string = "SELECT Commission_Month,
	Commission_Year,
	Movement_ID,
	order_id,
        Dispatcher_User,
	ISNULL(commission_amount, 0) AS commission_amount,
	CONVERT(DATE, commission_amount_date) AS commission_amount_date,
	CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
	bol_received,
	Delivery_Date,
	override_payee_id,
        Processing_Status
FROM BC_PayALL_temp WHERE Commission_Month IS NOT NULL AND Commission_Year > 2016 AND office_code IN ('SLC','NWA')
    AND department_id = 'CS'
ORDER BY Commission_Amount_Date DESC";
    } else {
    $string = "SELECT Commission_Month,
	Commission_Year,
	Movement_ID,
	order_id,
        Dispatcher_User,
	ISNULL(commission_amount, 0) AS commission_amount,
	CONVERT(DATE, commission_amount_date) AS commission_amount_date,
	CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
	bol_received,
	Delivery_Date,
	override_payee_id,
        Processing_Status
FROM BC_PayALL_temp WHERE Commission_Month IS NOT NULL AND Commission_Year > 2016 AND office_code = '$office'
    AND department_id = 'CS'
ORDER BY Commission_Amount_Date DESC";
    }
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row; //added data object to fix a formatting issue in jsonencode -Don
    }
    return $json;
}

// Set var equal to the array returned by the getRecords() function
$data = getRecords($office);
header('Content-Type: application/json');
echo json_encode($data);