<?php
$title = "Search Loads | Nationwide Trucking Brokerage | Knight Logistics";
//////////////////////////////////////// ODBC Connection MCLEOD ///////////////////////////////////
$user            = 'svc_lorl'; //service account with read/write perms
$pass            = 'Rate4ookup';
$port            = 'Port=1433';
$server_mcld     = '10.1.244.122';
$database_mcld   = 'McLeod_lme_1110_prod';
$connection_mcld = "DRIVER={SQL Server};SERVER=$server_mcld;$port;DATABASE=$database_mcld";
$conn_mcld       = odbc_connect($connection_mcld, $user, $pass);
///////////////////////////////////////// Functions //////////////////////////////////////////////
function formatDate($datetimestr,$format) {
    // Ex: 'n/d/Y H:i'
    // Ex: 'Y-m-d'
    $formatted  = (new DateTime($datetimestr))->format($format);
    return $formatted;
}
// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}
///////////////////////////////////////// Select loads ///////////////////////////////////////////
if(isset($_POST['search_loads_submit'])) {
    $debug = "Debug: ";
    $origin = sanitize($_POST['origin']);
    $destination = sanitize($_POST['destination']);
}
function getAvailLoads() {
    global $conn_mcld;
    $select      = "SELECT o.id                              AS Order_ID,
                o.equipment_type_id                          AS Equipment,
                o.bill_distance                              AS Miles,
		O.weight                                     AS Weight,
		RTRIM(os.city_name) + ', ' + RTRIM(OS.state) AS Pickup_Location,
                os.sched_arrive_early                        AS Pickup_Time,
                u.NAME                                       AS Broker_Name,
                RTRIM(u.office_phone)                        AS Broker_Phone,
                u.email_address                              AS Broker_Email,
		RTRIM(DS.city_name) + ', ' + RTRIM(DS.state) AS Delivery_Location,
                ds.sched_arrive_early                        AS Delivery_Time
         FROM   McLeod_lme_1110_prod.dbo.orders o (nolock)
                JOIN McLeod_lme_1110_prod.dbo.movement_order mo (nolock)
                  ON o.id = mo.order_id
                JOIN McLeod_lme_1110_prod.dbo.stop ds (nolock)
                  ON o.consignee_stop_id = ds.id
                JOIN McLeod_lme_1110_prod.dbo.stop os (nolock)
                  ON o.shipper_stop_id = os.id
                JOIN McLeod_lme_1110_prod.dbo.movement m (nolock)
                  ON mo.movement_id = m.id
                JOIN Logistics_Data_Warehouse.dbo.lb_broker_area ba (nolock)
                  ON os.state = ba.state
                JOIN McLeod_lme_1110_prod.dbo.users u (nolock)
                  ON Lower( ba.broker ) = u.id
                JOIN ( SELECT order_id,
                              Max( order_sequence ) AS stopcount
                       FROM   McLeod_lme_1110_prod.dbo.stop (nolock)
                       GROUP  BY order_id ) AS ms
                  ON o.id = ms.order_id
         WHERE  m.brokerage_status = 'READY'
                AND o.status = 'A' AND o.on_hold <> 'Y'
                AND o.loadboard = 'Y'
         GROUP  BY o.id,o.equipment_type_id,o.bill_distance,o.weight,os.city_name,os.state,os.sched_arrive_early,u.NAME,u.email_address,u.office_phone,ds.city_name,ds.state,ds.sched_arrive_early,ms.stopcount
         ORDER  BY o.id";
    $get         = odbc_prepare($conn_mcld, $select);
    odbc_execute($get);
    $avail_loads = '';
    while ($row         = odbc_fetch_array($get)) {
        $avail_loads .= "<tr>
                        <td>" . formatDate($row['Pickup_Time'], 'n/d H:i') . "<br>
                            " . $row['Pickup_Location'] . "
                        </td>
                        <td>N/A</td>
                        <td>" . formatDate($row['Delivery_Time'], 'n/d H:i') . "<br>
                            " . $row['Delivery_Location'] . "
                        </td>
                        <td>N/A</td>
                        <td>" . $row['Miles'] . "</td>
                        <td>" . $row['Weight'] . "</td>
                        <td>" . $row['Equipment'] . "</td>
                        <td>
                            <a class='callto-number' href='mailTo:".$row['Broker_Email']."?subject=Interested in Order # ".$row['Order_ID']."'><i class='fa fa-2x fa-envelope fa-blue'></i></a>
                            &nbsp;
                            <a class='mailto-number' href='tel:".$row['Broker_Phone']."'><i class='fa fa-2x fa-phone fa-blue'></i></a>
                        </td>
                    </tr>";
    } return $avail_loads;
}
$table_rows = getAvailLoads();
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <meta name="description" content="Knight Logistics offers nationwide transportation with trucking, port, rail and brokerage services. Find the right load for any destination."/>
        <meta name="robots" content="index" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

        <link rel="shortcut icon" href="images/swift_logistics_icon.ico" />

        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/plugins/knight_loadboard/bootstrap-select.min.css" rel="stylesheet"/>
        <link href="css/plugins/knight_loadboard/materialize.min.css" rel="stylesheet"/>
        <link href="css/plugins/knight_loadboard/knight_site.css" rel="stylesheet"/>

        <link href="css/plugins/knight_loadboard/daterangepicker.css" rel="stylesheet"/>

        <script src="js/jquery-3.1.1.js"></script>
        <script src="js/modernizr-2.6.2.js"></script>

        <script  src="https://cdnjs.cloudflare.com/ajax/libs/react/15.4.2/react.min.js"></script>
        <script  src="https://cdnjs.cloudflare.com/ajax/libs/react/15.4.2/react-dom.min.js"></script>

        <script async defer src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
        <script src='js/bootstrap.js' defer></script>
        <script src='js/bootstrap-select.min.js' defer></script>
        <script src='js/respond.js' defer></script>
        <script src='js/materialize.min.js' defer></script>


        <script src="js/common.js"></script>
        <script src="js/knight_site.js"></script>
        <!--[if IE 10]>
        <sctipt>
        $('.form-control').on('focus blur', function (e) {
            $(this).siblings('label').toggleClass('active', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
            <sctipt>
        <![endif]-->

        <!-- Data Tables -->
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css">
        <link href='css/plugins/knight_loadboard/dataTables.fontAwesome.css'>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-2671163-8', 'auto');
            ga('send', 'pageview');
        </script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {
                $('#example').dataTable();
            });
        </script>
        <script>
            function resetForm() {
                document.getElementById("searchLoadsForm").reset();
            }
        </script>
    </head>
    <body>

        <input type="hidden" id="hdnUrlName" value="availableloads" />
        <!-- set up the modal to start hidden and fade in and out -->
        <div id="divAlterForknight" class="modal vCenter fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- dialog body -->
                    <div class="modal-header"><h4 class="modal-title">Knight</h4><button type="button" class="close icon-within-header" data-dismiss="modal" value="Button">&times;</button></div>
                    <div class="modal-body"><p class="text-center"><span>Test</span></p></div>
                    <!-- dialog buttons -->
                </div>
            </div>
        </div>
        <div id="loader" style="display:none;"></div>
        <div class="overlay-loader" style="display:none;"></div>
        <div class="navbar menu-header navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-sm-3">
                        <div class="navbar-header">
                            <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-right"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                            <a class="navbar-brand" href="/LoadBoard/AvailableLoads"><img src="images/Webp.net-resizeimage.png" alt="logo" ></a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-sm-9">
                        <div id="menu" class="side-collapse hidden-xs-down in">
                            <nav class="navbar-collapse collapse">
                                <ul class="nav navbar-nav hidden-xs">
                                </ul>
                                <ul class="nav navbar-nav navbar-right">


                                    <li><!-- <a href="http://gopher2.knighttrans.com/broker_Loads_min.cfm" target="_blank">See Previous Load Board</a>-->

                                        <!--        <a href="#" class="btn" data-toggle="modal" data-target="#RegistrationPopUp">Register</a>-->

                                    </li>


                                </ul>

                            </nav>
                        </div>
                        <div id="menu-mobile" class="hidden-sm hidden-md hidden-lg side-collapse in">
                            <nav role="navigation" class="navbar-collapse">
                                <ul class="mm-listview list none">
                                    <li>
                                        <!--<a href="http://gopher2.knighttrans.com/broker_Loads_min.cfm" class="btn" target="_blank">See Previous Load Board</a>-->

                                    </li>
                                    <li>
                                        <!--<a href="#" class="btn" data-toggle="modal" data-target="#RegistrationPopUp">Register</a>-->
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="RegistrationPopUp" class="modal fade vCenter" role="dialog">
            <div class="modal-dialog alert-popup">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header no-padding">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="group text-center">Please call 602 - 2732 - 1500 to speak with a representative from Knight for new carrier registration.</div>
                        <div class="group m-b10 text-center">
                            <button type="button" class="btn btn-lg btn-yellow" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-wrapper" id="page">

            <input id="City" name="City" type="hidden" value="" /><input id="State" name="State" type="hidden" value="" />

            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

            <div class="container-fluid">
                <div id="grdAvailableLoad"></div>

                <div id="grdAvailableLoad">
                    <div data-reactroot="">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 desktop-p-r30 available-loads ">
                                <div id="sticky-anchor" style="height: 0px;"></div>
                                <div class="fixed-filter sticky" style="width: 1088.33px;">
                                    <div class="visible-ipad-mob">
                                        <div class="filter-mob-ipad bg-gray">
                                            <div class="row">
                                                <div class="col-sm-10 col-xs-10 Lato-Bold">
                                                    <div class="address-wrap">
                                                        <div class="address" id="divLocation"></div>
                                                        <div class="equipment" id="divDate"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-xs-2 text-right p-r10"><input type="button" class="btn-search-mob m-t5" id="btnSearchMobile" value=""></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if(isset($debug)) {?>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <span class="alert alert-info"><?php echo $debug; ?></span>
                                        </div>
                                    </div>
                                    <br>
                                    <?php }?>
                                    <section class="search-load">
                                        <div class="well bg-blue no-margin">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <span class="control-label text-bold text-white h-24x" id="lblAvailableLoads">
                                                            Search for Available Loads
                                                        </span>
                                                    </div>
                                                    <div class="col-xs-4 text-right">
                                                        <div>
                                                            <a href="#" id="Clearall" class="hidden-ipad-imp" onclick="resetForm()">Clear All</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row m-t10">
                                                    <div class="col-xs-12">
                                                        <div class="search-form-wrap label-within-box">
                                                            <div class="overlay-popup"></div>
                                                            <div class="modal-box-ipad-mob" id="divSearchPopup">
                                                                
                                                                <!-- Search Available Loads Form Input -->
                                                                <form id="searchLoadsForm" method="post" action="">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close icon-within-header"><span aria-hidden="true">×</span></button>
                                                                        <h4 class="modal-title">Search</h4>
                                                                    </div>
                                                                    <ul class="none ">
                                                                        <li class="origin">
                                                                            <div class="input-field"><input type="text" name="origin" class="form-control" id="txtPickUp"><label>Origin</label></div>
                                                                            <div class="input-field radius"><input type="text" min="0" max="1000" class="form-control" id="txtPickUpRadius" maxlength="4"><label>Radius</label></div>
                                                                        </li>
                                                                        <li class="destination">
                                                                            <div class="input-field"><input type="text" name="destination" class="form-control" id="txtDeliver"><label>Destination</label></div>
                                                                            <div class="input-field radius"><input type="text" class="form-control" id="txtDeliverRadius" maxlength="4"><label>Radius</label></div>
                                                                        </li>
                                                                        <li class="date pick-up">
                                                                            <!--<div class="input-field date pick-up"><input type="text" name="pickup_date" class="form-control addCalendarIcon" placeholder="&#xf073;"><label>Pick Up</label></div>-->
                                                                            <div class="input-field date pick-up"><input type="text" name="pickup_date" class="form-control"><span class="fa fa-2x fa-calendar fa-span"></span><label>Pick Up</label></div>
                                                                            <!--<div class="input-field date pick-up"><input type="text" class="form-control hasDatepicker" id="txtPickUpDate" name="datefilter" maxlength="10"><label>Pick Up</label></div>-->
                                                                        </li>
                                                                        <li class="date deliver">
                                                                            <div class="input-field date pick-up"><input type="text" name="deliver_date" class="form-control"><span class="fa fa-2x fa-calendar fa-span"></span><label>Deliver</label></div>
                                                                            <!--<div class="input-field"><input type="text" class="form-control hasDatepicker" id="txtDeliverDate" name="datefilter" maxlength="10"><label>Deliver</label></div>-->
                                                                        </li>
                                                                        <li class="equipment">
                                                                            <select class="form-control selectpicker bs-select-hidden" id="ddlEquipment">
                                                                                <option value="">All Equipment</option>
                                                                                <option value="V">Dry Van</option>
                                                                                <option value="F">Flatbed</option>
                                                                                <option value="Power Only">Power Only</option>
                                                                                <option value="Reefer">Reefer</option>
                                                                                <option value="Other">Other</option>
                                                                            </select>
                                                                        </li>
                                                                        <li class="action-search">
                                                                            <button type="submit" name="search_loads_submit" id="btnSearch" class="btn btn-lg btn-yellow m-r10" value="">GO</button>
                                                                            <div id="newCountTotal">  </div>
                                                                        </li>
                                                                    </ul>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="loads-board-wrap">

                                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">

                                        <thead class="tableFloatingHeaderOriginal tableFloatingHeader">
                                            <tr>
                                                <th data-priority="1">Pick Up</th>
                                                <th data-priority="2" title='Origin'>Deadhead</th>
                                                <th data-priority="3">Deliver</th>
                                                <th data-priority="4" title='Destination'>Deadhead</th>
                                                <th data-priority="5">Miles</th>
                                                <th data-priority="6">Weight</th>
                                                <th data-priority="7">Equipment</th>
                                                <th data-priority="9">Contact</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $table_rows;?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="mb30-per"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    if ($('#City').val() != '' && $('#State').val() != '') {
                        var search = $('#City').val() + ',' + $('#State').val();
                        $('#txtPickUp').val(search);
                        $('#txtPickUpRadius').val(100);
                    }

                    $('input[type="text"]').blur(function () {
                        var $this = $(this);
                        console.log($this.val());
                        if ($this.val()) {
                            $this.addClass('active');
                        } else {
                            $this.removeClass('active');
                        }
                    });

                    /// Relocates navbar so it doesn't cover table on scroll down ///
                    function sticky_relocate() {
                        var window_top = $(window).scrollTop();
                        var div_top = $('#sticky-anchor').offset().top;
                        if (window_top > div_top) {
                            $('.sticky').addClass('stick');
                            $('#sticky-anchor').height($('.sticky').outerHeight());
                            $('.navbar').addClass('navbar-hide');
                            $('html').removeClass('menu-open');
                        } else {
                            $('.sticky').removeClass('stick');
                            $('#sticky-anchor').height(0);
                            $('.navbar').removeClass('navbar-hide');
                            $('html').addClass('menu-open');
                        }
                    }

                    $(function () {
                        $(window).scroll(sticky_relocate);
                        sticky_relocate();
                    });

                    var dir = 1;
                    var MIN_TOP = 200;
                    var MAX_TOP = 350;

                    function autoscroll() {
                        var window_top = $(window).scrollTop() + dir;
                        if (window_top >= MAX_TOP) {
                            window_top = MAX_TOP;
                            dir = -1;
                        } else if (window_top <= MIN_TOP) {
                            window_top = MIN_TOP;
                            dir = 1;
                        }
                        $(window).scrollTop(window_top);
                        window.setTimeout(autoscroll, 100);
                    }
                });
            </script>
            <footer></footer>
        </div>

        <!--Reference the SignalR library. -->
        <script src="https://ajax.aspnetcdn.com/ajax/signalr/jquery.signalr-2.2.2.min.js"></script>
        <!--Reference the autogenerated SignalR hub script. -->
        <script src="https://loadboard.knightlogistics.com/webapi/signalr/hubs"></script>


        <script src="js/jquery/jquery.validate.js"></script>
        <script src="js/jquery/jquery.validate.unobtrusive.js"></script>

        <script src="js/moment.min.js"></script>
        <script src="js/daterangepicker.js"></script>
        <script src="js/jquery/jquery.mmenu.all.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="/Scripts/flexcroll.js"></script>

        <script src="/Scripts/JSX/AvailableLoadsTmw.jsx"></script>

        <script src="/Scripts/PageScripts/AvailableLoadEvents.js"></script>
        <script src="js/jquery/jquery.tmpl.min.js"></script>

    </body>
</html>

<script>
                $(document).ready(function () {
                    $('.navbar').append('<span class="nav-bg"></span>');
                    if ($('#hdnUrlName').val() == "availableloads") {
                    } else if ($('#hdnUrlName').val() == "myloads") {
                    } else if ($('#hdnUrlName').val() == "pastloads") {
                    } else {
                        $('.dropdown-toggle').click(function () {
                            if (!$(this).parent().hasClass('open')) {
                                $('html').addClass('menu-open');
                            } else {
                                $('html').removeClass('menu-open');
                            }
                        });
                    }
                    if ('false' == 'false') {
                        $('html').removeClass('menu-open');
                        $('html').addClass('close-menu');
                    }
                    var sideslider = $('[data-toggle=collapse-side]');
                    var sel = sideslider.attr('data-target');
                    var sel2 = sideslider.attr('data-target-2');
                    sideslider.click(function (event) {
                        $(sel).toggleClass('in');
                        $(sel2).toggleClass('out');
                    });
                    var loc = window.location.pathname;

                    if (loc == "/" && 'false' == 'true') {
                        $('.dropdown').addClass('Active');
                    }
                    if (loc.indexOf('availableloads') == 1) {
                        $('.dropdown').addClass('Active');
                    }
                    if (loc.indexOf('pastloads') == 1) {
                        $('.dropdown').addClass('Active');
                    }

                    if (loc.indexOf('LoadBoard') == 1) {
                        $('.dropdown').addClass('Active');
                    }

                    $('.navbar-toggle').click(function () {
                        $('body').toggleClass('mob-header-managed');
                    });
                    if ($('#dropdown-load').hasClass('Active')) {
                        $('#dropdown-load').addClass('open');
                        $('html').addClass('menu-open');
                    } else {
                        $('#dropdown-load').removeClass('open');
                        $('html').removeClass('menu-open');
                    }

                });
</script>
<script>

    $(document).on('click touchstart', function (a) {
        $('.navbar').append('<span class="nav-bg"></span>');
        if ($('#hdnUrlName').val() == "availableloads") {
        } else if ($('#hdnUrlName').val() == "myloads") {
        } else if ($('#hdnUrlName').val() == "pastloads") {
        } else {
            if ($(a.target).parents().index($('.navbar-nav')) == -1) {
                $('html').removeClass('menu-open');
            }
        }

    });
</script>