<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Broker_Report';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'bendc';
}
// Get active commission paystructure records and store in array
function getRecords($user_id) {
    global $conn;
    $string = "SELECT 
            CASE WHEN commission_cycle = 'A' THEN arecord_count
			WHEN commission_cycle = 'Q' THEN qrecord_count END AS Record_Count,
            is_active AS Pay_Old,
            is_active AS Pay_New,
            order_id AS Order_ID,
            Movement_ID AS movement_id,
            total_charge,
            Carrier_Total_Pay,
            bol_received,
            movement_margin AS margin,
            processing_status,
            is_active AS Commission_End_Date,
            CONVERT(DATE, Delivery_Date) AS Delivery_Date,
            CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
            movement_margin,
            CASE WHEN commission_cycle = 'A' THEN annual_running_total
                        WHEN commission_cycle = 'Q' THEN quarter_running_total END AS Running_Total,
            CASE WHEN commission_cycle = 'A' THEN Annual_Commission_Percent
                        WHEN commission_cycle = 'Q' THEN Quarterly_Commission_Percent END AS Commission_Percent,
            CONVERT(DATE, commission_amount_date) AS Pay_Date,
            dispatcher_user_id,
            override_payee_id,
            Destin_City + ',' + Destin_State AS Destination,
            Destin_Schedule_Arrive_Early,
            case when processing_status = 'Paid' then commission_amount
                when processing_status = 'Pending' and commission_cycle = 'Q' then  Quarterly_Commission_Pay
                when processing_status = 'Pending' and commission_cycle = 'A' then Annual_Commission_Pay
                end as Payroll_Amount
        FROM [Logistics_Data_Warehouse_QA].[dbo].[BC_PayALL_temp] WHERE dispatcher_user_id = '$user_id'
            AND Commission_Month IS NOT NULL AND Commission_Year > 2016 AND processing_status NOT IN ('Void','Ineligible')
            ORDER BY Dispatch_Date ASC";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getRecords($user_id);
header('Content-Type: application/json');
echo json_encode($data);