<?php
session_start();
require 'include_functions.php';
$title                = "Manager Dashboard";
$office               = getOffice('tund'/*$user_id*/);// Look up Dave's office code for testing
$limiter              = "office_code = '$office'";
//////////////////////////////////////////////// CALCULATE BROKER AGGREGATE TOTALS ///////////////////////////////////////////
$gross_margin_pending = "$" . number_format(getGrossMargin('Pending', $limiter));
$gross_margin_paid    = "$" . number_format(getGrossMargin('Paid', $limiter));
$comm_pay_current     = "$" . getCommPay($limiter);
//$comm_pay_last_month  = "$" . getCommPay(($current_month-1), $limiter);
$paid_load_count      = getLoadCount('Paid', $limiter);
$pending_load_count   = getLoadCount('Pending', $limiter);
$quarterly_pending    = number_format(getYTDPay('Quarterly_Pending_Pay', $limiter));
$quarterly_paid       = number_format(getYTDPay('Quarterly_Paid_Pay', $limiter));
$annual_pending       = number_format(getYTDPay('Annual_Pending_Pay', $limiter));
$annual_paid          = number_format(getYTDPay('Annual_Paid_Pay', $limiter));
//////////////////////////////////////////////// EXPORT DETAILED DATA INTO EXCEL FILE ///////////////////////////////////////////
// The download script has to be placed BEFORE any HTML is output so it can trigger the download and exit.
// Otherwise there will be HTML printed in the csv file.
if (isset($_POST['export_details_submit'])) {
    $report = $_POST['report'];
    if ($report === NULL) {
        $message = "Please choose a report.";
    } else {
        if ($report === 'current') { // Current Pay Period
            $sql      = "SELECT Commission_Month,
                    Commission_Year,
                    Movement_ID,
                    order_id,
                    Dispatcher_User,
                    office_code,
                    department_id,
                    ISNULL(commission_amount, 0) AS commission_amount,
                    CONVERT(DATE, commission_amount_date) AS commission_amount_date,
                    CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                    Quarterly_Commission_Percent,
                    Quarterly_Commission_Pay,
                    Annual_Commission_Percent,
                    Annual_Commission_Pay,
                    Processing_Status
            FROM BC_PayALL_temp WHERE Commission_Month = '$current_month' 
                AND Commission_Year = '$current_year' AND office_code = '$office' AND Processing_Status = 'Paid'
            ORDER BY Commission_Amount_Date DESC";
            $filename = "Current_Pay_Period_";
        } elseif ($report === 'pending') { // Pending Loads
            $sql      = "SELECT Commission_Month,
                    Commission_Year,
                    Movement_ID,
                    order_id,
                    Dispatcher_User,
                    office_code,
                    department_id,
                    ISNULL(commission_amount, 0) AS commission_amount,
                    CONVERT(DATE, commission_amount_date) AS commission_amount_date,
                    CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                    Quarterly_Commission_Percent,
                    Quarterly_Commission_Pay,
                    Annual_Commission_Percent,
                    Annual_Commission_Pay,
                    Processing_Status
            FROM BC_PayALL_temp WHERE Commission_Month = '$current_month' AND Commission_Year = '$current_year' AND office_code = '$office'
                AND Processing_Status = 'Pending'
            ORDER BY Commission_Amount_Date DESC";
            $filename = "Pending_Loads_";
        } elseif ($report === 'missing_pods') { // Missing POD's or Delivery Date Report
            $sql      = "SELECT dispatcher_user_id, 
                    order_id, 
                    Movement_ID, 
                    override_payee_id, 
                    Destin_City + ',' + Destin_State AS Destination, 
                    Destin_Schedule_Arrive_Early, 
                    Delivery_Date, 
                    bol_received
            FROM BC_PayALL_temp WHERE (bol_received = 'N' OR Delivery_Date is null) AND processing_status <> 'Void'";
            $filename = "Missing_POD_or_Delivery_Date_";
        } elseif ($report === 'overrides') { // Orders in the Negative Margin Override list
            $sql      = "SELECT Order_ID,
                Dispatcher_User,
                Override_Reason
                FROM BC_NegMargin_Overrides";
            $filename = "Negative_Margin_Overrides_";
        } else { // All Current Loads
            $sql      = "SELECT Commission_Month,
                    Commission_Year,
                    Movement_ID,
                    order_id,
                    Dispatcher_User,
                    office_code,
                    department_id,
                    ISNULL(commission_amount, 0) AS commission_amount,
                    CONVERT(DATE, commission_amount_date) AS commission_amount_date,
                    CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
                    Quarterly_Commission_Percent,
                    Quarterly_Commission_Pay,
                    Annual_Commission_Percent,
                    Annual_Commission_Pay,
                    Processing_Status
            FROM BC_PayALL_temp WHERE Commission_Month = '$current_month' 
                AND Commission_Year = '$current_year' AND office_code = '$office' 
                    AND Processing_Status IN ('Paid','Pending')
            ORDER BY Commission_Amount_Date DESC";
            //$sql      = "";
            $filename = "All_Current_Loads_";
        }
        exportCSV($sql, $filename);
    }
}
// Export details for commission pay
/* if (isset($_POST['export_commission_details'])) {
  $where    = "AND Processing_Status = 'Paid' AND commission_amount_date = (SELECT MAX (commission_amount_date) FROM BC_broker_commission_calc_temp)";
  $filename = "commission_details_";
  exportCommDetails($where, $user_id, $filename);
  }
  // Export details for pending loads
  if (isset($_POST['export_pending_details'])) {
  $where    = "AND Processing_Status = 'Pending'";
  $filename = "pending_details_";
  exportCommDetails($where, $user_id, $filename);
  }
  // Export details for void loads - Remove from page
  if (isset($_POST['export_void_details'])) {
  $where    = "AND Processing_Status = 'Void'";
  $filename = "void_details_";
  exportCommDetails($where, $user_id, $filename);
  }
 */
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Swift Logistics | <?php echo $title; ?></title>
        <link rel="icon" type="image/ico" href="images/swift_logistics_logo_circle.ico"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Data Tables -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>

    <body>

        <div id="wrapper">

            <!--Nav bar loaded here via include_navbar.php-->

            <div id="page-wrapper">

                <div class="container-fluid">
                    <?php if ($manager) { ?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Manager Dashboard<br/>
                                    <?php //echo "<pre>Current user: <strong>" . $user_id . "</strong></pre>"; ?>
                                    <small>Commission Details for all <?php echo $office; ?> Brokers</small>
                                </h1>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-4">
                                <!--GROSS MARGIN - PRIMARY BLUE PANEL-->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-percent fa-5x"></i>
                                                <div></div>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="panel-title"><?php echo $office; ?> GROSS MARGIN</div>
                                                <div class="not-so-huge">Pending: <?php echo $gross_margin_pending; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<a href="#">
                                        <div class="panel-footer">
                                            <span class="pull-left"><button>View Details</button></span>
                                            <span class="pull-right"><i class="fa fa-download"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>-->
                                </div>
                                <div class="row">
                                    <!--PENDING LOADS - YELLOW PANEL-->
                                    <div class="col-lg-12">
                                        <div class="panel panel-yellow">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-dollar fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <div class="panel-title">Quarterly Pay</div>
                                                        <div class="not-so-huge">Pending: $<?php echo $quarterly_pending; ?><br/>Paid: $<?php echo $quarterly_paid; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<a href="#">
                                                <div class="panel-footer">
                                                    <form method="POST" action="">
                                                        <span class="pull-left"><button type="submit" name="export_pending_details">Export Details</button></span>
                                                        <span class="pull-right"><i class="fa fa-download"></i></span>
                                                    </form>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>-->
                                        </div>
                                    </div>
                                </div>
                                <!--EXPORT DETAILS - PURPLE PANEL-->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-purple">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-download fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <div class="not-so-huge">Export Details</div>
                                                        <div><div id="message">If file is blank, there are no current records.<br/><?php echo $message; ?></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a>
                                                <div class="panel-footer">
                                                    <form method="POST" action="">
                                                        <span class="pull-left"><button type="submit" name="export_details_submit">Export</button></span>
                                                        <span class="pull-right"><i class="fa fa-file-excel-o"></i>&nbsp;<select name="report">
                                                                <option disabled selected value="">Choose Report</option>
                                                                <option value="current">Current Pay Period</option>
                                                                <option value="pending">Pending Loads</option>
                                                                <option value='missing_pods'>Missing POD/Delivery Date</option>
                                                                <?php if ($user_id === 'tund' || $user_id === 'stral' || $user_id === 'localhost\DEV') { ?><option value="overrides">Negative Margin Overrides</option><?php } ?>
                                                            </select></span>
                                                    </form>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!--COMMISSION PAY - GREEN PANEL-->
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-money fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="panel-title"><?php echo $office; ?> COMMISSION</div>
                                                <div class="not-so-huge"><?php echo $current_month_name . ": " . $comm_pay_current; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<a href="#">
                                        <div class="panel-footer">
                                            <form method="POST" action="">
                                                <span class="pull-left"><button type="submit" name="export_commission_details">Export Details</button></span>
                                                <span class="pull-right"><i class="fa fa-download"></i></span>
                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>-->
                                </div>
                                <!--RED PANEL-->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-red">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-dollar fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <div class="panel-title">Annual Pay</div>
                                                        <div class="not-so-huge">Pending: $<?php echo $annual_pending; ?><br/>Paid: $<?php echo $annual_paid; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<a href="#">
                                                <div class="panel-footer">
                                                    <form method="POST" action="">
                                                        <span class="pull-left"><button type="submit" name="export_void_details">Export Details</button></span>
                                                        <span class="pull-right"><i class="fa fa-download"></i></span>
                                                    </form>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </a>-->
                                        </div>
                                    </div>
                                </div>
                                <!--SEARCH RECORDS - CHARCOAL GREY PANEL-->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-charcoal">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-archive fa-5x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <div class="not-so-huge">Historical Commissions</div>
                                                        <div></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                <span class="pull-left"><button type="submit">Export</button></span>
                                                <span class="pull-right"><i class="fa fa-archive"></i></span>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <!--PIE CHART PANEL-->
                            <div class="col-lg-4">
                                <div class="panel panel-charcoal">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-pie-chart"></i> Load Count by Status*</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="pie-chart"></div>
                                        </div>
                                    </div>
                                    <a href="#">
                                        <div class="panel-footer">
                                            <form method="POST" action="">
                                                <span class="pull-left">*Not filtered by date</span>
                                                <span class="pull-right"></span>
                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div><!-- ./row-->


                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="page-header">All Records for 2017<br/>
                                    <small>For all employees assigned to the <?php echo $office; ?> Office</small>
                                </h1>
                                <table id="example" class ="table table-striped table-bordered">
                                    <thead>
                                        <tr style="color: #FFF;">
                                            <th>Month</th>
                                            <th>Year</th>
                                            <th>Movement</th>
                                            <th>Order</th>
                                            <th>Dispatcher</th>
                                            <th>Paid</th>
                                            <th>Paid Date</th>
                                            <th>Dispatch Date</th>
                                            <th>Quarterly Percent</th>
                                            <th>Quarterly Pay</th>
                                            <th>Annual Percent</th>
                                            <th>Annual Pay</th>
                                            <th>Processing Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="color: #FFF;">
                                            <th>Month</th>
                                            <th>Year</th>
                                            <th>Movement</th>
                                            <th>Order</th>
                                            <th>Dispatcher</th>
                                            <th>Paid</th>
                                            <th>Paid Date</th>
                                            <th>Dispatch Date</th>
                                            <th>Quarterly Percent</th>
                                            <th>Quarterly Pay</th>
                                            <th>Annual Percent</th>
                                            <th>Annual Pay</th>
                                            <th>Processing Status</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.DataTable -->
                        </div><!-- /.Row -->
                    <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script>
            new Morris.Donut({
                // ID of the element in which to draw the chart.
                element: 'pie-chart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    {label: 'Paid', value: '<?php echo $paid_load_count ?>'},
                    {label: 'Pending', value: '<?php echo $pending_load_count ?>'}
                ],
                colors: [
                    '#5CB85C', //green
                    '#F0AD4E', //yellow
                    '#D9534F'//red
                ],
                resize: true
            });
        </script>
        <script>

            $(document).ready(function () {
                //Add text input
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });

                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    "autoWidth": false,
                    "searching": true,
                    "dom": '<l<t>ip>',
                    "ajax": "/get_commission_details_manager.php",
                    "columns": [
                        {"data": "Commission_Month"},
                        {"data": "Commission_Year"},
                        {"data": "Movement_ID"},
                        {"data": "order_id"},
                        {"data": "Dispatcher_User"},
                        {"data": "commission_amount",
                            render: function (data, type, row) {
                                return '$' + data; // Adds dollar sign to pay column values
                            }
                        },
                        {"data": "commission_amount_date"},
                        {"data": "Dispatch_Date"},
                        {"data": "Quarterly_Commission_Percent"},
                        {"data": "Quarterly_Commission_Pay",
                            render: $.fn.dataTable.render.number(',', '.', 2) //Formats gross_margin to 2 decimal places
                        },
                        {"data": "Annual_Commission_Percent"},
                        {"data": "Annual_Commission_Pay",
                            render: $.fn.dataTable.render.number(',', '.', 2) //Formats gross_margin to 2 decimal places
                        },
                        {"data": "Processing_Status"},
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0}
                    ]
                });
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });//end tag
        </script>
    </body>
</html>