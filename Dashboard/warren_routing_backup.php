<?php
require 'include_navbar.php';
require 'include_functions.php';
$title   = 'Routing Guide Lookup';
$page_id = 'Routing_Guide';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
$page_access = getPageAccess($user_id, $page_id);
// Get Order ID from URL or form input
/*if (isset($_GET['OID'])) {
    $order_id = $_GET['OID'];
} else {
    $order_id = NULL;
}*/
?>
<html lang="en"><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <?php echo $violet_css; ?>
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <style>
            .btn-orange,.btn-orange:visited,.btn-orange:hover {
                color: #000;
                border-color: #EDB148;
                background-color: #EDB148
            }
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
                padding: 25px;
            }
            #panel_one {
                color: #000;
                border-color: #EDB148;
                background-color: #EDB148
            }
            table {
                margin-left: auto;
                margin-right: auto;
            }
            /* DataTable */
            #example th {
                background-color: #EDB148 !important;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if ($page_access) {?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?php echo $title; ?><br/></h1>
                            <?php echo $dev_server;
                            echo $debug; ?>
                            <form method="GET" action="">
                                Order <input type="text" name="OID"/>
                                <input type="submit" value="Get Routing Guide" class="btn-orange"/>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <div id="panel_one" class="div-content collapse in">
                                <h1 class="panel-heading">
                                <?php if (isset($order_id)) {
                                    echo 'Order ' . $order_id;
                                } else {
                                    echo 'Type in an order above to retrieve the routing guide.';
                                } ?>
                                </h1>
                                <div class="panel-footer">
                                    <table id="example" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                        <thead>
                                            <tr style="color: #000;">
                                                <th>Order</th>
                                                <th>BOL Number</th>
                                                <th>Carrier</th>
                                                <th>Freight</th>
                                                <th>B. Status</th>
                                                <th>M. Status</th>
                                                <th>Origin Address</th>
                                                <th>Destin Address</th>
                                                <th>Early Pickup</th>
                                                <th>Late Pickup</th>
                                                <th>Early Delivery</th>
                                                <th>Late Delivery</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr style="color: #000;">
                                                <th>Order</th>
                                                <th>BOL Number</th>
                                                <th>Carrier</th>
                                                <th>Freight</th>
                                                <th>B. Status</th>
                                                <th>M. Status</th>
                                                <th>Origin Address</th>
                                                <th>Destin Address</th>
                                                <th>Early Pickup</th>
                                                <th>Late Pickup</th>
                                                <th>Early Delivery</th>
                                                <th>Late Delivery</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.panel-footer -->
                            </div><!-- /.panel_one -->
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row -->
                    <?php } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        
        <script>
            $(document).ready(function () {
                // Suppress errors/warnings/alerts
                //$.fn.dataTable.ext.errMode = 'none';
                //Add text input fields to column headers
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: 'Loadboard',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "/get_warren_routing.php",
                    "columns": [
                        {"data": "Order_ID", width: "2%"},
                        {"data": "Blnum", width: "2%"},
                        {"data": "Carrier", width: "2%"},
                        {"data": "Freight_Charge",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "2%"
                        },
                        {"data": "Brokerage_Status", width: "2%"},
                        {"data": "Move_Status", width: "1%"},
                        {"data": "Origin_Address", width: "2%"},
                        {"data": "Destin_Address", width: "2%"},
                        {"data": "Early_Pickup", width: "2%"},
                        {"data": "Late_Pickup", width: "2%"},
                        {"data": "Early_Delivery", width: "2%"},
                        {"data": "Late_Delivery", width: "2%"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0},
                        {"visible": false,"searchable": false}
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Search bar
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </body>
</html>