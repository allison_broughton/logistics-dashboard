<?php
header('location:under_construction.php');
exit();
require 'include_navbar.php';
require 'include_functions.php';
$title        = 'Broker KPI';
$page_id      = 'Broker_KPI';
$current_user = stripUsername($username);
//////////////////////////////////// TRACKING AND EMULATION ///////////////////////////////////
// Insert user info to database to track page visits
if (!($user_id === 'localhost\DEV')) {
    trackVisit($page_id, $user_id, 'page_visit');
}
// Set user emulation form
$emulate_user_form = emulateUser('CS');
// Set current $user_id to chosen user
if (isset($_POST['emulate'])) {
    $user_id              = $_POST['emulate_user'];
    $update_emulated_user = "UPDATE Web_Page_Roles SET Emulate_User_ID = '$user_id' 
        WHERE Page_ID = '$page_id' AND User_ID = '$current_user'";
    $prepare_update       = odbc_prepare($conn, $update_emulated_user);
    try {
        odbc_execute($prepare_update);
    } catch (Exception $ex) {
        echo "<pre>" . odbc_errormsg($conn) . "</pre>";
    }
}
if ($username === 'localhost\DEV') {
    $admin = true;
} else {
    $admin = getAdminStatus($username, $page_id);
}
// If user is an admin, set their emulated user id
if ($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id, $user_id);
} elseif ($admin && $user_id === 'localhost\DEV') {
    $user_id = 'campaa';
}

/////////////////////////////////////// LINE CHART DATA ///////////////////////////////////////////////////
// Get dates from Broker KPI to use as labels and date parameters for each chart
function getDates($start_date, $end_date) {
    global $conn_dev;
    global $user_id;
    if ($start_date === '' || $end_date === '') {
        $TOP        = 'TOP 10';
        $date_range = '';
    } else {
        $TOP        = 'TOP 30';
        $date_range = "AND Dispatched_Date BETWEEN '$start_date' AND '$end_date'";
    }
    $string        = "SELECT CAST(B.Dispatched_Date AS DATE) AS Dispatched_Date
        FROM [Logistics_Data_Warehouse_Dev].[dbo].[Broker_KPI] B JOIN
            (SELECT DISTINCT $TOP Dispatched_Date AS Dispatched_Date
            FROM [Logistics_Data_Warehouse_Dev].[dbo].[Broker_KPI] WHERE BrokerID = '$user_id' $date_range
                ORDER BY Dispatched_Date DESC) AS A
       ON A.Dispatched_Date = B.Dispatched_Date GROUP BY b.Dispatched_Date ORDER BY B.Dispatched_Date ASC";
    $query         = odbc_prepare($conn_dev, $string);
    odbc_execute($query);
    $dates_array[] = "";
    while ($row           = odbc_fetch_array($query)) {
        $dates_array[] .= $row['Dispatched_Date'];
    }
    // Remove empty first element
    array_shift($dates_array);
    return $dates_array;
}

// Form handler for date range
$start_date = '';
$end_date   = '';
if (isset($_POST['filter_by_dates'])) {
    $start_date = sanitize($_POST['start_date']);
    $end_date   = sanitize($_POST['end_date']);
}
// Get labels for every chart
$labels_array = getDates($start_date, $end_date);
$days_array   = array_map(function($date) {
    return convertDate('j', $date);
}, $labels_array);
$short_dates = array_map(function($date) {
    return convertDate('n/j', $date);
}, $labels_array);
$labels               = implode("','", $short_dates);
// Loads per Broker chart
$loads_per_broker_arr = array_map(function($date) {
    global $user_id;
    return createChartData('loadsPerBroker', $user_id, $date);
}, $labels_array);
$loads_per_broker_data = implode(", ", $loads_per_broker_arr);
// Loads per Carrier chart
$loads_per_carrier_arr = array_map(function($date) {
    global $user_id;
    return createChartData('loadsPerCarrier', $user_id, $date);
}, $labels_array);
$loads_per_carrier_data = implode(", ", $loads_per_carrier_arr);
// Margin % chart
$margin_percent_arr     = array_map(function($date) {
    global $user_id;
    return createChartData('marginPercent', $user_id, $date);
}, $labels_array);
$margin_percent_data = implode(", ", $margin_percent_arr);
// Margin $ chart
$margin_dollar_arr   = array_map(function($date) {
    global $user_id;
    return createChartData('marginDollar', $user_id, $date);
}, $labels_array);
$margin_dollar_data = implode(", ", $margin_dollar_arr);
// Buy v. Market chart
$buy_market_arr     = array_map(function($date) {
    global $user_id;
    return createChartData('buyMarketRate', $user_id, $date);
}, $labels_array);
$buy_market_data = implode(", ", $buy_market_arr);

// Get chart aggregate totals
/* Loads per Broker */
$red_chart_yesterday     = getAggChartTotals('loadsPerBroker', 'Yesterday', $user_id);
$red_chart_this_week     = getAggChartTotals('loadsPerBroker', 'Week', $user_id);
$red_chart_this_month    = getAggChartTotals('loadsPerBroker', 'Month', $user_id);
/* Loads per Carrier */
$yellow_chart_yesterday  = getAggChartTotals('loadsPerCarrier', 'Yesterday', $user_id);
$yellow_chart_this_week  = getAggChartTotals('loadsPerCarrier', 'Week', $user_id);
$yellow_chart_this_month = getAggChartTotals('loadsPerCarrier', 'Month', $user_id);
/* Margin Percent */
$green_chart_yesterday   = getAggChartTotals('marginPercent', 'Yesterday', $user_id);
$green_chart_this_week   = getAggChartTotals('marginPercent', 'Week', $user_id);
$green_chart_this_month  = getAggChartTotals('marginPercent', 'Month', $user_id);
/* Margin Dollar */
$blue_chart_yesterday    = getAggChartTotals('marginDollar', 'Yesterday', $user_id);
$blue_chart_this_week    = getAggChartTotals('marginDollar', 'Week', $user_id);
$blue_chart_this_month   = getAggChartTotals('marginDollar', 'Month', $user_id);
/* Buy v. Market Rate */
$blue_chart_this_week    = getAggChartTotals('buyMarketRate', 'Week', $user_id);
$blue_chart_this_month   = getAggChartTotals('buyMarketRate', 'Month', $user_id);
?>
<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png"><!-- Favicon -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <?php echo $violet_css;?>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome CDN -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <link href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css" rel="stylesheet">
        <!--<link type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map" rel="stylesheet">
        <link type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss" rel="stylesheet">-->
        <script src='js/jquery.js'></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            #chart1 .ct-line {
                stroke: #D9534F;
            }
            #chart1 .ct-point {
                stroke: #D9534F;
            }
            #chart2 .ct-line {
                stroke: #F0AD4E;
            }
            #chart2 .ct-point {
                stroke: #F0AD4E;
            }
            #chart3 .ct-line {
                stroke: #5CB85C;
            }
            #chart3 .ct-point {
                stroke: #5CB85C;
            }
            #chart4 .ct-line {
                stroke: #337AB7;
            }
            #chart4 .ct-point {
                stroke: #337AB7;
            }
            #chart5 .ct-line {
                stroke: #663096;
            }
            #chart5 .ct-point {
                stroke: #663096;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
            function ctPointLabels(options) {
                return function ctPointLabels(chart) {
                    var defaultOptions = {
                        labelClass: 'ct-label',
                        labelOffset: {
                            x: 0,
                            y: -10
                        },
                        textAnchor: 'middle'
                    };

                    options = Chartist.extend({}, defaultOptions, options);

                    if (chart instanceof Chartist.Line) {
                        chart.on('draw', function (data) {
                            if (data.type === 'point') {
                                data.group.elem('text', {
                                    x: data.x + options.labelOffset.x,
                                    y: data.y + options.labelOffset.y,
                                    style: 'text-anchor: ' + options.textAnchor
                                }, options.labelClass).text(data.value.y);  // 07.11.17 added ".y"
                            }
                        });
                    }
                };
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($admin) {
                        echo "<div class='col-lg-12'>Emulating: <strong>" . $user_id . "</strong>$emulate_user_form</div>";
                    }
                    ?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Broker KPI's
                            </h1>
                            <?php echo $dev_server; ?>
                            <form method="post" action="">
                                Start Date <input type="date" name='start_date' value="<?php echo date($start_date); ?>"/>
                                End Date <input type="date" name='end_date' value="<?php echo date($end_date); ?>"/>
                                <input type='submit' name='filter_by_dates' value='Filter'/> (Default is last 10 days)
                            </form>
                        </div>
                    </div><!-- /.row -->
                    <!-- Page Content/Graphs -->
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-5">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Loads per Broker</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-4">YESTERDAY:<br/><?php echo $red_chart_yesterday; ?></div>
                                        <div class="col-lg-4">THIS WEEK:<br/><?php echo $red_chart_this_week; ?></div>
                                        <div class="col-lg-4">THIS MONTH:<br/><?php echo $red_chart_this_month; ?></div>
                                    </div><!--KPI = 'loadsPerBroker'-->
                                    <div id="chart1" class="ct-chart ct-minor-sixth"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Loads per Carrier</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-4">YESTERDAY:<br/><?php echo $yellow_chart_yesterday; ?></div>
                                        <div class="col-lg-4">THIS WEEK:<br/><?php echo $yellow_chart_this_week; ?></div>
                                        <div class="col-lg-4">THIS MONTH:<br/><?php echo $yellow_chart_this_month; ?></div>
                                    </div><!--KPI = 'loadsPerCarrier'-->
                                    <div id="chart2" class="ct-chart ct-minor-sixth"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2nd row of charts -->
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-5">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Margin %</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-4">YESTERDAY:<br/><?php echo $green_chart_yesterday; ?>%</div>
                                        <div class="col-lg-4">THIS WEEK:<br/><?php echo $green_chart_this_week; ?>%</div>
                                        <div class="col-lg-4">THIS MONTH:<br/><?php echo $green_chart_this_month; ?>%</div>
                                    </div><!--KPI = 'marginPercent'-->
                                    <div id="chart3" class="ct-chart ct-minor-sixth"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Margin $</h3>
                                </div><!--KPI = 'marginDollar'-->
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-4">YESTERDAY:<br/>$<?php echo $blue_chart_yesterday; ?></div>
                                        <div class="col-lg-4">THIS WEEK:<br/>$<?php echo $blue_chart_this_week; ?></div>
                                        <div class="col-lg-4">THIS MONTH:<br/>$<?php echo $blue_chart_this_month; ?></div>
                                    </div>
                                    <div id="chart4" class="ct-chart ct-minor-sixth"></div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-10">
                            <div class="panel panel-purple">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Buy v. Market</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-6">THIS WEEK:<br/><?php echo $blue_chart_this_week; ?>%</div>
                                        <div class="col-lg-6">THIS MONTH:<br/><?php echo $blue_chart_this_month; ?>%</div>
                                    </div><!--KPI = 'BuyvMarket'-->
                                    <div id="chart5" class="ct-chart ct-double-octave"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!--Chartist Plugins-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <!--<script src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>-->
        <script src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>
        <script>
            // Loads per Broker (Red Chart)
            new Chartist.Line('#chart1', {
                labels: ['<?php echo $labels; ?>'],
                series: [
                    [<?php echo $loads_per_broker_data; ?>]
                ]
            }, {
                plugins: [
                    Chartist.plugins.tooltip({
                        anchorToPoint: true
                    }),
                    ctPointLabels({
                        textAnchor: 'middle'
                    })
                ],
                axisY: {
                    onlyInteger: true
                },
                /*low: 0,*/
                chartPadding: 30,
                showArea: true
            });
            // Loads per Carrier (Yellow/Orange Chart)
            new Chartist.Line('#chart2', {
                labels: ['<?php echo $labels; ?>'],
                series: [
                    [<?php echo $loads_per_carrier_data; ?>]
                ]
            }, {
                plugins: [
                    Chartist.plugins.tooltip({
                        anchorToPoint: true
                    }),
                    ctPointLabels({
                        textAnchor: 'middle'
                    })
                ],
                axisY: {
                    onlyInteger: true
                },
                low: 0,
                chartPadding: 30,
                showArea: true
            });
            // Margin % (Green Chart)
            new Chartist.Line('#chart3', {
                labels: ['<?php echo $labels; ?>'],
                series: [
                    [<?php echo $margin_percent_data; ?>]
                ]
            }, {
                plugins: [
                    Chartist.plugins.tooltip({
                        anchorToPoint: true
                    }),
                    ctPointLabels({
                        textAnchor: 'middle'
                    })
                ],
                chartPadding: 30,
                showArea: true
            });
            // Margin $ (Blue Chart)
            new Chartist.Line('#chart4', {
                labels: ['<?php echo $labels; ?>'],
                series: [
                    [<?php echo $margin_dollar_data; ?>]
                ]
            }, {
                plugins: [
                    Chartist.plugins.tooltip({
                        anchorToPoint: true,
                        currency: '$'
                    }),
                    ctPointLabels({
                        textAnchor: 'middle'
                    })
                ],
                chartPadding: 30,
                showArea: true
            });
            // Buy v. Market (Purple Chart)
            new Chartist.Line('#chart5', {
                labels: ['<?php echo $labels; ?>'],
                series: [
                    [<?php echo $buy_market_data; ?>]
                ]
            }, {
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return -value;
                    }
                },
                plugins: [
                    Chartist.plugins.tooltip({
                        anchorToPoint: true
                    }),
                    ctPointLabels({
                        textAnchor: 'middle'
                    })
                ],
                /*low: 0,*/
                chartPadding: 30,
                showArea: false
            }).on('data', function (context) {
                context.data.series = context.data.series.map(function (series) {
                    return series.map(function (value) {
                        return -value;
                    });
                });
            });
        </script>
    </body>
</html>