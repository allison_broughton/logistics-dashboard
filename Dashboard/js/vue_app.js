/* global Vue */

/* Refs */
new Vue({
    el: '#vue-app-ref',
    data: {
        output: 'Your favorite food'
    },
    methods: {
        readRefs: function() {
            console.log(this.$refs.test.innerText);
            this.output = this.$refs.input.value;
        }
    }
});

/* Components */
Vue.component('greeting',{
    template: '<p>Hey there, I am {{ name }}. <button v-on:click="changeName" class="btn btn-aubergine">Change Name</button></p>',
    data: function() {
        return {
            name: 'Mercy'
        };
    },
    methods: {
        changeName: function() {
            this.name = 'Sombra';
        }
    }
});

new Vue({
    el: '#vue-app-one'
});

new Vue({
    el: '#vue-app-two'
});

/*var one = new Vue({
    el: '#vue-app-one',
    data: {
        title: 'Vue App One'
    },
    methods: {
        
    },
    computed: {
        greet:function() {
            return "Hello from app one :)";
        }
    }
});

var two = new Vue({
    el: '#vue-app-two',
    data: {
        title: 'Vue App Two'
    },
    methods: {
        changeTitle:function() {
            one.title = "Title Changed";
        }
    },
    computed: {
        greet:function() {
            return "Yo dudes, this is app 2 speaking to ya :)";
        }
    }
});

two.title = "Changed from outside";
*/

/*new Vue({
    el: '#vue-app',
    data: {
        health: 100,
        ended: false
    },
    methods: {
        punch:function() {
            this.health -= 10;
            if(this.health <= 0){
                this.ended = true;
            };
        },
        restart:function() {
            this.health = 100;
            this.ended = false;
        }
    },
    computed: {
        
    }
});*/

/*new Vue({
        el:'#vue-app',
        data:{
            name:'Allison',
            job:'Programmer',
            age: 25,
            website:'http://www.thenetninja.co.uk',
            websiteTag:'<a href="http://www.thenetninja.co.uk">The Net Ninja Website</a>',
            x:0,
            y:0,
            name2:'',
            age2:'',
            a: 0,
            b: 0,
            available: false,
            nearby: false,
            error:false,
            success:false,
            characters:['Widowmaker','Sombra','Soldier 76','Reinhardt'],
            healers: [
                {name:'Mercy', age:37},
                {name:'Lucio', age:26},
                {name:'Moira', age:48}
            ]
        },
        methods:{
            greet:function(time) {
                return 'Good ' + time + ' ' + this.name;
            },
            add:function(inc) {
                this.age += inc;
            },
            subtract:function(dec) {
                this.age -= dec;
            },
            updateXY:function(event) {
                this.x = event.offsetX;
                this.y = event.offsetY;
            },
            click:function() {
                alert('You clicked me');
            }
        },
        computed: {
                addToA:function() {
                    console.log('addToA');
                    return this.a + this.age;
                },
                addToB:function() {
                    console.log('addToB');
                    return this.b + this.age;
                },
                compClasses:function() {
                    return {
                        available: this.available,
                        nearby: this.nearby
                    };
                }
            }
     });*/