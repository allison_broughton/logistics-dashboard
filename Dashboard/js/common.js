//This settings for Jquery Datatables only
var Grid = {
    //http://legacy.datatables.net/usage/callbacks
    //lengthMenu: [[2, 10, 25, 50, -1], [2, 10, 25, 50, "All"]],
    //lengthMenu: [[10, 20, 50, 100, 200, 500, -1], [10, 20, 50, 100, 200, 500, "All"]],
    lengthMenu: [[2, 4, 6, 10, 20, 50, 100, 200, 500], [2, 4, 6, 10, 20, 50, 100, 200, 500]],
    emptyTable: "No Records found.",
    sDom: "Rfrtlip" //for showing page dropdown in bottom
};
var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
$(document).ready(function () {
    SITE.fnFontsRenderOnMacMachine();
    /* Fixed div width set */
    $('.fixed-filter').width($('.load-list-wrap, .available-loads').width())

    $(window).resize(function () {
        $('.fixed-filter').width($('.load-list-wrap, .available-loads').width())
    });



    $.fn.keepInSync = function ($targets) {
        // join together all the elements you want to keep in sync
        var $els = $targets.add(this);
        $els.on("keyup change", function () {
            var $this = $(this);
            // exclude the current element since it already has the value
            $els.not($this).val($this.val());
        });
        return this;
    };

    //Fast click for mobile
    if (typeof FastClick !== 'undefined') {
      // Don't attach to body if undefined
      if (typeof document.body !== 'undefined') {
        FastClick.attach(document.body);
      }
    }


});

var AccessControler = {
    GetIsActive: function (jsondata, timeOut, methodName, callback) {
        //console.log(jsondata, timeOut, methodName)
        $.ajax({
            type: "Get",
            data: jsondata,
            dataType: 'Json',
            timeout: timeOut,
            contentType: "application/json",
            url: methodName,
            success: function (response) {
                console.log(JSON.stringify(response));
                if (response.IsDeactivated != undefined) {
                    if (response.IsDeactivated == true) {
                        window.location.href = '/Account/CarrierDisApproved/';
                    }                   
                }
                else {
                    callback(response);
                }                
                //var IsActive = '{"IsSuccess":false,"Message":"Your account is Deactivated. Please contact with Carrier Admin.","StatusCode":0,"RedirectFrom":null}';
                //if (JSON.stringify(data) === IsActive) {
                //    window.location.href = '/Account/CarrierDisApproved/';
                //}
                //else {
                //    callback(data);
                //}
            }, error: function (request, status, error) {
                console.log(error);
            }
        });
    },

    PostIsActive: function (jsondata, timeOut, methodName, callback) {
        //console.log(jsondata, timeOut, methodName)
        $.ajax({
            type: "Post",
            data: jsondata,
            dataType: 'Json',
            timeout: timeOut,
            url: methodName,
            success: function (response) {
                console.log(JSON.stringify(response));
                if (response.IsDeactivated != undefined) {
                    if (response.IsDeactivated == true) {
                        window.location.href = '/Account/CarrierDisApproved/';
                    }                  
                }
                else {
                    callback(response);
                }
                //var IsActive = '{"IsSuccess":false,"Message":"Your account is Deactivated. Please contact with Carrier Admin.","StatusCode":0,"RedirectFrom":null}';
                //if (JSON.stringify(response) === IsActive) {
                //    window.location.href = '/Account/CarrierDisApproved/';
                //}
                //else {
                //    callback(response);
                //}
            }, error: function (request, status, error) {
                console.log(error);
                showKnightAlert("divAlterForknight", false, "Invalid data", 10000);
            }
        });
    },
}


//Manage textbox null values
function managetextTitleProperty() {
    $('.label-control').each(function () {
        if ($(this).val() == '') {
            //console.log("1:" + $(this).val());
            $(this).removeAttr("value");
            $(this).siblings("label").removeClass('active');
        }
        else {
            //console.log("2:" + $(this).val());
            $(this).siblings("label").addClass('active');
        }
    });
}
//Used to Show Error or Success Message on Page.
var loaderDiv = "#loader";
(function ($) {
    var xhrPool = [];
    $(document).ajaxSend(function (e, jqXHR, options) {
        xhrPool.push(jqXHR);
        $(".overlay-loader").show();
        $(loaderDiv).css({ "display": "block" });
        //$(loaderDiv).center().css({ "display": "block" });
    });
    $(document).ajaxComplete(function (e, jqXHR, options) {
        xhrPool = $.grep(xhrPool, function (x) { return x != jqXHR });
        if (xhrPool == null || xhrPool.length <= 0) {
            setTimeout(function () {
                $(".overlay-loader").hide();
                $(loaderDiv).css({ "display": "none" });
                //$(loaderDiv).center().css({ "display": "none" });
            }, 1000);
        }
    });
})(jQuery);
function showMessage(messagedvId, isSuccess, message, msgTime) {
    msgTime = typeof msgTime !== 'undefined' ? msgTime : 20000;
    $('#' + messagedvId).removeClass();
    $('#' + messagedvId + ' span').html(message);
    if (isSuccess == "warning") {
        $('#' + messagedvId).addClass('alert-box warning radius');
    }
    else {
        if (isSuccess) {
            $('#' + messagedvId).addClass('alert-box success radius');
        }
        else {
            $('#' + messagedvId).addClass('alert-box alert radius');
        }
    }
    $('#' + messagedvId).show();
    var timeoutHandle = window.setTimeout(function () {
        $('#' + messagedvId + ' span').html('');
        $('#' + messagedvId).hide();
    }, msgTime);
    // in your click function, call clearTimeout
    window.clearTimeout(timeoutHandle);
    // then call setTimeout again to reset the timer
    var timeoutHandle = window.setTimeout(function () {
        $('#' + messagedvId + ' span').html('');
        $('#' + messagedvId).hide();
    }, msgTime);
}
function showKnightAlert(messagedvId, isSuccess, message, symbol, msgTime) {
    //console.log(messagedvId, isSuccess, message, symbol, msgTime);
    msgTime = typeof msgTime !== 'undefined' ? msgTime : 20000;
    $('#' + messagedvId + ' span').html(message);
    $('#' + messagedvId).modal('show');
    //$('#' + messagedvId).show();
    var timeoutHandle = window.setTimeout(function () {
        $('#' + messagedvId + ' span').html('');
        $('#' + messagedvId).modal('hide');
        //$('#' + messagedvId).hide();
    }, msgTime);
    // in your click function, call clearTimeout
    window.clearTimeout(timeoutHandle);
    // then call setTimeout again to reset the timer
    var timeoutHandle = window.setTimeout(function () {
        $('#' + messagedvId + ' span').html('');
        $('#' + messagedvId).modal('hide');
        //$('#' + messagedvId).hide();
    }, msgTime);
}
//ScheduleDateFrom: FromDate, ScheduleDateTo:ToDate,formatType:Return Design
//AvailableLoadsTMW.jsx
function GetFormatedDate(ScheduleDateFrom, ScheduleDateTo, formatType) {
    //----------12/01 4:30 - 7:00p-------------
    if (ScheduleDateFrom != null && ScheduleDateTo != null) {
        //Get fromdate date Values
        var dateFrom = new Date(parseInt(ScheduleDateFrom.substr(6)));
        var dateStringFrom = new Date(dateFrom);
        //Get Integer Value only Do not use for Get Month name.
        var fromMonth = dateStringFrom.getMonth() + 1;
        var fromYear = dateStringFrom.getFullYear();
        var fromDate = dateStringFrom.getDate();
        var fromHours = dateStringFrom.getHours();
        var fromMinutes = dateStringFrom.getMinutes();
        //Change Time  in 12 hours Format un Comment Next Line
        //fromHours = fromHours % 12;
        fromMinutes = fromMinutes ? fromMinutes : 12; // the hour '0' should be '12'
        //----------------------
        //Get toDate date Values
        var dateTo = new Date(parseInt(ScheduleDateTo.substr(6)));
        var dateStringTo = new Date(dateTo);
        var toMonth = dateStringTo.getMonth();
        var toYear = dateStringTo.getFullYear();
        var toDate = dateStringTo.getDate();
        var today = dateStringTo.getDay();
        var toHours = dateStringTo.getHours();
        var toMinutes = dateStringTo.getMinutes();
        var ampm = toHours >= 12 ? 'a' : 'p';
        //Change Time in 12 hours Format un Comment Next Line
        //toHours = toHours % 12;
        toHours = toHours ? toHours : 12; // the hour '0' should be '12'
        toMinutes = toMinutes < 10 ? '0' + toMinutes : toMinutes;
        //--------------------------------
        var formatedDate = "";
        if (formatType == 1) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromMonth + "/" + fromDate;//+ ' ' + fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 2) {
            formatedDate = ("0" + (fromMonth)).slice(-2) + "/" + fromDate;
        }
        if (formatType == 3) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 4) {
            formatedDate = weekday[today] + " " + monthNames[toMonth] + " " + toDate;
        }
        //Dispaly Formate 03/01  16:30 - 19:00
        if (formatType == 5) {
            formatedDate = fromMonth + "/" + fromDate + " " + fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;
        }
        if (formatType == 6) {
            formatedDate = fromDate + "/" + fromMonth + "/" + fromYear + " " + fromHours + ":" + fromMinutes;
        }
        if (formatType == 7) {
            formatedDate = fromMonth + "/" + fromDate + "/" + fromYear;
        }
        if (formatType == 8) {
            formatedDate = ("0" + (fromMonth)).slice(-2) + "/" + fromDate + "/" + fromYear;
        }
        return formatedDate;
    }
    else {
        return "";
    }
}
function FormatedDate(ScheduleDateFrom, ScheduleDateTo, formatType) {
    //----------12/01 4:30 - 7:00p-------------
    if (ScheduleDateFrom != null && ScheduleDateTo != null) {
        //Get fromdate date Values
        var dateFrom = ScheduleDateFrom;
        var dateStringFrom = new Date(dateFrom);
        //Get Integer Value only Do not use for Get Month name.
        var fromMonth = dateStringFrom.getMonth() + 1;
        var fromYear = dateStringFrom.getFullYear();
        var fromDate = dateStringFrom.getDate();
        var fromHours = dateStringFrom.getHours();
        var fromMinutes = dateStringFrom.getMinutes();
        //Change Time  in 12 hours Format un Comment Next Line
        //fromHours = fromHours % 12;
        fromMinutes = fromMinutes ? fromMinutes : 12; // the hour '0' should be '12'
        //----------------------
        //Get toDate date Values
        var dateTo = ScheduleDateTo
        var dateStringTo = new Date(dateTo);
        var toMonth = dateStringTo.getMonth();
        var toDate = dateStringTo.getDate();
        var today = dateStringTo.getDay();
        var toHours = dateStringTo.getHours();
        var toMinutes = dateStringTo.getMinutes();
        var ampm = toHours >= 12 ? 'a' : 'p';
        //Change Time in 12 hours Format un Comment Next Line
        //toHours = toHours % 12;
        toHours = toHours ? toHours : 12; // the hour '0' should be '12'
        toMinutes = toMinutes < 10 ? '0' + toMinutes : toMinutes;
        //--------------------------------
        var formatedDate = "";
        if (formatType == 1) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromMonth + "/" + fromDate;//+ ' ' + fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 2) {
            formatedDate = fromMonth + "/" + fromDate;
        }
        if (formatType == 3) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 4) {
            formatedDate = weekday[today] + " " + monthNames[toMonth] + " " + toDate;
        }
        // time in hour and Minute for active load
        if (formatType == 5) {
            formatedDate = toHours + ":" + toMinutes;
        }
        
        return formatedDate;
    }
    else {
        return "";
    }
}

function GetFormatedNonJsonDate(ScheduleDateFrom, ScheduleDateTo, formatType) {
    //----------12/01 4:30 - 7:00p-------------
    if (ScheduleDateFrom != null && ScheduleDateTo != null) {
        //Get fromdate date Values
        var dateFrom = new Date(ScheduleDateFrom);
        var dateFrom = new Date(ScheduleDateFrom);
       
        if (dateFrom == "Invalid Date") {
            dateFrom = new Date(ScheduleDateFrom.replace(/-/g, '/'));
        }
        var dateStringFrom = new Date(dateFrom);
        //Get Integer Value only Do not use for Get Month name.
        var fromMonth = dateStringFrom.getMonth() + 1;
        var fromYear = dateStringFrom.getFullYear();
        var fromDate = dateStringFrom.getDate();
        var fromHours = dateStringFrom.getHours();
        var fromMinutes = dateStringFrom.getMinutes();
        var minutes = dateStringFrom.getMinutes();
        //Change Time  in 12 hours Format un Comment Next Line
        //fromHours = fromHours % 12;
        fromMinutes = fromMinutes ? fromMinutes : 12; // the hour '0' should be '12'
        //----------------------
        //Get toDate date Values
        var dateTo = new Date(ScheduleDateTo);
        if (dateTo == "Invalid Date") {
            dateTo = new Date(ScheduleDateTo.replace(/-/g, '/'));
            var arr = ScheduleDateTo.split(/[- :]/);
            var date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
            dateTo = date;
        }
        var dateStringTo = new Date(dateTo);
        var toMonth = dateStringTo.getMonth() +1;
        var toDate = dateStringTo.getDate();
        var today = dateStringTo.getDay();
        var year = dateStringTo.getFullYear();
        var toHours = dateStringTo.getHours();
        var toMinutes = dateStringTo.getMinutes();
        var ampm = toHours >= 12 ? 'a' : 'p';
        //Change Time in 12 hours Format un Comment Next Line
        //toHours = toHours % 12;
        toHours = toHours ? toHours : 12; // the hour '0' should be '12'
        toMinutes = toMinutes < 10 ? '0' + toMinutes : toMinutes;
        //--------------------------------
        var formatedDate = "";
        if (formatType == 1) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromMonth + "/" + fromDate;//+ ' ' + fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 2) {
            formatedDate = fromMonth + "/" + fromDate;
        }
        if (formatType == 3) {
            //Show AM/PM unComment add ampm varible in return string
            formatedDate = fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;// + ampm;
        }
        if (formatType == 4) {
            formatedDate = weekday[today] + " " + monthNames[toMonth] + " " + ("0" + (toDate)).slice(-2);
        }
        //Dispaly Formate 03/01  16:30 - 19:00
        if (formatType == 5) {
            formatedDate = fromMonth + "/" + fromDate + " " + fromHours + ":" + fromMinutes + " - " + toHours + ":" + toMinutes;
        }
        if (formatType == 6) {
            formatedDate = ("0" + (fromHours)).slice(-2) + ":" + ("0" + (minutes)).slice(-2);
        }
        if (formatType == 7) {
            formatedDate = fromDate + "/" + fromMonth + "/" + fromYear + " " + fromHours + ":" + fromMinutes;
        }
        if (formatType == 8)
        {
            formatedDate = fromMonth + "/" + fromDate + "/" + fromYear;
        }
        if (formatType == 9)
        {
            var todayDate = new Date();
            if (today == todayDate.getDay() && toMonth == todayDate.getMonth() + 1 && year == todayDate.getFullYear())
            {
                formatedDate = "Today " + toHours + ":" + toMinutes;
            }
            else {
                formatedDate = fromDate + "/" + toMonth + "/" + year + " " + toHours + ":" + toMinutes;
            }
            {

            }
        }

        return formatedDate;

    }
    else {
        return "";
    }
}

function Close()
{
    $(".chat_window").remove();
}