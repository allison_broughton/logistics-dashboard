<?php
require 'include_functions.php';
$page_id = 'BC_Broker_Report';
$title   = 'Broker Reports';
$real_id = stripUsername($username); // Avoids inserting emulated user id when report is clicked
// Insert user info to database to track page visits
if (!($user_id === 'localhost\DEV')) {
    trackVisit('BC_Broker_Report', $real_id, 'page_visit');
}
// Set user emulation form
$emulate_user_form = emulateUser('CS');
// Set current $user_id to chosen user
if (isset($_POST['emulate'])) {
    $user_id              = $_POST['emulate_user'];
    $current_user         = stripUsername($username);
    $update_emulated_user = "UPDATE Web_Page_Roles SET Emulate_User_ID = '$user_id' 
        WHERE Page_ID = '$page_id' AND User_ID = '$current_user'";
    $prepare_update       = odbc_prepare($conn, $update_emulated_user);
    try {
        odbc_execute($prepare_update);
    } catch (Exception $ex) {
        echo "<pre>" . odbc_errormsg($conn) . "</pre>";
    }
}
// Page Access
if ($username === 'localhost\DEV') {
    $admin = true;
} else {
    $admin = getAdminStatus($username, $page_id);
}
// If user is an admin, set their emulated user id
if ($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id, $user_id);
} elseif ($admin && $user_id === 'localhost\DEV') {
    $user_id = 'campaa';
}
$page_access  = getPageAccess($user_id, $page_id);
//$limiter      = "dispatcher_user_id = '$user_id'";
//$office       = getOffice($user_id);
///////////////////////////////////////////// REPORTS ///////////////////////////////////////////////////////////
$report_title = "Records for " . $current_year;
$ajax_src     = '/get_broker_report.php';
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
    </head>

    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($admin) {
                        echo "<div class='col-lg-12'>Emulating: <strong>" . $user_id . "</strong>$emulate_user_form</div>";
                    }
                    if ($page_access) {?>
                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="page-header"><span style="color: #005590;"><?php echo $report_title; ?></span><br/>
                                    <small>Click the Excel button below once the table has loaded in order to export data. Rows with an Order ID of 'HOTLDADJ' denote a Hot Load Adjustment.</small>
                                </h1><?php echo $dev_server;?>
                                <table id="example" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                    <thead>
                                        <tr style="color: #FFF;">
                                            <th>Order</th>
                                            <th>Movement</th>
                                            <th>Margin</th>
                                            <th>Status</th>
                                            <th>BOL?</th>
                                            <th>Dispatch</th>
                                            <th>Delivery</th>
                                            <th>Payee</th>
                                            <th>Dispatcher</th>
                                            <th>Paid On</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="color: #FFF;">
                                            <th>Order</th>
                                            <th>Movement</th>
                                            <th>Margin</th>
                                            <th>Status</th>
                                            <th>BOL?</th>
                                            <th>Dispatch</th>
                                            <th>Delivery</th>
                                            <th>Payee</th>
                                            <th>Dispatcher</th>
                                            <th>Paid On</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.DataTable -->
                        </div><!-- /.Row -->
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <script>
            // Data Table - All Records for Current Year
            $(document).ready(function () {
                // Suppress errors/warnings/alerts
                //$.fn.dataTable.ext.errMode = 'none';
                //Add text input fields to column headers
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: '<"top"Bf>tr<"bottom"lip><"clear">',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: '<?php echo $report_title; ?>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "<?php echo $ajax_src; ?>",
                    "columns": [
                        {"data": "order_id", width: "5%"},
                        {"data": "movement_id", width: "5%"},
                        {"data": "movement_margin",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "2%"
                        },
                        {"data": "processing_status", width: "2%"},
                        {"data": "bol_received", width: "1%"},
                        {"data": "Dispatch_Date", width: "5%"},
                        {"data": "Delivery_Date", width: "5%"},
                        {"data": "override_payee_id", width: "5%"},
                        {"data": "Dispatcher_User", width: "5%"},
                        {"data": "Pay_Date", width: "5%"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0},
                        {"visible": false,"searchable": false}
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Search bar
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </body>
</html>