<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Hotload Override';
$page_id = 'Hotload_Override';
// Check Admin Status
if ($username === 'localhost\DEV') {
    $admin = true;
} else {
    $admin = getAdminStatus($username, $page_id);
}
//Before going live, add each manager to Web_Page_Roles with Page_ID = 'Hotload_Override'
//$page_access = getLeaderStatus($user_id, $page_id);
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Swift Logistics | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>
        <script src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($admin) {
                        //echo "<pre>Debug: " . $bar_data . "</pre>";
                        ?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Hotload Override<br/>
                                </h1>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="page-header">Hotloads
                                    <small>Edit a cell's value by clicking and typing in a new value. Hit Enter when finished to save.</small>
                                </h1>
                                <table id="example" class ="table table-striped table-bordered">
                                    <thead>
                                        <tr style="color: #FFF;">
                                            <th>State</th>
                                            <th>Broker</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="color: #FFF;">
                                            <th>State</th>
                                            <th>Broker</th>
                                        </tr>
                                    </tfoot>
                                </table><!-- /.DataTable -->
                            </div>
                        </div>
                            <?php
                        } else {
                            echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                        }
                        ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <script>
            // Data Table - Hot Loads
            $(document).ready(function () {
                //Add text input - thead th
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5'
                    ],
                    "ajax": "/get_hotloads.php",
                    "columns": [
                        {"data": "State"},
                        {"data": "Broker"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0}
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Add the search fields to the header
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });//end tag
        </script>

    </body>
</html>