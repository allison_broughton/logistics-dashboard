<?php
//https://swiftcarriers.rmissecure.com/_s/reg/GeneralRequirementsV2.aspx
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Swift Logistics, LLC | Registration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--BS4 CDN-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!--Custom CSS Stylesheet-->
        <link rel="stylesheet" href="css/bootstrap4_style.css">
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand font-weight-bold" href="#">Swift Logistics, LLC</a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/swiftcarriers_registration.php">Registration <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://swiftcarriers.rmissecure.com/_s/client/UserClientLogin.aspx">Client Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://swiftcarriers.rmissecure.com/_c/std/carrier/Login.aspx">Carrier Login</a>
                    </li>
                </ul>
                <!--<form class="form-inline mt-2 mt-md-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>-->
            </div>
        </nav>

        <!-- Begin page content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="logo"><img src="images/Swift_Logistics.png" alt="Swift Logistics logo"/></div>
                </div>
                <div class="col-lg-6">
                    <span class="float-lg-right">
                    <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                    <div id="cicCwI" style="z-index:100;position:absolute"></div>
                    <div id="sccCwI" style="display:inline"></div>
                    <div id="sdcCwI" style="display:none"></div>
                    <script type="text/javascript">
                        var secCwI = document.createElement("script"); 
                        secCwI.type = "text/javascript"; 
                        var secCwIs = (location.protocol.indexOf("https") == 0 ? "https" : "http") + "://image.providesupport.com/js/rmis_trans/safe-standard.js?ps_h=cCwI&ps_t=" + new Date().getTime(); 
                        setTimeout("secCwI.src=secCwIs;document.getElementById('sdcCwI').appendChild(secCwI)", 1)
                    </script>
                    <noscript><div style="display:inline">
                        <a href="http://www.providesupport.com?messenger=rmis_trans" id="hiddenChat">Customer Support</a>
                    </div></noscript>
                    <!-- END ProvideSupport.com Graphics Chat Button Code -->
                    </span>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>Carrier Registration</h2>
                    <p class="lead">Thank you for your interest in becoming an approved carrier for Swift Logistics, LLC. Below are some of our key 
                        requirements in order for your company to qualify as an approved carrier. All elements identified as 'required' must be 
                        completed in order for the qualification process to be completed. If your company is not compliant to one or several of the 
                        qualification criteria, you may return at another time to register once the non-compliant items are corrected.
                    </p>

                    <h4>To become an Approved carrier for Swift Logistics, LLC you must provide and meet the following criteria:</h4>
                    <ul class="outer-list">
                        <li>Provide your MC/MX# or DOT# to begin registration. Intrastate carriers without a DOT# may provide an applicable state registration number to begin registration.</li>
                        <li> Carrier Contact must be authorized to enter into and bind your company to Swift Transportation Agreement.</li>
                        <li>Validate or provide the carrier contact information.</li>
                        <li> Submit an electronic W9.</li>
                        <li>Complete the Carrier Profile.</li>
                        <li>Read and ACCEPT the &quot;Swift Logistics, LLC Agreement&quot;. </li>
                        <li>Provide a Certificate of Insurance that meets the insurance minimum requirements below. Certificates must be submitted from your insurance producer (agent);
                            <ul class="inner-list">
                                <li>$100,000 Cargo Coverage. </li>
                                <li>$1,000,000 Auto Liability Coverage.</li>
                                <li>$1,000,000 General Liability Coverage.</li>
                                <li>$1,000,000 Umbrella/excess covering Auto Liability and General Liability.</li>
                                <li>Any carrier with more than 5 employees requires Worker's Compensation (If employee count grows greater than 5, carrier must immediately supply Worker's Comp.)</li>
                            </ul>
                        </li>
                        <li style="text-decoration: none;">If RMIS does not already have a copy of your certificate on file, we will request one for you from your insurance agent (producer).</li>
                        <li>You will be required to meet the following Swift Logistics, LLC Carrier Compliance Requirements:
                            <ul class="inner-list">
                                <li>Carrier shall at all times maintain a &quot;Satisfactory&quot;safety rating or &quot;No&quot;rating by the Federal Motor Carrier Safety Administration (FMCSA), U.S. Department of Transportation. </li>
                                <li>An Unsatisfactory or Conditional safety rating disqualifies the carrier.</li>
                                <li>Carrier must have an active MC/DOT number for 180 days or more. Carriers that have not had an active MC/DOT number for a minimum of 180 days are not qualified.</li>
                                <li>Companies must have active Common or Contract Authority.</li>
                                <li>Companies with Broker Authority only cannot register, and must contact Carrier Relations at <a href="mailto:carrierrelations@swifttrans.com">carrierrelations@swifttrans.com</a>.</li>
                                <li> Companies with Broker Authority accompanied with active Common or Contract Authority will be allowed to register. Carriers must understand double brokering Swift Logistics freight is strictly prohibited. </li>
                                <li>All Carriers must have appropriate Federal, State, or Provincial Authority.</li>
                                <li> Financially stable.</li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 offset-md-5">
                    <button type="button" class="btn btn-secondary"><a href="swiftcarriers_prequalification.php">Go To Next Step</a></button>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">For help or questions regarding the registration process on this website, contact RMIS at 888-643-8174.
                    &copy; 2017 <a href='http://www.registrymonitoring.com'>Registry Monitoring Insurance Services, Inc.</a></span>
            </div>
        </footer>
        <!-- Bootstrap core JavaScript -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/js/bootstrap.min.js"><\/script>');</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>