<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Activity Tracker';
$page_id = 'Activity_Tracker';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Check page access
$page_access = getPageAccess($user_id, $page_id);
// Filter by date and user type
if(!isset($_POST['filter'])) {
    $date = $today;
    $user_type = 'Active';
} else {
    $date = $_POST['date_filter'];
    $user_type = $_POST['user_type'];
}
///////////////////////////////////////////// BAR CHART DATA ////////////////////////////////////////////////////
// Labels
$user_list  = getUsers($user_type);
$full_names = array_map("getFullName", $user_list);
$labels     = implode("','", $full_names);
///////////////////////////////////////////// BC_Broker page visit //////////////////////////////////////////////
$bc_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker',$date,"Event = 'page_visit'");
}, $user_list);
$bc_data = implode(", ", $bc_arr);
///////////////////////////////////////////// BC_Broker_Report page visit ///////////////////////////////////////
$bc_report_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker_Report',$date,"Event = 'page_visit'");
}, $user_list);
$bc_report_data   = implode(", ", $bc_report_arr);
///////////////////////////////////////////// BC_Leaderboard page visit /////////////////////////////////////////
$leaderboard_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Leaderboard',$date,"Event = 'page_visit'");
}, $user_list);
$leaderboard_data = implode(", ", $leaderboard_arr);
///////////////////////////////////////////// Broker_Activity page visit ////////////////////////////////////////
$broker_activity_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Broker_Activity_Tracker',$date,"Event = 'page_visit'");
}, $user_list);
$broker_activity_data = implode(", ", $broker_activity_arr);
///////////////////////////////////////////// Carrier_Lookup page visit /////////////////////////////////////////
$carrier_lookup_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Carrier_Lookup',$date,"Event = 'page_visit'");
}, $user_list);
$carrier_lookup_data = implode(", ", $carrier_lookup_arr);
///////////////////////////////////////////// Home_Page page visit //////////////////////////////////////////////
$home_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Home_Page',$date,"Event = 'page_visit'");
}, $user_list);
$home_data = implode(", ", $home_arr);
///////////////////////////////////////////// Hotload_Override page visit ///////////////////////////////////////
$hotload_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Hotload_Override',$date,"Event = 'page_visit'");
}, $user_list);
$hotload_data   = implode(", ", $hotload_arr);
///////////////////////////////////////////// Negative_Margin page visit ////////////////////////////////////////
$negmargin_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Negative_Margin',$date,"Event = 'page_visit'");
}, $user_list);
$negmargin_data   = implode(", ", $negmargin_arr);
///////////////////////////////////////////// New_Hire_Form page visit //////////////////////////////////////////
$newhire_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'New_Hire_Form',$date,"Event = 'page_visit'");
}, $user_list);
$newhire_data = implode(", ", $newhire_arr);
///////////////////////////////////////////// Spot_Rate_Modifier page visit /////////////////////////////////////
$spotmod_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Spot_Rate_Modifier',$date,"Event = 'page_visit'");
}, $user_list);
$spotmod_data = implode(", ", $spotmod_arr);
///////////////////////////////////////////// User_Management page visit ////////////////////////////////////////
$usermgmt_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Transit_Calculator',$date,"Event = 'page_visit'");
}, $user_list);
$usermgmt_data = implode(", ", $usermgmt_arr);
///////////////////////////////////////////// McLoop page visit /////////////////////////////////////////////////
$mcloop_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'McLeod_Backup',$date,"Event = 'page_visit'");
}, $user_list);
$mcloop_data = implode(", ", $mcloop_arr);
///////////////////////////////////////////// McLoop page visit /////////////////////////////////////////////////
$routing_arr = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Routing_Guide',$date,"Event = 'page_visit'");
}, $user_list);
$routing_data = implode(", ", $routing_arr);
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
    </head>
    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($page_access) {?>
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Activity Tracker<br/>
                                </h1>
                                <?php echo $dev_server;?>
                                <form method="post" action="">
                                    <input type="date" name='date_filter' value="<?php if ($date === '') {echo $today;} else {echo date($date);}?>"/>
                                    <select name="user_type">
                                        <option value=''></option>
                                        <option value='Active'>Active</option>
                                        <option value='Admin'>Admin</option>
                                        <option value='All'>All</option>
                                        <option value='Broker'>Broker</option>
                                    </select>
                                    <input type='submit' name='filter' value='Filter'/>
                                </form>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <!--chart.js Stacked Chart Panel-->
                                <div id="leaderboard">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bar-chart "></i> Page Visits for <?php echo $date;?></h3>
                                        </div>
                                        <div class="panel-body">
                                            <canvas id="officeLeaderboard" width="200" height="75"></canvas>
                                        </div>
                                        <div class="panel-footer">
                                            <span class="pull-left"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                    }
                    ?>

                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>

        <script>
            var ctx = document.getElementById("officeLeaderboard");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['<?php echo $labels; ?>'],
                    datasets: [{
                            type: 'bar',
                            label: 'Commissions',
                            data: [<?php echo $bc_data; ?>],
                            backgroundColor: 'red',
                            borderColor: 'red',
                            hoverBackgroundColor: 'red',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Broker Report',
                            data: [<?php echo $bc_report_data; ?>],
                            backgroundColor: 'orangered',
                            borderColor: 'orangered',
                            hoverBackgroundColor: 'orangered',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Leaderboard',
                            data: [<?php echo $leaderboard_data; ?>],
                            backgroundColor: 'orange',
                            borderColor: 'orange',
                            hoverBackgroundColor: 'orange',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Broker Activity',
                            data: [<?php echo $broker_activity_data; ?>],
                            backgroundColor: 'yellow',
                            borderColor: 'yellow',
                            hoverBackgroundColor: 'yellow',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Carrier Lookup',
                            data: [<?php echo $carrier_lookup_data; ?>],
                            backgroundColor: 'yellowgreen',
                            borderColor: 'yellowgreen',
                            hoverBackgroundColor: 'yellowgreen',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Home Page',
                            data: [<?php echo $home_data; ?>],
                            backgroundColor: 'green',
                            borderColor: 'green',
                            hoverBackgroundColor: 'green',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Hotload Override',
                            data: [<?php echo $hotload_data; ?>],
                            backgroundColor: 'springgreen',
                            borderColor: 'springgreen',
                            hoverBackgroundColor: 'springgreen',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Negative Margin',
                            data: [<?php echo $negmargin_data; ?>],
                            backgroundColor: 'cyan',
                            borderColor: 'cyan',
                            hoverBackgroundColor: 'cyan',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'New Hire/Term',
                            data: [<?php echo $newhire_data; ?>],
                            backgroundColor: 'dodgerblue',
                            borderColor: 'dodgerblue',
                            hoverBackgroundColor: 'dodgerblue',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Spot Rate Modifier',
                            data: [<?php echo $spotmod_data; ?>],
                            backgroundColor: 'blue',
                            borderColor: 'blue',
                            hoverBackgroundColor: 'blue',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Transit Calculator',
                            data: [<?php echo $usermgmt_data; ?>],
                            backgroundColor: 'blueviolet',
                            borderColor: 'blueviolet',
                            hoverBackgroundColor: 'blueviolet',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'McLoop',
                            data: [<?php echo $mcloop_data; ?>],
                            backgroundColor: 'indigo',
                            borderColor: 'indigo',
                            hoverBackgroundColor: 'indigo',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Warren Routing Guide',
                            data: [<?php echo $routing_data; ?>],
                            backgroundColor: 'magenta',
                            borderColor: 'magenta',
                            hoverBackgroundColor: 'magenta',
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    title: {
                        display: false,
                        text: 'Page Visits per User'
                    },
                    legend: {
                        display: true
                    },
                    scales: {
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 1
                                }
                            }],
                        xAxes: [{
                                stacked: true,
                                ticks: {
                                    autoSkip: false,
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
    </body>
</html>