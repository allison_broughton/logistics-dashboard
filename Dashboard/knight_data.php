<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Testing with Knight Data';

$page_access = true;

$ajax_src = "/get_knight_data_test.php";
?>
<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <style>
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #panel_one {
                color: #fff;
                border-color: #B10D28;
                background-color: #B10D28;
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
            #example th {
                background-color: #B10D28;
            }
            .btn-primary {
                background-color: #B10D28;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
    </head>
    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if($page_access) {?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                <?php echo $title;?>
                            </h1>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">                            
                            <div id="panel_one" class="div-content">
                                <h1 class="panel-heading">Knight CSS Data</h1>
                                <div class="panel-footer">
                                    <table id="example" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                    <thead>
                                        <tr style="color: #FFF;">
                                            <th data-priority="0">Category</th>
                                            <th data-priority="0">KMA</th>
                                            <th data-priority="0">Legal Name</th>
                                            <th data-priority="0">Carrier MC</th>
                                            <th data-priority="0">Broker MC</th>
                                            <th data-priority="0">Total Count</th>
                                            <th data-priority="0">HQ State</th>
                                            <th data-priority="0">Main Phone</th>
                                            <th data-priority="0">Power Units</th>
                                            <th data-priority="0">Destination KMA</th>
                                            <th data-priority="13">Destination Perc Lane</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="color: #FFF;">
                                            <th data-priority="0">Category</th>
                                            <th data-priority="0">KMA</th>
                                            <th data-priority="0">Legal Name</th>
                                            <th data-priority="0">Carrier MC</th>
                                            <th data-priority="0">Broker MC</th>
                                            <th data-priority="0">Total Count</th>
                                            <th data-priority="0">HQ State</th>
                                            <th data-priority="0">Main Phone</th>
                                            <th data-priority="0">Power Units</th>
                                            <th data-priority="0">Destination KMA</th>
                                            <th data-priority="13">Destination Perc Lane</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <?php } else {
                        //echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                    }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <script>
            
            $(document).ready(function () {
                // Suppress errors/warnings/alerts
                $.fn.dataTable.ext.errMode = 'none';
                //Add text input fields to column headers
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: 'carrier_lookup',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "<?php echo $ajax_src; ?>",
                    "columns": [
                        {"data": "Category", width: "4%"},
                        {"data": "Kma", width: "3%"},
                        {"data": "Legal_Name", width: "3%"},
                        {"data": "Carrier_Mc", width: "4%"},
                        {"data": "Broker_Mc", width: "5%"},
                        {"data": "Total_Cnt", width: "1%"},
                        {"data": "Hq_State", width: "1%"},
                        {"data": "Main_Phone", width: "1%"},
                        {"data": "Power_Units", width: "1%"},
                        {"data": "Dest_Kma", width: "3%"},
                        {"data": "Dest_Perc_Lane", width: "5%"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0},
                        {"visible": false,"searchable": false}
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Search bar
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </body>
</html>