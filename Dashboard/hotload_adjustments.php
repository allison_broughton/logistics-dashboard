<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Hotload Override';
$page_id = 'Hotload_Override';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
$page_access = getPageAccess($user_id, $page_id);
// Create dropdown menu for choosing broker to reassign hotload to
$options = createOptions('Broker');
// Filename for Export Data (Yellow Panel)
$filename_adjusted = 'adjusted_hotloads_' . $today_datetime;
// Filename for Export Data (Blue Panel)
$filename_review = 'committed_hotload_adjustments_' . $today_datetime;
// Get available hotloads (Red Panel)
function getHotLoads() {
    global $conn;
    global $options;
    $select_loads  = "SELECT HotloadID,
        Order_ID,
        Movement_ID,
        Dispatcher_User_ID,
        Hotload_Valid,
        Hotload_Added_By,
        Outside_Broker_Area,
        Origin_State,
        Revenue_Code_ID,
        Equipment_Type_ID,
        Movement_Margin,
        reassign_user_id,
        reassign_user_id_amount,
        reassign_user2_id,
        reassign_user2_id_amount,
        reassign_user3_id,
        reassign_user3_id_amount,
        reassign_user4_id,
        reassign_user4_id_amount
        FROM BC_Hotload_Preview WHERE Moved_to_Approved = 'N'";
    $get_loads     = odbc_prepare($conn, $select_loads);
    odbc_execute($get_loads);
    $hotload_data = "";
    while ($row           = odbc_fetch_array($get_loads)) {
        $hotload_data .= "<form id='red_adjust_hotload' method='POST' action=''><tr>
                <input type='hidden' name='row_id' value='" . $row['HotloadID'] . "'>
                    <input type='hidden' name='order_id' value='" . $row['Order_ID'] . "'>
                        <input type='hidden' name='movement_id' value='" . $row['Movement_ID'] . "'>
                <td>" . $row['Order_ID'] . "</td>
                <td>" . $row['Movement_ID'] . "</td>
                <td>" . $row['Dispatcher_User_ID'] . "</td>
                <td>" . $row['Hotload_Valid'] . "</td>
                <td>" . $row['Hotload_Added_By'] . "</td>
                <td>" . $row['Outside_Broker_Area'] . "</td>
                <td>" . $row['Origin_State'] . "</td>
                <td>" . $row['Revenue_Code_ID'] . "</td>
                <td>" . $row['Equipment_Type_ID'] . "</td>
                <td>" . $row['Movement_Margin'] . "</td>
                <td><select name='reassign_user_id' required><option selected value=''></option>" . $options . "</select></td>
                <td><input type='number' name='reassign_user_id_amount' required/></td>
                <td><select name='reassign_user2_id'><option selected value=''></option>" . $options . "</select></td>
                <td><input type='number' name='reassign_user2_id_amount'/></td>
                <td><select name='reassign_user3_id'><option selected value=''></option>" . $options . "</select></td>
                <td><input type='number' name='reassign_user3_id_amount'/></td>
                <td><select name='reassign_user4_id'><option selected value=''></option>" . $options . "</select></td>
                <td><input type='number' name='reassign_user4_id_amount'/></td>
                <td><input type='submit' value='Adjust' name='red_adjust_hotload' class='btn btn-danger'></td>
            </tr></form>";
    } return $hotload_data;
}

// Adjust hotloads in Red or Green Panel
if (isset($_POST['red_adjust_hotload'])||isset($_POST['green_adjust_hotload'])) {
    // POST variables
    $row_id            = sanitize($_POST['row_id']);
    $order_id          = sanitize($_POST['order_id']);
    $movement_id       = sanitize($_POST['movement_id']);
    $reassign_user     = sanitize($_POST['reassign_user_id']);
    $reassign_amount   = sanitize($_POST['reassign_user_id_amount']);
    $reassign_user2    = sanitize($_POST['reassign_user2_id']);
    $reassign_amount2  = sanitize($_POST['reassign_user2_id_amount']);
    $reassign_user3    = sanitize($_POST['reassign_user3_id']);
    $reassign_amount3  = sanitize($_POST['reassign_user3_id_amount']);
    $reassign_user4    = sanitize($_POST['reassign_user4_id']);
    $reassign_amount4  = sanitize($_POST['reassign_user4_id_amount']);
    // Global variables
    $last_modified     = $today_datetime;
    $modified_by       = $user_id;
    // UPDATE BC_Hotload_Preview with reassigned brokers and their amounts
    $sqlStart          = "UPDATE BC_Hotload_Preview 
        SET reassign_user_id = '$reassign_user',
            reassign_user_id_amount = '$reassign_amount',";
    $columns_to_update = "";
    // Only update columns with non-empty field input - add those fields to the query
    if (!empty($reassign_user2)) {
        $columns_to_update .= " reassign_user2_id = '$reassign_user2',";
    }
    if (!empty($reassign_amount2)) {
        $columns_to_update .= " reassign_user2_id_amount = '$reassign_amount2',";
    }
    if (!empty($reassign_user3)) {
        $columns_to_update .= " reassign_user3_id = '$reassign_user3',";
    }
    if (!empty($reassign_amount3)) {
        $columns_to_update .= " reassign_user3_id_amount = '$reassign_amount3',";
    }
    if (!empty($reassign_user4)) {
        $columns_to_update .= " reassign_user4_id = '$reassign_user4',";
    }
    if (!empty($reassign_amount4)) {
        $columns_to_update .= " reassign_user4_id_amount = '$reassign_amount4',";
    }
    $sqlEnd         = "Modified_Date_Time = '$last_modified',Modified_By_User = '$modified_by' 
        WHERE HotloadID = '$row_id' AND Order_ID = '$order_id' AND Movement_ID = '$movement_id'";
    // Put the 3 sql segments together
    $update_query   = $sqlStart . $columns_to_update . $sqlEnd;
    // Run the update query
    $adjust_hotload = odbc_prepare($conn, $update_query);
    $result         = odbc_execute($adjust_hotload);
    if (!$result) {
        $message = "Query Failed: " . odbc_errormsg($conn) . "<br/>";
    } else {
        $message = "Adjustment was successfully saved.<br/>";
    }
    $message .= $update_query;
}

// Get hotloads for Approve Adjustments table (green div)
function getAdjustedHotLoads() {
    global $conn;
    global $options;
    $select_loads  = "SELECT HotloadID,
        Order_ID,
        Movement_ID,
        Dispatcher_User_ID,
        Hotload_Valid,
        Hotload_Added_By,
        Outside_Broker_Area,
        Origin_State,
        Revenue_Code_ID,
        Equipment_Type_ID,
        Movement_Margin,
        reassign_user_id,
        reassign_user_id_amount,
        reassign_user2_id,
        reassign_user2_id_amount,
        reassign_user3_id,
        reassign_user3_id_amount,
        reassign_user4_id,
        reassign_user4_id_amount
        FROM BC_Hotload_Preview WHERE Moved_to_Approved = 'N'";
    $get_loads     = odbc_prepare($conn, $select_loads);
    odbc_execute($get_loads);
    $hotload_data = "";
    while ($row = odbc_fetch_array($get_loads)) {
        $hotload_data .= "<form id='green_approve_hotload' method='POST' action=''><tr>
                <input type='hidden' name='row_id' value='" . $row['HotloadID'] . "'>
                    <input type='hidden' name='order_id' value='" . $row['Order_ID'] . "'>
                        <input type='hidden' name='movement_id' value='" . $row['Movement_ID'] . "'>
                <td>" . $row['Order_ID'] . "</td>
                <td>" . $row['Movement_ID'] . "</td>
                <td>" . $row['Dispatcher_User_ID'] . "</td>
                <td>" . $row['Hotload_Valid'] . "</td>
                <td>" . $row['Hotload_Added_By'] . "</td>
                <td>" . $row['Outside_Broker_Area'] . "</td>
                <td>" . $row['Origin_State'] . "</td>
                <td>" . $row['Revenue_Code_ID'] . "</td>
                <td>" . $row['Equipment_Type_ID'] . "</td>
                <td>$" . $row['Movement_Margin'] . "</td>
                    
                <td><select name='reassign_user_id'>
                    <option value=''>*CLEAR*</option>
                        <option selected value='" . $row['reassign_user_id'] . "'>" . $row['reassign_user_id'] . "</option>
                            " . $options . "
                </select></td>
                    
                <td><input type='number' name='reassign_user_id_amount' value='" . $row['reassign_user_id_amount'] . "'/></td>
                    
                <td><select name='reassign_user2_id'>
                    <option selected value=''>*CLEAR*</option>
                        <option selected value='" . $row['reassign_user2_id'] . "'>" . $row['reassign_user2_id'] . "</option>
                            " . $options . "</select></td>
                            
                <td><input type='number' name='reassign_user2_id_amount' value='" . $row['reassign_user2_id_amount'] . "'/></td>
                    
                <td><select name='reassign_user3_id'>
                    <option selected value=''>*CLEAR*</option>
                        <option selected value='" . $row['reassign_user3_id'] . "'>" . $row['reassign_user3_id'] . "</option>
                            " . $options . "</select></td>
                            
                <td><input type='number' name='reassign_user3_id_amount' value='" . $row['reassign_user3_id_amount'] . "'/></td>
                    
                <td><select name='reassign_user4_id'>
                    <option selected value=''>*CLEAR*</option>
                        <option selected value='" . $row['reassign_user4_id'] . "'>" . $row['reassign_user4_id'] . "</option>
                            " . $options . "</select></td>
                            
                <td><input type='number' name='reassign_user4_id_amount' value='" . $row['reassign_user4_id_amount'] . "'/></td>
                    
                <td><input type='submit' value='Adjust' name='green_adjust_hotload' class='btn btn-success'></td>
            </tr></form>";
    } return $hotload_data;
}
// Rows for Red Panel table
$hotload_data = getHotLoads();
// Rows for Green Panel table
$approve_hotloads = getAdjustedHotLoads();
// Approve All Adjustments - Green Panel
if (isset($_POST['green_approve_all'])) {
    trackVisit($page_id,$user_id,'approved_hotloads');
    $appr_date  = $today_datetime;
    $appr_by    = $user_id;
    $appr_str   = "INSERT INTO BC_Hotload_Final 
            (Movement_ID, Order_ID, Dispatcher_User_ID, Hotload_Valid,Hotload_Added_By,
            Outside_Broker_Area,Origin_State,Revenue_Code_ID,Equipment_Type_ID,Movement_Margin,reassign_user_id,reassign_user_id_amount,
            reassign_user2_id,reassign_user2_id_amount,reassign_user3_id,reassign_user3_id_amount,reassign_user4_id,reassign_user4_id_amount,
            Approved_Date_Time,Approved_By_User,Paid,Pay_Date)
        SELECT 
            Movement_ID, Order_ID, Dispatcher_User_ID, Hotload_Valid,Hotload_Added_By,
            Outside_Broker_Area,Origin_State,Revenue_Code_ID,Equipment_Type_ID,Movement_Margin,reassign_user_id,reassign_user_id_amount,
            reassign_user2_id,reassign_user2_id_amount,reassign_user3_id,reassign_user3_id_amount,reassign_user4_id,reassign_user4_id_amount,
            '$appr_date','$appr_by','N',''
        FROM BC_Hotload_Preview
        WHERE Moved_to_Approved = 'N'";
    $appr_query = odbc_prepare($conn, $appr_str);
    $result     = odbc_execute($appr_query);
    if (!$result) {
        $message = "Loads failed to insert into BC_Hotload_Final: " . odbc_errormsg($conn) . "<br/>";
    } else {
        $message = "Adjustments were successfully approved.<br/>";
    }
    $message          .= $appr_str;
    $update_all_str   = "UPDATE BC_Hotload_Preview 
        SET Moved_to_Approved = 'Y',Modified_Date_Time = '$today_datetime',Modified_By_User = '$user_id'
        WHERE Moved_to_Approved = 'N'";
    $update_all_query = odbc_prepare($conn, $update_all_str);
    $update_result    = odbc_execute($update_all_query);
    if (!$update_result) {
        $message .= "Loads in BC_Hotload_Preview failed to update: " . odbc_errormsg($conn) . "<br/>";
    } else {
        $message .= "Adjustments were successfully updated.<br/>";
    }
    // Automated e-mail notification on approve all adjustments
    /*$to  = "SwiftLogistics_Reporting@swifttrans.com";
    $sub = "**UPDATE**Hotload Adjustments Approved!";
    $msg = "Donna has approved all hotload adjustments.";
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'From: Swift Logistics Reporting <swiftlogistics_reporting@swifttrans.com>' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $mail = mail($to, $sub, $msg, $headers);
    if ($mail) {
        $message .= "Notification has been sent.";
    } else {
        $message .= "Mail sending failed.";
    }*/
}

?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#export_hotloads").show();
                $("#approve_adj").hide();
                $("#review_adj").hide();
                $("#yellow").on("click",function(){
                    $("#export_hotloads").toggle();
                    $("#approve_adj").hide();
                    $("#review_adj").hide();
                });
                $("#green").on("click",function(){
                    $("#approve_adj").toggle();
                    $("#export_hotloads").hide();
                    $("#review_adj").hide();
                });
                $("#blue").on("click",function(){
                    $("#review_adj").toggle();
                    $("#export_hotloads").hide();
                    $("#approve_adj").hide();
                });
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
        <style>
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #export_hotloads {
                color: #fff;
                border-color: #dd8913;/*yellow*/
                background-color: #dd8913;
            }
            #approve_adj {
                color: #fff;
                border-color: #449D44;/*green*/
                background-color: #449D44
            }
            #review_adj {
                color: #fff;
                border-color: #337ab7;/*blue*/
                background-color: #337ab7
            }
            table {
                /*display: block;*/
                table-layout: fixed;
                max-height: 500px;
                /*overflow-x: auto;
                overflow-y: auto;*/
            }
            th,td {
                column-width: 100px;
                /*min-width: 100px;
                max-width: 175px;*/
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
            /* Yellow DataTable */
            #example th {
                background-color: #F0AD4E !important;
            }
            
            /* Blue DataTable */
            #committed_hotloads th {
                background-color: #337ab7 !important;
            }
            
        </style>
    </head>
    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($page_access) {
                        if (isset($message)) {
                            echo "<pre>" . $message . "</pre>";
                        }?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Hotload Adjustments<br/>
                                </h1><?php echo $dev_server; ?>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Click a button below to display its content.</h3>
                                
                                <button type="button" id="yellow" class="btn btn-warning">Show Exportable Data</button>
                                <button type="button" id="green" class="btn btn-success" title="This is NOT the approval button">Show Hotloads to Approve</button>
                                <button type="button" id="blue" class="btn btn-primary">Show Review Table</button>
                                
                                <!--Yellow Panel-->
                                <div id="export_hotloads" class="div-content collapse">
                                    <h1 class="panel-heading">Export Data
                                        <small>Export adjusted hotloads to spreadsheet and send to managers for approval.</small>
                                    </h1>
                                    <div class="panel-footer">
                                        <table id="example" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                            <thead>
                                                <tr style="color: #FFF;">
                                                    <th>Order ID</th>
                                                    <th>Movement</th>
                                                    <th>Dispatcher</th>
                                                    <th>Valid</th>
                                                    <th>Added By</th>
                                                    <th>Outside Area</th>
                                                    <th>Origin</th>
                                                    <th>Revenue</th>
                                                    <th>Equipment</th>
                                                    <th>Margin</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr style="color: #FFF;">
                                                    <th>Order ID</th>
                                                    <th>Movement</th>
                                                    <th>Dispatcher</th>
                                                    <th>Valid</th>
                                                    <th>Added By</th>
                                                    <th>Outside Area</th>
                                                    <th>Origin</th>
                                                    <th>Revenue</th>
                                                    <th>Equipment</th>
                                                    <th>Margin</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!--Green Panel-->
                                <div id="approve_adj" class="div-content collapse">
                                    <h1 class="panel-heading">Approve Adjustments
                                        <small>Click the "Approve All" button to confirm all changes and send to payroll. 
                                            You may also make further adjustments before committing your changes.
                                        </small>
                                    </h1>
                                    <div class="panel-footer">
                                        <form action="" method="post">
                                            <i class="fa fa-2x fa-arrow-right blink_me fa-green"></i>
                                            <input type="submit" name="green_approve_all" class="btn btn-success" value="Approve All"/>
                                        </form>
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Order ID</th>
                                            <th>Movement</th>
                                            <th>Dispatcher</th>
                                            <th>Valid?</th>
                                            <th>Added By</th>
                                            <th>Outside Area?</th>
                                            <th>Origin</th>
                                            <th>Revenue</th>
                                            <th>Equipment</th>
                                            <th>Margin</th>
                                            <th>Reassigned</th>
                                            <th>Amount</th>
                                            <th>Reassigned</th>
                                            <th>Amount</th>
                                            <th>Reassigned</th>
                                            <th>Amount</th>
                                            <th>Reassigned</th>
                                            <th>Amount</th>
                                            <th><i class="fa fa-save fa-lg" aria-hidden="true"></i></th>
                                            </thead>
                                            <?php echo $approve_hotloads; ?>
                                        </table>
                                    </div>
                                </div>
                                <!--Blue Panel-->
                                <div id="review_adj" class="div-content collapse">
                                    <h1 class="panel-heading">Review Committed Adjustments
                                        <small>Review and export committed adjustments here.</small>
                                    </h1>
                                    <div class="panel-footer">
                                        <table id="committed_hotloads" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                            <thead>
                                                <tr style="color: #FFF;">
                                                    <th>Order ID</th>
                                                    <th>Movement</th>
                                                    <th>Dispatcher</th>
                                                    <th>Valid</th>
                                                    <th>Added By</th>
                                                    <th>Outside Area</th>
                                                    <th>Origin</th>
                                                    <th>Revenue</th>
                                                    <th>Equipment</th>
                                                    <th>Margin</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                    <th>Reassigned</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <script>
            $(document).ready(function () {
                // Suppress errors/warnings/alerts
                //$.fn.dataTable.ext.errMode = 'none';
                /*Yellow Panel Datatable*/
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({

                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: '<"top"Bf>tr<"bottom"lip><"clear">',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: '<?php echo $filename_adjusted; ?>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "/get_hotloads.php",
                    "columns": [
                        {"data": "Order_ID", width: "5%"},
                        {"data": "Movement_ID", width: "5%"},
                        {"data": "Dispatcher_User_ID", width: "4%"},
                        {"data": "Hotload_Valid", width: "3%"},
                        {"data": "Hotload_Added_By", width: "5%"},
                        {"data": "Outside_Broker_Area", width: "3%"},
                        {"data": "Origin_State", width: "3%"},
                        {"data": "Revenue_Code_ID", width: "4%"},
                        {"data": "Equipment_Type_ID", width: "3%"},
                        {"data": "Movement_Margin",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "4%"},
                        {"data": "reassign_user_id", width: "5%"},
                        {"data": "reassign_user_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"},
                        {"data": "reassign_user2_id", width: "5%"},
                        {"data": "reassign_user2_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"},
                        {"data": "reassign_user3_id", width: "5%"},
                        {"data": "reassign_user3_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"},
                        {"data": "reassign_user4_id", width: "5%"},
                        {"data": "reassign_user4_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"}
                    ],
                    "columnDefs": [
                        {
                            "width": "10px", "targets": 0
                        },
                        {
                            /*"targets": [3,5],*/
                            "visible": false,
                            "searchable": false
                        }
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Search bar
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
                /*Blue Panel Datatable*/
                /*$('#committed_hotloads tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });*/
                var table = $('#committed_hotloads').DataTable({

                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: '<"top"Bf>tr<"bottom"lip><"clear">',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: '<?php echo $filename_review; ?>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "/get_committed_hotloads.php",
                    "columns": [
                        {"data": "Order_ID", width: "4%"},
                        {"data": "Movement_ID", width: "5%"},
                        {"data": "Dispatcher_User_ID", width: "4%"},
                        {"data": "Hotload_Valid", width: "3%"},
                        {"data": "Hotload_Added_By", width: "5%"},
                        {"data": "Outside_Broker_Area", width: "3%"},
                        {"data": "Origin_State", width: "3%"},
                        {"data": "Revenue_Code_ID", width: "4%"},
                        {"data": "Equipment_Type_ID", width: "3%"},
                        {"data": "Movement_Margin",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "4%"},
                        {"data": "reassign_user_id", width: "5%"},
                        {"data": "reassign_user_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"},
                        {"data": "reassign_user2_id", width: "5%"},
                        {"data": "reassign_user2_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"},
                        {"data": "reassign_user3_id", width: "5%"},
                        {"data": "reassign_user3_id_amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"}
                    ],
                    "columnDefs": [
                        {
                            "width": "10px", "targets": 0
                        },
                        {
                            /*"targets": [3,5],*/
                            "visible": false,
                            "searchable": false
                        }
                    ]
                });
                // Inserts the footer directly below the header
                /*$('#committed_hotloads tfoot tr').insertAfter($('#committed_hotloads thead tr'));*/
                // Search bar
                /*table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });*/
            });
        </script>

    </body>
</html>