<?php
session_start();
require 'include_functions.php';
// Emulate user
//$user_id = 'STRAL';
$user_id = strtolower($user_id);
// Check if user has loadboard profile
$profile = getProfile($user_id);
// Get user loadboard settings
$settings = getSettings($profile);
// Explode each string into an array then implode with quotes 
$zones       = $settings['zones'];
$zones_arr   = explode(',', $zones);
$zones_str   = trimSpaces(implode("','", $zones_arr));

$revenue     = $settings['revenue'];
$revenue_arr = explode(',', $revenue);
$revenue_str = trimSpaces(implode("','", $revenue_arr));

$status      = $settings['status'];
$status_arr  = explode(',', $status);
$status_str  = trimSpaces(implode("','", $status_arr));

function getLoads($zones_str,$revenue_str,$status_str) {
    global $conn_mcld;
    if ($zones_str === '*' || $zones_str === '') {
        $zones = "WHERE ";
    } else {
        $zones = "WHERE OS.state IN ('" . $zones_str . "') AND ";
    }
    if ($revenue_str === '*' || $revenue_str === '') {
        $revenue = "";
    } else {
        $revenue = "O.revenue_code_id IN ('" . $revenue_str . "') AND ";
    }
    if ($status_str === '*' || $status_str === '') {
        $status = "M.brokerage_status IN ('AVAILABL', 'FOLLOWUP', 'HOTLOAD', 'READY', 'PROGRESS','COVERED')";
    } else {
        $status = "M.brokerage_status IN ('" . $status_str . "')";
    }
    $string = "SELECT O.id AS Order_ID,
            O.freight_charge AS Freight_Charge,
            M.brokerage_status AS Brokerage_Status,
            M.status AS Move_Status,
            P.name AS Carrier,
            O.order_type_id AS Order_Type,
            OS.city_name AS Origin_City,
            OS.state AS Origin_State,
            DS.city_name AS Destin_City,
            DS.state AS Destin_State,
            O.bill_distance AS Distance,
            O.customer_id,
            O.operations_user AS Ops_User,
            OS.location_id AS Ship_ID,
            OS.location_name AS Shipper_Name,
            DS.location_id AS Cons_ID,
            DS.location_name AS Consignee_Name,
            O.commodity AS Commodity,
            O.hazmat AS Hazmat,
            O.revenue_code_id AS Revenue,
            O.weight AS Weight,
            O.pieces AS Pieces,
            OS.zone_id AS Origin_Zone,
            OS.zip_code AS Origin_Zip,
            DS.zone_id AS Dest_Zone,
            DS.zip_code AS Dest_Zip,
            O.curr_movement_id AS Movement,
            O.otherchargetotal AS Other_Charges,
            O.total_charge AS Total_Charge,
            O.pay_gross AS Total_Pay,
            O.blnum AS Blnum,
            O.consignee_refno AS Consignee_refno,
            O.equipment_type_id AS Equipment,
            OS.sched_arrive_early AS Early_Pickup,
            OS.sched_arrive_late AS Late_Pickup,
            DS.sched_arrive_early AS Early_Delivery,
            DS.sched_arrive_late AS Late_Delivery,
            O.ordered_date,
            O.planning_comment AS Planning_Comment,
            M.carrier_contact AS Carrier_Contact,
            M.carrier_phone AS Carrier_Phone,
            M.carrier_email AS Carrier_Email,
            O.carrier_refno AS Carrier_Refno,
            OS.location_id AS Origin_Location_ID,
            RTRIM(OS.address) + ', ' + RTRIM(OS.city_name) + ', ' + RTRIM(OS.state) + ', ' + OS.zip_code AS Origin_Address,
            RTRIM(DS.address) + ', ' + RTRIM(DS.city_name) + ', ' + RTRIM(DS.state) + ', ' + DS.zip_code AS Destin_Address,
            DS.location_id AS Destin_Location_ID,
            M.operations_user AS Ops_User,
            M.dispatcher_user_id AS Dispatcher
    FROM orders O
    LEFT JOIN movement M
            ON M.id = O.curr_movement_id
    LEFT JOIN stop OS
            ON OS.id = M.origin_stop_id AND OS.movement_id = M.id
    LEFT JOIN stop DS
            ON DS.id = M.dest_stop_id AND DS.movement_id = M.id
    LEFT JOIN payee P
            ON P.id = M.override_payee_id
    $zones $revenue $status";
    $stmt   = odbc_prepare($conn_mcld, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getLoads($zones_str,$revenue_str,$status_str);
header('Content-Type: application/json');
echo json_encode($data);