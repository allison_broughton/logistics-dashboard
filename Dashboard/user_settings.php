<?php
session_start();
require 'include_functions.php';
require 'include_navbar.php';
///////////////////// ODBC Connection /////////////////////////////////////////////////
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);
///////////////////// Initialize Variables ////////////////////////////////////////////
$title             = "User Settings";
//////////////////// Form Submission //////////////////////////////////////////////////
if(isset($_POST['submit'])) {
    $icon = $_POST['icon'];
    $sql = "UPDATE Web_Page_Roles SET User_Icon = '$icon' WHERE User_ID = '$user_id'";
    $query = odbc_prepare($conn, $sql);
    try {
        odbc_execute($query);
    } catch (Exception $exc) {
        echo odbc_errormsg($conn) . $exc->getTraceAsString();
    }
}
// Change specified user's icon
/*$sql   = "UPDATE Web_Page_Roles SET User_Icon = 'fa-smile-o' WHERE User_ID = 'everd'";
$query = odbc_prepare($conn, $sql);
try {
    odbc_execute($query);
} catch (Exception $exc) {
    echo odbc_errormsg($conn) . $exc->getTraceAsString();
}*/
// Look up users who have chosen icons
/* $sql = "SELECT DISTINCT(User_ID), User_Icon FROM Web_Page_Roles WHERE User_Icon IS NOT NULL
   ORDER BY User_ID ASC";
 */
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Data Tables -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                User Settings<br/>
                                <small></small>
                            </h1><?php echo $dev_server;?>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4">
                            <form method="POST" action="" class="form-horizontal">
                                <div class="form-group">
                                    <label for="icon" class="control-label col-xs-4" style='font-size:20px;'>Choose an icon</label>
                                    <div class="col-xs-8">
                                        <select name='icon' style='height: 35px; font-family:Verdana, FontAwesome; font-size:20px;'>
                                            <option value='' disabled selected>Please choose one...</option>
                                            <option value='fa-anchor'>&#xf13d; &nbsp; Anchor</option>
                                            <option value='fa-android'>&#xf17b; &nbsp; Android</option>
                                            <option value='fa-apple'>&#xf179; &nbsp; Apple</option>
                                            <option value='fa-beer'>&#xf0fc; &nbsp; Beer</option>
                                            <option value='fa-black-tie'>&#xf27e; &nbsp; Black Tie</option>
                                            <option value='fa-bolt'>&#xf0e7; &nbsp; Bolt</option>
                                            <option value='fa-bug'>&#xf188; &nbsp; Bug</option>
                                            <option value='fa-car'>&#xf1b9; &nbsp; Car</option>
                                            <option value='fa-coffee'>&#xf0f4; &nbsp; Coffee</option>
                                            <option value='fa-diamond'>&#xf219; &nbsp; Diamond</option>
                                            <option value='fa-dribbble'>&#xf17d; &nbsp; Dribble</option>
                                            <option value='fa-empire'>&#xf1d1; &nbsp; Empire</option>
                                            <option value='fa-fighter-jet'>&#xf0fb; &nbsp; Fighter Jet</option>
                                            <option value='fa-first-order'>&#xf2b0; &nbsp; First Order</option>
                                            <option value='fa-fort-awesome'>&#xf286; &nbsp; Fort Awesome</option>
                                            <option value='fa-gamepad'>&#xf11b; &nbsp; Game Controller</option>
                                            <option value='fa-heart'>&#xf004; &nbsp; Heart</option>
                                            <option value='fa-key'>&#xf084; &nbsp; Key</option>
                                            <option value='fa-leaf'>&#xf06c; &nbsp; Leaf</option>
                                            <option value='fa-linux'>&#xf17c; &nbsp; Linux</option>
                                            <option value='fa-meh-o'>&#xf11a; &nbsp; Meh</option>
                                            <option value='fa-moon-o'>&#xf186; &nbsp; Moon</option>
                                            <option value='fa-music'>&#xf001; &nbsp; Music</option>
                                            <option value='fa-paint-brush'>&#xf1fc; &nbsp; Paint Brush</option>
                                            <option value='fa-paw'>&#xf1b0; &nbsp; Pawprint</option>
                                            <option value='fa-puzzle-piece'>&#xf12e; &nbsp; Puzzle Piece</option>
                                            <option value='fa-rebel'>&#xf1d0; &nbsp; Rebel Alliance</option>
                                            <option value='fa-smile-o'>&#xf118; &nbsp; Smile</option>
                                            <option value='fa-snowflake-o'>&#xf2dc; &nbsp; Snowflake</option>
                                            <option value='fa-soccer-ball-o'>&#xf1e3; &nbsp; Soccer</option>
                                            <option value='fa-hand-spock-o'>&#xf259; &nbsp; Spock</option>
                                            <option value='fa-superpowers'>&#xf2dd; &nbsp; Superpowers</option>
                                            <option value='fa-user-circle'>&#xf2bd; &nbsp; User</option>
                                            <option value='fa-user-secret'>&#xf21b; &nbsp; User - Incognito</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-3 col-xs-9">
                                        <button type="submit" name="submit" class="btn btn-primary col-lg-offset-2">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <?php if(isset($_POST['submit'])) {
                                echo "<div class='alert alert-success'>Icon successfully updated!</div>";
                            }
?>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>

    </body>
</html>
