<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Broker_Report';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'bendc';
}
// Get active commission paystructure records and store in array
function getRecords($user_id) {
    global $conn;
    $string = "SELECT  CASE WHEN commission_cycle = 'A' THEN arecord_count
			    WHEN commission_cycle = 'Q' THEN qrecord_count END AS Record_Count,
		Pay_Old,
		Pay_New,
                Order_ID,
		movement_id,
		total_charge,
		Carrier_Total_Pay,
                bol_received,
		CASE WHEN pay_old = 'Y' THEN ordermargin 
                     WHEN pay_new = 'Y' THEN movement_margin END AS margin,
		processing_status,
		CONVERT(DATE, Commission_End_Date) AS Commission_End_Date,
		CONVERT(DATE, Delivery_Date) AS Delivery_Date,
		CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
		movement_margin,
		CASE WHEN commission_cycle = 'A' THEN annual_running_total
                     WHEN commission_cycle = 'Q' THEN quarter_running_total END AS Running_Total,
		CASE WHEN pay_new = 'Y' AND commission_cycle = 'A' THEN Annual_Commission_Percent
                     WHEN pay_new = 'Y' AND commission_cycle = 'Q' THEN Quarterly_Commission_Percent 
                     WHEN pay_old = 'Y' THEN (commission_percentage / 100) END AS Commission_Percent,
		Payroll_Amount,
		CONVERT(DATE, Pay_Date) AS Pay_Date,
                dispatcher_user_id,
                override_payee_id,
                Destin_City + ',' + Destin_State AS Destination,
                Destin_Schedule_Arrive_Early
        FROM [Logistics_Data_Warehouse_QA].[dbo].[BC_PayALL_final] WHERE dispatcher_user_id = '$user_id'
            ORDER BY Pay_Date,Record_Count ASC";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getRecords($user_id);
header('Content-Type: application/json');
echo json_encode($data);