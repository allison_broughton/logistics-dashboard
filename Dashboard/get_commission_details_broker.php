<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Broker';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'torris';
}
// Get active commission paystructure records and store in array
function getRecords($user_id) {
    global $conn;
    $dispatcher = "AND dispatcher_user_id = '$user_id'";
    $string = "SELECT Commission_Month,
	Commission_Year,
	Movement_ID,
	order_id,
	ISNULL(commission_amount, 0.00) AS commission_amount,
	CONVERT(DATE, commission_amount_date) AS commission_amount_date,
	CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
	bol_received,
	CONVERT(DATE, Delivery_Date) AS Delivery_Date,
	override_payee_id,
        Processing_Status
FROM BC_PayALL_temp WHERE Commission_Month IS NOT NULL AND Commission_Year > 2016 $dispatcher
ORDER BY Commission_Amount_Date DESC";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row; //added data object to fix a formatting issue in jsonencode -Don
    }
    return $json;
}

$data = getRecords($user_id);
header('Content-Type: application/json');
echo json_encode($data);