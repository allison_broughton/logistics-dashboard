<?php
require 'include_functions.php';
if (isset($_GET['OS'])) {
    $origin = $_GET['OS'];
} else {
    $origin = '';
}
if (isset($_GET['DS'])) {
    $destin = $_GET['DS'];
} else {
    $destin = '';
}

function getCarriers($origin,$destin) {
    global $conn_dev;
    $string = "DECLARE @dt datetimeoffset = switchoffset (CONVERT(datetimeoffset, GETDATE()), '-04:00'); 
                SELECT TOP 500 
                    name,
                    payee_id,
                    icc_number,
                    CAST(MAX_of_dispatched_date as date) AS Last_Dispatch,
                    city,
                    state,
                    power_units,
                    Dispatch_Name,
                    Dispatch_Phone,
                    Carrier_Dispatch_Email,
                    is_active,
                    no_dispatch,
                    CAST(cargo_ins_amount as int) AS CargoIns, 
                    (COUNT(DISTINCT REPORT_STATE)*100)+(
                        CASE WHEN power_units > 500 THEN 25
                      WHEN power_units > 100 THEN 50
                      ELSE power_units END
                    )+(datediff(M,@dt,MAX_of_dispatched_date)*25+1) AS score 
                FROM DM_CarrierInspection 
                WHERE REPORT_STATE = '$origin' OR REPORT_STATE = '$destin' 
                GROUP BY 
                    payee_id,
                    name,
                    icc_number,
                    MAX_of_dispatched_date,
                    city,state,
                    power_units,
                    Dispatch_Name,
                    Dispatch_Phone,
                    Carrier_Dispatch_Email,
                    is_active,
                    no_dispatch,
                    cargo_ins_amount
            ORDER BY score DESC";
    $stmt   = odbc_prepare($conn_dev, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
    //return $string;
}

$data = getCarriers($origin,$destin);
header('Content-Type: application/json');
echo json_encode($data);
//echo $data;