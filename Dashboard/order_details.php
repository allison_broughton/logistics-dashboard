<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Order Details';
$page_id = 'Order_Details';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Get Order ID from URL or form input
if (isset($_GET['OID'])) {
    $order_id = $_GET['OID'];
} else {
    $order_id = NULL;
}
// Select order details from McLeod db
$details        = getOrderDetails($order_id);
$o_contact_info = getContactInfo($order_id, 'PU');
$d_contact_info = getContactInfo($order_id, 'SO');
?>
<html lang="en"><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <?php echo $violet_css;?>
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <style>
            .btn-purple,.btn-purple:visited,.btn-purple:hover {
                color: #fff;
                border-color: #8500bd;/*purple*/
                background-color: #9400d3
            }
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
                padding: 25px;
            }
            #panel_one {
                color: #fff;
                border-color: #8500bd;
                background-color: #9400d3
            }
            table {
                margin-left: auto;
                margin-right: auto;
            }
            #order_details th, td {
                border: 1px solid #666666;
                padding: 3px;
            }
            #order_details th {
                background-color: #666666;
                color: #fff;
            }
            #order_details .table-divider th {
                background-color: #8500bd;
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
        </style>
        <style media="screen" type="text/css">
            #loader {
                width: 100%;
                height: 100%;
                background-color: white;
                margin: 0;
            }
            #purple-spinner {
                color: #9933ff;
            }
        </style>
        <script>
          $(window).load(function() {      //Do the code in the {}s when the window has loaded 
            $("#loader").fadeOut("slow");  //Fade out the #loader div
          });
        </script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div id="loader">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <div id="loader">
                                    <i id="purple-spinner" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Order Details<br/>
                            </h1><?php echo $dev_server; echo $debug;?>
                            <form method="GET" action="">
                                Order <input type="text" name="OID"/>
                                <input type="submit" value="Search" class="btn-purple"/>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">                            
                            <div id="panel_one" class="div-content collapse in">
                                <h1 class="panel-heading">
                                    <?php if(isset($order_id)) {echo 'Order '.$order_id;}?>
                                </h1>
                                <div class="panel-footer">
                                    <?php
                                    if (!isset($order_id)) {
                                        echo 'Type in an order above to look up its details.';
                                    } else {
                                        echo "
                                        <table id='order_details'>
                                            <tr><th>Movement</th><td>".$details['movement_id']."</td>
                                                <th>BOL</th><td>".$details['bol']."</td>
                                                <th>Freight Charge</th><td>$".$details['freight_charge']."</td>
                                            </tr>
                                            <tr><th>Dispatcher</th><td>".$details['dispatcher']."</td>
                                                <th>Ops User</th><td>".$details['ops']."</td>
                                                <th>Distance</th><td>".$details['miles']." miles</td>
                                            </tr>
                                            <tr><th>Broker Status</th><td>".$details['b_status']."</td>
                                                <th>Equipment Type</th><td>".$details['equipment']."</td>
                                                <th>Planning Comment</th><td>".$details['comment']."</td>
                                            </tr>
                                            <tr><th>Movement Status</th><td>".$details['m_status']."</td>
                                                <th>Revenue</th><td>".$details['revenue']."</td>
                                                <th>Pieces</th><td>".$details['pieces']."</td>
                                            </tr>
                                            <tr><th>Commodity</th><td>".$details['commodity']."</td>
                                                <th>Hazmat</th><td>".$details['hazmat']."</td>
                                                <th>Weight</th><td>".$details['weight']."</td>
                                            </tr>
                                            <tr class='table-divider'><th colspan='6'></th></tr>
                                            <tr><th>Carrier</th><td>".$details['carrier']."</td>
                                                <th>Shipper</th><td>".$details['shipper']."</td>
                                                <th>Consignee</th><td>".$details['consignee']."</td>
                                            </tr>
                                            <tr><th>Carrier Contact</th><td>".$details['carrier_contact']."</td>
                                                <th>Origin Address</th><td>".$details['o_address']."</td>
                                                <th>Destination Address</th><td>".$details['d_address']."</td>
                                            </tr>
                                            <tr><th>Carrier Phone</th><td>".$details['carrier_phone']."</td>
                                                <th>Origin Contact(s)</th><td>".$o_contact_info['contact_string']."</td>
                                                <th>Destination Contact(s)</th><td>".$d_contact_info['contact_string']."</td>
                                            </tr>
                                            <tr><th>Carrier E-Mail</th><td>".$details['carrier_email']."</td>
                                                <td colspan='2'></td>
                                                <th>Consignee Ref</th><td>".$details['d_refno']."</td>
                                            </tr>
                                            <tr><th>Carrier Ref #</th><td>".$details['carrier_refno']."</td>
                                                <th>PO Ref #'s</th><td colspan='3'>".$details['po_ref_string']."</td>
                                            </tr>
                                            <tr><th>Other Charges</th><td>$".$details['other_charges']."</td>
                                                <th>PU Ref #'s</th><td colspan='3'>".$details['pu_ref_string']."</td>
                                            </tr>
                                            <tr><th>Total Charges</th><td>$".$details['total_charge']."</td>
                                                <th>Early Pickup</th><td>".$details['early_pickup']."</td>
                                                <th>Early Delivery</th><td>".$details['early_delivery']."</td>
                                            </tr>
                                            <tr><th>Total Pay</th><td>$".$details['total_pay']."</td>
                                                <th>Late Pickup</th><td>".$details['late_pickup']."</td>
                                                <th>Late Delivery</th><td>".$details['late_delivery']."</td>
                                            </tr>
                                        </table>";
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
    </body>
</html>