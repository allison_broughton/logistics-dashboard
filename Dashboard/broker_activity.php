<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Broker Activity';
$page_id = 'Broker_Activity_Tracker';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Check page access
$page_access = getPageAccess($user_id, $page_id);
// Filter by date
if(!isset($_POST['filter_date'])) {
    $date = $today;
} else {
    $date = $_POST['date_filter'];
}
/////////////////////////////////////// BAR CHART DATA ///////////////////////////////////////
// Labels
$broker_list  = getUsers('Broker');
$full_names   = array_map("getFullName", $broker_list);
$labels       = implode("','", $full_names);
// Bar Data Set 1 - BC_Broker page visit
$data_array_1 = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker',$date,"Event = 'page_visit'");
}, $broker_list);
$bar_data_1   = implode(", ", $data_array_1);
// Bar Data Set 1b - BC_Broker report export
/*$data_array_1b = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker',$date,"Event <> 'page_visit'");
}, $broker_list);
$bar_data_1b   = implode(", ", $data_array_1b);*/
// Bar Data Set 2 - BC_Manager page visit
/*$data_array_2 = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Manager',$date,"Event = 'page_visit'");
}, $broker_list);
$bar_data_2   = implode(", ", $data_array_2);*/
// Bar Data Set 2b - BC_Manager report export
/*$data_array_2b = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Manager',$date,"Event <> 'page_visit'");
}, $broker_list);
$bar_data_2b   = implode(", ", $data_array_2b);*/
// Bar Data Set 3 - BC_Broker_Report page visit
$data_array_3 = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker_Report',$date,"Event = 'page_visit'");
}, $broker_list);
$bar_data_3   = implode(", ", $data_array_3);
// Bar Data Set 3b - BC_Broker_Report report views
/*$data_array_3b = array_map(function($user) {
    global $date;
    return getPageVisits($user,'BC_Broker_Report',$date,"Event <> 'page_visit'");
}, $broker_list);
$bar_data_3b   = implode(", ", $data_array_3b);*/
// Bar Data Set 4 - Broker_KPI page visit
/*$data_array_4 = array_map(function($user) {
    global $date;
    return getPageVisits($user,'Broker_KPI',$date,"Event = 'page_visit'");
}, $broker_list);
$bar_data_4   = implode(", ", $data_array_4);*/
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- CSS -->
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
    </head>
    <body>  
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($page_access) {?>
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    <?php echo $title;?><br/>
                                </h1>
                                <?php echo $dev_server;?>
                                <form method="post" action="">
                                    <input type="date" name='date_filter' value="<?php if ($date === '') {echo $today;} else {echo date($date);}?>"/>
                                    <input type='submit' name='filter_date' value='Filter'/>
                                </form>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <!--chart.js Stacked Chart Panel-->
                                <div id="leaderboard">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bar-chart "></i> Page Visits for <?php echo $date;?></h3>
                                        </div>
                                        <div class="panel-body">
                                            <canvas id="officeLeaderboard" width="200" height="75"></canvas>
                                        </div>
                                        <div class="panel-footer">
                                            <span class="pull-left"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                    }
                    ?>

                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>

        <script>
            var ctx = document.getElementById("officeLeaderboard");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['<?php echo $labels; ?>'],
                    datasets: [{
                            type: 'bar',
                            label: 'Commissions Page Visit',
                            data: [<?php echo $bar_data_1; ?>],
                            backgroundColor: '#9400D3',
                            borderColor: '#7600a8',
                            hoverBackgroundColor: '#9e19d7',
                            borderWidth: 1
                        },/* {
                            type: 'bar',
                            label: 'Commissions Report Extract',
                            data: [<?php //echo $bar_data_1b; ?>],
                            backgroundColor: '#be66e4',
                            borderColor: '#9851b6',
                            hoverBackgroundColor: '#c475e6',
                            borderWidth: 1
                        },*/ /*{
                            type: 'bar',
                            label: 'Manager Dashboard Page Visit',
                            data: [<?php //echo $bar_data_2; ?>],
                            backgroundColor: '#00d394',
                            borderColor: '#00a876',
                            hoverBackgroundColor: '#19d79e',
                            borderWidth: 1
                        }, {
                            type: 'bar',
                            label: 'Manager Dashboard Report Extract',
                            data: [<?php //echo $bar_data_2b; ?>],
                            backgroundColor: '#66e4be',
                            borderColor: '#5bcdab',
                            hoverBackgroundColor: '#75e6c4',
                            borderWidth: 1
                        },*/ {
                            type: 'bar',
                            label: 'Broker Reports Page Visit',
                            data: [<?php echo $bar_data_3; ?>],
                            backgroundColor: '#00d394',
                            borderColor: '#00a876',
                            hoverBackgroundColor: '#19d79e',
                            borderWidth: 1
                        }/*, {
                            type: 'bar',
                            label: 'Broker Reports Viewed Report',
                            data: [<?php //echo $bar_data_3b; ?>],
                            backgroundColor: '#66e4be',
                            borderColor: '#b69851',
                            hoverBackgroundColor: '#e6c475',
                            borderWidth: 1
                        }*//*, {
                            type: 'bar',
                            label: 'Broker KPI Page Visit',
                            data: [<?php //echo $bar_data_4; ?>],
                            backgroundColor: '#005590',
                            borderColor: '#005590',
                            hoverBackgroundColor: '#005590',
                            borderWidth: 1
                        }*/
                    ]
                },
                options: {
                    title: {
                        display: false,
                        text: 'Page Visits per User'
                    },
                    legend: {
                        display: true
                    },
                    scales: {
                        yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 1
                                }
                            }],
                        xAxes: [{
                                stacked: true,
                                ticks: {
                                    autoSkip: false,
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
    </body>
</html>