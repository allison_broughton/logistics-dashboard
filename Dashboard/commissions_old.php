<?php
/*header('location:under_construction.php');
exit();*/
require 'include_functions.php';
$page_id = 'BC_Broker';
$title   = 'Commissions';
$last_updated = getLastUpdate();
$current_user = stripUsername($username);
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Set user emulation form
$emulate_user_form = emulateUser('Broker');
// Set current $user_id to chosen user
if(isset($_POST['emulate'])) {
    $user_id = $_POST['emulate_user'];
    $update_emulated_user = "UPDATE Web_Page_Roles SET Emulate_User_ID = '$user_id' 
        WHERE Page_ID = '$page_id' AND User_ID = '$current_user'";
    $prepare_update = odbc_prepare($conn, $update_emulated_user);
    try {
        odbc_execute($prepare_update);
    } catch (Exception $ex) {
        echo "<pre>" . odbc_errormsg($conn) . "</pre>";
    }
}
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'bendc';
}
$broker  = getBrokerStatus($user_id);
$limiter = "dispatcher_user_id = '$user_id'";
$office  = getOffice($user_id);
$cycle   = getCycle($user_id);
if($cycle === 'Q') {
    $cycle_full = 'Quarterly';
} elseif($cycle === 'A') {
    $cycle_full = 'Annual';
}
//////////////////////////////////////////////// CALCULATE BROKER AGGREGATE DETAILS ///////////////////////////////////////////
// Bar Chart Values for "My Current Margin Level" box
// Let user choose quarter/year to view margin info
if(isset($_POST['filter_page_date'])) {
    $quarter = sanitize($_POST['quarter_filter']);
    $year    = sanitize($_POST['year_filter']);
} else {
    $quarter = $current_quarter;
    $year    = $current_year;
}
// Calculate bar chart values
$my_current_margin    = getCommissionLevel($limiter,$quarter,$year,$cycle);
$current_gross_margin = getCurrentGrossMargin($limiter,$quarter,$year,$cycle);
$goal_to_level_up     = getLevelUp($limiter,$quarter,$year,$cycle,$my_current_margin);
// Define max bar height once level 10 is reached
if($my_current_margin >= 10) {
    $set_max_y_axis = 'high: 375000';
    $margin_message = '<span style="color: #9900cc;"><strong>Congratulations! You have reached Level 10!</strong></span><br/>';
} elseif($goal_to_level_up < 15000 && $my_current_margin < 10) {
    $margin_message = '<span style="color: #00B789;"><strong>Keep going! You have almost reached the next level!</strong></span><br/>';
} else {
    $set_max_y_axis = '';
    $margin_message = '';
}
// Colored boxes for aggregate totals
$min_date             = getMinDate('BC_PayALL_temp',$cycle);
$max_date             = getMaxDate('BC_PayALL_temp');
$gross_margin         = getGrossMargin($limiter);
$comm_pay_current     = getCommPay($limiter);
$historical_pay       = getHistoricalPay("Dispatcher = '$user_id'");
// Donut Chart for load counts
$paid_load_count      = getLoadCount('Paid', $limiter);
$pending_load_count   = getLoadCount('Pending', $limiter);
$missing_pod_count    = getMissingPODCount("bol_received = 'N' AND Delivery_Date IS NOT NULL",$limiter);
$missing_date_count   = getMissingPODCount("bol_received = 'Y' AND Delivery_Date IS NULL",$limiter);
$missing_both_count   = getMissingPODCount("bol_received = 'N' AND Delivery_Date IS NULL", $limiter);
$total_count          = $paid_load_count + $pending_load_count + $missing_pod_count + $missing_date_count + $missing_both_count;
//////////////////////////////////////////////// EXPORT DETAILED DATA INTO EXCEL FILE ///////////////////////////////////////////
// The download script has to be placed BEFORE any HTML is output so it can trigger the download and exit.
// Otherwise there will be HTML printed in the csv file.
if (isset($_POST['export_details_submit'])) {
    $report = $_POST['report'];
    if ($report === NULL) {
        $message = "Please choose a report.";
    } else {
        if ($report === 'missing_pods') { // Orders with Missing POD's and/or Delivery Date
            trackVisit('BC_Broker', $current_user, 'Export_Missing_POD_or_Delivery_Date');
            $sql      = "SELECT dispatcher_user_id, 
                    order_id, 
                    Movement_ID, 
                    override_payee_id, 
                    Destin_City + ',' + Destin_State AS Destination, 
                    Destin_Schedule_Arrive_Early, 
                    Delivery_Date, 
                    bol_received
            FROM BC_PayALL_temp WHERE (bol_received = 'N' OR Delivery_Date is null) AND processing_status <> 'Void' AND Dispatcher_User_Id = '$user_id'";
            $filename = "Missing_POD_or_Delivery_Date_";
        } elseif($report === 'all_paid_history') { // All Paid History
            trackVisit('BC_Broker', $current_user, 'Export_All_Paid_History');
            $sql      = "SELECT dispatcher, 
                Pay_New, 
                Pay_Old, 
                Order_ID, 
                movement_id, 
                payroll_amount, 
                dispatch_date, 
                pay_date,
                CASE WHEN pay_old = 'Y' THEN ordermargin 
                    WHEN pay_new = 'Y' THEN movement_margin 
                    END AS margin,
                CASE WHEN pay_new = 'Y' AND commission_cycle = 'A' THEN Annual_Commission_Percent
					WHEN pay_new = 'Y' AND commission_cycle = 'Q' THEN Quarterly_Commission_Percent 
					WHEN pay_old = 'Y' THEN (commission_percentage / 100) END AS Commission_Percent
                FROM BC_PayALL_final WHERE dispatcher_user_id = '$user_id'";
            $filename = "All_Paid_History_";
        } elseif($report === 'paid_last_period') { // Paid Last Period (Current Pay Period, essentially)
            trackVisit('BC_Broker', $current_user, 'Export_Paid_Last_Period');
            $sql = "SELECT dispatcher, 
                Pay_New, 
                Pay_Old, 
                Order_ID, 
                movement_id, 
                payroll_amount, 
                dispatch_date, 
                pay_date,
                CASE WHEN pay_old = 'Y' THEN ordermargin 
                    WHEN pay_new = 'Y' THEN movement_margin 
                    END AS margin,
                CASE WHEN pay_new = 'Y' AND commission_cycle = 'A' THEN Annual_Commission_Percent
					WHEN pay_new = 'Y' AND commission_cycle = 'Q' THEN Quarterly_Commission_Percent 
					WHEN pay_old = 'Y' THEN (commission_percentage / 100) END AS Commission_Percent
            FROM BC_PayALL_final WHERE CAST(Pay_Date AS DATE) = 
                (SELECT MAX(CAST(pay_date AS DATE)) FROM BC_PayALL_final) AND dispatcher_user_id = '$user_id'";
            $filename = "Paid_Last_Period_";
        }
        exportCSV($sql, $filename);
    }
}
if (isset($_POST['export_historical'])) {
    trackVisit('BC_Broker', $current_user, 'Export_Historical_Commissions');
    $sql      = "SELECT  [Order_ID]
	  ,[movement_id]
	  ,[Dispatcher]
	  ,[commission_percentage]
	  ,[OrderMargin]
	  ,[bookmonthfinal]
	  ,[bookyearfinal]
	  ,[Payroll_Amount]
      ,[Pay_Date]
  FROM [Logistics_Data_Warehouse_QA].[dbo].[BC_PayALL_final]
  WHERE pay_old = 'Y' AND Dispatcher = '$user_id'";
    $filename = "Historical_Commissions_";
    exportCSV($sql, $filename);
}

//////////////////////////////////////////////////// ICON TESTING /////////////////////////////////////////
/*$icon_sql = "SELECT User_Icon AS icon FROM Web_Page_Roles WHERE User_ID = 'stral'";
$icon_query = odbc_prepare($conn, $icon_sql);
odbc_execute($icon_query);
$row = odbc_fetch_array($icon_query);
$icon_code = $row['icon'];*/
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
            $(document).ready(function() {
                //alert('You can extract your entire list of orders using the Excel button above the data table at the bottom half of this page. This report will include a detailed order by order list that you can use to compare with your own reports.');
                //alert('If you have any questions, comments, or suggestions, please click on the E-Mail Support button in the menu to send your concerns to the developer.');
            });
        </script>
        <style>
            .panel-table table, .panel-table th, .panel-table td {
                color: #fff;
                font-size: 22px;
                border: 1px solid #fff;
                padding: 3px;   
                text-align: center;
            }
            #donut-chart-legend table, #donut-chart-legend th, #donut-chart-legend td {
                color: #36454F;
                font-size: 16px;
                border: 1px solid #36454F;
                font-weight: bold;
                padding: 3px;
                text-align: left;
            }
        </style>
    </head>

    <body>

        <div id="wrapper">

            <!--Nav bar loaded here via include_navbar.php-->

            <div id="page-wrapper">

                <div class="container-fluid">
                    
                    <?php 
                    if($admin) { // If DEV, Allison, Nikki, or Natina...
                        echo "<div class='col-lg-12'>Emulating: <strong>" . $user_id . "</strong>$emulate_user_form</div>";
                    }
                    if ($broker) { // If user_id belongs to a broker, show the page content                        
                        ?>
                    
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    My Dashboard<br/>
                                    <small><strong>Commission Details for <?php echo $cycle_full; ?> Cycle</strong></small><small class='blink_me' style='color: #9400D3;'> Page last updated: <strong><?php echo $last_updated;?></strong></small><br/>
                                    <small style="color:#E59316;">Orders are selected based on <strong>Dispatched Date.</strong> You will be paid if the <span style="text-decoration: underline;" class="hint"><strong>Delivery Date is entered and the POD/BOL 
                                            has been received</strong><span class="hinttext">All required documents must be added to each order prior to the final commit for the month. No exceptions. The deadline is the Wednesday before 
                                            the second paycheck of each month. Any documentation that is added after the deadline will be paid on the next commission cycle.</span></span>.</small><br/>
                                    <small><strong>Disclaimer:</strong> All values on this page are estimates until finalized by management and do not necessarily reflect the true amount to be paid.</small>
                                </h1><?php echo $dev_server;?>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                                <div class="col-lg-4">
                                    <!--Chartist Stacked Chart-->
                                    <div class="col-lg-12">
                                        <div class="panel panel-charcoal">
                                            <div class="panel-heading">
                                                <h3 class="panel-title hint"><i class="fa fa-bar-chart "></i> My Current Margin Level
                                                    <span class="hinttext">Estimated based on orders dispatched period to date.
                                                        <br/>*IF NO CHART IS PRESENT*<br/>This means that you have no data yet for this cycle. Once you have
                                                        dispatched some loads for the current cycle the page will update on the FOLLOWING DAY. Check back then.</span>
                                                </h3>
                                                <br/>Showing Quarter <?php echo $quarter; ?> of <?php echo $year; ?>
                                                <form method="post" action="">
                                                    <select name="quarter_filter">
                                                        <option value="" selected disabled>Select Quarter</option>
                                                        <option value="1">Quarter 1</option>
                                                        <option value="2">Quarter 2</option>
                                                        <option value="3">Quarter 3</option>
                                                        <option value="4">Quarter 4</option>
                                                    </select>
                                                    <select name="year_filter">
                                                        <option value="2017" selected>2017</option>
                                                    </select>
                                                    <input type="submit" name="filter_page_date" value="Filter">
                                                </form>
                                            </div>
                                            <div class="panel-body">
                                                <div class="flot-chart">
                                                    <div class="ct-chart ct-perfect-fourth"></div>
                                                </div>
                                                <div class="text-center"><?php echo $margin_message; ?></div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="panel panel-charcoal">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <span class="col-xs-5"><strong>Quarterly Margin</strong></span>
                                                            <span class="col-xs-2 text-center"><strong>Level</strong></span>
                                                            <span class="col-xs-5 text-right"><strong>Annual Margin</strong></span>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer">
                                                        <div class="row">
                                                            <div class="col-xs-5">
                                                                0 - 62,500<br/>
                                                                62,501 - 175,000<br/>
                                                                175,001 - 250,000<br/>
                                                                250,001 - 375,000<br/>
                                                            </div>
                                                            <div class="col-xs-2 text-center">
                                                                3%<br/>
                                                                6%<br/>
                                                                7%<br/>
                                                                10%<br/>
                                                            </div>
                                                            <div class="col-xs-5 text-right">
                                                                0 - 187,500<br/>
                                                                187,501 - 525,000<br/>
                                                                525,001 - 750,000<br/>
                                                                750,001 - 12,000,000<br/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- panel-footer div -->
                                        </div>
                                    </div>
                                </div>
                            <div class="col-lg-4">
                                <!--GROSS MARGIN - PRIMARY BLUE PANEL-->
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="fa fa-dollar fa-5x"></i>
                                                <div></div>
                                            </div>
                                            <div class="col-xs-10 text-center">
                                                <div class="panel-title pull-right"><div>GROSS MARGIN</div></div>
                                                <?php echo $gross_margin; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-left"><strong>Current Period: <?php echo $min_date;?> - <?php echo $max_date;?></strong><br/>
                                            Margin is calculated from a DAILY running total for each period. For details, go to the 
                                            <a href="broker_reports.php">Reports</a> page and select either Paid Last Period or All Paid History.</span>
                                                    <span class="pull-right"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                </div>
                                <div class="row">
                                    <!--COMMISSION PAY - GREEN PANEL-->
                                    <div class="col-lg-12">
                                            <div class="panel panel-green">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-money fa-5x"></i>
                                                            <div>&nbsp;<br/>&nbsp;</div>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class='panel-title'>MY COMMISSION<br/></div>
                                                            <?php echo $comm_pay_current; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <span class="pull-left">Commissions from new program that started on 4/1/2017. For details, go to the 
                                                    <a href="broker_reports.php">Reports</a> page and select either Paid Last Period or All Paid History.</span>
                                                    <span class="pull-right"></span>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <!--DONUT CHART PANEL-->
                            <div class="col-lg-4">
                                <div class="panel panel-charcoal">
                                    <div class="panel-heading">
                                        <h3 class="panel-title hint"><i class="fa fa-pie-chart"></i> Load Count by Status<span class="hinttext">
                                                The green segment denotes how many loads you have been paid on. The orange/yellow segments denote 
                                                unpaid loads. See the legend below for reasons. For details on loads, go to the <a href="broker_reports.php">
                                                    Reports</a> page and choose the Missing POD/Delivery Date report.</span></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="pie-chart"></div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                <div style="margin-left: 20px;">
                                                    <table id="donut-chart-legend">
                                                        <tr><td><div class="legend-key green-key"></div></td><td>PAID</td><td>Loads scheduled to be paid</td></tr>
                                                        <tr><td><div class="legend-key yellow-dark-key"></div></td><td>PENDING PAY</td><td>Loads to be paid next cycle</td></tr>
                                                        <tr><td><div class="legend-key yellow-medium-key"></div></td><td>MISSING POD</td><td>Loads missing POD</td></tr>
                                                        <tr><td><div class="legend-key yellow-key"></div></td><td>NOT DELIVERED</td><td>Loads missing Delivery Date</td></tr>
                                                        <tr><td><div class="legend-key yellow-light-key"></div></td><td>MISC PENDING</td><td>Loads in transit or not delivered & missing POD</td></tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                </div>
                            </div>
                        </div><!-- ./row-->
                        
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <!--jQuery Temp Gauge Plugin-->
        <!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>-->
        <script type="text/javascript" src="js/jquery.tempgauge.js"></script>
        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>
        <script>
            // Pie Chart for Load Count :  Pending v. Paid
            new Morris.Donut({
                // ID of the element in which to draw the chart.
                element: 'pie-chart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                    {label: 'Paid', value: '<?php echo $paid_load_count ?>'},
                    {label: 'Pending Pay', value: '<?php echo $pending_load_count ?>'},
                    {label: 'Missing POD', value: '<?php echo $missing_pod_count ?>'},
                    {label: 'Missing Date', value: '<?php echo $missing_date_count ?>'},
                    {label: 'Misc Pending', value: '<?php echo $missing_both_count ?>'}
                ],
                colors: [
                    '#5CB85C', //green
                    '#ec971f', //dark yellow
                    '#F0AD4E', //medium yellow
                    '#f4c37d', //yellow
                    '#f6ce95' //light yellow
                ],
                formatter: function(y, data) { return y + ' loads' + '\n' + Math.round(y/<?php echo $total_count;?> *100) + '%'},
                resize: true
            });
        </script>

        <script>
            // Data Table - All Records for Current Year
            $(document).ready(function () {
                //Add text input - thead th
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 4, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer with column totals
                    $( api.column( 4 ).footer() ).html(
                        '$'+Math.round(pageTotal*100)/100 +' / $'+ Math.round(total*100)/100
                    );
                },
                    
                    
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    /*"dom": '<l<t>ip>',*/
                    dom: 'Bfrtip',
                    buttons: [
                        'excelHtml5'
                    ],
                    "ajax": "/get_commission_details_broker.php",
                    "columns": [
                        {"data": "Commission_Month", width: "2%"},
                        {"data": "Commission_Year", width: "3%"},
                        {"data": "Movement_ID", width: "5%"},
                        {"data": "order_id", width: "5%"},
                        {"data": "commission_amount",
                            render: $.fn.dataTable.render.number( ',', '.', 2, '$' ),
                            width: "10%"
                        },
                        {"data": "commission_amount_date", width: "10%"},
                        {"data": "Dispatch_Date", width: "10%"},
                        {"data": "bol_received", width: "1%"},
                        {"data": "Delivery_Date", width: "10%"},
                        {"data": "override_payee_id", width: "10%"},
                        {"data": "Processing_Status", width: "10%"}
                    ],
                    "columnDefs": [
                        {"width": "10px", "targets": 0}
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Add the search fields to the header
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            });//end tag
        </script>
        <script>
            new Chartist.Bar('.ct-chart', {
                labels: ['Current Level:  <?php echo $my_current_margin;?>%'],
                series: [
                    [
                        {meta: 'Current Margin', value: <?php echo $current_gross_margin; ?>}
                    ],
                    [
                        {meta: 'Margin to Next Level', value: <?php echo $goal_to_level_up; ?>}
                    ]
                ]
            },
            {
                fullWidth: true,
                plugins: [
                    Chartist.plugins.tooltip({
                    currency: '$',
                    anchorToPoint: false
                  }),
                  Chartist.plugins.ctPointLabels({
                    textAnchor: 'middle'
                  })
                ],
                chartPadding: 30,
                stackBars: true,
                axisY: {
                    labelInterpolationFnc: function (value) {
                         return '$' + (value / 1000) + 'k';
                    },
                    <?php echo $set_max_y_axis;?> /*Set this value to define a max value for the bar(s) */
                }
            }).on('draw', function (data) {
                if (data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 60px'
                    });
                }
            });
        </script>
    </body>
</html>