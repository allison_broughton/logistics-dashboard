<?php
require 'include_functions.php';
$title = "Home";
$page_id = "Home_Page";
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Page Access
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If new post has been submitted...
if (isset($_POST['submit_post'])) {
    $title       = sanitize($_POST['title']);
    $content     = sanitize($_POST['content']);
    $posted_by   = $user_id;
    $posted_date = $today;
    $sql         = "INSERT INTO Loop_News (Title,Content,Posted_By,Posted_Date,Is_Active)
                    VALUES ('$title','$content','$posted_by','$posted_date','Y')";
    $query       = odbc_prepare($conn_dev, $sql);
    $result      = odbc_execute($query);
    if (!$result) {
        $message = "<div class='alert alert-danger'>
                    <strong>Failed!</strong> Your news item was not posted.<br/>
                    Query Failed: " . odbc_errormsg($conn_dev) . "
                    </div>";
    } else {
        $message = "<div class='alert alert-success'>
                    <strong>Success!</strong> New item was successfully posted.<br/>
                    </div>";
    }
}
// If post has been deleted...
if ((isset($_GET['id'])) & $admin) {
    $id     = sanitize($_GET['id']);
    $sql    = "UPDATE Loop_News SET Is_Active = 'N' WHERE Id = '$id'";
    $query  = odbc_prepare($conn_dev, $sql);
    $result = odbc_execute($query);
    if (!$result) {
        $message = "<div class='alert alert-danger'>
                    <strong>Failed!</strong> Post was not deleted.<br/>
                    Query Failed: " . odbc_errormsg($conn_dev) . "
                    </div>";
    } else {
        $message = "<div class='alert alert-success'>
                    <strong>Success!</strong> Post was successfully deleted.
                    </div>";
    }
}
// Select 5 newest news posts to display
function getNews() {
    global $conn_dev;
    global $admin;
    $sql   = "SELECT TOP 5 * FROM Loop_News WHERE Is_Active = 'Y' ORDER BY Posted_Date DESC";
    $query = odbc_prepare($conn_dev, $sql);
    odbc_execute($query);
    $news  = "";
    while ($row   = odbc_fetch_array($query)) {
        if ($row['Posted_By'] === 'stral') {
            $color = 'purple';
        } else {
            $color = 'primary';
        }
        $news .= createNewsPanel($admin, $row['Id'], $row['Title'], $row['Posted_By'], $row['Posted_Date'], $row['Content'], $color);
    }
    return $news;
}
// Must include last so as not to overwrite something from the navbar file - if placed earlier in the script
// the Brokers menu tab disappears
require 'include_navbar.php';
?>
<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title;?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <script src='js/jquery.js'></script><!-- jQuery -->
        <script src='js/bootstrap.min.js'></script><!-- Bootstrap Core JavaScript -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="jumbotron text-center">
                        <h2 class='text-uppercase'>Welcome to LOOP!</h2>
                        <h3>Logistics Optimized Office Program</h3>
                        <?php echo $dev_server; ?>
                    </div>
                    <?php if($admin) {?>
                    <div class="row">
                        <div class='col-lg-8 col-lg-offset-2'>
                            <?php if (isset($message)) { echo $message;} ?>
                            <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#new_post">New Post</button>
                            <div id="new_post" class="div-content collapse">
                                <div class="spacer"></div>
                                <div class='panel panel-primary'>
                                    <div class='panel-heading'>
                                        <div class='panel-title'><i class='fa fa-plus-circle'></i> Add New Item</div>
                                    </div>
                                    <div class='panel-footer'>
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="title">Title:</label>
                                                <input name="title" type="text" class="form-control" id="title" maxlength="40" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="content">Content:</label>
                                                <textarea name="content" class="form-control" rows="5" id="content" maxlength="3500" required></textarea>
                                            </div>
                                            <input type="submit" name="submit_post" class="btn btn-primary" value="Post"/>
                                        </form>
                                        <div class='clearfix'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                    <?php }?>
                    <div class="row">
                        <?php 
                            getNews();
                        ?>
                    </div>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
    </body>
</html>