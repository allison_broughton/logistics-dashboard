﻿<?php
///////////////////////////////////////// ODBC Connection /////////////////////////////////////////
$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$dbc              = odbc_connect($connection_string, $user, $pass);

// Sanitize user input to prevent SQL injection
function sanitize($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    $string = preg_replace('/\s*,\s*/', ',', $string);
    $string = str_replace(',', ', ', $string);
    return $string;
}

if (!$dbc) {
    // Show error if we cannot connect.
    echo 'ERROR: Could not connect to the database.';
} else {
    // Is there a posted query string?
    if (isset($_POST['queryString'])) {
        $queryString = sanitize($_POST['queryString']);
        $stopType    = sanitize($_POST['stopType']);
        // Is the string length greater than 0?

        if (strlen($queryString) > 0) {
            if(strpbrk($queryString, '0123456789')) {
                $string = "SELECT TOP 10 CAST([5ZIPNUM] AS INT) AS zip, [Cityname] FROM usmarkets WHERE [5ZIPNUM] LIKE '$queryString%'";
            } else {
                $string = "SELECT TOP 10 CAST([5ZIPNUM] AS INT) AS zip, [Cityname] FROM usmarkets WHERE cityname LIKE '$queryString%'";
            }
            $query = odbc_prepare($dbc,$string);
            $execute = odbc_execute($query);
            if ($execute) {
                // While there are results loop through them
                while ($result = odbc_fetch_array($query)) {
                    // Format the results, I'm using <li> for the list, you can change it.
                    // The onClick function fills the textbox with the result.
                    if($stopType === 'destination') {
                        echo '<li onClick="fillD(\'' . $result['Cityname'] . ' '. $result['zip'] . '\');">' . $result['Cityname'] . ' '. $result['zip'] .'</li>';
                    } else {
                        echo '<li onClick="fill(\'' . $result['Cityname'] . ' '. $result['zip'] . '\');">' . $result['Cityname'] . ' '. $result['zip'] .'</li>';
                    }
                }
            } else {
                echo 'ERROR: ' . odbc_errormsg($dbc) . '<br>Query: ' . $string;
            }
        } else {
            // Dont do anything.
        } // There is a queryString.
    } else {
        echo 'There should be no direct access to this script!';
    }
}
?>