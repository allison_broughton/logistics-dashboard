<?php
require 'include_functions.php';
// Initialize session and login variable
session_start();
$_SESSION['login'] = "";
$login_error       = "";

// Set up and test ldap connection
$link = ldap_connect('swift.com');
if (!$link) {
    $ldap_status = "Could not connect to server";
} else {
    $ldap_status = "LDAP Status: Connected to swift.com server";
}

if (isset($_POST['login_submit'])) {
    //Append supplied username to "@swift.com"
    $ad_user = (sanitize($_POST['login_username'])) . "@swift.com";
    $ad_pass = sanitize($_POST['login_password']);

    // Check for empty strings
    if (empty($ad_user) or empty($ad_pass)) {
        exit();
    }
    if ($link) {
        ldap_set_option($link, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap_bind = ldap_bind($link, $ad_user, $ad_pass);
        if ($ldap_bind) {
            $_SESSION['login'] = "true";
            //$login_status .= "SESSION['login'] = " . $_SESSION['login'];
            echo "Login successful";
        } else {
            //$login_status = "FAIL";
            $login_error = "<h3 class='alert alert-danger'><strong>Oh snap!</strong> Those are invalid credentials.</h3>";
        }
    }
}

// Write logout function and unset the SESSION login variable
if (isset($_POST['logout'])) {
    unset($_SESSION['login']);
}

// Set up and test db connection
/* $server            = '10.86.0.33'; //stcphx-sql1802
  $user              = 'svc_lorl'; //service account with read/write perms
  $pass              = 'Rate4ookup';
  $port              = 'Port=1433';
  $database          = 'Logistics_Data_Warehouse_Dev';
  $connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
  $conn              = odbc_connect($connection_string, $user, $pass);
  if ($conn) {
  $conn_status = "Connected";
  } else {
  die;
  $conn_status = "Could not connect";
  } */


// Must assign session variable or the reset button will clear it and log the user out
if (isset($_POST['reset'])) {
    session_start();
    $_SESSION['login'] = true;
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Q-LOOP | Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- jQuery -->
        <script src='js/jquery.js'></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>

        <?php
        // If login session variable is empty, show the login form
        if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
            ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <form action="" method="POST" autocomplete="off">
                            <h2>Please sign in</h2>
                            <?php echo $login_error; ?>
                            <input type="text" name="login_username" class="form-control" placeholder="Username" required autofocus>
                            <br/>
                            <input type="password" name="login_password" class="form-control" placeholder="Password" required>
                            <br/>
                            <button class="btn btn-lg btn-primary btn-block" type="submit" name="login_submit">Sign in</button>
                        </form>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        <?php } ?>

    </body>
</html>
