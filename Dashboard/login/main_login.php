<?php
if(!isset($_SESSION)) {
    session_start();
}
require '../include_functions.php';
require 'odbc_conn.php';

//error_reporting(E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_ERROR | E_CORE_ERROR); // Shows ONLY errors - use this setting when page is live
error_reporting(E_ALL ^ E_NOTICE); // shows ALL notices, warnings, errors
ini_set('display_errors', 1);

if (isset($_POST['submit'])) {
    $username = sanitize($_POST['myusername']);
    $password = $_POST['mypassword'];
    if (!empty($username && $password)) {
        $sql    = "SELECT username, password FROM web_app_users_AB WHERE username = '$username'";
        $query  = odbc_prepare($conn, $sql);
        $result = odbc_execute($query);
        if ($result === FALSE) {
            die(odbc_error($conn));
        } else {
            while ($row = odbc_fetch_array($query)) {
                //if ($username === $row['username'] && $password === $row['password']) {
                if(password_verify($password, $row['password'])) {
                    $_SESSION['username'] = $username;
                    // Redirect to home page
                    header("location:../tableau.php");
                }
            }
        } $login_message = "<div id='login_message' class='alert alert-danger'>Oh snap! You have entered an incorrect username/password.</div>";
    } else {
        $login_message = "<div id='login_message' class='alert alert-danger'>Please enter your username and password.</div>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Q-LOOP | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="main.css" rel="stylesheet" media="screen">
    </head>

    <body>
        <div class="container">

            <form class="form-signin" name="form1" method="post" action="">
                <h2 class="form-signin-heading text-center">Login</h2>
                <input name="myusername" id="myusername" type="text" class="form-control" placeholder="Username" autofocus>
                <input name="mypassword" id="mypassword" type="password" class="form-control" placeholder="Password">

                <button name="submit" id="submit" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

                <?php echo $login_message; ?>
            </form>

        </div> <!-- /container -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-2.2.4.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <!-- The AJAX login script -->
        <script src="js/login.js"></script>

    </body>
</html>
