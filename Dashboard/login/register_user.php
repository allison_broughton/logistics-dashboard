<?php
session_start();
if (isset($_SESSION['internal_user'])) {
    header("location:../index.php");
}
require '../include_functions.php';
require 'odbc_conn.php';


if(isset($_POST['submit'])) {
    $new_username = sanitize($_POST['myusername']);
    $first_name = sanitize($_POST['myfirstname']);
    $last_name = sanitize($_POST['mylastname']);
    $password = password_hash($_POST['mypassword'], PASSWORD_DEFAULT);
    $email    = sanitize($_POST['myemail']);
    $company  = sanitize($_POST['mycompany']);
    
    if(!empty($new_username && $password && $email && $company)) {
        $register_message = "<div id='login_message' class='alert alert-success'>User Registered Successfully!</div>";
        $sql = "INSERT INTO web_app_users_AB (create_date,username,first_name,last_name,password,email,company_name,is_active) VALUES (
            '$today_datetime',
            '$new_username',
            '$first_name',
            '$last_name',
            '$password',
            '$email',
            '$company',
            'Y'
            )";
        $query = odbc_prepare($conn, $sql);
        //echo $sql;
        odbc_execute($query);
    } else {
        $register_message = "<div id='login_message' class='alert alert-danger'>Please fill out all fields</div>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Q-LOOP | Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="main.css" rel="stylesheet" media="screen">
  </head>

  <body>
    <div class="container">
        <a href="../index.php" target="_self"><i class="fa fa-arrow-circle-left"></i> Back to Logistics Dashboard</a>
<?php if($username === "SWIFT\stral" || $username === "localhost\DEV") {?>
        <form class="form-signin" name="form1" method="post" action="" autocomplete="off">
        <h2 class="form-signin-heading text-center">Register New User</h2>
        <input name="myusername" id="myusername" type="text" class="form-control" placeholder="Username" autofocus>
        <input name="myfirstname" id="myfirstname" type="text" class="form-control" placeholder="First Name">
        <input name="mylastname" id="mylastname" type="text" class="form-control" placeholder="Last Name">
        <input name="mypassword" id="mypassword" type="password" class="form-control" placeholder="Password">
        <input name="myemail" id="myemail" type="email" class="form-control" placeholder="E-Mail">
        <input name="mycompany" id="mycompany" type="text" class="form-control" placeholder="Company">

        <button name="submit" id="submit" class="btn btn-lg btn-primary btn-block" type="submit">Register</button>

        <?php echo $register_message;?>
      </form>
<?php } else echo "<div class='alert alert-danger text-center'>You are not authorized for access to this page. Please contact the administrator.</div>";?>
    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <!-- The AJAX login script -->
    <script src="js/login.js"></script>

  </body>
</html>
