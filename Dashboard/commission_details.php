<?php
session_start();
require 'include_functions.php';

$server            = '10.86.0.33'; //stcphx-sql1802
$user              = 'svc_lorl'; //service account with read/write perms
$pass              = 'Rate4ookup';
$port              = 'Port=1433';
$database          = 'Logistics_Data_Warehouse_QA';
$connection_string = "DRIVER={SQL Server};SERVER=$server;$port;DATABASE=$database";
$conn              = odbc_connect($connection_string, $user, $pass);

$sql = "SELECT commission_cycle,
            Movement_ID,
            dispatcher_user_id,
            commission_amount,
            commission_amount_date,
            Commission_percentage_date,
            order_id,
            bol_received,
            Broker_Commission_Eligible,
            BC_Paid,
            Processing_Status,
            Move_Percent,
            MovementEligible,
            Order_Margin,
            Commission_Month,
            Commission_Year,
            Commission_Quarter
            FROM BC_broker_commission_calc_temp
            WHERE Processing_Status = 'Paid' AND commission_amount_date = (SELECT MAX (commission_amount_date) FROM BC_broker_commission_calc_temp) $dispatcher";

// Change column headings
function map_columns($input) {
    //global $colnames;
    $colnames = [
        'commission_cycle'           => "Commission Cycle",
        'Movement_ID'                => "Movement ID",
        'dispatcher_user_id'         => "Dispatcher",
        'commission_amount'          => "Commission Amount",
        'commission_amount_date'     => "Commission Amount Date",
        'Commission_percentage_date' => "Commission Percentage Date",
        'order_id'                   => "Order ID",
        'bol_received'               => "BOL Received",
        'Broker_Commission_Eligible' => "Commission Eligible",
        'BC_Paid'                    => "Paid",
        'Processing_Status'          => "Processing Status",
        'Move_Percent'               => "Move Percent",
        'MovementEligible'           => "Movement Eligible",
        'Order_Margin'               => "Order Margin",
        'Commission_Month'           => "Commission Month",
        'Commission_Year'            => "Commission Year",
        'Commission_Quarter'         => "Commission Quarter"
    ];
    return isset($colnames[$input]) ? $colnames[$input] : $input;
}

 function exportDetails($sql) {
    global $conn;
    $filename = "commission_details" . date('Ymd') . ".csv";
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");// Comment this line to debug
    $out      = fopen("php://output", 'w');
    //header("Content-Type: text/plain"); // Uncomment this line to debug
    $flag     = false;
    $query    = odbc_prepare($conn, $sql);
    odbc_execute($query);
    while (false !== ($row      = odbc_fetch_array($query))) {
        if (!$flag) {
            // Display field/column names as first row
            $firstline = array_map(__NAMESPACE__ . '\map_columns', array_keys($row));
            fputcsv($out, $firstline, ',', '"');
            $flag      = true;
        }
        array_walk($row, __NAMESPACE__ . '\cleanData');
        fputcsv($out, array_values($row), ',', '"');
    }
    fclose($out);
    exit;
}

if(isset($_POST['export_details'])) {
    exportDetails($sql);
}
?>
<html>
    <body>
        <button type="submit" name="export_details">Export Details</button>
    </body>
</html>