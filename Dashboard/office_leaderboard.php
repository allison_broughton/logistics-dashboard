<?php
require 'include_functions.php';
//redirect('under_construction.php');
if (!($user_id === 'localhost\DEV')) {
    redirect('under_construction.php');
}
$page_id = 'BC_Leaderboard';
$title = 'Leaderboard';
// Insert user info to database to track page visits
if (!($user_id === 'localhost\DEV')) {trackVisit($page_id, $user_id, 'page_visit');}
// Check page access
$page_access          = getPageAccess($user_id, $page_id);
//Labels for each Broker's bar
$array_brokers        = getBrokers();
$broker_full_names    = array_map("getBrokerFullName", $array_brokers);
$labels               = implode("','", $broker_full_names);
//Gross margin data for each bar height
$gross_margin_arr     = array_map("getMarginforDataSet", $array_brokers);
$gross_margin_data    = implode(", ", $gross_margin_arr);
//Background color
$background_color_arr = array_map("getBackgroundColor", $array_brokers);
$background_color_set = implode("',' ", $background_color_arr);
//Border color
$border_color_arr     = array_map("getBorderColor", $array_brokers);
$border_color_set     = implode("',' ", $border_color_arr);
//Hover color
$hover_color_arr      = array_map("getHoverColor", $array_brokers);
$hover_color_set      = implode("',' ", $hover_color_arr);
//Data Set generated - only works for stacked bar chart
$data_arr             = array_map("createDataSet", $array_brokers);
$data_set_array       = implode("},{", $data_arr);
///////////////////////////////// SWITCHED TO CHART.JS LIBRARY TO HAVE MORE CONTROL OVER BAR COLORS //////////////////////////////
$background_colors    = "'#0d254c',
                         '#008000',
                         '#c21807',
                         '#cc5500'"; //BLUE, GREEN, RED, ORANGE
$border_colors        = "'#091935',
                         '#005900',
                         '#871004',
                         '#8E3B00'";
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/><!-- DataTables -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map">
        <link rel="stylesheet" type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
          $(window).load(function() {
            $("#loader").fadeOut("slow");
            $(".overlay-loader").fadeOut("slow");
          });
        </script>
    </head>
    <body>
        <div id="loader">
            <div class="col-lg-12 text-center">
               <i id="spinner" class="fa fa-cog fa-spin fa-huge"></i>
            </div>
        </div>
        <div class="overlay-loader"></div>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                    if ($page_access) {
                        ?>
                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    <?php echo $title;?><br/>
                                </h1><?php echo $dev_server;?>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <!--Chart.JS Stacked Chart-->
                                <div id="leaderboard">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bar-chart "></i> Gross Margin per Broker for <?php echo $current_year;?></h3>
                                            <span class="pull-right legend">
                                                <span class="legend-key" style="background: #ffd700"></span> LRDO<br/>
                                                <span class="legend-key" style="background: #253A5D"></span> NWA<br/>
                                                <span class="legend-key" style="background: #008000"></span> PHX<br/>
                                                <span class="legend-key" style="background: #C21807"></span> SLC<br/>
                                                <span class="legend-key" style="background: #CC5500"></span> WMA
                                            </span>
                                        </div>
                                        <div class="panel-body">
                                            <canvas id="officeLeaderboard" width="200" height="75"></canvas>
                                        </div>
                                        <div class="panel-footer">
                                            <span class="pull-right"></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>

                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>

        <!-- Flot Charts JavaScript -->
       <!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
        <script src="js/plugins/flot/jquery.flot.js"></script>
        <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="js/plugins/flot/jquery.flot.resize.js"></script>
        <script src="js/plugins/flot/jquery.flot.pie.js"></script>
        <script src="js/plugins/flot/flot-data.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <!--Chartist Plugin-->
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-pointlabels-master/src/scripts/chartist-plugin-pointlabels.js"></script>
        <script type="text/javascript" src="js/plugins/chartist-plugin-tooltip-master/src/scripts/chartist-plugin-tooltip.js"></script>

        <script>
            var ctx = document.getElementById("officeLeaderboard");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['<?php echo $labels; ?>'],
                    datasets: [{
                            label: 'Current Margin',
                            data: [<?php echo $gross_margin_data; ?>],
                            backgroundColor: ['<?php echo $background_color_set; ?>'],
                            borderColor: ['<?php echo $border_color_set; ?>'],
                            hoverBackgroundColor: ['<?php echo $border_color_set; ?>'],
                            borderWidth: 2
                        }
                    ]
                },
                options: {
                    tooltips: {
                        displayColors: false,
                        callbacks: {
                        label: function(tooltipItems, data) { 
                            return 'Margin: ' + '$' + tooltipItems.yLabel.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                        }
                    }
                    },
                    scaleBeginAtZero: true,
                    //tooltipTemplate: "<%if (label){%><%=label%>: <%}%>$<%= value %>",
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                stacked: false,
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        return '$' + (value / 1000) + 'k';
                                    }
                                }
                            }],
                        xAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>

    </body>
</html>