<?php

if (isset($_POST['submit'])) {
    $origin        = sanitize($_POST['origin']);
    $destin        = sanitize($_POST['destination']);
    $pickup_date   = sanitize($_POST['pickup_date']);
    $pickup_time   = sanitize($_POST['pickup_time']);
    $delivery_date = sanitize($_POST['delivery_date']);
    $delivery_time = sanitize($_POST['delivery_time']);
    // Validate cities first
    $valid_origin = validateCity($origin);
    $valid_destin = validateCity($destin);

    // If one or both cities were not found, kill the script and alert user
    if (!($valid_origin) || !($valid_destin)) {
        $alert_type = 'alert-warning';
        $message    = "No cities found. Please check the spelling and state abbreviation(s).";
    } else {
        $alert_type = 'alert-success';
        $message_header = "Result:";
        //$message    = "Congratulations. Those are real cities.<br/>";
        // Look up KMA Lane
        $kma_origin = getMarket(fuzzySearch($origin));
        $kma_destin = getMarket(fuzzySearch($destin));
        // Check to see if any markets exist for the chosen origin-destination pair
        if ((strpos($kma_origin, 'No market') !== false) || (strpos($kma_destin, 'No market') !== false)) {
            $alert_type = 'alert-warning';
            $message    .= "<br/>However, there were no KMA lanes for one or both of those places.";
        } else {
            $kma_lane = $kma_origin . "-" . $kma_destin;
            //$message    .= "<br/>And guess what? There is also a KMA lane for that city pair! $kma_lane<br/>";
            // Calculate distance in miles
            $distance = 2733; // For testing formula quickly
            //$distance = getMileage($origin, $destin);
            // Set variables for transit formula
            $base_mph = 47.5;
            $buffer = .82;
            $transit = ($distance/$base_mph)+$buffer;
            $message .= "Transit time: ( $distance / $base_mph ) + $buffer ) = " . $transit . "<br/>";
            // Calculate solo transit hours
            if($transit < 10) {
                $solo_transit = number_format($transit,1);
                $team_transit = "N/A";
            } else {
                $solo_transit = number_format((number_format(($transit)/10)*12+(($distance/$base_mph)+$buffer)),1);
                $message .= "Solo: TRUNC ( ( TRUNC( $transit / 10 ) * 12 ) + ( ( $distance / $base_mph ) + $buffer ) )<br/>";
                // Calculate team transit hours
                $team_transit = number_format((($distance/$base_mph)+$buffer),1);
            }
            
        }
    }
}
?>
<form method="POST" action="" id="calc_transit" class="form-horizontal">
    <div class="form-group">
        <label for="origin" class="control-label col-xs-3">Origin </label>
        <div class="col-xs-9">
            <input type="text" name="origin" id="origin" required="true" placeholder="City, ST" value="<?php echo $origin; ?>" autofocus/>
        </div>
    </div>
    <div class="form-group">
        <label for="destination" class="control-label col-xs-3">Destination </label>
        <div class="col-xs-9">
            <input type="text" name="destination" id="destination" required="true" placeholder="City, ST" value="<?php echo $destin; ?>" />
        </div>
    </div>
    <?php if (isset($_POST['submit'])) { ?>
        <div class="form-group">
            <label for="distance" class="control-label col-xs-3">Distance (miles) </label>
            <div class="col-xs-9">
                <input type="number" name="distance" id="distance" class='form-result' value="<?php echo $distance; ?>" readonly/>
            </div>
        </div>

        <div class="form-group">
            <span class="col-md-2 control-label">Transit Hours</span>
            <div class="col-md-6">
                <div class="form-group row">
                    <label for="solo_transit" class="col-md-1 control-label">Solo</label>
                    <div class="col-md-2">
                        <input type="number" name="solo_transit" id="solo_transit" class='form-result' value="<?php echo $solo_transit; ?>" readonly/>
                    </div>
                    <label for="team_transit" class="col-md-1 control-label">Team</label>
                    <div class="col-md-2">
                        <input type="number" name="team_transit" id="team_transit" class='form-result' value="<?php echo $team_transit; ?>" readonly/>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>

    <div class="form-group">
        <div class="col-xs-offset-1 col-xs-11">
            <button type="submit" name="calc_transit" class="btn btn-primary col-lg-offset-2">Submit</button>
            <!--<input id="calc_transit" type="button" value="Calculate Transit" class="btn btn-primary col-lg-offset-2" />-->
        </div>
    </div>
</form>