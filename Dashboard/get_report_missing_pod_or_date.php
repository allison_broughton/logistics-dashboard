<?php
session_start();
require 'include_functions.php';
$page_id = 'BC_Broker_Report';
// Set up user emulation
if($username === 'localhost\DEV') {
    $admin   = true;
} else {
    $admin   = getAdminStatus($username,$page_id);
}
// If user is an admin, set their emulated user id
if($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id,$user_id);
} elseif($admin && $user_id === 'localhost\DEV') {
    $user_id = 'bendc';
}
// Get active commission paystructure records and store in array
function getRecords($user_id) {
    global $conn;
    // MUST select the same fields as the other reports, otherwise datatable will throw an error
    // Since thise query pulls from a different table than the other reports, some columns did not
    // exist. Replaced these column names with Neg_Margin_Override and aliased as the missing columns
    // These columns are being hidden for this report so they will not be displayed anyway.
    $string = "SELECT  CASE WHEN commission_cycle = 'A' THEN arecord_count
			    WHEN commission_cycle = 'Q' THEN qrecord_count END AS Record_Count,
		Neg_Margin_Override AS Pay_Old,
		Neg_Margin_Override AS Pay_New,
                Order_ID,
		movement_id,
		total_charge,
		Carrier_Total_Pay,
                bol_received,
		Neg_Margin_Override AS margin,
		processing_status,
		Neg_Margin_Override AS Commission_End_Date,
		CONVERT(DATE, Delivery_Date) AS Delivery_Date,
		CONVERT(DATE, Dispatch_Date) AS Dispatch_Date,
		movement_margin,
		CASE WHEN commission_cycle = 'A' THEN annual_running_total
                     WHEN commission_cycle = 'Q' THEN quarter_running_total END AS Running_Total,
		Neg_Margin_Override AS Commission_Percent,
		Neg_Margin_Override AS Payroll_Amount,
                Neg_Margin_Override AS Pay_Date,
                dispatcher_user_id,
                override_payee_id,
		Destin_City + ',' + Destin_State AS Destination,
                CONVERT(DATE, Destin_Schedule_Arrive_Early) AS Destin_Schedule_Arrive_Early
        FROM BC_PayALL_temp WHERE (bol_received = 'N' OR Delivery_Date is null) 
        AND processing_status <> 'Void' AND Dispatcher_User_Id = '$user_id'";
    $stmt   = odbc_prepare($conn, $string);
    odbc_execute($stmt);
    $json   = array();
    while ($row = odbc_fetch_array($stmt)) {
        $json['data'][] = $row;
    }
    return $json;
}

$data = getRecords($user_id);
header('Content-Type: application/json');
echo json_encode($data);