<?php
require 'include_functions.php';
$page_id = 'BC_Broker_Report';
$title   = 'Broker Reports';
$real_id = stripUsername($username); // Avoids inserting emulated user id when report is cliked
///// Clean up records /////
/*$sql = "UPDATE Web_Page_Visits SET User_ID = 'stral' WHERE VisitID = '1378'";
$query = odbc_prepare($conn, $sql);
odbc_execute($query);*/
// Insert user info to database to track page visits
if (!($user_id === 'localhost\DEV')) {
    trackVisit('BC_Broker_Report', $real_id, 'page_visit');
}
// Set user emulation form
$emulate_user_form = emulateUser('Broker');
// Set current $user_id to chosen user
if (isset($_POST['emulate'])) {
    $user_id              = $_POST['emulate_user'];
    $current_user         = stripUsername($username);
    $update_emulated_user = "UPDATE Web_Page_Roles SET Emulate_User_ID = '$user_id' 
        WHERE Page_ID = '$page_id' AND User_ID = '$current_user'";
    $prepare_update       = odbc_prepare($conn, $update_emulated_user);
    try {
        odbc_execute($prepare_update);
    } catch (Exception $ex) {
        echo "<pre>" . odbc_errormsg($conn) . "</pre>";
    }
}
// Page Access
if ($username === 'localhost\DEV') {
    $admin = true;
} else {
    $admin = getAdminStatus($username, $page_id);
}
// If user is an admin, set their emulated user id
if ($admin && !($user_id === 'localhost\DEV')) {
    $user_id = checkEmulation($page_id, $user_id);
} elseif ($admin && $user_id === 'localhost\DEV') {
    $user_id = 'bendc';
}
$broker       = getBrokerStatus($user_id);
$limiter      = "dispatcher_user_id = '$user_id'";
$office       = getOffice($user_id);
///////////////////////////////////////////// MY SUMMARY TOTALS /////////////////////////////////////////////////
$agg_margin   = 24601;
$agg_loads    = 246;
$agg_percent  = 6;
///////////////////////////////////////////// REPORTS ///////////////////////////////////////////////////////////
$report_title = "My Records for " . $current_year;
$ajax_src     = '/get_broker_report_default.php';
$hide_columns = '[1,2,7,8,10,18,19,20]';
if (isset($_POST['paid_last_period'])) {
    $report_title = 'Paid Last Period';
    $ajax_src     = '/get_report_paid_last_period.php';
    $hide_columns = '[7,8,10,13,18,19,20]';
    if (!($username === 'localhost\DEV')) {
        trackVisit('BC_Broker_Report', $real_id, 'viewed_paid_last_period');
    }
} elseif (isset($_POST['all_paid_history'])) {
    $report_title = 'All Paid History';
    $ajax_src     = '/get_report_all_paid_history.php';
    $hide_columns = '[7,8,10,13,18,19,20]';
    if (!($username === 'localhost\DEV')) {
        trackVisit('BC_Broker_Report', $real_id, 'viewed_all_paid_history');
    }
} elseif (isset($_POST['missing_pod_or_date'])) {
    $report_title = 'Missing POD/Delivery Date';
    $ajax_src     = '/get_report_missing_pod_or_date.php';
    $hide_columns = '[1,2,5,6,7,8,9,11,12,14,15,16]';
    if (!($username === 'localhost\DEV')) {
        trackVisit('BC_Broker_Report', $real_id, 'viewed_missing_pod_or_date');
    }
} elseif (isset($_POST['all_loads'])) {
    $report_title = 'All Loads';
    $ajax_src     = '/get_report_all_loads.php';
    $hide_columns = '[0,1,2,7,8,11,13,18,19,20]';
    if (!($username === 'localhost\DEV')) {
        trackVisit('BC_Broker_Report', $real_id, 'viewed_all_loads');
    }
}
//////////////////////////////////////////////// INCLUDE NAVBAR /////////////////////////////////////////////////////////////////
/* The navbar has to be loaded AFTER the exportCSV function is triggered, otherwise its HTML content will be included in the csv */
require 'include_navbar.php';
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Q-LOOP | <?php echo $title; ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/colreorder/1.3.2/css/colReorder.bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet"/>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome -->
        <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- JavaScript -->
        <script src="js/jquery.js"></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
    </head>

    <body>

        <div id="wrapper">

            <!--Nav bar loaded here via include_navbar.php-->

            <div id="page-wrapper">

                <div class="container-fluid">

                    <?php
                    if ($admin) { // If DEV, Allison, Nikki, or Natina...
                        echo "<div class='col-lg-12'>Emulating: <strong>" . $user_id . "</strong>$emulate_user_form</div>";
                    }
                    if ($broker) { // If user_id belongs to a broker, show the page content                        
                        ?>
                        <!-- Page Heading -->
                        <div class="row">
                            
                            <?php if($username === 'localhost\DEV') {?>
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    <span style='color: #008000;'>My Summary</span><br/>
                                    <small>View your totals by month</small>
                                </h1>
                                <form method="post" action="">
                                    <select name="filter_month" required>
                                        <option value="" selected disabled>Filter month...</option>
                                        <!--Write a PHP function to grab distinct months from table-->
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select name="filter_year" required>
                                        <option value="" selected disabled>Filter year...</option>
                                        <option value="2017">2017</option>
                                    </select>
                                    <select name="filter_status" required>
                                        <option value="" selected disabled>Filter status...</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Paid">Paid</option>
                                    </select>
                                    <button type="submit" class="btn btn-success" name="submit_month" id="button">Submit</button>
                                </form>
                                <div class="col-lg-2">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class='huge'>$<?php echo $agg_margin;?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <span style='color: #008000;'>TOTAL MARGIN</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class='huge'><?php echo $agg_loads;?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <span style='color: #008000;'>TOTAL LOADS</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="panel panel-green">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <div class='huge'><?php echo $agg_percent;?>%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer text-center">
                                            <span style='color: #008000;'>TOTAL PERCENT</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php echo $dev_server;?>
                        <div class="page-divider"></div>
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="page-header">
                                    <small>Choose a report below:</small>
                                </h1>
                                <form method="post" action="">
                                    <button type="submit" class="btn btn-primary" name="paid_last_period">Paid Last Period</button>
                                    <button type="submit" class="btn btn-primary" name="all_paid_history">All Paid History</button>
                                    <button type="submit" class="btn btn-primary" name="missing_pod_or_date">Missing POD/Delivery Date</button>
                                    <button type="submit" class="btn btn-primary" name="all_loads">All Loads</button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h1 class="page-header"><span style="color: #0078cc;"><?php echo $report_title; ?></span><br/>
                                    <small>Click the Excel button below once the table has loaded in order to export data. Orders with an ID of 'HOTLDADJ' denote a Hot Load Adjustment.</small>
                                </h1>
                                <table id="example" class ="table table-striped table-bordered display nowrap" style="max-width: none !important;">
                                    <thead>
                                        <tr style="color: #FFF;">
                                            <th>Record #</th>
                                            <th>Pay Old</th>
                                            <th>Pay New</th>
                                            <th>Order</th>
                                            <th>Movement</th>
                                            <th>Margin</th>
                                            <th>Running Total</th>
                                            <th>Percent</th>
                                            <th>Payroll</th>
                                            <th>Status</th>
                                            <th>BOL?</th>
                                            <th>Commission End</th>
                                            <th>Dispatch</th>
                                            <th>Delivery</th>
                                            <th>Paid On</th>
                                            <th>Total Charge</th>
                                            <th>Carrier Pay</th>
                                            <th>Dispatcher</th>
                                            <th>Payee</th>
                                            <th>Destination</th>
                                            <th>Arrival</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr style="color: #FFF;">
                                            <th>Record #</th>
                                            <th>Pay Old</th>
                                            <th>Pay New</th>
                                            <th>Order</th>
                                            <th>Movement</th>
                                            <th>Margin</th>
                                            <th>Running Total</th>
                                            <th>Percent</th>
                                            <th>Payroll</th>
                                            <th>Status</th>
                                            <th>BOL?</th>
                                            <th>Commission End</th>
                                            <th>Dispatch</th>
                                            <th>Delivery</th>
                                            <th>Paid On</th>
                                            <th>Total Charge</th>
                                            <th>Carrier Pay</th>
                                            <th>Dispatcher</th>
                                            <th>Payee</th>
                                            <th>Destination</th>
                                            <th>Arrival</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.DataTable -->
                        </div><!-- /.Row -->
                        <?php
                    } else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }
                    ?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/colreorder/1.3.2/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

        <script>
            // Data Table - All Records for Current Year
            $(document).ready(function () {
                // Suppress errors/warnings/alerts
                //$.fn.dataTable.ext.errMode = 'none';
                //Add text input fields to column headers
                $('#example tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="" />');
                });
                var table = $('#example').DataTable({
                    "footerCallback": function ( row, data, start, end, display ) {
                     var api = this.api(), data;
                     
                     // Remove the formatting to get integer data for summation
                     var intVal = function ( i ) {
                     return typeof i === 'string' ?
                     i.replace(/[\$,]/g, '')*1 :
                     typeof i === 'number' ?
                     i : 0;
                     };
                     
                     // Total over all pages
                     total = api
                     .column( 5 )
                     .data()
                     .reduce( function (a, b) {
                     return intVal(a) + intVal(b);
                     }, 0 );
                     
                     // Total over this page
                     pageTotal = api
                     .column( 5, { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                     return intVal(a) + intVal(b);
                     }, 0 );
                     
                     // Update footer with column totals
                     $( api.column( 5 ).footer() ).html(
                     '$'+Math.round(pageTotal*100)/100 +' / $'+ Math.round(total*100)/100
                     );
                     },
    
                    //"sScrollX": "110%",
                    //"sScrollY": "200px",
                    //"bScrollCollapse": true,
                    //"scrollX": true,
                    colReorder: true,
                    responsive: true,
                    stateSave: false,
                    "autoWidth": false,
                    "searching": true,
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            filename: '<?php echo $report_title; ?>',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }
                    ],
                    "ajax": "<?php echo $ajax_src; ?>",
                    "columns": [
                        {"data": "Record_Count", 
                            render: $.fn.dataTable.render.number(',', '.', 0),
                            width: "2%"},
                        {"data": "Pay_Old", width: "2%"},
                        {"data": "Pay_New", width: "2%"},
                        {"data": "Order_ID", width: "5%"},
                        {"data": "movement_id", width: "5%"},
                        {"data": "margin",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "2%"
                        },
                        {"data": "Running_Total",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "4%"
                        },
                        {"data": "Commission_Percent",
                            render: $.fn.dataTable.render.number(',', '.', 2),
                            width: "2%"
                        },
                        {"data": "Payroll_Amount",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "3%"
                        },
                        {"data": "processing_status", width: "2%"},
                        {"data": "bol_received", width: "1%"},
                        {"data": "Commission_End_Date", width: "5%"},
                        {"data": "Dispatch_Date", width: "5%"},
                        {"data": "Delivery_Date", width: "5%"},
                        {"data": "Pay_Date", width: "5%"},
                        {"data": "total_charge",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"
                        },
                        {"data": "Carrier_Total_Pay",
                            render: $.fn.dataTable.render.number(',', '.', 2, '$'),
                            width: "5%"
                        },
                        {"data": "dispatcher_user_id", width: "5%"},
                        {"data": "override_payee_id", width: "5%"},
                        {"data": "Destination", width: "10%"},
                        {"data": "Destin_Schedule_Arrive_Early", width: "10%"}
                    ],
                    "columnDefs": [
                        {
                            "width": "10px", "targets": 0
                        },
                        {
                            "targets": <?php echo $hide_columns; ?>,
                            "visible": false,
                            "searchable": false
                        }
                    ]
                });
                // Inserts the footer directly below the header
                $('#example tfoot tr').insertAfter($('#example thead tr'));
                // Search bar
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
    </body>
</html>