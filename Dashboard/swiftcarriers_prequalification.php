<?php
//https://swiftcarriers.rmissecure.com/_c/cstm/4189/reg/DOTLookup_new.aspx
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Swift Logistics, LLC | Pre-Qualification</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--BS4 CDN-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!--Custom CSS Stylesheet-->
        <link rel="stylesheet" href="css/bootstrap4_style.css">
        <script>
            $(document).ready(function(){
                $("#usdotrequired").hide();
                $('#intraStateCarrierStates').change(function(){
                    var state = $( "#intraStateCarrierStates option:selected" ).val();
                    var array = ['','AR','CA','DC','DE','HI','ID','IL','LA','MA','MS','NV','NH','NM','ND','RI','SD','VT',
                    'VA','AB','BC','MB','NB','NL','NT','NS','NU','ON','PE','QC','SK','YT','PQ'];
                        /*if($( "#intraStateCarrierStates option:selected" ).val()==='AL'){*/
                        if (jQuery.inArray(state, array) <= -1) {
                            /*alert("Alert: " + state +" has additional requirements. Please see below.");*/
                            $("#usdotrequired").show();
                            $("input[id=dotNumber]").addClass("red-background");
                        } else {
                            $("#usdotrequired").hide();
                            $("input[id=dotNumber]").removeClass("red-background");
                        }
                    });
            });
        </script>
        <style>
            .red-background { 
                background: #ff3333; 
            }
        </style>
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand font-weight-bold" href="#">Swift Logistics, LLC</a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/swiftcarriers_registration.php">Registration</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://swiftcarriers.rmissecure.com/_s/client/UserClientLogin.aspx">Client Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://swiftcarriers.rmissecure.com/_c/std/carrier/Login.aspx">Carrier Login</a>
                    </li>
                </ul>
            </div>
        </nav>

        <!-- Begin page content -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="logo"><img src="images/Swift_Logistics.png" alt="Swift Logistics logo"/></div>
                </div>
                <div class="col-lg-6">
                    <span class="float-lg-right">
                        <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
                        <div id="cicCwI" style="z-index:100;position:absolute"></div>
                        <div id="sccCwI" style="display:inline"></div>
                        <div id="sdcCwI" style="display:none"></div>
                        <script type="text/javascript">
                            var secCwI = document.createElement("script");
                            secCwI.type = "text/javascript";
                            var secCwIs = (location.protocol.indexOf("https") == 0 ? "https" : "http") + "://image.providesupport.com/js/rmis_trans/safe-standard.js?ps_h=cCwI&ps_t=" + new Date().getTime();
                            setTimeout("secCwI.src=secCwIs;document.getElementById('sdcCwI').appendChild(secCwI)", 1)
                        </script>
                        <noscript><div style="display:inline">
                            <a href="http://www.providesupport.com?messenger=rmis_trans" id="hiddenChat">Customer Support</a>
                        </div></noscript>
                        <!-- END ProvideSupport.com Graphics Chat Button Code -->
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h2>Pre-Qualification</h2>
                    <div class="offset-md-2 col-md-10"><br/>

                        <form name="carrier_prequal" method="POST" action="" class="form-horizontal">
                            <p><span class="font-weight-bold">Please enter ONE carrier identifier for registration.</span></p>
                            <div class="form-group row">
                                <label for="docketType" class="col-md-4 col-form-label">US Docket Number (MC, FF, MX)</label>
                                <div class="col-md-2">
                                    <select id="docketType" class="form-control">
                                        <option value="MC">MC</option>
                                        <option value="FF">FF</option>
                                        <option value="MX">MX</option>
                                    </select></div>
                                <div class="col-md-4">
                                    <input id="docketNumber" class="form-control" type="text" maxlength="6">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="dotNumber" class="col-md-4 col-form-label">US DOT Number</label>
                                <div class="col-md-6">
                                    <input id="dotNumber" class="form-control" type="text" maxlength="7">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="intraState" class="col-md-4 col-form-label">Intra-State State/Permit Number</label>
                                <div class="col-md-2">
                                    <select id="intraStateCarrierStates" class="form-control">
                                        <option selected="selected"></option>
                                        <option value="AL">AL</option>
                                        <option value="AK">AK</option>
                                        <option value="AZ">AZ</option>
                                        <option value="AR">AR</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DC">DC</option>
                                        <option value="DE">DE</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="HI">HI</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="IA">IA</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="ME">ME</option>
                                        <option value="MD">MD</option>
                                        <option value="MA">MA</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MS">MS</option>
                                        <option value="MO">MO</option>
                                        <option value="MT">MT</option>
                                        <option value="NE">NE</option>
                                        <option value="NV">NV</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NY">NY</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PA">PA</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VT">VT</option>
                                        <option value="VA">VA</option>
                                        <option value="WA">WA</option>
                                        <option value="WV">WV</option>
                                        <option value="WI">WI</option>
                                        <option value="WY">WY</option>
                                        <option value="AB">AB</option>
                                        <option value="BC">BC</option>
                                        <option value="MB">MB</option>
                                        <option value="NB">NB</option>
                                        <option value="NL">NL</option>
                                        <option value="NT">NT</option>
                                        <option value="NS">NS</option>
                                        <option value="NU">NU</option>
                                        <option value="ON">ON</option>
                                        <option value="PE">PE</option>
                                        <option value="QC">QC</option>
                                        <option value="SK">SK</option>
                                        <option value="YT">YT</option>
                                        <option value="PQ">PQ</option>
                                    </select></div>
                                <div class="col-md-4">
                                    <input id="intraStateCarrierNumber" class="form-control" type="text" maxlength="6">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div id="usdotrequired" class="alert-warning">
                                    <span>*This state requires a U.S. DOT Number in addition to your intrastate permit. 
                                        Please enter your U.S DOT Number above.</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-2 offset-md-5">
                    <button type="button" class="btn btn-secondary">Go To Next Step</button>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">For help or questions regarding the registration process on this website, contact RMIS at 888-643-8174.
                    &copy; 2017 <a href='http://www.registrymonitoring.com'>Registry Monitoring Insurance Services, Inc.</a></span>
            </div>
        </footer>
        <!-- Bootstrap core JavaScript -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/js/bootstrap.min.js"><\/script>');</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>