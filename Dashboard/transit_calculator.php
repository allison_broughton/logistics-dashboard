<?php
require 'include_functions.php';
require 'include_navbar.php';
///////////////////// Initialize Variables //////////////////////////////////////////////////////////////////////
$title             = 'Transit Calculator';
$page_id           = 'Transit_Calculator';
$page_access = getPageAccess($user_id, $page_id);
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Set message properties before form is submitted
$alert_type     = 'alert-info';
$message_header = "Please Note";
$message        = "You may leave Origin/Destination blank and enter the Distance manually
    if you wish. If you choose to enter the cities, then the distance will be auto calculated.";
// Set default date/time for pickup/delivery
$default_date   = $today;
$now            = date('H:i');
$default_time   = date('H:i', strtotime($now . ' + 4 hours'));
//////////////////// Form Submission ////////////////////////////////////////////////////////////////////////////
if (isset($_POST['calc_transit'])) {
    $alert_type     = 'alert-success';
    $message_header = "Success!";
    $message        = "You may now choose Pickup or Delivery to calculate the date/times.";
    // Form POST variables
    $origin            = sanitize($_POST['origin']);
    $destin            = sanitize($_POST['destination']);
    $distance          = sanitize($_POST['distance']);
    $pickup_date       = sanitize($_POST['pickup_date']);
    $pickup_time       = sanitize($_POST['pickup_time']);
    $pickup_datetime   = $pickup_date . ' ' . $pickup_time;
    $formatted_pickup  = userFriendlyDate($pickup_datetime);
    $delivery_date     = sanitize($_POST['delivery_date']);
    $delivery_time     = sanitize($_POST['delivery_time']);
    $delivery_datetime = $delivery_date . ' ' . $delivery_time;
    $formatted_delivery = userFriendlyDate($delivery_datetime);
    // Transit Time formula variables
    $base_mph      = 47.5;
    $buffer        = .82;
    if ($pickup_date === $today) {
        if($pickup_time < $default_time) {
            $alert_type = 'alert-warning';
            $message_header = "Warning!";
            $message = "You must choose a time at least 4 hours from now.";
        }
    }
    // If Origin AND Destination cities are set
    if (!empty($origin) && !empty($destin)) {
        // Validate cities
        $valid_origin = validateCity($origin);
        $valid_destin = validateCity($destin);
        // If one or both cities were not found, kill script and alert user
        if (!($valid_origin) || !($valid_destin)) {
            $alert_type = 'alert-warning';
            $message_header = "Warning!";
            $message    = "No cities found for $origin and/or $destin. Please check your spelling.";
        } else {
            // Calculate distance whether it is empty or not
            $distance       = getMileage($origin, $destin);
            $distance_class = 'form-result';
        }
        $transit = ($distance / $base_mph) + $buffer;
        // Calculate transit hours
        if ($transit < 10) { // No breaks
            $solo_transit = number_format($transit, 1);
            $team_transit = "N/A";
        } else { // Add break time
            $solo_transit = number_format((floor($transit / 10) * 12) + $transit, 1);
            $team_transit = number_format((($distance / $base_mph) + $buffer), 1);
        }
        $display_form_results = true;
    } // If no cities were entered but the distance was...
    elseif (empty($origin) && empty($destin) && !empty($distance)) {
        $transit = ($distance / $base_mph) + $buffer;
        // Calculate transit hours
        if ($transit < 10) { // No breaks
            $solo_transit = number_format($transit, 1);
            $team_transit = "N/A";
        } else { // Add break time
            $solo_transit = number_format((floor($transit / 10) * 12) + $transit, 1);
            $team_transit = number_format((($distance / $base_mph) + $buffer), 1);
        }
        $display_form_results = true;
    } else {
        $alert_type           = 'alert-danger';
        $message_header       = "Error:";
        $message              = "You must enter either an Origin/Destination pair or the Distance.";
        $display_form_results = false;
    }
    if(!empty($pickup_date) && !empty($pickup_time)) {
        $show_pickup = 'in';
        $show_pickup_results = true;
        $solo_min = convertTime($solo_transit);
        $team_min = convertTime($team_transit);
        // Solo Delivery
        $solo_delivery_dt   = date('Y-m-d H:i', strtotime($pickup_datetime . "+{$solo_min} minutes"));
        //$solo_delivery_date = date("Y-m-d", strtotime($solo_delivery_dt));
        //$solo_delivery_time = date("H:i", strtotime($solo_delivery_dt));
        // Team Delivery
        $team_delivery_dt   = date('Y-m-d H:i', strtotime($pickup_datetime . "+{$team_min} minutes"));
        //$team_delivery_date = date("Y-m-d", strtotime($team_delivery_dt));
        //$team_delivery_time = date("H:i", strtotime($team_delivery_dt));
    }
    if(!empty($delivery_date) && !empty($delivery_time)) {
        if ($delivery_date === $today) {
            if ($delivery_time < $default_time) {
                $alert_type     = 'alert-warning';
                $message_header = "Warning!";
                $message        = "You must choose a time at least 4 hours from now.";
            }
        }
        // Hide Pickup form
        $show_pickup = '';
        $show_pickup_results = false;
        // Show delivery form
        $show_delivery = 'in';
        $show_delivery_results = true;
        $solo_min = convertTime($solo_transit);
        $team_min = convertTime($team_transit);
        // Solo Pickup
        $solo_pickup_dt   = date('Y-m-d H:i', strtotime($delivery_datetime . "-{$solo_min} minutes"));
        //$solo_pickup_date = date("Y-m-d", strtotime($solo_pickup_dt));
        //$solo_pickup_time = date("H:i", strtotime($solo_pickup_dt));
        // Team Pickup
        $team_pickup_dt   = date('Y-m-d H:i', strtotime($delivery_datetime . "-{$team_min} minutes"));
        //$team_pickup_date = date("Y-m-d", strtotime($team_pickup_dt));
        //$team_pickup_time = date("H:i", strtotime($team_pickup_dt));
    }
}
?>

<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <?php echo $violet_css;?>
        <!-- Custom Fonts -->
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#show_pickup").hide();
                $("#show_delivery").hide();
                $("#calc_pickup").on("click",function(){
                    $("#show_pickup").toggle();
                    $("#show_delivery").hide();
                });
                $("#calc_delivery").on("click",function(){
                    $("#show_delivery").toggle();
                    $("#show_pickup").hide();
                });
            });
        </script>
        <!--<script>
            $(document).ready(function () {
                $('#calc_transit').submit(function(event) {
                    alert('YASSS');
                    event.preventDefault();
                        $.ajax({
                            type: 'POST',
                            url: 'ajax_calc_transit.php',
                            data: $(this).serialize(),
                            dataType: 'json'
                        }).done(function() {
                            reloadDiv();
                        });
                });
            });
        </script>
        <script>
            function reloadDiv(){
                $('#divToReload').load('ajax_calc_transit.php');
            }
        </script>-->
        <style>
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #add_mod {
                color: #fff;
                border-color: #005590;
                background-color: #005590
            }
            table {
                table-layout: fixed;
                max-height: 500px;
            }
            th,td {
                column-width: 100px;
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
            input.form-result, textarea { 
                background: #59cd32; 
            }
            #show_pickup, #show_delivery {
                border: none;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if ($page_access) { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    <?php echo $title;?><br/>
                                    <small></small>
                                </h1><?php echo $dev_server;?>
                                <?php if(isset($solo_delivery_dt) && $user_id === 'localhost\DEV') {
                                    //echo "<pre>Solo Transit: $solo_min minutes<br/>Pickup: $pickup_datetime + $solo_transit hours ($solo_min minutes) = $solo_delivery_dt</pre>";
                                    //echo "<pre>Team Transit: $team_min minutes<br/>Pickup: $pickup_datetime + $team_transit hours ($team_min minutes) = $team_delivery_dt</pre>";
                                }?>
                            </div>
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="add_mod" class="div-content">
                                    <h1 class="panel-heading">Calculate Solo/Team Transit Times
                                        <small>And calculate pickup/delivery times.</small>
                                    </h1>
                                    <div class="panel-footer">
                                        <div class="alert <?php echo $alert_type; ?> alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong><?php echo $message_header; ?></strong><br/>
                                            <?php echo $message; ?>
                                        </div>
                                        <div id="divToReload">
                                            <form method="POST" action="" id="calc_transit" class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="origin" class="control-label col-xs-3">Origin</label>
                                                    <div class="col-xs-9">
                                                        <input type="text" name="origin" id="origin" placeholder="City, ST" value="<?php echo $origin; ?>" autofocus/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="destination" class="control-label col-xs-3">Destination</label>
                                                    <div class="col-xs-9">
                                                        <input type="text" name="destination" id="destination" placeholder="City, ST" value="<?php echo $destin; ?>" />
                                                    </div>
                                                </div>
                                                
                                                    <div class="form-group">
                                                        <label for="distance" class="control-label col-xs-3">Distance (miles)*</label>
                                                        <div class="col-xs-9">
                                                            <input type="number" name="distance" id="distance" class='<?php echo $distance_class;?>' value="<?php echo $distance; ?>"/>
                                                        </div>
                                                    </div>
                                                <?php if ($display_form_results) { ?>
                                                    <div class="form-group">
                                                        <span class="col-md-2 control-label">Transit Hours</span>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="solo_transit" class="col-md-1 control-label">Solo</label>
                                                                <div class="col-md-2">
                                                                    <input type="number" name="solo_transit" id="solo_transit" class='form-result' value="<?php echo $solo_transit; ?>" readonly/>
                                                                </div>
                                                                <label for="team_transit" class="col-md-1 control-label">Team</label>
                                                                <div class="col-md-2">
                                                                    <input type="number" name="team_transit" id="team_transit" class='form-result' value="<?php echo $team_transit; ?>" readonly/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <input id="calc_pickup" type="button" value="Enter Pickup" class="btn btn-success col-xs-offset-3" data-toggle="collapse" data-target="#show_pickup" />
                                                        <input id="calc_delivery" type="button" value="Enter Delivery" class="btn btn-warning" data-toggle="collapse" data-target="#show_delivery" />
                                                    </div>
                                                    <div id="show_pickup" class="div-content collapse <?php echo $show_pickup;?>">
                                                        <div class="form-group">
                                                            <label for="pickup_datetime" class="control-label col-xs-3">Desired Pick-Up Date/Time</label>
                                                            <div class="col-xs-9">
                                                                <input type="date" name="pickup_date" id="pickup_datetime" min="<?php echo $default_date; ?>" value="<?php echo date($pickup_date);?>" />
                                                                <input type="time" name="pickup_time" value="<?php echo $pickup_time;?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php if($show_pickup_results) {?>
                                                <div class="form-group">
                                                        <span class="col-md-2 control-label">Est. Delivery Times for <br/><?php echo $formatted_pickup;?> Pickup</span>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="solo_delivery" class="col-md-1 control-label">Solo</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" name="solo_delivery_date" id="solo_delivery" class='form-result' value="<?php echo userFriendlyDate($solo_delivery_dt);?>" readonly/>
                                                                    <!--<input type="text" name="solo_delivery_time" class='form-result'  value="<?php //echo $solo_delivery_time; ?>" readonly/>-->
                                                                </div>
                                                                <label for="team_delivery" class="col-md-1 control-label">Team</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" name="team_delivery_date" id="team_delivery" class='form-result' value="<?php echo userFriendlyDate($team_delivery_dt); ?>" readonly/>
                                                                    <!--<input type="time" name="team_delivery_time" class='form-result'  value="<?php //echo $team_delivery_time; ?>" readonly/>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                                    <div id="show_delivery" class="div-content collapse <?php echo $show_delivery;?>">
                                                        <div class="form-group">
                                                            <label for="delivery_datetime" class="control-label col-xs-3">Desired Delivery Date/Time</label>
                                                            <div class="col-xs-9">
                                                                <input type="date" name="delivery_date" id="delivery_datetime" min="<?php echo $default_date; ?>" value="<?php echo date($delivery_date);?>" />
                                                                <input type="time" name="delivery_time" value="<?php echo $delivery_time; ?>"  />
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php if($show_delivery_results) {?>
                                                <div class="form-group">
                                                        <span class="col-md-2 control-label">Est. Pickup Times for <br/><?php echo $formatted_delivery;?> Delivery</span>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label for="solo_pickup" class="col-md-1 control-label">Solo</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" name="solo_pickup_date" id="solo_pickup" class='form-result' value="<?php echo userFriendlyDate($solo_pickup_dt);?>" readonly/>
                                                                    <!--<input type="time" name="solo_pickup_time" class='form-result'  value="<?php //echo $solo_pickup_time; ?>" readonly/>-->
                                                                </div>
                                                                <label for="team_pickup" class="col-md-1 control-label">Team</label>
                                                                <div class="col-md-3">
                                                                    <input type="text" name="team_pickup_date" id="team_pickup" class='form-result' value="<?php echo userFriendlyDate($team_pickup_dt); ?>" readonly/>
                                                                    <!--<input type="time" name="team_pickup_time" class='form-result'  value="<?php //echo $team_pickup_time; ?>" readonly />-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                                <?php } ?>
                                                <div class="form-group">
                                                    <div class="col-xs-offset-1 col-xs-11">
                                                        <button type="submit" name="calc_transit" class="btn btn-primary col-lg-offset-2">Submit</button>
                                                        <!--<input id="calc_transit" type="button" value="Calculate Transit" class="btn btn-primary col-lg-offset-2" />-->
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.row -->
                        <?php } else {
                            echo "<div class='alert alert-danger'>You do not have access to this page.</div>";
                        }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
    </body>
</html>
