<?php
require 'include_functions.php';
require 'include_navbar.php';
$title   = 'Hire or Fire';
$page_id = 'New_Hire_Form';
// Insert user info to database to track page visits
if(!($user_id === 'localhost\DEV')) {trackVisit($page_id,$user_id,'page_visit');}
// Check page access
if ($username === 'localhost\DEV') {
    $page_access = true;
} else {
    $page_access = getAdminStatus($username, $page_id);
}
// Get Division Options
function getOptions($table) {
    global $conn_mcld;
    if($table === 'brokerage') {
        $sql = "SELECT id FROM brokerage_planning 
            WHERE brokerage_planning.company_id = 'TMS' ORDER BY id";
        $column_id = 'id';
        $column_val = 'id';
    } elseif($table === 'office') {
        $sql = "SELECT id, descr FROM office_code 
            WHERE office_code.company_id = 'TMS' AND is_active='Y' ORDER BY id";
        $column_id = 'id';
        $column_val = 'descr';
    } elseif($table === 'role') {
        $sql = "SELECT id, descr FROM department 
            WHERE department.company_id = 'TMS' ORDER BY descr";
        $column_id = 'id';
        $column_val = 'descr';
    } elseif($table === 'revenue') {
        $sql = "SELECT descr, id FROM revenue_code WHERE revenue_code.company_id = 'TMS' ORDER BY descr";
        $column_id = 'id';
        $column_val = 'descr';
    }
    $query = odbc_prepare($conn_mcld, $sql);
    odbc_execute($query);
    $options = '';
    while($row = odbc_fetch_array($query)) {
        $options .= "<option value='".$row[$column_id]."'>".$row[$column_val]."</option>";
    }
    return $options;
}
$revenue_options = getOptions('revenue');
$office_options  = getOptions('office');
//$division_options = getOptions('division');
$role_options    = getOptions('role');

// E-Mail variables
$user_name = getFullName($user_id);
$user_email = getEmail($user_id);

// New Employee form submit
if (isset($_POST['new_employee'])) {
    $emp_name     = sanitize($_POST['employee_name']);
    $emp_usrid    = sanitize($_POST['employee_user_id']);
    $emp_email    = sanitize($_POST['employee_email']);
    $emp_phone    = sanitize($_POST['employee_phone']);
    $emp_fax      = sanitize($_POST['employee_fax']);
    $emp_cell     = sanitize($_POST['employee_cell']);
    $emp_leader   = sanitize($_POST['employee_leader']);
    $emp_office   = sanitize($_POST['employee_office']);
    $emp_division = sanitize($_POST['employee_division']);
    $emp_role     = sanitize($_POST['employee_role']);
    $emp_revenue  = sanitize($_POST['employee_revenue']);
    $emp_br_cover = sanitize($_POST['employee_broker_coverage']);
    $emp_programs = sanitize($_POST['employee_programs']);
    $emp_comments = sanitize($_POST['employee_comments']);
    
    // Iterate through programs checked off
    $programs ='';
    if (!empty($_POST['employee_programs'])) {
        foreach ($_POST['employee_programs'] as $program) {
            $programs .= "<li>" . $program . "</li>";
        }
    }
    // Send E-Mail to Logistics Systems Support
    $to      = "Allison_Broughton@swifttrans.com";
    $sub     = "Logistics - New User $emp_usrid - McLeod";
    $msg     = "<h2>Notice of New Employee</h2>
        Requested by $user_name on $today_datetime <br/><br/>
        Full Name: $emp_name <br/>
        User ID: $emp_usrid <br/>
        E-Mail: $emp_email <br/>
        Phone: $emp_phone <br/>
        Fax: $emp_fax <br/>
        Cell: $emp_cell <br/>
        Leader: $emp_leader <br/>
        Office: $emp_office <br/>
        Division: $emp_division <br/>
        Role/Department: $emp_role <br/>
        Revenue Code: $emp_revenue <br/>
        Broker Coverage Area: $emp_br_cover <br/>
        Comments: $emp_comments <br/>
        Programs: <ul>$programs</ul>";
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= "From: $user_name <$user_email>" . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $mail = mail($to, $sub, $msg, $headers);
    if ($mail) {
        $message .= "Notification has been sent.";
    } else {
        $message .= "Mail sending failed.";
    }
    $debug = "<br/><br/>Message to be sent:<br/><br/><p>" . $msg . "</p>";
}
// Terminate form submit
if(isset($_POST['terminate_employee'])) {
    $term_name  = sanitize($_POST['term_userid']);
    $term_email = sanitize($_POST['term_email']);
    $term_date  = sanitize($_POST['term_date']);
    $to         = "Allison_Broughton@swifttrans.com";
    $sub        = "Logistics - Deactivate $term_name - McLeod";
    $msg        = "The employee $term_name (email: $term_email) has been terminated as of $term_date by 
            $user_id on $today_datetime. Please deactivate them in McLeod and ensure that they have been
            deactivated in Active Directory.";
    $headers    = 'MIME-Version: 1.0' . "\r\n";
    $headers    .= "From: $user_name <$user_email>" . "\r\n";
    $headers    .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $mail = mail($to, $sub, $msg, $headers);
    if ($mail) {
        $message .= "Notification has been sent.";
    } else {
        $message .= "Mail sending failed.";
    }
}
/////////////////////////////////////// GRAB FORM INPUTS ///////////////////////////////////////
?>
<html lang="en"><head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>LOOP | <?php echo $title; ?></title>
        <link rel="shortcut icon" type="image/ico" href="images/swift_logistics_logo_circle.png"><!-- Favicon -->
        <link href="css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap Core CSS -->
        <link href="css/sb-admin.css" rel="stylesheet"><!-- Custom CSS -->
        <?php echo $violet_css;?>
        <script src="https://use.fontawesome.com/d441f54e4c.js"></script><!-- Font Awesome CDN -->
        <link href="css/plugins/chartist.min.css" rel="stylesheet"><!-- Chartist Custom CSS -->
        <link href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css" rel="stylesheet">
        <!--<link type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/css/chartist-plugin-tooltip.css.map" rel="stylesheet">
        <link type="text/css" href="js/plugins/chartist-plugin-tooltip-master/src/scss/chartist-plugin-tooltip.scss" rel="stylesheet">-->
        <script src='js/jquery.js'></script><!-- jQuery -->
        <script src="js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
        <script src="js/plugins/bootstrap-form-helpers/js/bootstrap-formhelpers-phone.js"></script>        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .btn-purple,.btn-purple:visited,.btn-purple:hover {
                color: #fff;
                border-color: #8500bd;/*purple*/
                background-color: #9400d3
            }
            .div-content {
                border: 2px solid #999999;
                border-radius: 15px;
                margin-top: 1%;
            }
            .div-content small {
                color: #fff;
            }
            .div-content .panel-footer {
                color: #000;
                font-size: 18px;
                border-radius: 0 0 15px 15px;
                margin: auto;
            }
            #new_employee {
                color: #fff;
                border-color: #8500bd;/*purple*/
                background-color: #9400d3
            }
            #termination {
                color: #fff;
                border-color: #C9302C;/*red*/
                background-color: #d9534f
            }
            table {
                /*display: block;*/
                table-layout: fixed;
                max-height: 500px;
                /*overflow-x: auto;
                overflow-y: auto;*/
            }
            th,td {
                column-width: 100px;
                /*min-width: 100px;
                max-width: 175px;*/
            }
            select {
                max-width: 100%;
            }
            input[type=number] {
                max-width: 100%;
                line-height: 10px;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Load footer at bottom of page
                $("#footer").load("footer.html #footer");
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#new_employee").show();
                $("#termination").hide();
                $("#new_hire").on("click",function(){
                    $("#new_employee").toggle();
                    $("#termination").hide();
                });
                $("#terminate").on("click",function(){
                    $("#termination").toggle();
                    $("#new_employee").hide();
                });
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!--Nav bar loaded here via include_navbar.php-->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php if($page_access) {?>
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                New Employee/Termination Form<br/>
                                <small>Choose a form below</small>
                            </h1><?php echo $dev_server;?>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="button" id="new_hire" class="btn btn-purple">New Employee</button>
                            <button type="button" id="terminate" class="btn btn-danger">Termination</button>
                            <?php if (isset($_POST['new_employee']) || isset($_POST['terminate_employee'])) {
                                        echo $message;
                                        if($user_id === 'localhost\DEV' || $user_id === 'stral') {
                                            echo $debug;
                                        }
                                    } ?>
                            <div id="new_employee" class="div-content">
                                <h1 class="panel-heading">New Employee Information</h1>
                                <div class="panel-footer">
                                    
                                    <form name="new_employee" method="POST" action="" class="form-horizontal">
                                        <div class="col-xs-offset-3 col-xs-9">* Denotes required field</div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee Name*</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_name' autofocus required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee User ID*</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_user_id' required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee E-Mail*</label>
                                            <div class="col-xs-8">
                                                <input type='email' name='employee_email' required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee Phone #*</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_phone' class="input-medium bfh-phone" data-format=" (ddd) ddd-dddd" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee Fax #</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_fax' class="input-medium bfh-phone" data-format=" (ddd) ddd-dddd">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee Cell #</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_cell' class="input-medium bfh-phone" data-format=" (ddd) ddd-dddd">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Leader*</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='employee_leader' placeholder='Enter their userid' required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Office*</label>
                                            <div class="col-xs-8">
                                                <select name='employee_office' required>
                                                    <option value='' selected disabled>Choose Office</option>
                                                    <?php echo $office_options; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Division*</label>
                                            <div class="col-xs-8">
                                                <select name='employee_division' required>
                                                    <option value='' selected disabled>Choose Division</option>
                                                    <option value='994'>Logistics Corporate</option>
                                                    <option value='730'>Brokerage Corporate</option>
                                                    <option value='731'>PHX</option>
                                                    <option value='733'>Warehouse Brokerage</option>
                                                    <option value='735'>SLC</option>
                                                    <option value='736'>NWA</option>
                                                    <option value='737'>WMA</option>
                                                    <option value='738'>Laredo</option>
                                                    <option value='739'>PA</option>
                                                    <option value='741'>Warehouse Corporate</option>
                                                    <option value='724'>Warehouse - York, PA</option>
                                                    <option value='742'>Warehouse - Winston-Salem</option>
                                                    <option value='751'>FUM</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4" style='font-size:16px;'>Role/Dept*</label>
                                            <div class="col-xs-8">
                                                <select name='employee_role' required>
                                                    <option value='' selected disabled>Choose Role</option>
                                                    <?php echo $role_options; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!--If Broker is chosen for the Role/Dept-->
                                        <!--Display note or field to notify that brokers must sign broker commission form and 
                                        choose Quarterly or Annual cycle. Should also be added to the Broker E-Mail group-->

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Revenue Code</label>
                                            <div class="col-xs-8">
                                                <select name='employee_revenue'>
                                                    <option value='' selected>Choose Revenue Code</option>
                                                    <?php echo $revenue_options; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Broker Coverage Area</label>
                                            <div class="col-xs-8">
                                                <input type="text" name='employee_broker_coverage' placeholder='Enter state(s)/zip code(s)' />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-offset-3 col-xs-9">
                                                <fieldset>
                                                    <legend>Check programs below that user will need access to:</legend>
                                                    <input type='checkbox' name='employee_programs[]' value='McLeod/Imaging'> McLeod/Imaging<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='DAT Power Broker'> DAT Power Broker<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='DAT Rate View'> DAT Rate View<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='SalesForce'> SalesForce<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='WebEx'> WebEx<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='Conference Call #'> Conference Call #<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='JIRA'> JIRA<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='Confluence'> Confluence<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='MacroPoint'> MacroPoint<br/>
                                                    <input type='checkbox' name='employee_programs[]' value='Other'> Other (Please specify in comments)
                                                </fieldset>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Other Comments</label>
                                            <div class="col-xs-8">
                                                <textarea name='employee_comments' style='width: 50%;'></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-offset-3 col-xs-9">
                                                <input type="submit" name="new_employee" class="btn btn-purple col-lg-offset-2" value="Submit"/>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            
                            <div id="termination" class="div-content">
                                <h1 class="panel-heading">Notice of Employee Termination</h1>
                                <div class="panel-footer">
                                    <form name="terminate_employee" method="POST" action="" class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee User ID*</label>
                                            <div class="col-xs-8">
                                                <input type='text' name='term_userid' autofocus required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Employee E-Mail*</label>
                                            <div class="col-xs-8">
                                                <input type='email' name='term_email' required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Date of Termination*</label>
                                            <div class="col-xs-8">
                                                <input type="date" name="term_date" required/>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-xs-offset-3 col-xs-9">
                                                <input type="submit" name="terminate_employee" class="btn btn-danger col-lg-offset-2" value="Submit"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row -->
                    <?php }
                    else {
                        echo "<div class='alert alert-danger'>You do not have access to this page. Please click the E-Mail Support link at the top of the page.</div>";
                    }?>
                </div><!-- /.container-fluid -->
            </div><!-- /#page-wrapper -->
            <div id="footer"></div>
        </div><!-- /#wrapper -->
    </body>
</html>