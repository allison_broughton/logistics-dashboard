import Vue from 'vue'
import App from './App.vue'
//import Ninjas from './Ninjas.vue'

/*Globally register component from Ninjas.vue*/
//Vue.component('ninjas', Ninjas)

/* Create event bus */
export const bus = new Vue();

new Vue({
  el: '#app',
  render: h => h(App)
})
