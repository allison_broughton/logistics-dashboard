<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Remove "SWIFT\" from usernames
function stripUsername($username) {
    if($username === "localhost\DEV") {
        $user_id = "localhost\DEV";
    } else {
        $user_id = substr(stripslashes($username),5);
    }
    return $user_id;
}

function getJobID() {
    global $conn;
    global $user_id;
    $query_job_id = "SELECT TOP 1 JobID FROM Job_Log WHERE Job_Name = 'Logistics_Market_Rate_Tool' AND User_Name = '$user_id' ORDER BY JobID DESC";
    $get_job_id = odbc_prepare($conn, $query_job_id);
    odbc_execute($get_job_id);
    while ($row = odbc_fetch_array($get_job_id)) {
        $job_id = $row['JobID'];
        return $job_id;
    }
}

function getTodaysLookups($limit) {
    global $conn;
    $today_datetime = date('Y-m-d') . " 00:00:00";
    $string = "SELECT TOP 30
       JobLog.Has_Errors
      ,JobLog.User_Name
      ,JobLog.Start_Date_Time
      ,JobLog.End_Date_Time
      ,JobLog.Processing_Time
      ,MarketLog.IP
      ,MarketLog.Input_Trailer_Type
      ,MarketLog.Input_Origin
      ,MarketLog.Input_Destin
      ,MarketLog.Kma_Kma_Lane
      ,MarketLog.Output_Miles
      ,MarketLog.Output_Spot_Rate
      ,MarketLog.Output_Linehaul
      ,MarketLog.Output_FSC
  FROM Job_Log JobLog
  LEFT JOIN Logistics_Market_Rate_Log MarketLog ON JobLog.JobID = MarketLog.JobID
  WHERE Job_Name = 'Logistics_Market_Rate_Tool' AND Start_Date_Time >= '$today_datetime' $limit
  ORDER BY JobLog.JobID DESC";
    $get_lookups = odbc_prepare($conn, $string);
    odbc_execute($get_lookups);
    while ($row = odbc_fetch_array($get_lookups)) {
        if($row['Has_Errors'] === '1') {
            $row_color = "style='color: #ff2247'";
        } else {
            $row_color = "style='color: #00cc66'";
        }
        $lookups_today .= "<tr $row_color>
                <td>" . $row['User_Name'] . "</td>
                <td>" . $row['Start_Date_Time'] . "</td>
                <td>" . $row['Input_Trailer_Type'] . "</td>
                <td>" . $row['Input_Origin'] . "</td>
                <td>" . $row['Input_Destin'] . "</td>
                <td>" . $row['Kma_Kma_Lane'] . "</td>
                <td>" . number_format($row['Output_Miles'],2) . "</td>
                <td>$" . number_format($row['Output_Spot_Rate'],2) . "</td>
            </tr>";
    } return $lookups_today;
}